//
//  CartModel.swift
//  Tailory
//
//  Created by Jaydeep on 10/09/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class CartModel: NSObject {    
    var category_id = ""
    var business_id = ""
    var quantity = ""
    var dictProductJson = JSON()
}
