//
//  Service.swift
//  Tailory
//
//  Created by YASH on 02/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

struct CommonService
{
    func Service(url:String,param : [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        
        print("url - ",url)
        var dict = param
        dict["is_login_guest"] = getUserDetail("is_login_guest") == "1" ? "1" : "0"
        dict["device_token"] = Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString
        print("url - ",dict)
        
        Alamofire.request(url, method: .post, parameters: dict, encoding: URLEncoding.default, headers: nil).authenticate(user: basic_username, password:basic_password).responseSwiftyJSON(completionHandler:
            {
     
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    print("StatusCode : \(statusCode)")
                    let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                    print("API result str - ",str)

                    if(statusCode == 500)
                    {
                        
                    }
                    else if(statusCode != nil)
                    {
                       /// APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }
                    else
                    {
                        
                        makeToast(strMessage: serverNotResponding)
                        completion($0.result)
                    }
                }
                else
                {
                       makeToast(strMessage: serverNotResponding)
//                    completion($0.result)
                }
        })
    }
 
    
    /*
    func Service(url:String,param : [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        
        print("url - ",url)
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).authenticate(user: basic_username, password:basic_password).responseSwiftyJSON(completionHandler:
            {
                
                if $0.result.isSuccess || $0.response?.statusCode == 200
                {
                    let str = String(decoding: $0.data ?? Data(), as: UTF8.self)
                    print("API result str - ",JSON(str))

                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    print("StatusCode : \(statusCode)")
                    
                    if(statusCode == 500)
                    {
                        
                    }
                    else if(statusCode != nil)
                    {
                        /// APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }
                    else
                    {
                        
                        makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                        completion($0.result)
                    }
                }
                else
                {
                    makeToast(strMessage: getCommonString(key: "Server_not_responding_Please_try_again_later_key"))
                    //                    completion($0.result)
                }
        })
    }
 */
    func uploadImagesService(url:String,img:UIImage,withName:String,imgCar:UIImage,withCarName:String,imgDrivingRecord:UIImage,withDrivingRecord:String,param : [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        
        print("url - ",url)
        
        let unit64:UInt64 = 10_000_000
        
        let PasswordString =  String(format: "\(basic_username):\(basic_password)")
        let PasswordData = PasswordString.data(using: .utf8)
        let base64EncodedCredential = PasswordData!.base64EncodedString(options: .lineLength64Characters)
        
        let headers: HTTPHeaders = ["Authorization":"Basic \(base64EncodedCredential)"]
        print("headers:==\(headers)")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            
            if let imgData = img.jpegData(compressionQuality: 0.7)
            {
                multipartFormData.append(imgData, withName: withName, fileName:"image" , mimeType: "image/png")
            }
            
            for (key, value) in param {
                multipartFormData.append(value.data(using:String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
            
        }, usingThreshold: unit64, to: url, method: .post, headers: headers, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                    //send progress using delegate
                })
                upload.responseSwiftyJSON(completionHandler:
                    {
                        
                        print("Result : \($0.result)")
                        
                        if $0.result.isSuccess
                        {
                            completion($0.result)
                        }
                        else if $0.result.isFailure
                        {
                            let statusCode = $0.response?.statusCode
                            print("StatusCode : \(statusCode)")
                            if(statusCode == 500)
                            {
                                
                            }else if(statusCode != nil)
                            {
                                /// APIResponseHandle(statusCode: statusCode!)
                                completion($0.result)
                            }
                            else
                            {
                                
                                makeToast(strMessage: serverNotResponding)
                                
//                                completion($0.result)
                            }
                        }else
                        {
                            makeToast(strMessage: serverNotResponding)
                            
                            //                    completion($0.result)
                        }
                })
                
            case .failure(_):
                makeToast(strMessage: serverNotResponding)
            }
        })
        
    }
    
    
    
    
    func ServiceGETMethod(url:String,param : [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        
        print("url - ",url)
        
        Alamofire.request(url, method: .get, parameters: param, encoding: URLEncoding.default, headers: nil).authenticate(user: basic_username, password:basic_password).responseSwiftyJSON(completionHandler:
            {
                
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    print("StatusCode : \(statusCode)")
                    if(statusCode == 500)
                    {
                        
                    }else if(statusCode != nil)
                    {
                        /// APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }
                    else
                    {
                        
                        makeToast(strMessage: serverNotResponding)
                        
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: serverNotResponding)
                    
                    //                    completion($0.result)
                }
        })
    }
    
    
}

