//
//  ServiceListPostfix.swift
//  Tailory
//
//  Created by YASH on 03/02/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation

//MainURL

/*let urlUser = "http://139.59.79.228/tailory/api/user/"

let urlTrailor = "http://139.59.79.228/tailory/api/tailor/"

let urlFabric = "http://139.59.79.228/tailory/api/fabrics/"

let urlAccessories = "http://139.59.79.228/tailory/api/accessories/"

let urlOrder = "http://139.59.79.228/tailory/api/order/"

let urlPayment = "http://139.59.79.228/tailory/api/payment/"*/


let urlUser = "http://tailory-kw.com/admin/api/user/"

let urlTrailor = "http://tailory-kw.com/admin/api/tailor/"

let urlFabric = "http://tailory-kw.com/admin/api/fabrics/"

let urlAccessories = "http://tailory-kw.com/admin/api/accessories/"

let urlOrder = "http://tailory-kw.com/admin/api/order/"

let urlPayment = "http://tailory-kw.com/admin/api/payment/"

let kUrlMain = "http://www.tailory-kw.com/admin/"

let kCallBackUrl = "http://tailory-kw.com/admin/payment/"

//LoginURL
let urlLogin = "login"

let urlForgotPassword = "forgot_password"


//Guest User URL

let urlAccessTokenGenerate = "generate_access_token"

//Register
let urlRegister = "register"

let urlTrailorList = "tailor_list"

let urlTailorDesignList = "get_tailor_designs"

let urlTailorCustomizeList = "get_customize_style"

let urlSetDesignToProfile = "set_design_to_profile"

let urlMyDesignList = "my_design_list"

let urlFabricList = "fabrics_branch_list"

let urlFabricStyleList = "get_fabrics_style"

let urlFabricStyleDesign = "get_fabrics_designs"

let urlReviewList = "user_review_list"

let urlGiveRate = "give_review"

let urlUserAddress = "my_addresses"

let urlTailorDetail = "get_trailor_details"

let urlFabricDetail = "get_fabrics_branch_details"

let urlFavUnfav = "business_set_as_favorites"

let urlAccessoriesList = "accessories_category_list"

let urlAccessoriesProductList = "accessories_product_list"

let urlAccessoriesDetail = "product_details"

let urlAddAddress = "add_address"

let urlGuestAddAddress = "guest_checkout"

let urlUpdateUserToCart = "update_user_to_cart"

let urlRemoveAddress = "remove_address"

let urlMyMeasurement = "my_measurements"

let urlRemoveMeasurement = "remove_measurement"

//Add and edit measurment
let urlAddEditMeasurment = "add_measurement"

let urlMyDefaultAddress = "my_default_address"

let urlChangePassword = "change_password"

let urlUpdateProfile = "update_profile"

let urlAddToCart = "add_to_cart"

let urlCartList = "cart_list"

let urlAddRemoveCart = "add_remove_product_to_cart"

let urlAddRemoveLaundry = "add_laundry_to_cart"

let urlAddRemoveCombineDelivery = "combine_delivery"

let urlGetDesignToCart = "get_design_to_cart"

let urlEditDesignCart = "edit_design_to_cart"

let urlEditMeasurement = "edit_measurement_to_cart"

let urlRemoveMyDesign = "remove_my_design"

let urlCustomizeDesign = "get_my_customize_design"

let urlFabricsFavoriteList = "fabrics_favorite_list"

let urlTailorsFavoriteList = "tailors_favorite_list"

let urlProductFavoriteList = "product_favorite_list"

let urlCheckoutList = "get_checkout_details"

let urlAddOrder = "add_order"

let urlLogout = "logout"

let urlFindTailor = "find_tailor"

let urlTermsConditions = "terms_conditions"

let urlPrivacyPolicy = "privacy_policy"

let urlMyOrderList = "my_order_list"

let urlOrderDetails = "order_details"

let reorder = "reorder"

let urlSavedForLater = "saved_for_later"

let urlMySaveLaterList = "my_save_later_list"

let urlRemoveSavedItems = "remove_saved_items"

let urlAddToCartBySavedLater = "add_to_cart_by_saved_later"

let urlTrackOrder = "track_order"

let urlSwitchNotification = "switch_notification"

let urlChangeLanguage = "change_language"

let urlFatoorahPayment = "fatoorah_paymentpage"

let urlCategoryList = "category_list"


//NOTIFICATION LIST GET
let urlNotificationList = "notification_list"

//CART COUNT GET
let urlCartCount = "cart_count"

//GET LUANDRY PRICE
let urlLuandryPrice = "laundry_price"

let urlManageTransaction = "manage_transaction"

//User Details
let urlUserDetails = "user_details"

let urlCheckCartItem  = "item_added_from_different_branch"

let urlCheckSameFabric = "tailor_with_same_fabric_branch"

let urlCheckMeasurementData = "check_measurement_data"

let kUserGuestAddress = "check_guest_address"

let kTermsConditionAr = "terms_and_conditions_ar"

let kTermsConditionEn = "terms_and_conditions"

let kPrivacyPolicyEn = "privacy_policies"

let kPrivacyPolicyAr = "privacy_policies_ar"

let kPaymentTrasactionStatus = "payment_transaction_status"




