//
//  LoginVc.swift
//  Tailory
//
//  Created by Haresh on 13/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import FirebaseMessaging


enum isCheckParentControllerForLogin
{
    case firstTimeLanuch
    case fromGuesdUserDetailPopup
}


class LoginVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var btnLoginHeader: CustomButton!
    @IBOutlet weak var vwLoginHeader: CustomView!
    @IBOutlet weak var btnRegisterHeader: CustomButton!
    
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var btnLogin: CustomButton!
    @IBOutlet weak var btnContinueAsGuest: CustomButton!
    
    
    @IBOutlet weak var btnForgotPassword: CustomButton!
    
    //MARK: - Variable
    
    var selectedController = isCheckParentControllerForLogin.firstTimeLanuch
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        if !isEnglish  {
            setUpArabicUI()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedController == .firstTimeLanuch
        {
            self.navigationController?.navigationBar.isHidden = true
        }
        else if selectedController == .fromGuesdUserDetailPopup
        {
            self.navigationController?.navigationBar.isHidden = false
            
            if isEnglish
            {
                let leftButton = UIBarButtonItem(image: UIImage(named: "ic_login_back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped))
                leftButton.tintColor = UIColor.white
                self.navigationItem.leftBarButtonItem = leftButton
            }
            else
            {
                let rightButton = UIBarButtonItem(image: UIImage(named: "ic_login_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonTapped))
                rightButton.tintColor = UIColor.white
                self.navigationItem.rightBarButtonItem = rightButton
            }
            
            
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.clear
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.clear
            }

            self.navigationItem.hidesBackButton = true
            self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.backgroundColor = UIColor.clear
            
        }
        else
        {
            self.navigationController?.navigationBar.isHidden = true
        }
    }
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        
        btnLoginHeader.backgroundColor = UIColor.clear
        
        btnLoginHeader.setTitleColor(UIColor.white, for: .normal)
        btnRegisterHeader.setTitleColor(UIColor.white, for: .normal)
        btnRegisterHeader.backgroundColor = UIColor.clear
        
        [btnLoginHeader,btnRegisterHeader].forEach { (btn) in
            btn.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        }
        
        btnForgotPassword.setTitleColor(UIColor.white, for: .normal)
        btnForgotPassword.setTitle(getCommonString(key: "Forgot_Password_key"), for: .normal)
        
        btnLoginHeader.setTitle(getCommonString(key: "Login_key"), for: .normal)
        btnRegisterHeader.setTitle(getCommonString(key: "Register_key"), for: .normal)
        
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        
        [txtEmail,txtPassword].forEach { (txt) in
            txt?.tintColor = UIColor.white
            txt?.textColor = UIColor.white
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.placeHolderColor = UIColor.white
            txt?.leftPaddingView = 50
            txt?.rightPaddingView = 0
        }
        
        if !isEnglish {
           txtEmail.font = UIFont(name: themeFonts.light.rawValue, size: 15)
        }
        
        btnLogin.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        btnLogin.setTitle(getCommonString(key: "Login_key"), for: .normal)
        
        btnContinueAsGuest.setTitleColor(UIColor.white, for: .normal)
        btnContinueAsGuest.setTitle(getCommonString(key: "Continue_as_guest"), for: .normal)
        btnContinueAsGuest.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        
        vwLoginHeader.backgroundColor = UIColor.appThemeSilverColor.withAlphaComponent(0.5)
        vwLoginHeader.shadowColors = UIColor.clear
        vwLoginHeader.cornerRadius = btnLoginHeader.bounds.height/2
        
    }
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        [txtEmail,txtPassword].forEach { (txt) in
            txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
            txt?.textAlignment = .right
            txt?.rightPaddingView = 50
        }
        
        [btnLoginHeader,btnRegisterHeader, btnLogin, btnForgotPassword, btnContinueAsGuest].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
    }
    
}

//MARK: - IBAction

extension LoginVc
{
    
    @IBAction func btnLoginHeaderTapped(sender:UIButton)
    {
        print("Do Nothing")
    }
    
    @IBAction func btnRegisterHeaderTapped(sender:UIButton)
    {
        self.view.endEditing(true)
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RegisterVc") as! RegisterVc
        obj.selectedController = selectedController
        self.navigationController?.pushViewController(obj, animated: false)
        
    }
    
    @IBAction func btnBottomLoginAction(_ sender: Any) {
        
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_email_key"))
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_password_key"))
        }
        else {
            print("login")
            loginAPIcalling()
        }
        /*
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_email_key"))
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_password_key"))
        }
        else if (txtPassword.text?.count)! < GlobalVariables.passwordLegth
        {
            makeToast(strMessage: getCommonString(key: "Password_must_be_at_least_8_characters_long_key"))
        }
        else if !(isValidPassword(passwordString: txtPassword.text!))
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_password_key"))
        }
        else
        {
            print("login")
            loginAPIcalling()
        }
        */
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVc") as! ForgotPasswordVc
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnContinueAsGuestTapped(_ sender: CustomButton) {
        self.forGuestUser()
    }
    
}

//MARK: - API calling

extension LoginVc
{
    
    func loginAPIcalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlLogin)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "email_phone_no" : txtEmail.text ?? "",
                         "password" : txtPassword.text ?? "",
                         "timezone" : getCurrentTimeZone(),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                         "register_id" : Messaging.messaging().fcmToken ?? "",
                         "device_type" : GlobalVariables.strDeviceType
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        
                        let data = json["data"]                        
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        self.guestUpdateUserToCart()
                        /*
                        //Redirect to TabBar screen
                        appdelgate.tailerTabbarVC = TailerTabBarVC()
                        appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
                        */
//                        makeToast(strMessage: json["msg"].stringValue)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                       
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
    func forGuestUser()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlAccessTokenGenerate)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "username" : generateAccessTokenName ,
                         "password" : generateAccessTokenPassword ,
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                         "register_id" : Messaging.messaging().fcmToken ?? ""
                        ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        //Redirect to TabBar screen
                        appdelgate.tailerTabbarVC = TailerTabBarVC()
                        appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
                        
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func guestUpdateUserToCart() {
            self.view.endEditing(true)
            
            if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
                
                let url = "\(urlUser)\(urlUpdateUserToCart)"
                
                print("URL: \(url)")
                
                var param = [String:String]()
                
                param = [
                    "lang" : lang,
                    "user_id" : getUserDetail("user_id"),
                    "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                    "access_token" : getUserDetail("access_token"),
                    "is_login_guest" : getUserDetail("is_login_guest")
                ]
                
                print("param :\(param)")
                
                self.showLoader()
                
                CommonService().Service(url: url, param: param) { (respones) in
                    
                    self.hideLoader()
                    
                    if let json = respones.value {
                        print("JSON : \(json)")
                        
                        if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                            let data = json["data"]
                            
                            guard let rowdata = try? data.rawData() else {return}
                            Defaults.setValue(rowdata, forKey: "userDetails")
                            Defaults.synchronize()
                            
                            //Redirect to TabBar screen
                            appdelgate.tailerTabbarVC = TailerTabBarVC()
                            appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
                        }
                        else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                            self.logoutAPICalling()
                        }
                        else {
                            //                        makeToast(strMessage: json["msg"].stringValue)
                        }
                        
                    }
                    else {
                        makeToast(strMessage: serverNotResponding)
                    }
                }
            }
            else {
                makeToast(strMessage: networkMsg)
            }
            
        }
    
}
