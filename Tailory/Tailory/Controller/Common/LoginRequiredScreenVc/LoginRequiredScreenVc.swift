//
//  LoginRequiredScreenVc.swift
//  Tailory
//
//  Created by YASH on 12/09/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

enum checkVcToCome {
    case favoriteVC
    case myOrderVC
}

class LoginRequiredScreenVc: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblLoginRequiredMsg: UILabel!
    @IBOutlet weak var btnLogin: CustomButton!
    
    
    //MARK: - Variable
    var checkVC = checkVcToCome.myOrderVC
    
    
    //MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        // Do any additional setup after loading the view.
    }

    func setupUI()
    {
        
        lblLoginRequiredMsg.font = themeFont(size: 17, fontname: .medium)
        lblLoginRequiredMsg.textColor = UIColor.appThemeSilverColor
        lblLoginRequiredMsg.text = getCommonString(key: "Login_required_please_login_first_key")

        if checkVC == .favoriteVC {
            lblLoginRequiredMsg.text = getCommonString(key: "Please_login_to_view_favorites_key")
        }
        
        btnLogin.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        btnLogin.setTitle(getCommonString(key: "Login_key"), for: .normal)

    }
    
    @IBAction func btnLoginTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        obj.hidesBottomBarWhenPushed = true
        obj.selectedController = .fromGuesdUserDetailPopup
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
}
