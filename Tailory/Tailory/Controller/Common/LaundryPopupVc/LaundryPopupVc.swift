//
//  LaundryPopupVc.swift
//  Tailory
//
//  Created by Haresh on 24/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

protocol delegateWantLaundryPopup {
    func yesForLaundry()
    func noForLaundry()
}



class LaundryPopupVc: UIViewController {

    //MARK: - Outlet
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblwantLaundry: UILabel!
    @IBOutlet weak var lblwantLaundrySubMsg: UILabel!
    
    @IBOutlet weak var vwButtons: UIView!
    @IBOutlet weak var btnNo: CustomButton!
    @IBOutlet weak var btnYes: CustomButton!

    //MARK: - Variable
    
    var delegateWantLaundry : delegateWantLaundryPopup?
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        
        lblwantLaundry.font = themeFont(size: 19, fontname: .heavy)
        lblwantLaundry.textColor = UIColor.appThemeBlackColor
        
        lblwantLaundrySubMsg.font = themeFont(size: 14, fontname: .light)
        lblwantLaundrySubMsg.textColor = UIColor.appThemeLightGrayColor
        
        btnNo.setupThemeButtonUI(backColor: UIColor.white)
        btnNo.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        btnNo.borderColor = UIColor.appThemeSilverColor
        btnNo.borderWidth = 1.0
        
        btnYes.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        
        lblwantLaundry.text = getCommonString(key: "want_laundry_key")
        var LaundryPrice = String()
        if (Defaults.object(forKey: "LaundryPrice") != nil){
            print("LaundryPrice exist")
            LaundryPrice = Defaults.object(forKey: "LaundryPrice") as? String ?? ""
        }else {
            print("Does not LaundryPrice exist")
        }
        lblwantLaundrySubMsg.text = "\(getCommonString(key: "This_service_will_add_extra_key")) \(LaundryPrice) \(getCommonString(key: "KD_for_each_Dishdashah_key"))"//getCommonString(key: "This_service_will_add_extra_0.500_KD_for_each_Dishdashah_key")
        
        btnNo.setTitle(getCommonString(key: "No_key"), for: .normal)
        btnYes.setTitle(getCommonString(key: "Yes_key"),  for: .normal)
        
        if !isEnglish
        {
            setupArabicUI()
        }
        
    }
    
    func setupArabicUI()
    {
        
        vwButtons.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [btnNo,btnYes].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }

}


//MARK: - IBAction

extension LaundryPopupVc
{
    
    @IBAction func btnNoTapped(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
        delegateWantLaundry?.noForLaundry()
        
    }
    
    
    @IBAction func btnYesTapped(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
        delegateWantLaundry?.yesForLaundry()
        
    }
    
}
