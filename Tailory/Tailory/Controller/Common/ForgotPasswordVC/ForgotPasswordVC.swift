//
//  ForgotPasswordVc.swift
//  Tailory
//
//  Created by Haresh on 26/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON

class ForgotPasswordVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblForgotPasswordTitle: UILabel!
    @IBOutlet weak var lblForgotPasswordDetails: UILabel!
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var btnSend: CustomButton!
    
    //MARK: - Variable
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = false
        
        if isEnglish
        {
            let leftButton = UIBarButtonItem(image: UIImage(named: "ic_login_back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped))
            leftButton.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = leftButton
        }
        else
        {
            let rightButton = UIBarButtonItem(image: UIImage(named: "ic_login_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonTapped))
            rightButton.tintColor = UIColor.white
            self.navigationItem.rightBarButtonItem = rightButton
        }
        
        
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
    }
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        
        lblForgotPasswordTitle.text = getCommonString(key: "Forgot_Password_key")
        lblForgotPasswordTitle.font = themeFont(size: 20, fontname: .medium)
        lblForgotPasswordTitle.textColor = UIColor.white
        
        lblForgotPasswordDetails.text = getCommonString(key: "Forgot_Password_details_key")
        lblForgotPasswordDetails.font = themeFont(size: 16, fontname: .medium)
        lblForgotPasswordDetails.textColor = UIColor.white
        
        btnSend.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        btnSend.setTitle(getCommonString(key: "Send_key"), for: .normal)
        
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtEmail.tintColor = UIColor.white
        txtEmail.textColor = UIColor.white
        txtEmail.font = UIFont(name: themeFonts.light.rawValue, size: 15)
//        txtEmail.font = themeFont(size: 15, fontname: .medium)
        txtEmail.placeHolderColor = UIColor.white
        txtEmail.leftPaddingView = 50
        txtEmail.rightPaddingView = 0
        
        if !isEnglish
        {
            self.setUpArabicUI()
        }
        
        if !isEnglish {
           txtEmail.font = UIFont(name: themeFonts.light.rawValue, size: 15)
        }
        
    }
    
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        [txtEmail].forEach { (txt) in
            txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
            txt?.textAlignment = .right
            txt?.rightPaddingView = 50
        }
        
        btnSend.transform = CGAffineTransform(scaleX: -1, y: 1)
        
    }

}

//MARK: - IBAction method

extension ForgotPasswordVc {
    
    @IBAction func btnSendTapped(_ sender: UIButton) {
        
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: getCommonString(key: "Please_enter_email_key"))
        }
        else {
            self.forgotPasswordAPICalling()
        }
    }
}

//MARK: - API calling

extension ForgotPasswordVc {
    
    func forgotPasswordAPICalling() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(urlForgotPassword)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "email" : txtEmail.text ?? ""
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
}



