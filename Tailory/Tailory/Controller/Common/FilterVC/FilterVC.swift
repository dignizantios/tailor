//
//  FilterVC.swift
//  Tailory
//
//  Created by Khushbu on 09/10/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class FilterVC: UIViewController {

    //MARK: Outlets
    @IBOutlet var vwMain: UIView!
    @IBOutlet var lblAvailability: UILabel!

    @IBOutlet var btnAvailable: UIButton!
    @IBOutlet var btnBusy: UIButton!

    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var vwSliderPrice: UIView!
    @IBOutlet var sliderPrice: RangeSeekSlider!
    @IBOutlet var lblReview: UILabel!
    @IBOutlet var vwRating: HCSStarRatingView!

    @IBOutlet var lblReviewValue: UILabel!
    @IBOutlet var lblDistance: UILabel!

    @IBOutlet var vwSliderDistance: UIView!
    @IBOutlet var sliderDistance: RangeSeekSlider!
    @IBOutlet var vwBottom: UIView!
    @IBOutlet var btnClear: CustomButton!
    @IBOutlet var btnApply: CustomButton!
    @IBOutlet weak var viewAvailibility: UIView!
    
    //MARK: Variables
    var isTrailors:Bool = true
    var dictFilterData = JSON()
    var handlerFilter : (JSON?) -> Void = {_ in}
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.viewAvailibility.isHidden = !isTrailors
        
        self.setUpUI()
        self.setUpData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Filter_key"))
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

}

//MARK: setUpUI
extension FilterVC {
    
    func setUpUI() {
        
        [lblAvailability, lblPrice, lblReview, lblDistance, lblReviewValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .medium)
            lbl?.textColor = .black
        }
        lblReviewValue.font = themeFont(size: 12, fontname: .light)
        
        [btnClear].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: .appThemeSilverColor)
        }
        
        [btnApply].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: .white)
            btn?.borderColor = UIColor.appThemeSilverColor
            btn?.borderWidth = 1
            btn?.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        }
        
        lblAvailability.text = getCommonString(key: "Availability_key")
        lblPrice.text = getCommonString(key: "Price_key")
        lblReview.text = getCommonString(key: "Reviews_key")
        self.lblReviewValue.text = "\(0)+ \(getCommonString(key: "Reviews_key"))"
        btnAvailable.setTitle(" \(getCommonString(key: "Available_key"))", for: .normal)
        btnBusy.setTitle(" \(getCommonString(key: "Busy_key"))", for: .normal)
        if !isTrailors {
            btnBusy.setTitle(" \(getCommonString(key: "Unavailable_key")) ", for: .normal)
        }
        
        btnClear.setTitle(getCommonString(key: "Clear_key"), for: .normal)
        btnApply.setTitle(getCommonString(key: "Apply_key"), for: .normal)
        
        setUpSlider()
        
        if !isEnglish {
            
            btnAvailable.setTitle(" \(getCommonString(key: "Available_key")) ", for: .normal)
            btnBusy.setTitle(" \(getCommonString(key: "Busy_key")) ", for: .normal)
            
            if !isTrailors {
                btnBusy.setTitle(" \(getCommonString(key: "Unavailable_key")) ", for: .normal)
            }
            
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblAvailability, lblPrice, lblReview, lblDistance].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            lblReviewValue.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            lblReview.transform = CGAffineTransform(scaleX: -1, y: 1)
            [btnClear, btnApply].forEach { (btn) in
                btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            [btnAvailable, btnBusy].forEach { (btn) in
                btn?.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            }
            
            
        }
        
    }
    
    func setUpSlider() {
        [sliderDistance].forEach { (slider) in
            slider?.delegate = self
            slider?.handleDiameter = 10.0
            slider?.lineHeight = 4.0
            slider?.tintColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)

            slider?.numberFormatter.positivePrefix = ""
            slider?.numberFormatter.positiveSuffix = " M"
            slider?.numberFormatter.numberStyle = .currency
            
            ////----- label value top show
            slider?.minLabel.transform = CATransform3DMakeTranslation(0, -40, 0)
            slider?.maxLabel.transform = CATransform3DMakeTranslation(0, -40, 0)

        }
        
        [sliderPrice].forEach { (slider) in
            slider?.delegate = self
            slider?.handleDiameter = 10.0
            slider?.lineHeight = 4.0
            slider?.tintColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)
            
            slider?.numberFormatter.positivePrefix = ""
            slider?.numberFormatter.positiveSuffix = " (KD)"
            slider?.numberFormatter.numberStyle = .currency
            
            ////----- label value top show
            slider?.minLabel.transform = CATransform3DMakeTranslation(0, -40, 0)
            slider?.maxLabel.transform = CATransform3DMakeTranslation(0, -40, 0)
        }
        
        if !isEnglish {
            [sliderDistance, sliderPrice].forEach { (slider) in
                
                
                slider?.minLabel.transform = CATransform3DMakeRotation(1 * .pi, 0.0, -40.0, 0)
                slider?.maxLabel.transform = CATransform3DMakeRotation(1 * .pi, 0.0, -40.0, 0)
                
                slider?.minLabel.anchorPoint = CGPoint(x: 0, y: 3.5)
                slider?.maxLabel.anchorPoint = CGPoint(x: 0, y: 3.5)
            }
            
            [sliderPrice].forEach { (slider) in
                
                slider?.numberFormatter.positivePrefix = ""
                slider?.numberFormatter.positiveSuffix = " KD"
            }
            
            [sliderDistance].forEach { (slider) in
                
                slider?.numberFormatter.positivePrefix = ""
                slider?.numberFormatter.positiveSuffix = " M"
            }
        }
        
    }
    
    func setUpData() {
        self.sliderPrice.minValue = 1
        self.sliderPrice.maxValue = 1000

        self.sliderDistance.minValue = 0
        self.sliderDistance.maxValue = 100

        if dictFilterData.isEmpty {
            self.btnAvailable.isSelected = true
            self.btnBusy.isSelected = false
            
            dictFilterData["is_available"].intValue = 0
            dictFilterData["min_price"].intValue = Int(self.sliderPrice.minValue)
            dictFilterData["max_price"].intValue = Int(self.sliderPrice.maxValue)
            dictFilterData["min_distance"].intValue = Int(self.sliderDistance.minValue)
            dictFilterData["max_distance"].intValue = Int(self.sliderDistance.maxValue)
            dictFilterData["star_rating"].intValue = 0
        }
        else {
            if dictFilterData["is_available"].intValue == 0 {
                self.btnAvailable.isSelected = true
                self.btnBusy.isSelected = false
            }
            else {
                self.btnAvailable.isSelected = false
                self.btnBusy.isSelected = true
            }
            self.sliderPrice.selectedMinValue = CGFloat(dictFilterData["min_price"].intValue)
            self.sliderPrice.selectedMaxValue = CGFloat(dictFilterData["max_price"].intValue)
            self.sliderDistance.selectedMinValue = CGFloat(dictFilterData["min_distance"].intValue)
            self.sliderDistance.selectedMaxValue = CGFloat(dictFilterData["max_distance"].intValue)
            self.vwRating.value = CGFloat(dictFilterData["star_rating"].floatValue)
            self.lblReviewValue.text = "\(dictFilterData["star_rating"].intValue)+ \(getCommonString(key: "Reviews_key"))"
        }
    }
}

//MARK: RangeSliderDelegate
extension FilterVC : RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
     
        if slider == sliderPrice {
            print("Standard slider updated. Min Value: \(Int(minValue)) Max Value: \(Int(maxValue))")
            dictFilterData["min_price"].intValue = Int(minValue + 1)
            dictFilterData["max_price"].intValue = Int(maxValue + 1)
        }
        else if slider == sliderDistance {
            print("Standard slider updated. Min Value: \(Int(minValue)) Max Value: \(Int(maxValue))")
            dictFilterData["min_distance"].intValue = Int(minValue + 1)
            dictFilterData["max_distance"].intValue = Int(maxValue + 1)
        }
    }
}

//MARK: StarRatingAction
extension FilterVC {
    
    @IBAction func vwRatingAction(_ sender: Any) {
        self.lblReviewValue.text = "\(Int(vwRating.value))+ \(getCommonString(key: "Reviews_key"))"
        dictFilterData["star_rating"].intValue = Int(vwRating.value)
//        print("Rating:", "\(vwRating.value)")
    }
}

//MARK: ButtonAction
extension FilterVC {
    
    @IBAction func btnAvailableAction(_ sender: Any) {
        self.btnAvailable.isSelected = true
        self.btnBusy.isSelected = false
        dictFilterData["is_available"].intValue = 0
    }
    
    @IBAction func btnBusyAction(_ sender: Any) {
        self.btnAvailable.isSelected = false
        self.btnBusy.isSelected = true
        dictFilterData["is_available"].intValue = 1
    }
    
    @IBAction func btnClearAction(_ sender: Any) {
        dictFilterData = JSON()
        self.handlerFilter(dictFilterData)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApplyAction(_ sender: Any) {
        dictFilterData["star_rating"].intValue = Int(self.vwRating.value)
        print("FilterData:", dictFilterData)
        self.handlerFilter(dictFilterData)
        self.navigationController?.popViewController(animated: false)
    }
}
