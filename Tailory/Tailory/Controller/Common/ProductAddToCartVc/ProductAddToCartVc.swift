//
//  ProductAddToCartVc.swift
//  Tailory
//
//  Created by Haresh on 23/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

enum VcToCome {
    case fabricMeasurment
    case fabrics
    case availableFabric
}

protocol delegateProductAddCartPopup {
    
    func redirectToGoToCart()
    func redirectToAddMore()
    
}

class ProductAddToCartVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblProductAddToCart: UILabel!
    @IBOutlet weak var lblProductSubMsg: UILabel!
    
    @IBOutlet var vwSaveForLater: UIView!
    @IBOutlet var btnSaveForLater: UIButton!
    @IBOutlet var btnInfo: UIButton!
    
    @IBOutlet weak var vwButtons: UIView!
    @IBOutlet weak var btnGotoCart: CustomButton!
    @IBOutlet weak var btnAddmore: CustomButton!
    
    //MARK: - Variable
    
    var delegateAddCartPopup : delegateProductAddCartPopup?
    var isSaveForLater = false
    var isCheckVC = VcToCome.fabricMeasurment
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        
        lblProductAddToCart.font = themeFont(size: 19, fontname: .heavy)
        lblProductAddToCart.textColor = UIColor.appThemeBlackColor
        
        lblProductSubMsg.font = themeFont(size: 14, fontname: .light)
        lblProductSubMsg.textColor = UIColor.appThemeLightGrayColor
        
        btnGotoCart.setupThemeButtonUI(backColor: UIColor.white)
        btnGotoCart.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        btnGotoCart.borderColor = UIColor.appThemeSilverColor
        btnGotoCart.borderWidth = 1.0
        
        btnAddmore.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        
        if isCheckVC == .fabrics {
            self.vwSaveForLater.isHidden = true
            lblProductSubMsg.text = getCommonString(key: "You_can_add_more_fabrics_or_go_to_cart_key")
        }
        else {
            self.vwSaveForLater.isHidden = false
            lblProductSubMsg.text = getCommonString(key: "You_can_add_more_designs_or_go_to_cart_key")
        }
        
        lblProductAddToCart.text = getCommonString(key: "Product_added_to_cart_key")
        
        btnGotoCart.setTitle(getCommonString(key: "Go_to_cart_key"), for: .normal)
        btnAddmore.setTitle(getCommonString(key: "Add_more_key"),  for: .normal)
        
        [btnSaveForLater].forEach { (btn) in
            btn?.setTitle(getCommonString(key: "Saved_for_later_key"), for: .normal)
            btn?.titleLabel?.font = themeFont(size: 16, fontname: .medium)
            btn?.setTitleColor(UIColor.appThemeBlackColor, for: .normal)
        }
        
//        self.vwSaveForLater.isHidden = true
        if isSaveForLater {
            self.vwSaveForLater.isHidden = false
        }
        
        if !isEnglish {
            setupArabicUI()
        }
        
    }
    
    func setupArabicUI()
    {
        vwSaveForLater.transform = CGAffineTransform(scaleX: -1, y: 1)
        vwButtons.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [btnGotoCart,btnAddmore, btnSaveForLater].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
   
    
}

//MARK: - IBAction

extension ProductAddToCartVc
{
    
    @IBAction func btnGotoCartTapped(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
        delegateAddCartPopup?.redirectToGoToCart()
        
    }
    
    
    @IBAction func btnAddMoreTapped(_ sender: UIButton) {        
        
        self.dismiss(animated: false, completion: nil)
        
        delegateAddCartPopup?.redirectToAddMore()
        
    }
 
    @IBAction func btnSaveForLaterAction(_ sender: Any) {
        saveForLater()
    }
    
    @IBAction func btnInfoAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: getCommonString(key: "Info_key"), preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}


//MARK: Service
extension ProductAddToCartVc {
    
    func saveForLater(){
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(urlSavedForLater)"
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "device_token":Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "business_id":objCartModel.business_id,
                          "branch_id" : dictFabricDetail["branch_id"].stringValue,
                          "quantity":objCartModel.quantity,
                          "product_cart_json":JSON(objCartModel.dictProductJson).rawString()
                ] as! [String:String]
            
            print("Quantity:", objCartModel.quantity)
            print("param : \(param)", objCartModel.business_id)
            
            self.showLoader()
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                if let json = respones.value {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }

}
