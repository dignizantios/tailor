//
//  RegisterVc.swift
//  Tailory
//
//  Created by Haresh on 13/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import FirebaseMessaging

class RegisterVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var btnLoginHeader: CustomButton!
    @IBOutlet weak var btnRegisterHeader: CustomButton!
    @IBOutlet weak var vwRegisterHeader: CustomView!
    
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtPhoneNumber: CustomTextField!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!

    @IBOutlet weak var btnRegister: CustomButton!
    
    //MARK:- Variable Declaration
    
    var selectedController = isCheckParentControllerForLogin.firstTimeLanuch
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if selectedController == .firstTimeLanuch
        {
            self.navigationController?.navigationBar.isHidden = true
        }
        else if selectedController == .fromGuesdUserDetailPopup
        {
            self.navigationController?.navigationBar.isHidden = false
            
            if isEnglish
            {
                let leftButton = UIBarButtonItem(image: UIImage(named: "ic_login_back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped))
                leftButton.tintColor = UIColor.white
                self.navigationItem.leftBarButtonItem = leftButton
            }
            else
            {
                let rightButton = UIBarButtonItem(image: UIImage(named: "ic_login_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonTapped))
                rightButton.tintColor = UIColor.white
                self.navigationItem.rightBarButtonItem = rightButton
            }
            
            
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.clear
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.clear
            }

            self.navigationItem.hidesBackButton = true
            self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.backgroundColor = UIColor.clear
            
        }
        else
        {
            self.navigationController?.navigationBar.isHidden = true
        }
    }
    
    override func backButtonTapped() {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }

    func setupUI()
    {
        
        self.hidekeyBoardWhenTappedAround()
        
        btnRegisterHeader.backgroundColor = UIColor.clear
        btnRegisterHeader.setTitleColor(UIColor.white, for: .normal)
        
        btnLoginHeader.setTitleColor(UIColor.white, for: .normal)
        btnLoginHeader.backgroundColor = UIColor.clear
        
        [btnLoginHeader,btnRegisterHeader].forEach { (btn) in
            btn.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        }
        
        btnLoginHeader.setTitle(getCommonString(key: "Login_key"), for: .normal)
        btnRegisterHeader.setTitle(getCommonString(key: "Register_key"), for: .normal)
        
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        txtName.placeholder = getCommonString(key: "Name_key")
        txtConfirmPassword.placeholder = getCommonString(key: "Confirm_password_key")
        txtPhoneNumber.placeholder = getCommonString(key: "Phone_number_key")
        addDoneButtonOnKeyboard(textfield: txtPhoneNumber)
        
        [txtName,txtEmail,txtPassword,txtConfirmPassword,txtPhoneNumber].forEach { (txt) in
            txt?.tintColor = UIColor.white
            txt?.textColor = UIColor.white
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.placeHolderColor = UIColor.white
            txt?.leftPaddingView = 50
            txt?.rightPaddingView = 0
            txt?.delegate = self
            txt?.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        }
        
        txtPhoneNumber.delegate = self
        
        btnRegister.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        btnRegister.setTitle(getCommonString(key: "Register_key"), for: .normal)
        
        vwRegisterHeader.backgroundColor = UIColor.appThemeSilverColor.withAlphaComponent(0.5)
        vwRegisterHeader.shadowColors = UIColor.clear
        vwRegisterHeader.cornerRadius = btnLoginHeader.bounds.height/2

        if !isEnglish {
           txtEmail.font = UIFont(name: themeFonts.light.rawValue, size: 15)
        }
    }
    
    func setUpArabicUI() {
        
        vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        [txtName,txtEmail,txtPassword,txtConfirmPassword,txtPhoneNumber].forEach { (txt) in
            txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
            txt?.textAlignment = .right
            txt?.rightPaddingView = 50
            txt?.leftPaddingView = 0
        }
        
        [btnLoginHeader,btnRegisterHeader, btnRegister].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}

//MARK: - IBAction method

extension RegisterVc
{
    
    @IBAction func btnLoginHeaderTapped(sender:UIButton)
    {
        self.view.endEditing(true)
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginVc.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    @IBAction func btnRegisterHeaderTapped(sender:UIButton)
    {
        if (txtName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_name_key"))
        }
        else if !(txtName.text?.validateName() ?? true)
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_name_key"))
        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_email_key"))
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_email_key"))
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_password_key"))
        }
        else if (txtPassword.text?.count)! < GlobalVariables.passwordLegth
        {
            makeToast(strMessage: getCommonString(key: "Password_must_be_at_least_eight_characters_long_key"))
        } /*else if containsSpecialCharacters(string: txtPassword.text!) == false{
            makeToast(strMessage: getCommonString(key: "Please_enter_At_least_8_characters_1_symbol_1_number_etc"))
        } else if doStringContainsAlpha(string: txtPassword.text!) == false{
             makeToast(strMessage: getCommonString(key: "Please_enter_At_least_8_characters_1_symbol_1_number_etc"))
        } else if doStringContainsNumber(string: txtPassword.text!) == false {
            makeToast(strMessage: getCommonString(key: "Please_enter_At_least_8_characters_1_symbol_1_number_etc"))
        }*/
        else if (txtConfirmPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_confirm_password_key"))
        }        
        else if(!(txtPassword.text! == txtConfirmPassword.text!))
        {
            makeToast(strMessage: getCommonString(key: "Password_and_confirmation_password_must_match_key"))
        }
//        else if (txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
//        {
//            makeToast(strMessage: getCommonString(key: "Please_enter_mobile_number_key"))
//        }
        else
        {
            if txtPhoneNumber.text!.isEmpty {
                userSignup()
            } else {
                if txtPhoneNumber.text!.count != 8 {
                        makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
                } else if txtPhoneNumber.text?.isNumeric == false {
                    makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
                } else {
                    userSignup()
                }
            }
        }
    }
    
}

extension RegisterVc : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textfield:UITextField){
        print("textfield \(textfield.text)")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == txtPhoneNumber {
            let allowedCharacters = CharacterSet(charactersIn:"+0123456789")
            let characterSet = CharacterSet(charactersIn: string)
            
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return allowedCharacters.isSuperset(of: characterSet) && newString.length <= GlobalVariables.mobileLegth
        }
        return true
    }
}

//MARK: - API calling

extension RegisterVc
{
    
    func userSignup()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlRegister)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "fullname" : txtName.text ?? "",
                         "email" : txtEmail.text ?? "",
                         "password" : txtPassword.text ?? "",
                         "phone_no" : txtPhoneNumber.text ?? "",
                         "timezone" : getCurrentTimeZone(),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                        "register_id" : Messaging.messaging().fcmToken ?? "",
                        "device_type" : GlobalVariables.strDeviceType
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
//                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        self.guestUpdateUserToCart()
                        /*
                        //Redirect to TabBar screen
                        appdelgate.tailerTabbarVC = TailerTabBarVC()
                        appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
                        */
                        makeToast(strMessage: json["msg"].stringValue)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.hideLoader()
                    }
                    else
                    {
                        self.hideLoader()
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func guestUpdateUserToCart() {
            self.view.endEditing(true)
            
            if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
                
                let url = "\(urlUser)\(urlUpdateUserToCart)"
                
                print("URL: \(url)")
                
                var param = [String:String]()
                
                param = [
                    "lang" : lang,
                    "user_id" : getUserDetail("user_id"),
                    "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                    "access_token" : getUserDetail("access_token"),
                    "is_login_guest" : getUserDetail("is_login_guest")
                ]
                
                print("param :\(param)")
                
    //            self.showLoader()
                
                CommonService().Service(url: url, param: param) { (respones) in
                    
                    self.hideLoader()
                    
                    if let json = respones.value {
                        print("JSON : \(json)")
                        
                        if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                            let data = json["data"]
                            
                            guard let rowdata = try? data.rawData() else {return}
                            Defaults.setValue(rowdata, forKey: "userDetails")
                            Defaults.synchronize()
                            
                            //Redirect to TabBar screen
                            appdelgate.tailerTabbarVC = TailerTabBarVC()
                            appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
                        }
                        else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                            self.logoutAPICalling()
                        }
                        else {
                            //                        makeToast(strMessage: json["msg"].stringValue)
                        }
                        
                    }
                    else {
                        makeToast(strMessage: serverNotResponding)
                    }
                }
            }
            else {
                makeToast(strMessage: networkMsg)
            }
            
        }
    
}
