//
//  GuestPopupVc.swift
//  Tailory
//
//  Created by Haresh on 23/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

protocol delegateGuestPopupRedirection {
    
    func redirectToCreateAccount()
    func redirectToContinue()
    
}


class GuestPopupVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblContinueAsGuest: UILabel!
    @IBOutlet weak var lblContinueSubMsg: UILabel!
    @IBOutlet weak var vwButtons: UIView!
    @IBOutlet weak var btnCreateAccount: CustomButton!
    @IBOutlet weak var btnContinue: CustomButton!
    
    //MARK: - Variable
    
    var delegateGuestPopup : delegateGuestPopupRedirection?
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        
        lblContinueAsGuest.font = themeFont(size: 19, fontname: .heavy)
        lblContinueAsGuest.textColor = UIColor.appThemeBlackColor
        
        lblContinueSubMsg.font = themeFont(size: 14, fontname: .light)
        lblContinueSubMsg.textColor = UIColor.appThemeLightGrayColor
        
        btnCreateAccount.setupThemeButtonUI(backColor: UIColor.white)
        btnCreateAccount.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        btnCreateAccount.borderColor = UIColor.appThemeSilverColor
        btnCreateAccount.borderWidth = 1.0

        
        lblContinueAsGuest.text = getCommonString(key: "Continue_as_guest_?_key")
        lblContinueSubMsg.text = getCommonString(key: "Continue_as_guest_sub_msg_key")
        
        btnCreateAccount.setTitle(getCommonString(key: "Create_account_key"), for: .normal)
        
        btnContinue.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        btnContinue.setTitle(getCommonString(key: "Continue_key"), for: .normal)

        if !isEnglish
        {
            setupArabicUI()
        }
        
    }
    
    func setupArabicUI()
    {
        
        vwButtons.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [btnCreateAccount,btnContinue].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
}

//MARK: - IBAction

extension GuestPopupVc
{
    
    @IBAction func btnCreateAccountTapped(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
        delegateGuestPopup?.redirectToCreateAccount()
        
    }
    
    @IBAction func btnContinueTapped(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
        delegateGuestPopup?.redirectToContinue()
        
    }
    
}

