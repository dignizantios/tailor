//
//  SortingVC.swift
//  Tailory
//
//  Created by Khushbu on 09/10/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class SortingVC: UIViewController {

    //MARK: Outlets
    @IBOutlet var vwMain: UIView!
    @IBOutlet var lblSorting: UILabel!
    @IBOutlet var btnAtoZ: UIButton!
    @IBOutlet var btnZtoA: UIButton!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var btnLowToHigh: UIButton!
    @IBOutlet var btnHighToLow: UIButton!
    @IBOutlet var btnDone: UIButton!
    
    //MARK: Variables
    var dictSortingData = JSON()
    var handlerSort : (JSON?) -> Void = {_ in}

    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }    
}

//MARK: SetUpUI
extension SortingVC {
    
    func setUpUI() {
        
        vwMain.layer.cornerRadius = 8
        vwMain.layer.masksToBounds = true
        vwMain.layer.shadowColor = UIColor.lightGray.cgColor
        vwMain.layer.shadowOpacity = 1
        vwMain.layer.shadowOffset = CGSize.zero
        vwMain.layer.shadowRadius = 5
        
        lblPrice.textColor = .black
        lblPrice.font = themeFont(size: 18, fontname: .medium)
        lblPrice.text = getCommonString(key: "Price_key")
        
        btnDone.setupThemeButtonUI(backColor: .white)
        btnDone.setTitleColor(UIColor.black, for: .normal)
        btnDone.setTitle(getCommonString(key: "Done_key"), for: .normal)
        
        btnAtoZ.setTitle(" \(getCommonString(key: "AtoZ_key"))", for: .normal)
        btnZtoA.setTitle(" \(getCommonString(key: "ZtoA_key"))", for: .normal)
        btnLowToHigh.setTitle(" \(getCommonString(key: "LowtoHigh_key"))", for: .normal)
        btnHighToLow.setTitle(" \(getCommonString(key: "HightoLow_key"))", for: .normal)
        lblSorting.text = getCommonString(key: "Sorting_key")
        
        if !isEnglish {
            
            self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.lblSorting.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.lblPrice.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.lblPrice.textAlignment = .right
            self.btnDone.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [btnAtoZ, btnZtoA, btnLowToHigh, btnHighToLow].forEach { (btn) in
                btn?.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            btnAtoZ.setTitle("\(getCommonString(key: "AtoZ_key")) ", for: .normal)
            btnZtoA.setTitle("\(getCommonString(key: "ZtoA_key")) ", for: .normal)
            btnLowToHigh.setTitle("\(getCommonString(key: "LowtoHigh_key")) ", for: .normal)
            btnHighToLow.setTitle("\(getCommonString(key: "HightoLow_key")) ", for: .normal)
        }
        self.setUpData()
    }
    
    func setUpData() {
        
        if dictSortingData.isEmpty {
            dictSortingData["sort"].intValue = 1
            
            btnAtoZ.isSelected =  true
            btnZtoA.isSelected = false
            btnLowToHigh.isSelected = false
            btnHighToLow.isSelected = false
        }
        else {
            btnAtoZ.isSelected = dictSortingData["sort"].intValue == 1 ? true : false
            btnZtoA.isSelected = dictSortingData["sort"].intValue == 2 ? true : false
            btnLowToHigh.isSelected = dictSortingData["sort"].intValue == 3 ? true : false
            btnHighToLow.isSelected = dictSortingData["sort"].intValue == 4 ? true : false
        }
        
    }
}

//MARK: Button Action
extension SortingVC {
    
    @IBAction func btnAtoZAction(_ sender: Any) {
        btnAtoZ.isSelected = true
        btnZtoA.isSelected = false
        btnLowToHigh.isSelected = false
        btnHighToLow.isSelected = false
        dictSortingData["sort"] = 1
    }
    
    @IBAction func btnZtoAAction(_ sender: Any) {
        btnAtoZ.isSelected = false
        btnZtoA.isSelected = true
        btnLowToHigh.isSelected = false
        btnHighToLow.isSelected = false
        dictSortingData["sort"] = 2
    }
    
    @IBAction func btnLowToHighAction(_ sender: Any) {
        btnAtoZ.isSelected = false
        btnZtoA.isSelected = false
        btnLowToHigh.isSelected = true
        btnHighToLow.isSelected = false
        dictSortingData["sort"] = 3
    }
    
    @IBAction func btnHighToLowAction(_ sender: Any) {
        btnAtoZ.isSelected = false
        btnZtoA.isSelected = false
        btnLowToHigh.isSelected = false
        btnHighToLow.isSelected = true
        dictSortingData["sort"] = 4
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        self.handlerSort(dictSortingData)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:
extension SortingVC {
    
}
