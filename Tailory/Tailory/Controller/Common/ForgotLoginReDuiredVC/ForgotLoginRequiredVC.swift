//
//  ForgotLoginRequiredVC.swift
//  Tailory
//
//  Created by Khushbu on 23/09/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit


class ForgotLoginRequiredVC: UIViewController {

    //MARK: Outlets
    @IBOutlet var lblLoginRequired: UILabel!
    @IBOutlet var lblLoginRequiredSubMsg: UILabel!
    @IBOutlet var vwButtons: UIView!
    @IBOutlet var btnCancel: CustomButton!
    @IBOutlet var btnLogin: CustomButton!
    
    //MARK: Variables
    var handlerIsGuest: () -> Void = {}

    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        self.setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Cart_key"))
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
}

//MARK: SetUp
extension ForgotLoginRequiredVC {
    
    func setUpUI() {
        
        [lblLoginRequired].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 19, fontname: .heavy)
        }
        
        [lblLoginRequiredSubMsg].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 14, fontname: .light)
        }
        
        btnCancel.setupThemeButtonUI(backColor: UIColor.white)
        btnCancel.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        btnCancel.borderColor = UIColor.appThemeSilverColor
        btnCancel.borderWidth = 1.0
        
        btnLogin.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        
        self.lblLoginRequired.text = getCommonString(key: "Login_required_key")
        self.lblLoginRequiredSubMsg.text = getCommonString(key: "Login_required_msg_key")
        btnCancel.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        btnLogin.setTitle(getCommonString(key: "Login_key"), for: .normal)
        
        if !isEnglish {
            setupArabicUI()
        }
    }
    
    func setupArabicUI() {
        
        vwButtons.transform = CGAffineTransform(scaleX: -1, y: 1)
        [btnCancel, btnLogin].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}

//MARK: ButtonAction
extension ForgotLoginRequiredVC {
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnLoginAction(_ sender: Any) {
        
        self.handlerIsGuest()
        self.dismiss(animated: false, completion: nil)
    }
    
}
