//
//  MyDetailsVc.swift
//  Tailory
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class MyDetailsVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var txtName: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPhoneNumber: CustomTextField!
    @IBOutlet var txtAge: CustomTextField!
    
    @IBOutlet weak var btnUpdate: CustomButton!
    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        getUserDetailsData()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "My_details_key"))
    }
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        
        [txtEmail,txtName,txtPhoneNumber,txtAge].forEach { (txt) in
            txt?.tintColor = UIColor.appThemeBlackColor
            txt?.textColor = UIColor.appThemeBlackColor
            txt?.delegate = self
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.leftPaddingView = 50
            txt?.rightPaddingView = 0
        }
        
        addDoneButtonOnKeyboard(textfield: txtPhoneNumber)
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtName.placeholder = getCommonString(key: "Name_key")
        txtPhoneNumber.placeholder = getCommonString(key: "Phone_number_key")
        txtAge.placeholder = getCommonString(key: "Age_key")
        
        txtEmail.isUserInteractionEnabled = false
        if !isEnglish {
           txtEmail.font = UIFont(name: themeFonts.light.rawValue, size: 15)
        }
        
        btnUpdate.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnUpdate.setTitle(getCommonString(key: "Update_key"), for: .normal)
        
        if !isEnglish {
            self.setUpArabicUI()
        }
        
    }
    
    func setUpArabicUI()
    {
        vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [txtEmail,txtName,txtPhoneNumber,txtAge].forEach { (txt) in
            txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
            txt?.leftPaddingView = 0
            txt?.rightPaddingView = 50
            txt?.textAlignment = .right
        }
        
    }

    func setData(data: JSON) {
        txtName.text = data["full_name"].stringValue
        txtEmail.text = data["email"].stringValue
        txtPhoneNumber.text = data["phone_no"].stringValue
        txtAge.text = data["age"].stringValue
    }
    
}


//MARK: - IBAction

extension MyDetailsVc
{

    @IBAction func btnUpdateTapped(_ sender: UIButton) {
        
        if (txtName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_name_key"))
        }
        else if !(txtName.text?.validateName() ?? true)
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_name_key"))
        }
        else if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_email_key"))
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_email_key"))
        } else {
            if txtPhoneNumber.text!.isEmpty {
                if (txtAge.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
                {
                    makeToast(strMessage: getCommonString(key: "Please_enter_age_key"))
                }
                else if txtAge.text?.isNumeric == false
                {
                    makeToast(strMessage: getCommonString(key: "Please_enter_valide_age_key"))
                }
                else
                {
                    self.updateProfileData()
                }
            } else {
                if txtPhoneNumber.text!.count != 8 {
                    makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
                } else if txtPhoneNumber.text?.isNumeric == false {
                    makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
                } else {
                    if (txtAge.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
                    {
                        makeToast(strMessage: getCommonString(key: "Please_enter_age_key"))
                    }
                    else if txtAge.text?.isNumeric == false
                    {
                        makeToast(strMessage: getCommonString(key: "Please_enter_valide_age_key"))
                    }
                    else
                    {
                        self.updateProfileData()
                    }
                }
            }
            
        }
        /*else if (txtPhoneNumber.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_mobile_number_key"))
        }
        else if txtPhoneNumber.text?.isNumeric == false
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
        }
        else if txtPhoneNumber.text!.count != 8
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
        }
        else if (txtAge.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_age_key"))
        }
        else if txtAge.text?.isNumeric == false
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valide_age_key"))
        }
        else
        {
            self.updateProfileData()
        }*/
    }

}

//MARK: - TextFIeld Delegate

extension MyDetailsVc: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                         replacementString string: String) -> Bool
      {
        if textField == txtPhoneNumber {
            let allowedCharacters = CharacterSet(charactersIn:"+0123456789")
            let characterSet = CharacterSet(charactersIn: string)
            
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return allowedCharacters.isSuperset(of: characterSet) && newString.length <= GlobalVariables.mobileLegth
        } else if textField == txtAge {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")
            let characterSet = CharacterSet(charactersIn: string)
            
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return allowedCharacters.isSuperset(of: characterSet) && newString.length <= GlobalVariables.ageLegth
        }
        return true
      }
}

//MARK: - API calling

extension MyDetailsVc {
    
    func getUserDetailsData() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(urlUserDetails)"
            
            print("URL: \(url)")
            
            let param = [
                "user_id" : getUserDetail("user_id"),
                "access_token" : getUserDetail("access_token"),
                "lang" : lang
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let data = json["data"]
                        self.setData(data: data)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func updateProfileData()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlUpdateProfile)"
            
            print("URL: \(url)")
            
            let param = [
                        "user_id" : getUserDetail("user_id"),
                        "access_token"  : getUserDetail("access_token"),
                         "lang" : lang,
                         "name" : txtName.text ?? "",
                         "email" : txtEmail.text ?? "",
                         "timezone" : getCurrentTimeZone(),
                         "phone_no" : txtPhoneNumber.text ?? "",
                         "age" : txtAge.text ?? ""
               
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        self.navigationController?.popViewController(animated: true)
                        
                     //   makeToast(strMessage: json["msg"].stringValue)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
}
