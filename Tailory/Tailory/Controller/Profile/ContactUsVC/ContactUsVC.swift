//
//  ContactUsVC.swift
//  Tailory
//
//  Created by Khushbu on 11/10/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON


class ContactUsVC: UIViewController {

    //MARK: Outlets
    @IBOutlet var tblContactUs: UITableView!
    
    //MARK: Variables
    var arrayContactUs : [JSON] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        setUpData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Contact_Us_key"))
    }
}

//MARK: SetUp
extension ContactUsVC {
    
    func setUpUI() {
        
        self.tblContactUs.register(UINib(nibName: "ProfileTblCell", bundle: nil), forCellReuseIdentifier: "ProfileTblCell")
        self.tblContactUs.tableFooterView = UIView()
    }
    
    func setUpData() {
        
        var dict = JSON()
        dict["title"].stringValue = getCommonString(key: "call_key")
        self.arrayContactUs.append(dict)
        
        dict = JSON()
        dict["title"].stringValue = getCommonString(key: "Email_contact_key")
        self.arrayContactUs.append(dict)
        
    }
}

//MARK: TableView Delegate/Datasource
extension ContactUsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayContactUs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTblCell", for: indexPath) as! ProfileTblCell
        
        let dict = self.arrayContactUs[indexPath.row]
        
        cell.lblName.text = dict["title"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.redirectToContact(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 45
    }
    
    func redirectToContact(index: Int) {
        
        if index == 0 {
            
            if let url = URL(string: "tel://+96524830805"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
        }
        else if index == 1 {
            
            let email = "contact@tailory.com"
            if let url = URL(string: "mailto:\(email)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}

//MARK:
extension ContactUsVC {
    
}
