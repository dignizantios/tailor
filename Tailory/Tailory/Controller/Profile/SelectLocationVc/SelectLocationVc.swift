//
//  SelectLocationVc.swift
//  Tailory
//
//  Created by Haresh on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps

class SelectLocationVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwGoogleMaps: GMSMapView!
    @IBOutlet weak var btnConfirm: CustomButton!
    @IBOutlet weak var btnCurrentLocation: CustomButton!
    
    //MARK: - Variable
    
    var strAddress = String()
    var currentLocation = GMSAddress()
    var handlerAddNewAddress:(JSON) -> Void = {_ in}
    var selectedContollerForAddDetails = checkParentControllerForEnterDetails.addNewAddress
    var selectedControllerAddEdit = checkParentControllerAddess.add
    var dictAddress = JSON()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Select_location_key"))
        
    }
    
    func setupUI()
    {
        self.btnConfirm.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        self.btnConfirm.setTitle(getCommonString(key: "Confirm_key"), for: .normal)
        self.vwGoogleMaps.delegate = self
        
        if selectedControllerAddEdit == .edit{
            let cameraCoord = CLLocationCoordinate2D(latitude: dictAddress["latitude"].doubleValue, longitude: dictAddress["longitude"].doubleValue)
            self.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: GlobalVariables.googleZoomLevel)
        } else {
            let cameraCoord = CLLocationCoordinate2D(latitude: userCurrentLocation?.coordinate.latitude ?? 0.0, longitude: userCurrentLocation?.coordinate.longitude ?? 0.0)
            self.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: GlobalVariables.googleZoomLevel)
        }
    }
}

//MARK: - IBAction

extension SelectLocationVc
{
    
    @IBAction func btnCurrentLocationAction(_ sender:UIButton){
        let cameraCoord = CLLocationCoordinate2D(latitude: userCurrentLocation?.coordinate.latitude ?? 0.0, longitude: userCurrentLocation?.coordinate.longitude ?? 0.0)
        self.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: GlobalVariables.googleZoomLevel)
    }
    
    @IBAction func btnConfirmTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "AddNewAddressVc") as! AddNewAddressVc
        if selectedControllerAddEdit == .edit{
            obj.dictAddress = dictAddress
        }
        /*
         if getUserDetail("is_login_guest") == "1" {
         self?.setupAddress(dict: dict)
         }
         */
        obj.selectedAddress = currentLocation
        obj.selectedControllerAddEdit = selectedControllerAddEdit
        obj.selectedContollerForAddDetails = selectedContollerForAddDetails
        obj.handlerAddnewAddress = {[weak self] dict in
            self?.handlerAddNewAddress(dict)
            
            DispatchQueue.main.async {
                self?.navigationController?.popViewController(animated: false)
            }
//            self?.navigationController?.popViewController(animated: true)
        }
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
}

//MARK:- Mapview Delegate

extension SelectLocationVc:GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("Defalult latitude \(position.target.latitude)")
        print("Defalult longitude \(position.target.longitude)")
        
        reverseGeoCoding(latitude: position.target.latitude, longitude: position.target.longitude) { (address) in
            self.currentLocation = address
        }
        
    }
}

