//
//  TailorInfoVC.swift
//  Tailory
//
//  Created by Khushbu on 24/09/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class TailorInfoVC: UIViewController {

    //MARK: Outlets
    @IBOutlet var lblTailorName: UILabel!
    @IBOutlet var txtTailorName: CustomTextField!
    
    @IBOutlet var lblTailorInfo: UILabel!
    @IBOutlet var lblEnterInfo: UILabel!
    @IBOutlet var txtvwEnterInfo: UITextView!
    
    @IBOutlet var btnSubmit: UIButton!
    
    
    //MARK: Variables
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Tailor_info_key"))
    }

    //MARK: Button Action
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        self.checkValidation()
//        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: SetUp
extension TailorInfoVC {
    
    func setupUI() {
        
        [txtTailorName].forEach { (txt) in
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.textColor = UIColor.appThemeLightGrayColor
            txt?.placeholder = getCommonString(key: "Enter_name_key")
        }
        
        [lblEnterInfo, lblTailorInfo, lblTailorName].forEach { (lbl) in
            
            lbl?.font = themeFont(size: 15, fontname: .medium)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        lblEnterInfo.font = themeFont(size: 13, fontname: .medium)
        lblEnterInfo.textColor = UIColor.appThemeLightGrayColor
        
        lblTailorName.text = getCommonString(key: "Tailor_Name_key")
        lblTailorInfo.text = getCommonString(key: "Tailor_info_key")
        lblEnterInfo.text =  getCommonString(key: "Enter_name_key")
        
        txtvwEnterInfo.font = themeFont(size: 15, fontname: .medium)
        txtvwEnterInfo.textColor = UIColor.appThemeLightGrayColor
        
        btnSubmit.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
    func setUpArabicUI() {
        
        self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [txtTailorName].forEach { (txt) in
            txt?.textAlignment = .right
            txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        txtvwEnterInfo.transform = CGAffineTransform(scaleX: -1, y: 1)
        txtvwEnterInfo.textAlignment = .right
        
        [lblTailorName, lblTailorInfo, lblEnterInfo].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        btnSubmit.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    func checkValidation() {
        
        if (self.txtTailorName.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: getCommonString(key: "Please_enter_tailor_name_key"))
        }
        else if (self.txtvwEnterInfo.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
            makeToast(strMessage: getCommonString(key: "Please_enter_tailor_info_key"))
        }
        else {
            self.tailorInfo()
        }
    }

}


//MARK: Textview delegate
extension TailorInfoVC : UITextViewDelegate {

    func textViewDidChange(_ textView: UITextView) {
        
        lblEnterInfo.isHidden = textView.text == "" ? false : true
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK: Service
extension TailorInfoVC {
    
    func tailorInfo() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlTrailor)\(urlFindTailor)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = [
                    "lang" : lang,
                    "user_id" : getUserDetail("user_id"),
                    "access_token" : getUserDetail("access_token"),
                    "tailor_name":self.txtTailorName.text ?? "",
                    "tailor_info":self.txtvwEnterInfo.text ?? ""
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        makeToast(strMessage: json["msg"].stringValue)
                        
                        DispatchQueue.main.async {
                            self.navigationController?.popViewController(animated: false)
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
}
