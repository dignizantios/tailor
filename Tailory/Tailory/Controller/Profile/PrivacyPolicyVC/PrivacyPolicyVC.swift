//
//  PrivacyPolicyVC.swift
//  Tailory
//
//  Created by Khushbu on 24/09/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import WebKit

class PrivacyPolicyVC: UIViewController {

    
    //MARK: Variables    
    var webView = WKWebView()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpUI()
        
    }
    
    
    
    func setUpUI() {
        
//        self.webView.delegate = self
        self.webView.frame = CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width-20, height: UIScreen.main.bounds.height)
       // let url = URL(string: "http://139.59.79.228/tailory/privacy_policies")
        //self.webView.loadRequest(URLRequest(url: url! as URL))
        self.webView.backgroundColor = .clear
        self.webView.scrollView.delegate = self
        self.webView.isMultipleTouchEnabled = false
        self.view.addSubview(self.webView)

    }
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Privacy_policy_key"))
        if lang == "en"{
            let url = URL(string: "\(kUrlMain)\(kPrivacyPolicyEn)")
            let webRequest : NSURLRequest = NSURLRequest(url: url!)
            webView.load(webRequest as URLRequest)
        }else{
            let url = URL(string: "\(kUrlMain)\(kPrivacyPolicyAr)")
            let webRequest : NSURLRequest = NSURLRequest(url: url!)
            webView.load(webRequest as URLRequest)
        }
    }
}

//MARK: - UIScrollViewDelegate
extension PrivacyPolicyVC : UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}

extension PrivacyPolicyVC: WKNavigationDelegate{
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print(#function)
        stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print(#function)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(#function)
        stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
        showLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(#function)
        stopAnimating()
    }
}

//MARK: Services
extension PrivacyPolicyVC {
    
    /*
    func privacyPolicyAPI() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(urlPrivacyPolicy)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = [
                "lang" : lang
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        let url = URL(string: "http://139.59.79.228/tailory/privacy_policies")
                        self.webView.loadRequest(URLRequest(url: url! as URL))
                        
//                        webView.loadRequest(URLRequest())
//                        makeToast(strMessage: json["msg"].stringValue)
//                        self.webView.load(<#T##data: Data##Data#>, mimeType: <#T##String#>, textEncodingName: <#T##String#>, baseURL: <#T##URL#>)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
     */
}
