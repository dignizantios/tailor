//
//  NotificationTblCell.swift
//  Tailory
//
//  Created by Haresh on 27/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class NotificationTblCell: UITableViewCell {

    //MARK:- Outlet
    
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var lblMainNotification: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblSubNotification: UILabel!
    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        vwMain.cornerRadius = 5.0
        
        lblMainNotification.font = themeFont(size: 14, fontname: .roman)
        lblMainNotification.textColor = UIColor.appThemeBlackColor
        
        lblSubNotification.font = themeFont(size: 12, fontname: .roman)
        lblSubNotification.textColor = UIColor.appThemeDarkGrayColor
        
        lblTime.font = themeFont(size: 11, fontname: .roman)
        lblTime.textColor = UIColor.appThemeLightGrayColor
        
        if !isEnglish
        {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblMainNotification,lblSubNotification,lblTime].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
            lblTime.textAlignment = .left
            
        }
        
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
