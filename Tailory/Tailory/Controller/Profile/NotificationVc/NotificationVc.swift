//
//  NotificationVc.swift
//  Tailory
//
//  Created by Haresh on 27/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class NotificationVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblNotification: UITableView!
    
    //MARK:-  Variable
    
    var arrayNotification : [JSON] = []
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var strMessage = String()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        NotificationList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Notifications_key"))
    }
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblNotification.addSubview(upperReferesh)
        self.tblNotification.tableFooterView = UIView()
        
        self.tblNotification.register(UINib(nibName: "NotificationTblCell", bundle: nil), forCellReuseIdentifier: "NotificationTblCell")
        self.tblNotification.tableFooterView = UIView()
        
       // setArrayNotification()
    }
    
    func setArrayNotification()
    {
        
        var dict = JSON()
        dict["main_notification"] = "Admin"
        dict["sub_notification"] = "Thank you for register with Tailor application."
        dict["time"] = "2 min ago"
        arrayNotification.append(dict)
        
        dict = JSON()
        dict["main_notification"] = "Admin"
        dict["sub_notification"] = "Thank you for register with Tailor application."
        dict["time"] = "2 min ago"
        arrayNotification.append(dict)
        
        dict = JSON()
        dict["main_notification"] = "Admin"
        dict["sub_notification"] = "Thank you for register with Tailor application."
        dict["time"] = "2 min ago"
        arrayNotification.append(dict)
        
        dict = JSON()
        dict["main_notification"] = "Admin"
        dict["sub_notification"] = "Thank you for register with Tailor application."
        dict["time"] = "2 min ago"
        arrayNotification.append(dict)
        
        dict = JSON()
        dict["main_notification"] = "Admin"
        dict["sub_notification"] = "Thank you for register with Tailor application."
        dict["time"] = "2 min ago"
        arrayNotification.append(dict)
        
        dict = JSON()
        dict["main_notification"] = "Admin"
        dict["sub_notification"] = "Thank you for register with Tailor application."
        dict["time"] = "2 min ago"
        arrayNotification.append(dict)
        
        dict = JSON()
        dict["main_notification"] = "Admin"
        dict["sub_notification"] = "Thank you for register with Tailor application."
        dict["time"] = "2 min ago"
        arrayNotification.append(dict)
        
        dict = JSON()
        dict["main_notification"] = "Admin"
        dict["sub_notification"] = "Thank you for register with Tailor application."
        dict["time"] = "2 min ago"
        arrayNotification.append(dict)
        
        self.tblNotification.reloadData()
        
    }

}


//MARK: - TableView delegate Method

extension NotificationVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayNotification.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return self.arrayNotification.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTblCell") as! NotificationTblCell
        
        let dict = self.arrayNotification[indexPath.row]
        
        cell.lblMainNotification.text = dict["title"].stringValue
        cell.lblSubNotification.text = dict["message"].stringValue
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let yourDate = formatter.date(from: "\(dict["date"].stringValue)")
        cell.lblTime.text = timeAgoSinceDate(date:yourDate ?? Date(), numericDates:true)
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //notification_type 1 = offer
         //notification_type 2 = order
        if  self.arrayNotification[indexPath.row]["notification_type"].stringValue == "2"{
            var dict = JSON()
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyOrderDetailsVc") as! MyOrderDetailsVc
            dict["id"].stringValue = self.arrayNotification[indexPath.row]["order_id"].stringValue
            dict["address_id"].stringValue = self.arrayNotification[indexPath.row]["address_id"].stringValue
            obj.dicMyOrderData = dict
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
    }
}
//#MARK:- Service calling
extension NotificationVc{
    
    @objc func upperRefreshTable()
    {
        intOffset = 0
        NotificationList()
    }
    
    func NotificationList() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlUser)\(urlNotificationList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString
                        ]
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrayNotification = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblNotification.reloadData()
                            return
                        }
                        
                        if self.intOffset == 0
                        {
                            self.arrayNotification = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrayNotification + aryData
                        self.arrayNotification = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayNotification = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblNotification.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblNotification.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblNotification.contentSize.height).rounded(toPlaces: 2)) - (Double(tblNotification.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                NotificationList()
            }
        }
    }
}
