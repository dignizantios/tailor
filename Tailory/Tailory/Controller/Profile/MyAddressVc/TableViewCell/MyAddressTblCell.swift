//
//  MyAddressTblCell.swift
//  Tailory
//
//  Created by Haresh on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class MyAddressTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var mainView: CustomView!
    @IBOutlet weak var lblMainAddress: UILabel!
    @IBOutlet weak var lblSubAddress: UILabel!
    @IBOutlet weak var vwRadioButton: UIView!
    @IBOutlet weak var btnRadioBtn: UIButton!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainView.cornerRadius = 4
        
        lblMainAddress.font = themeFont(size: 17, fontname: .heavy)
        lblMainAddress.textColor = UIColor.appThemeBlackColor
        
        lblSubAddress.font = themeFont(size: 15, fontname: .light)
        lblSubAddress.textColor = UIColor.appThemeLightGrayColor
        
        if !isEnglish
        {
            
            self.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
            [lblSubAddress,lblMainAddress].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
