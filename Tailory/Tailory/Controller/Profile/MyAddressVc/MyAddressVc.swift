//
//  MyAddressVc.swift
//  Tailory
//
//  Created by Haresh on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class MyAddressVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblAddressList: UITableView!
    
    //MARK: - Variable
    
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var arrAddressList:[JSON] = []
    var strMessage = String()
    var selectedController = checkForParentControllerOfMyMeasurment.fromProfile
    var handlerUpdateAddress:(JSON) -> Void = {_ in}
    var dictAddress = JSON()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "My_addresses_key"))
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_my_measurement_add_new_detail_plush"), style: .plain, target: self, action: #selector(addAddressTapped))
        rightButton.tintColor = UIColor.white
        
        if isEnglish
        {
            self.navigationItem.rightBarButtonItem = rightButton
        }
        else
        {
            self.tblAddressList.semanticContentAttribute = .forceRightToLeft
            self.navigationItem.leftBarButtonItem = rightButton
        }
        
    }
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        self.tblAddressList.register(UINib(nibName: "MyAddressTblCell", bundle: nil), forCellReuseIdentifier: "MyAddressTblCell")
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblAddressList.addSubview(upperReferesh)
        
        tblAddressList.tableFooterView = UIView()
        
        upperRefreshTable()
    }
    
    @objc func btnDeleteTapped(_ sender:UIButton)
    {
        let alertController = UIAlertController(title: getCommonString(key: "Tailors_key"), message: getCommonString(key: "Are_you_sure_you_want_to_remove_address_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            let dict = self.arrAddressList[sender.tag]
            self.removeAddress(addressId: dict["address_id"].stringValue)
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func btnEditAction(_ sender:UIButton)
    {
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SelectLocationVc") as! SelectLocationVc
        obj.selectedContollerForAddDetails = .addNewAddress
        obj.selectedControllerAddEdit = .edit
        obj.dictAddress = arrAddressList[sender.tag]
        obj.handlerAddNewAddress = {[weak self] dict in
            /*if dict["is_default"].stringValue == "1"{
                for i in 0..<self!.arrAddressList.count{
                    var dict = self?.arrAddressList[i]
                    dict?["is_default"].stringValue = "0"
                    self?.arrAddressList[i] = dict ?? JSON()
                }
            }*/
//            self?.arrAddressList.append(dict)
//            self?.setupAddress()
            self?.upperRefreshTable()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }

}

//MARK: - TableView delegate Method

extension MyAddressVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrAddressList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAddressTblCell") as! MyAddressTblCell
        
        let dict = arrAddressList[indexPath.row]
        
        cell.lblMainAddress.text = dict["address_name"].stringValue
        
        var strSubAddress = "\n\(getCommonString(key: "Floor_key")) : \(dict["floor"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Building_key")) : \(dict["building"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Avenu_key")) : \(dict["avenue"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Street_key")) : \(dict["street"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Block_key")) : \(dict["block"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Postal_code_key")) : \(dict["postal_code"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "City_key")) : \(dict["city"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Governorate_key")) : \(dict["governorate"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Country_key")) : \(dict["country"].stringValue) "
        
        cell.lblSubAddress.text = strSubAddress
        
        cell.btnDelete.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteTapped), for: .touchUpInside)
        cell.btnEdit.addTarget(self, action: #selector(btnEditAction), for: .touchUpInside)
        
        if selectedController == .fromTailor || selectedController == .fromCart
        {
            cell.vwRadioButton.isHidden = false
            
            if dict["is_default"].stringValue == "0"
            {
                cell.btnRadioBtn.isSelected = false
            }
            else
            {
                cell.btnRadioBtn.isSelected = true
            }
            
        }
        else if selectedController == .fromProfile
        {
            cell.vwRadioButton.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedController == .fromTailor{
            for i in 0..<self.arrAddressList.count{
                self.arrAddressList[i]["is_default"].stringValue = "0"
            }
            self.arrAddressList[indexPath.row]["is_default"].stringValue = "1"
            self.tblAddressList.reloadData()
            handlerUpdateAddress(self.arrAddressList[indexPath.row])
            self.navigationController?.popViewController(animated: true)
        } else if selectedController == .fromCart{
            //TODO:- After New Changes
            let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "CheckoutVc") as! CheckoutVc
            obj.dictAddress = self.arrAddressList[indexPath.row]
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0 {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        }
        else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }

    ///--- Swipe to delete row
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: getCommonString(key: "Delete_key")) { (action, index) in
            let alertController = UIAlertController(title: getCommonString(key: "My_addresses_key"), message: getCommonString(key: "Are_you_sure_want_to_delete_key"), preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                print("Yes")
                let dict = self.arrAddressList[indexPath.row]
                self.removeAddress(addressId: dict["address_id"].stringValue)
            }
            let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        delete.backgroundColor = .red
        return [delete]
    }
}

//MARK: - IBAction

extension MyAddressVc
{
    @objc func addAddressTapped()
    {
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SelectLocationVc") as! SelectLocationVc
        obj.handlerAddNewAddress = {[weak self] dict in
            /*if dict["is_default"].stringValue == "1"{
                for i in 0..<self!.arrAddressList.count{
                    var dict = self?.arrAddressList[i]
                    dict?["is_default"].stringValue = "0"
                    self?.arrAddressList[i] = dict ?? JSON()
                }
            }
            self?.arrAddressList.append(dict)
            self?.setupAddress()*/
            self?.upperRefreshTable()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func setupAddress(){
        var isAddDefaultAddress = false
        for i in 0..<self.arrAddressList.count{
            var dict = self.arrAddressList[i]
            if dict["is_default"].stringValue == "1"{
                isAddDefaultAddress = true
                self.handlerUpdateAddress(dict)
            }
        }
        if selectedController == .fromTailor{
            if !isAddDefaultAddress{
                for i in 0..<self.arrAddressList.count{
                    var dict = self.arrAddressList[i]
                    dict["is_default"].stringValue = "0"
                    if self.dictAddress["address_id"].stringValue == dict["address_id"].stringValue{
                        dict["is_default"].stringValue = "1"
                        self.handlerUpdateAddress(dict)
                    }
                    self.arrAddressList[i] = dict
                }
            } else {
                
            }
        }
//        self.tblAddressList.reloadData()
    }
}


//MARK: - Redirect To Guestpopup

extension MyAddressVc: delegateGuestPopupRedirection
{
    func redirectToCreateAccount() {
        
        self.view.endEditing(true)
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        obj.selectedController = .fromGuesdUserDetailPopup
        self.navigationController?.pushViewController(obj, animated: false)
        
    }
    
    func redirectToContinue() {
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "AddNewAddressVc") as! AddNewAddressVc
        obj.selectedContollerForAddDetails = .addGuestUserDetails
        self.navigationController?.pushViewController(obj, animated: true)        
    }
    
}

extension MyAddressVc
{
    @objc func upperRefreshTable()
    {
        intOffset = 0
        getMyAddressList()
    }
    
    func getMyAddressList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlUser)\(urlUserAddress)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "offset" : String(self.intOffset)]
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrAddressList = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblAddressList.reloadData()
                            self.handlerUpdateAddress(JSON())
                            return
                        }
                        
                        if self.intOffset == 0
                        {
                            self.arrAddressList = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrAddressList + aryData
                        self.arrAddressList = aryData
                        if self.selectedController == .fromTailor || self.selectedController == .fromCart{
                            var isAnyDefault = false
                            for i in 0..<self.arrAddressList.count{
                                var dict = self.arrAddressList[i]
                                if self.dictAddress["address_id"].stringValue == dict["address_id"].stringValue{
                                    isAnyDefault = true
                                    dict["is_default"].stringValue = "1"
                                } else {
                                    dict["is_default"].stringValue = "0"
                                }
                                self.arrAddressList[i] = dict
                            }
                            if !isAnyDefault{
                                var dict = self.arrAddressList[0]
                                dict["is_default"].stringValue = "1"
                                self.arrAddressList[0] = dict
                                self.handlerUpdateAddress(self.arrAddressList[0])
                            } else if self.arrAddressList.count == 1{
                                var dict = self.arrAddressList[0]
                                dict["is_default"].stringValue = "1"
                                self.arrAddressList[0] = dict
                                self.handlerUpdateAddress(self.arrAddressList[0])
                            }
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrAddressList = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblAddressList.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func removeAddress(addressId:String)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlRemoveAddress)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "address_id" : addressId]
            
            print("param : \(param)")
           
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
              
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.upperRefreshTable()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }

                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
           if (Double(tblAddressList.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblAddressList.contentSize.height).rounded(toPlaces: 2)) - (Double(tblAddressList.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                getMyAddressList()
            }
        }
    }
}

