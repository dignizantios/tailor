//
//  MyOrderVc.swift
//  Tailory
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

class MyOrderVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblMyOrder: UITableView!
    
    //MARK: - Variable
    
    var arrayOrderList : [JSON] = []
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var arrMyOrderList:[JSON] = []
    var strMessage = String()
    let Captured = "0"
    let Success = "1"
    let Processing = "2"
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isOnOrderScreen = true
        getCartCount()
        setUpNavigationBarWithTitleAndCart(strTitle: getCommonString(key: "Orders_key"))
        NotificationCenter.default.addObserver(self, selector: #selector(upperRefreshTable), name: .didRefreshOrderList, object: nil)
        if isComeFromPush{
            isComeFromPush = false
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyOrderDetailsVc") as! MyOrderDetailsVc
            obj.handlerReorderData = {[weak self] in
//                makeToast(strMessage: str)
                self?.upperRefreshTable()
            }
            obj.dicMyOrderData = dictPushData
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isOnOrderScreen = false
    }
    
    //MARK: - SetupUI
    
    func setupUI() {
        self.tblMyOrder.register(UINib(nibName: "MyOrderTblCell", bundle: nil), forCellReuseIdentifier: "MyOrderTblCell")
        
        self.tblMyOrder.tableFooterView = UIView()
        
        if getUserDetail("is_login_guest") == "1" {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginRequiredScreenVc") as! LoginRequiredScreenVc
            addChild(obj)
            self.view.addSubview(obj.view)
            obj.didMove(toParent: self)
        }
        else {
            upperRefreshTable()
        }
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblMyOrder.addSubview(upperReferesh)
        
        tblMyOrder.tableFooterView = UIView()
        
//        upperRefreshTable()
        
    }
}

//MARK: - TableView delegate Method

extension MyOrderVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrMyOrderList.count == 0 {
            
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            if !isEnglish {
                lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            return 0
        }
        tableView.backgroundView = nil
        return self.arrMyOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderTblCell") as! MyOrderTblCell
        
        let dict = self.arrMyOrderList[indexPath.row]

        cell.lblVendorsName.text = dict["vendors_name"].stringValue
//        cell.lblOrderStatus.text = dict["order_status"].stringValue
        cell.lblOrderID.text = "\(getCommonString(key: "Order_id_key")) \(dict["order_id"].stringValue)"
        cell.lblDate.text = "\(getCommonString(key: "Date_key")) \(dict["order_date"].stringValue)"
        cell.lblTotalAmount.text = "\(getCommonString(key: "Total_amount:_key")) \(dict["total_price"].stringValue)"
        
        if dict["order_status"].stringValue == "9" {
            cell.btnReorder.isHidden = false
            cell.btnReorder.tag = indexPath.row
            cell.btnReorder.addTarget(self, action: #selector(btnReorderAction(_:)), for: .touchUpInside)
            cell.lblOrderStatus.text = dict["status_name"].stringValue
            cell.lblOrderStatus.textColor = UIColor.green
        }else{
            cell.btnReorder.isHidden = true
            cell.lblOrderStatus.text = dict["status_name"].stringValue
            cell.lblOrderStatus.textColor = UIColor(red: 112/255, green: 110/255, blue: 220/255, alpha: 1.0)
        }
        /*else if dict["status_name"].stringValue == "received" {
            cell.btnReorder.isHidden = true
            cell.lblOrderStatus.text = dict["status_name"].stringValue
            cell.lblOrderStatus.textColor = UIColor(red: 112/255, green: 110/255, blue: 220/255, alpha: 1.0)
        }*/

        
        /*
        if dict["order_status"].stringValue == "Success" {
            cell.btnReorder.isHidden = false
            cell.lblOrderStatus.textColor = UIColor.green
        }
        else
        {
            cell.btnReorder.isHidden = true
            cell.lblOrderStatus.textColor = UIColor(red: 112/255, green: 110/255, blue: 220/255, alpha: 1.0)
        }
        */
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.redirectToOrderDetails(index:indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0 {
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        }
        else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if self.intOffset != -1 && self.intOffset != 0 {
            
            if (Double(tblMyOrder.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblMyOrder.contentSize.height).rounded(toPlaces: 2)) - (Double(tblMyOrder.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2)) {
                myOrderListApi()
            }
        }
    }
    
    @objc func btnReorderAction(_ sender: UIButton) {
        
        let OrderId = self.arrMyOrderList[sender.tag]["id"].stringValue
        self.reorderAPICalling(orderId: OrderId)
    }
}

//MARK: - Redirection

extension MyOrderVc {
    
    func redirectToOrderDetails(index:Int)
    {
        
        let dict = self.arrMyOrderList[index]
        
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyOrderDetailsVc") as! MyOrderDetailsVc
        
        if dict["order_status"].stringValue == Captured {
            obj.selectedFrom = .capture
        }
        else if dict["order_status"].stringValue == Success {
            obj.selectedFrom = .success
            obj.handlerReorderData = {[weak self] in
//                makeToast(strMessage: str)
                self?.upperRefreshTable()
            }
        }
        else if dict["order_status"].stringValue == Processing {
            obj.selectedFrom = .proccessing
        }
        obj.dicMyOrderData = dict
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}


//MARK: Services
extension MyOrderVc {
    
    @objc func upperRefreshTable() {
        intOffset = 0
        myOrderListApi()
    }
    
    func getCartCount()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlCartCount)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "is_login_guest": getUserDetail("is_login_guest") == "0" ? "0" : "1",
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString]
            
            print("param : \(param)")
            
            //self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                //self.hideLoader()
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].exists(){
                            let data = json["data"].dictionaryValue
                            isCartCount = data["cart_count"]?.intValue ?? 0
                            self.setUpNavigationBarWithTitleAndCart(strTitle: getCommonString(key: "Orders_key"))
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        //makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    // makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            //makeToast(strMessage: networkMsg)
        }
    }
    
    func myOrderListApi() {
        
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            if self.intOffset < 0{
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlOrder)\(urlMyOrderList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)"            ]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        if json["data"].arrayValue.count == 0 {
                            self.arrMyOrderList = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblMyOrder.reloadData()
                            return
                        }
                        
                        if self.intOffset == 0 {
                            self.arrMyOrderList = []
                        }
                        
                        if self.intOffset < 0 {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrMyOrderList + aryData
                        
                        self.arrMyOrderList = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        if getUserDetail("is_login_guest") != "1" {
                            makeToast(strMessage: json["msg"].stringValue)
                        }
                        self.arrMyOrderList = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblMyOrder.reloadData()
                }
                else {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func reorderAPICalling(orderId: String) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlOrder)\(reorder)"
            print("URL: \(url)")
            
            let param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "order_id" : orderId,
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                         "is_login_guest" : getUserDetail("is_login_guest") == "1" ? "1" : "0",
                         "lang": lang
            ]
            
            print("param :\(param)")
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in

                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let cartVC = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
                        
                        cartVC.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(cartVC, animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
}
