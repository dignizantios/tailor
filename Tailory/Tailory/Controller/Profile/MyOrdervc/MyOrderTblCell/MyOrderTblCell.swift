//
//  MyOrderTblCell.swift
//  Tailory
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class MyOrderTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblVendorsName: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var btnReorder: CustomButton!
    
    //MARK: - Variable
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblVendorsName.textColor = UIColor.appThemeBlackColor
        lblVendorsName?.font = themeFont(size: 15, fontname: .heavy)

        lblOrderStatus?.font = themeFont(size: 14, fontname: .roman)

        [lblOrderID,lblDate,lblTotalAmount].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 13, fontname: .roman)
        }
        
        btnReorder.setTitle(getCommonString(key: "Reorder_key"), for: .normal)
        btnReorder.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        btnReorder.backgroundColor = UIColor.white
        btnReorder.titleLabel?.font = themeFont(size: 12, fontname: .roman)
        btnReorder.layer.cornerRadius = 1.5
        btnReorder.layer.cornerRadius = 5
        btnReorder.layer.masksToBounds = true
        btnReorder.borderWidth = 1.0
        btnReorder.borderColor = UIColor.appThemeSilverColor
        
        if !isEnglish
        {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblOrderID,lblDate,lblTotalAmount,lblVendorsName].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
            btnReorder.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblOrderStatus.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblOrderStatus.textAlignment = .left
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
