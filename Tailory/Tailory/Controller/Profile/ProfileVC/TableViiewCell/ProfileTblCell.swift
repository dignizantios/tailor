//
//  ProfileTblCell.swift
//  Tailory
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class ProfileTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet var btnSwitch: UISwitch!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblName.textColor = UIColor.appThemeBlackColor
        lblName?.font = themeFont(size: 15, fontname: .heavy)

        if !isEnglish
        {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblName.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblName.textAlignment = .right
        }
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
