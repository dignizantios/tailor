//
//  ProfileVC.swift
//  Tailory
//
//  Created by Khushbu on 15/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ProfileVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblProfile: UITableView!
    @IBOutlet var vwFooter: UIView!
    
    @IBOutlet weak var lblFooterTitle: UILabel!
    @IBOutlet weak var lbFooterSubTitle: UILabel!
    @IBOutlet var btnFindFooter: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    
    //MARK: - Variable
    
    var arrayProfile : [JSON] = []
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCartCount()
        self.setUpNavigationBarWithTitleAndCart(strTitle: getCommonString(key: "Profile_key"))
        self.tblProfile.reloadData()
    }
    
    //MARK: - setupUI,Arabic and Array
    
    func setupUI()
    {
        self.tblProfile.register(UINib(nibName: "ProfileTblCell", bundle: nil), forCellReuseIdentifier: "ProfileTblCell")
        
        
        
        lblFooterTitle.textColor = UIColor.appThemeBlackColor
        lblFooterTitle?.font = themeFont(size: 15, fontname: .heavy)
        lbFooterSubTitle.textColor = UIColor.appThemeLightGrayColor
        lbFooterSubTitle?.font = themeFont(size: 15, fontname: .roman)

        btnLogout.setTitle(getCommonString(key: "Logout_key"), for: .normal)
        btnLogout.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        btnLogout.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        
        lblFooterTitle.text = getCommonString(key: "Can_not_find_tailor_in_the_app_?_key")
        lbFooterSubTitle.text = getCommonString(key: "Kindly_send_us_tailor_name_and_info_key")
        
        if getUserDetail("is_login_guest") as? String == "1"
        {
            setArrayForGuest()
            self.tblProfile.tableFooterView = UIView()
        }
        else
        {
            setArrayProfile()
            self.tblProfile.tableFooterView = vwFooter
        }
        
        
        if !isEnglish  {
            setUpArabicUI()
        }
        
    }
    
    func setUpArabicUI()
    {
        lblFooterTitle.textAlignment = .right
        lbFooterSubTitle.textAlignment = .right
    }
    func setArrayProfile()
    {
        
//        var dict = JSON()
//        dict["name"].stringValue = getCommonString(key: "My_orders_key")
//        self.arrayProfile.append(dict)
        
        var dict = JSON()
        dict["name"].stringValue = getCommonString(key: "My_measurments_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Saved_desings_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "My_addresses_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "My_details_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Change_password_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Settings_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Terms_of_use_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Privacy_policy_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Contact_Us_key")
        self.arrayProfile.append(dict)

        self.tblProfile.reloadData()
        
    }
    
    
    func setArrayForGuest()
    {
        
        var dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Settings_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Terms_of_use_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Privacy_policy_key")
        self.arrayProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Contact_Us_key")
        self.arrayProfile.append(dict)
        
        self.tblProfile.reloadData()
        
    }
    
    
    @IBAction func btnLogoutTapped(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "", message: getCommonString(key: "Are_you_sure_you_want_to_logout?_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.logoutAPICalling()
//           self.redirectToLogin()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func btnFindFooterAction(_ sender: Any) {
        
        let tailorInfoVC = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "TailorInfoVC") as! TailorInfoVC
        
        tailorInfoVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(tailorInfoVC, animated: false)
    }
    
    func redirectToLogin()
    {
        Defaults.removeObject(forKey: "userDetails")
        Defaults.synchronize()
        
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let rearNavigation = UINavigationController(rootViewController: vc)
        appdelgate.window?.rootViewController = rearNavigation
    }
}


//MARK: - TableView delegate Method

extension ProfileVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayProfile.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTblCell") as! ProfileTblCell
        
        cell.lblName.text = self.arrayProfile[indexPath.row]["name"].stringValue
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if getUserDetail("is_login_guest") as? String == "1"
        {
            redirectGuestUser(index: indexPath.row)
        }
        else
        {
            redirectToController(index: indexPath.row)
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    //set header for
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)?[0] as? HeaderView
        
        if !isEnglish {
            vw?.cwBG.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        if getUserDetail("is_login_guest") == "1" {
            vw?.lblTitleName.text = getCommonString(key: "Welcome_key") + ", " + getCommonString(key: "Guest_key")
            vw?.btnLogin.setTitle(getCommonString(key: "Login_key"), for: .normal)
            vw?.btnLogin.isHidden = false
            vw?.btnLogin.addTarget(self, action: #selector(btnLoginAction(_:)), for: .touchUpInside)
        }
        else {
            vw?.btnLogin.setTitle("", for: .normal)
            vw?.lblTitleName.text = getCommonString(key: "Welcome_key") + ", " + getUserDetail("full_name")
//            vw?.btnLogin.addTarget(self, action: #selector(btnLoginAction(_:)), for: .touchUpInside)
        }
        
        return vw
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    @objc func btnLoginAction (_ sender: UIButton) {
        let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        
        objc.selectedController = .fromGuesdUserDetailPopup
        objc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(objc, animated: false)
    }
}

//MARK: - Redirection

extension ProfileVC
{
    func getCartCount()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlCartCount)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "is_login_guest": getUserDetail("is_login_guest") == "0" ? "0" : "1",
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString]
            
            print("param : \(param)")
            
            //self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                //self.hideLoader()
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].exists(){
                            let data = json["data"].dictionaryValue
                            isCartCount = data["cart_count"]?.intValue ?? 0
                            self.setUpNavigationBarWithTitleAndCart(strTitle: getCommonString(key: "Profile_key"))
                            
                        }
                        
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        //makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    // makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            //makeToast(strMessage: networkMsg)
        }
    }
    
    func redirectToController(index:Int)
    {
        //Old flow
        /*
        if index == 0
        {
            //My Orders
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyOrderVc") as! MyOrderVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if index == 1
        {
            //My Measurment
            
            let myMeasurmentVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyMeasurmentVC") as! MyMeasurmentVC
            
            self.navigationController?.pushViewController(myMeasurmentVC, animated: true)
            
        }
        else if index == 2
        {
            //Saved designs
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SavedDesignVc") as! SavedDesignVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if index == 3
        {
            //My address
            
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVc") as! MyAddressVc
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else if index == 4
        {
            //My details
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyDetailsVc") as! MyDetailsVc
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else if index == 5
        {
            //Change password
            
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVc") as! ChangePasswordVc
            
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else if index == 6
        {
            //Settings
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SettingsVc") as! SettingsVc
            
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if index == 7
        {
            //Terms of use
            
            
        }
        else if index == 8
        {
            //Privacy policy
            
//            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LaundryPopupVc") as! LaundryPopupVc
//            obj.delegateWantLaundry = self
//            obj.modalPresentationStyle = .overCurrentContext
//            obj.modalTransitionStyle = .crossDissolve
//            self.present(obj, animated: false, completion: nil)
            
        }
        */
        
        //New flow
        
        if index == 0
        {
            //My Measurment
            
            let myMeasurmentVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyMeasurmentVC") as! MyMeasurmentVC
            myMeasurmentVC.selectedController = .fromProfile
            self.navigationController?.pushViewController(myMeasurmentVC, animated: true)
        }
        else if index == 1
        {
            //Saved designs
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SavedDesignVc") as! SavedDesignVc
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else if index == 2
        {
            //My address
            
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVc") as! MyAddressVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if index == 3
        {
            //My details
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyDetailsVc") as! MyDetailsVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if index == 4
        {
            //Change password
            
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVc") as! ChangePasswordVc
            
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if index == 5
        {
            //Settings
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SettingsVc") as! SettingsVc
            
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else if index == 6
        {
            let termsConditionVC = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
            
            termsConditionVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(termsConditionVC, animated: true)
            
            //Terms of use
        }
        else if index == 7
        {
            //Privacy policy
            let privacyPolicyVC = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
            
            privacyPolicyVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(privacyPolicyVC, animated: true)

        }
        else if index == 8 {
            
            //Contact Us
            let contactUsVC = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            
            contactUsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(contactUsVC, animated: true)
        }
        
    }
    
    
    func redirectGuestUser(index:Int)
    {
        if index == 0
        {
            //Settings
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SettingsVc") as! SettingsVc
            
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        else if index == 1
        {
            //Terms of use
            let termsConditionVC = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "TermsConditionVC") as! TermsConditionVC
            
            termsConditionVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(termsConditionVC, animated: true)
        }
        else if index == 2
        {
            //Privacy policy
            let privacyPolicyVC = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
            
            privacyPolicyVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(privacyPolicyVC, animated: true)
            
        }
        else if index == 3 {
            
            //Contact Us
            let contactUsVC = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            
            contactUsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(contactUsVC, animated: true)
        }
    }
    
}


