//
//  ChangePasswordVc.swift
//  Tailory
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON

class ChangePasswordVc: UIViewController {

    //MARK: - Outlet
    
    
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var txtOldPassword: CustomTextField!
    @IBOutlet weak var txtNewPassword: CustomTextField!
    @IBOutlet weak var txtConfirmPassword: CustomTextField!
    
    @IBOutlet weak var btnChange: CustomButton!

    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Change_password_key"))
    }
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        
        [txtOldPassword,txtNewPassword,txtConfirmPassword].forEach { (txt) in
            txt?.tintColor = UIColor.appThemeBlackColor
            txt?.textColor = UIColor.appThemeBlackColor
            txt?.delegate = self
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.leftPaddingView = 50
            txt?.rightPaddingView = 0
        }
        
        txtOldPassword.placeholder = getCommonString(key: "Old_password_key")
        txtNewPassword.placeholder = getCommonString(key: "New_password_key")
        txtConfirmPassword.placeholder = getCommonString(key: "Confirm_password_key")
        
        btnChange.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnChange.setTitle(getCommonString(key: "Change_key"), for: .normal)
        
        if !isEnglish
        {
            self.setUpArabicUI()
        }
        
    }
    
    func setUpArabicUI()
    {
        
        vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [txtOldPassword,txtNewPassword,txtConfirmPassword].forEach { (txt) in
            txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
            txt?.leftPaddingView = 0
            txt?.rightPaddingView = 50
            txt?.textAlignment = .right
        }
        
    }

}

//MARK: - IBAction

extension ChangePasswordVc
{
    @IBAction func btnChangeTapped(_ sender: UIButton) {
        if (txtOldPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty{
            makeToast(strMessage: getCommonString(key: "Please_enter_old_password_key"))
        }
        else if (txtNewPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty{
            makeToast(strMessage: getCommonString(key: "Please_enter_new_password_key"))
        }
        else if (txtNewPassword.text?.count)! < GlobalVariables.passwordLegth{
            makeToast(strMessage: getCommonString(key: "New_password_must_be_at_least_8_characters_long_key"))
        }
        /*else if containsSpecialCharacters(string: txtNewPassword.text!) == false{
            makeToast(strMessage: getCommonString(key: "Please_enter_At_least_8_characters_1_symbol_1_number_etc"))
        } else if doStringContainsAlpha(string: txtNewPassword.text!) == false{
             makeToast(strMessage: getCommonString(key: "Please_enter_At_least_8_characters_1_symbol_1_number_etc"))
        } else if doStringContainsNumber(string: txtNewPassword.text!) == false {
            makeToast(strMessage: getCommonString(key: "Please_enter_At_least_8_characters_1_symbol_1_number_etc"))
        }*/
        /*else if !(isValidPassword(passwordString: txtNewPassword.text!)){
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_new_password_key"))
        }*/
        else if (txtConfirmPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty{
            makeToast(strMessage: getCommonString(key: "Please_enter_confirm_password_key"))
        }
        else if(!(txtConfirmPassword.text! == txtNewPassword.text!)){
            makeToast(strMessage: getCommonString(key: "New_password_and_confirmation_password_must_match_key"))
        } else {
            changePassword()
        }
    }
}

//MARK: - TextFIeld Delegate

extension ChangePasswordVc: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK: - API calling

extension ChangePasswordVc
{
    func changePassword()
    {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!        {
            
            let url = "\(urlUser)\(urlChangePassword)"
            print("URL: \(url)")
            let param = ["lang" : lang,
                         "user_id" : getUserDetail("user_id"),
                         "access_token" :getUserDetail("access_token"),
                         "current_password" : txtOldPassword.text ?? "",
                         "new_password" : txtNewPassword.text ?? "",
                         "device_token": Defaults.value(forKey: "device_token") as? String ??  UIDevice.current.identifierForVendor!.uuidString
]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}
