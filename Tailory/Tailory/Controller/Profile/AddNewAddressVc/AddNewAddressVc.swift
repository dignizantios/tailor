//
//  AddNewAddressVc.swift
//  Tailory
//
//  Created by Haresh on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//


enum checkParentControllerForEnterDetails
{
    case addNewAddress
    case addGuestUserDetails
}

enum checkParentControllerAddess:String {
    case add = "0"
    case edit = "1"
}

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import GoogleMaps
import FirebaseMessaging
import DropDown

class AddNewAddressVc: UIViewController {
    
    //MARK:- Outlet
    
    @IBOutlet weak var vwFullName: CustomView!
    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var txtFullName: UITextField!
    
    @IBOutlet var vwEmail: CustomView!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var txtCountry: UITextField!
    
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var lblAddressName: UILabel!
    @IBOutlet weak var txtAddressName: UITextField!
    
    @IBOutlet weak var lblGovernorate: UILabel!
    @IBOutlet weak var txtGovernorate: UITextField!
    
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtCity: UITextField!
    
    @IBOutlet weak var lblPostalCode: UILabel!
    @IBOutlet weak var txtPostalCode: UITextField!
    
    @IBOutlet weak var lblBlock: UILabel!
    @IBOutlet weak var txtBloack: UITextField!
    
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var txtStreet: UITextField!
    
    @IBOutlet weak var lblAvenu: UILabel!
    @IBOutlet weak var txtAvenu: UITextField!
    
    @IBOutlet weak var lblBulding: UILabel!
    @IBOutlet weak var txtBuilding: UITextField!
    
    @IBOutlet weak var lblFloor: UILabel!
    @IBOutlet weak var txtFloor: UITextField!
    
    @IBOutlet weak var lblExtraNotes: UILabel!
    @IBOutlet weak var txtvwExtraNotes: UITextView!
    
    @IBOutlet weak var vwDefaultAddress: CustomView!
    @IBOutlet weak var lblSetasDefaultAddress: UILabel!
    @IBOutlet weak var switchDefaultAddress: UISwitch!
    
    @IBOutlet weak var btnContinue: CustomButton!
    
    
    //MARK: - Variable
    
    var selectedContollerForAddDetails = checkParentControllerForEnterDetails.addNewAddress
    var selectedControllerAddEdit = checkParentControllerAddess.add
    var selectedAddress = GMSAddress()
    var handlerAddnewAddress:(JSON) -> Void = {_ in}
    var dictAddress = JSON()
    var governorateDD = DropDown()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if selectedContollerForAddDetails == .addNewAddress
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Add_new_address_key"))
        }
        else if selectedContollerForAddDetails == .addGuestUserDetails
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Guest_checkout_key"))
            self.getGuestAddress()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        configureDropdown(dropdown: governorateDD, sender: txtGovernorate)
    }
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        [txtFullName,txtEmail,txtCountry,txtPhoneNumber,txtAddressName,txtGovernorate,txtCity,txtPostalCode,txtBloack,txtStreet,txtAvenu,txtBuilding,txtFloor].forEach { (txt) in
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.textColor = UIColor.appThemeLightGrayColor
            txt?.placeholder = getCommonString(key: "Enter_here_key")
            txt?.delegate = self
        }
        
        if !isEnglish {
           txtEmail.font = UIFont(name: themeFonts.light.rawValue, size: 15)
        }
        
        txtPhoneNumber.delegate = self
        addDoneButtonOnKeyboard(textfield: txtPhoneNumber)
        
        txtvwExtraNotes.font = themeFont(size: 15, fontname: .medium)
        txtvwExtraNotes.textColor = UIColor.appThemeLightGrayColor
        txtvwExtraNotes.delegate = self
       
        [lblFullname,lblEmail,lblCountry,lblPhoneNumber,lblAddressName,lblGovernorate,lblCity,lblPostalCode,lblBlock,lblStreet,lblAvenu,lblBulding,lblFloor,lblExtraNotes].forEach { (lbl) in
            
            lbl?.font = themeFont(size: 15, fontname: .medium)
            lbl?.textColor = UIColor.appThemeBlackColor
            
        }
        
        lblSetasDefaultAddress.font = themeFont(size: 15, fontname: .heavy)
        lblSetasDefaultAddress.textColor = UIColor.appThemeBlackColor
        
        btnContinue.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnContinue.setTitle(getCommonString(key: "Continue_key"), for: .normal)
        
        lblFullname.text = "\(getCommonString(key: "Full_name_key")) *"
        lblEmail.text = "\(getCommonString(key: "Email_key")) *"
        lblCountry.text = "\(getCommonString(key: "Country_key")) *"
        lblPhoneNumber.text = "\(getCommonString(key: "Phone_number_key")) *"
        lblAddressName.text = "\(getCommonString(key: "Address_name_key")) *"
        lblGovernorate.text = "\(getCommonString(key: "Governorate_key"))"
        lblCity.text = "\(getCommonString(key: "City_key")) *"
        lblPostalCode.text = getCommonString(key: "Postal_code_key")
        lblBlock.text = "\(getCommonString(key: "Block_key")) *"
        lblStreet.text = "\(getCommonString(key: "Street_key")) *"
        lblAvenu.text = getCommonString(key: "Avenu_key")
        lblBulding.text = "\(getCommonString(key: "Building_key")) *"
        lblFloor.text = "\(getCommonString(key: "Floor_key"))"
        lblExtraNotes.text = getCommonString(key: "Extra_notes_key")
        
        lblSetasDefaultAddress.text = getCommonString(key: "Set_as_default_address_key")
        
        
        if selectedContollerForAddDetails == .addGuestUserDetails
        {
            self.vwDefaultAddress.isHidden = true
            self.vwFullName.isHidden = false
            self.vwEmail.isHidden = false
        }
        else if selectedContollerForAddDetails == .addNewAddress
        {
            self.vwDefaultAddress.isHidden = false
            self.vwFullName.isHidden = true
            self.vwEmail.isHidden = true
        }
        
        if getUserDetail("is_login_guest") == "1" {
            vwDefaultAddress.isHidden = true
        }
        else {
            vwDefaultAddress.isHidden = false
        }
        
        if !isEnglish
        {
            setUpArabicUI()
        }
        
        fillAddress()
        if selectedControllerAddEdit == .edit{
            fillEditAddress()
        }
        
    }
    
    func fillAddress(){
        self.txtCountry.text =  selectedAddress.country
//        let lines = selectedAddress.lines
//        self.txtAddressName.text = (lines?.joined(separator: ",") ?? "")
        self.txtGovernorate.text = selectedAddress.administrativeArea
        self.txtCity.text = selectedAddress.locality
        self.txtPostalCode.text = selectedAddress.postalCode
        self.txtStreet.text = selectedAddress.thoroughfare
    }
    
    func fillEditAddress(){
        if selectedAddress.coordinate.latitude != dictAddress["latitude"].doubleValue  &&  selectedAddress.coordinate.longitude != dictAddress["longitude"].doubleValue {
            self.txtCountry.text =  selectedAddress.country
            //        let lines = selectedAddress.lines
            //        self.txtAddressName.text = (lines?.joined(separator: ",") ?? "")
            self.txtGovernorate.text = selectedAddress.administrativeArea
            self.txtCity.text = selectedAddress.locality
            self.txtPostalCode.text = selectedAddress.postalCode
            self.txtStreet.text = selectedAddress.thoroughfare
            //        self.txtCountry.text =  dictAddress["country"].stringValue
            self.txtAddressName.text = dictAddress["address_name"].stringValue
            //        self.txtGovernorate.text = dictAddress["governorate"].stringValue
            //        self.txtCity.text = dictAddress["city"].stringValue
            //        self.txtPostalCode.text = dictAddress["postal_code"].stringValue
            //        self.txtStreet.text = dictAddress["street"].stringValue
        } else {
            /*self.txtCountry.text =  selectedAddress.country
             //        let lines = selectedAddress.lines
             //        self.txtAddressName.text = (lines?.joined(separator: ",") ?? "")
             self.txtGovernorate.text = selectedAddress.administrativeArea
             self.txtCity.text = selectedAddress.locality
             self.txtPostalCode.text = selectedAddress.postalCode
             self.txtStreet.text = selectedAddress.thoroughfare*/
            self.txtCountry.text =  dictAddress["country"].stringValue
            self.txtAddressName.text = dictAddress["address_name"].stringValue
            self.txtGovernorate.text = dictAddress["governorate"].stringValue
            self.txtCity.text = dictAddress["city"].stringValue
            self.txtPostalCode.text = dictAddress["postal_code"].stringValue
            self.txtStreet.text = dictAddress["street"].stringValue
        }
        
        self.txtPhoneNumber.text = dictAddress["phone_number"].stringValue
        self.txtBloack.text = dictAddress["block"].stringValue
        self.txtAvenu.text = dictAddress["avenue"].stringValue
        self.txtBuilding.text = dictAddress["building"].stringValue
        self.txtAvenu.text =  dictAddress["avenue"].stringValue
        self.txtvwExtraNotes.text = dictAddress["notes"].stringValue
        self.txtFloor.text = dictAddress["floor"].stringValue
        self.switchDefaultAddress.isOn = dictAddress["is_default"].stringValue == "0" ? false : true
        print("dictAddress:", dictAddress)
        
    }
    
    func fillupGeustAddressDetail(dict:JSON) {
        self.txtCountry.text =  dict["country"].stringValue
        self.txtAddressName.text = dict["address_name"].stringValue
        self.txtGovernorate.text = dict["governorate"].stringValue
        self.txtCity.text = dict["city"].stringValue
        self.txtPostalCode.text = dict["postal_code"].stringValue
        self.txtStreet.text = dict["street"].stringValue
        self.txtPhoneNumber.text = dict["phone_number"].stringValue
        self.txtBloack.text = dict["block"].stringValue
        self.txtAvenu.text = dict["avenue"].stringValue
        self.txtBuilding.text = dict["building"].stringValue
        self.txtAvenu.text =  dict["avenue"].stringValue
        self.txtvwExtraNotes.text = dict["notes"].stringValue
        self.txtFloor.text = dict["floor"].stringValue
    }
    
    func setUpArabicUI()
    {
        [txtFullName,txtEmail,txtCountry,txtPhoneNumber,txtAddressName,txtGovernorate,txtCity,txtPostalCode,txtBloack,txtStreet,txtAvenu,txtBuilding,txtFloor].forEach { (txt) in
            
            txt?.textAlignment = .right
        }
        
        
        [lblFullname,lblEmail,lblCountry,lblPhoneNumber,lblAddressName,lblGovernorate,lblCity,lblPostalCode,lblBlock,lblStreet,lblAvenu,lblBulding,lblFloor,lblExtraNotes].forEach { (lbl) in
            
            lbl?.textAlignment = .right
            
        }
        
        txtvwExtraNotes.textAlignment = .right
        
        vwDefaultAddress.transform = CGAffineTransform(scaleX: -1, y: 1)
        lblSetasDefaultAddress.transform = CGAffineTransform(scaleX: -1, y: 1)
        lblSetasDefaultAddress.textAlignment = .right
        
    }
    
    
    @IBAction func btnContinueTapped(_ sender: UIButton) {
        if selectedContollerForAddDetails == .addNewAddress
        {
            if(self.txtCountry.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                makeToast(strMessage: getCommonString(key: "Please_enter_country_name_key"))
            } else {
                /*if txtPhoneNumber.text!.isEmpty {
                   if(self.txtAddressName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                       makeToast(strMessage: getCommonString(key: "Please_enter_address_key"))
                   }else if(self.txtGovernorate.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                       makeToast(strMessage: getCommonString(key: "Please_enter_governorate_name_key"))
                   } else if(self.txtCity.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                       makeToast(strMessage: getCommonString(key: "Please_enter_city_name_key"))
                   }
                   else if(self.txtBloack.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                       makeToast(strMessage: getCommonString(key: "Please_enter_block_name_key"))
                   } else if(self.txtStreet.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                       makeToast(strMessage: getCommonString(key: "Please_enter_street_key"))
                   }
                   else if(self.txtBuilding.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                       makeToast(strMessage: getCommonString(key: "Please_enter_building_key"))
                   }
//                   else if(self.txtFloor.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
//                       makeToast(strMessage: getCommonString(key: "Please_enter_floor_key"))
//                   }
                   else {
                       getData()
                       addAddress()
                   }
                } else {*/
                if txtPhoneNumber.text!.isEmpty {
                    makeToast(strMessage: getCommonString(key: "Please_enter_mobile_number_key"))
                }
                
                    else if txtPhoneNumber.text!.count < 8 {
                            makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
                    } else if txtPhoneNumber.text?.isNumeric == false {
                        makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
                    } else {
                        if(self.txtAddressName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_address_key"))
                        }/*else if(self.txtGovernorate.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_governorate_name_key"))
                        }*/ else if(self.txtCity.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_city_name_key"))
                        }
                        else if(self.txtBloack.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_block_name_key"))
                        } else if(self.txtStreet.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_street_key"))
                        }
                        else if(self.txtBuilding.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_building_key"))
                        }
//                        else if(self.txtFloor.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
//                            makeToast(strMessage: getCommonString(key: "Please_enter_floor_key"))
//                        }
                        else {
                            getData()
                            addAddress()
                        }
                    }
//                }
            }
        }
        else if selectedContollerForAddDetails == .addGuestUserDetails {
            if(self.txtFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                makeToast(strMessage: getCommonString(key: "Please_enter_full_name_key"))
            }
            else if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty{
                makeToast(strMessage: getCommonString(key: "Please_enter_email_key"))
            }
            else if !(isValidEmail(emailAddressString: txtEmail.text!)){
                makeToast(strMessage: getCommonString(key: "Please_enter_valid_email_key"))
            }
            else if(self.txtCountry.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                makeToast(strMessage: getCommonString(key: "Please_enter_country_name_key"))
            } else {
                /*if txtPhoneNumber.text!.isEmpty {
                    if(self.txtAddressName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                        makeToast(strMessage: getCommonString(key: "Please_enter_address_key"))
                    }
                    else if(self.txtGovernorate.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                        makeToast(strMessage: getCommonString(key: "Please_enter_governorate_name_key"))
                    }
                    else if(self.txtCity.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                        makeToast(strMessage: getCommonString(key: "Please_enter_city_name_key"))
                    }
                    else if(self.txtBloack.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                        makeToast(strMessage: getCommonString(key: "Please_enter_block_name_key"))
                    }
                    else if(self.txtStreet.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                        makeToast(strMessage: getCommonString(key: "Please_enter_street_key"))
                    }
                    else if(self.txtBuilding.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                        makeToast(strMessage: getCommonString(key: "Please_enter_building_key"))
                    }
//                    else if(self.txtFloor.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
//                        makeToast(strMessage: getCommonString(key: "Please_enter_floor_key"))
//                    }
                    else {
                        guestAddAddress()
                    }
                } else {*/
                if txtPhoneNumber.text!.isEmpty {
                    makeToast(strMessage: getCommonString(key: "Please_enter_mobile_number_key"))
                }
                    if txtPhoneNumber.text!.count < 8 {
                            makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
                    } else if txtPhoneNumber.text?.isNumeric == false {
                        makeToast(strMessage: getCommonString(key: "Please_enter_valid_mobile_number_key"))
                    } else {
                        if(self.txtAddressName.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_address_key"))
                        }
                        /*else if(self.txtGovernorate.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_governorate_name_key"))
                        }*/
                        else if(self.txtCity.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_city_name_key"))
                        }
                        else if(self.txtBloack.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_block_name_key"))
                        }
                        else if(self.txtStreet.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_street_key"))
                        }
                        else if(self.txtBuilding.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
                            makeToast(strMessage: getCommonString(key: "Please_enter_building_key"))
                        }
//                        else if(self.txtFloor.text?.trimmingCharacters(in: .whitespacesAndNewlines))!.isEmpty {
//                            makeToast(strMessage: getCommonString(key: "Please_enter_floor_key"))
//                        }
                        else {
                            guestAddAddress()
                        }
                //    }
                }
            }
        }
        
    }
    
    func getData() {
        if getUserDetail("is_login_guest") == "1" {
            dictAddress["email"].stringValue = txtEmail.text ?? ""
            dictAddress["country"].stringValue = self.txtCountry.text ?? ""
            dictAddress["address_name"].stringValue = self.txtAddressName.text ?? ""
            dictAddress["governorate"].stringValue = self.txtGovernorate.text ?? ""
            dictAddress["city"].stringValue = self.txtCity.text ?? ""
            dictAddress["postal_code"].stringValue = self.txtPostalCode.text ?? ""
            dictAddress["street"].stringValue = self.txtStreet.text ?? ""
            dictAddress["phone_number"].stringValue = self.txtPhoneNumber.text ?? ""
            dictAddress["block"].stringValue = self.txtBloack.text ?? ""
            dictAddress["avenue"].stringValue = self.txtAvenu.text ?? ""
            dictAddress["building"].stringValue = self.txtBuilding.text ?? ""
            dictAddress["avenue"].stringValue = self.txtAvenu.text ?? ""
            dictAddress["notes"].stringValue = self.txtvwExtraNotes.text ?? ""
            dictAddress["floor"].stringValue = self.txtFloor.text ?? ""
            dictAddress["address_id"] = "0"
            dictAddress["is_default"].stringValue = "0"
        }
    }
    
}

//MARK: - TextField Delegate

extension AddNewAddressVc:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                         replacementString string: String) -> Bool
      {
          if textField == txtPhoneNumber {
              let allowedCharacters = CharacterSet(charactersIn:"+0123456789")
              let characterSet = CharacterSet(charactersIn: string)
                      
              let currentString: NSString = textField.text! as NSString
              let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
              return allowedCharacters.isSuperset(of: characterSet) && newString.length <= GlobalVariables.mobileLegth
          }
          return true
      }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtGovernorate {
            var arrGovernorate = [String]()
            arrGovernorate.append(getCommonString(key: "Hawalli_key"))
            arrGovernorate.append(getCommonString(key: "Farwaniyah_key"))
            arrGovernorate.append(getCommonString(key: "Mubarak_Al_Kabeer_key"))
            arrGovernorate.append(getCommonString(key: "Ahmadi_key"))
            arrGovernorate.append(getCommonString(key: "Jahra_key"))
            governorateDD.dataSource = arrGovernorate
            governorateDD.direction = .bottom
            governorateDD.show()
            governorateDD.selectionAction = { (index: Int, item: String) in
                self.txtGovernorate.text = item
            }
            
            return false
        }
        return true
    }
}

//MARK: - TextView Delegate

extension AddNewAddressVc:UITextViewDelegate
{
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

}

//MARK:- Service

extension AddNewAddressVc {
    
    func addAddress()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlAddAddress)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param =
                [
                    "lang" : lang,
                    "user_id" : getUserDetail("user_id"),
                    "access_token" : getUserDetail("access_token"),
                    "country" : txtCountry.text ?? "",
                    "phone_number" : txtPhoneNumber.text ?? "",
                    "address_name" : txtAddressName.text ?? "",
                    "governorate" : txtGovernorate.text ?? "",
                    "city" : txtCity.text ?? "",
                    "postal_code" : txtPostalCode.text ?? "",
                    "block" : txtBloack.text ?? "",
                    "street" : txtStreet.text ?? "",
                    "avenue" : txtAvenu.text ?? "",
                    "building" : txtBuilding.text ?? "",
                    "floor" : txtFloor.text ?? "",
                    "is_default" : switchDefaultAddress.isOn == true ? "1" : "0" ,
                    "latitude" : "\(selectedAddress.coordinate.latitude)",
                    "longitude" : "\(selectedAddress.coordinate.longitude)",
                    "notes": txtvwExtraNotes.text ?? ""
            ]
            
            let str = selectedControllerAddEdit.rawValue
            
            if selectedControllerAddEdit == .edit
            {
                param["address_id"] = dictAddress["address_id"].stringValue // 0 for add -- 1 for update
            }
            else
            {
                param["address_id"] = "0" // 0 for add -- 1 for update
            }
            
            if getUserDetail("is_login_guest") == "1" {
                param["is_default"] = "0"
            }
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        self.handlerAddnewAddress(data)
                        makeToast(strMessage: json["msg"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        if getUserDetail("is_login_guest") == "1" {
                            self.handlerAddnewAddress(self.dictAddress)
                            guestDictAddress = self.dictAddress
                            self.navigationController?.popViewController(animated: false)
                        }
                        else {
                            makeToast(strMessage: json["msg"].stringValue)
                        }
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func guestAddAddress() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(urlGuestAddAddress)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = [
                "lang" : lang,
                "full_name" : txtFullName.text ?? "",
                "country" : txtCountry.text ?? "",
                "email" : txtEmail.text ?? "",
                "phone_no" : txtPhoneNumber.text ?? "",
                "address_name" : txtAddressName.text ?? "",
                "governorate" : txtGovernorate.text ?? "",
                "city" : txtCity.text ?? "",
                "postal_code" : txtPostalCode.text ?? "",
                "block" : txtBloack.text ?? "",
                "street" : txtStreet.text ?? "",
                "avenue" : txtAvenu.text ?? "",
                "building" : txtBuilding.text ?? "",
                "floor" : txtFloor.text ?? "",
                "latitude" : "\(selectedAddress.coordinate.latitude)",
                "longitude" : "\(selectedAddress.coordinate.longitude)",
                "notes": txtvwExtraNotes.text ?? "",
                "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                "register_id" : Messaging.messaging().fcmToken ?? "",
                "device_type" : GlobalVariables.strDeviceType
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let data = json["data"]
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        self.guestUpdateUserToCart()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        //                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func guestUpdateUserToCart() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(urlUpdateUserToCart)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = [
                "lang" : lang,
                "user_id" : getUserDetail("user_id"),
                "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                "access_token" : getUserDetail("access_token"),
                "is_login_guest" : getUserDetail("is_login_guest")
            ]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
//                        let data = json["data"]
//
//                        guard let rowdata = try? data.rawData() else {return}
//                        Defaults.setValue(rowdata, forKey: "userDetails")
//                        Defaults.synchronize()
                        
                        let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "CheckoutVc") as! CheckoutVc
                        self.getData()
                        obj.dictAddress = self.dictAddress
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        //                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func getGuestAddress() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(kUserGuestAddress)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = [
                "lang" : lang,
                "is_login_guest":getUserDetail("is_login_guest"),
                "access_token":getUserDetail("access_token"),
                "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                "register_id" : Messaging.messaging().fcmToken ?? "",
                "device_type" : GlobalVariables.strDeviceType,
                "user_id" : getUserDetail("user_id")
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let data = json["data"]
                        
                        self.fillupGeustAddressDetail(dict: data["address"])
                       
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        //                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
}

