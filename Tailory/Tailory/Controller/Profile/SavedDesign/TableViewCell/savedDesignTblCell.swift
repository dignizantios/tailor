//
//  savedDesignTblCell.swift
//  Tailory
//
//  Created by Haresh on 20/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class savedDesignTblCell: UITableViewCell {
    
    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDesignName: UILabel!
    
    @IBOutlet weak var tblInnerCustomDesign: UITableView!
    @IBOutlet weak var contrainTblInnerCustomDesignHeight: NSLayoutConstraint!

    @IBOutlet weak var lblCollarStyle: UILabel!
    @IBOutlet weak var lblCuffs: UILabel!
    @IBOutlet weak var lblSideLine: UILabel!
    
    @IBOutlet weak var btnEdit: CustomButton!
    @IBOutlet weak var btnDelete: CustomButton!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        img.layer.cornerRadius = 5.0
        img.layer.masksToBounds = true
        
        lblDesignName.font = themeFont(size: 16, fontname: .medium)
        lblDesignName.textColor = UIColor.appThemeBlackColor
       
        /*
        [lblCollarStyle,lblCuffs,lblSideLine].forEach { (lbl) in
            lbl?.font = themeFont(size: 13, fontname: .light)
        }
        */
        [btnEdit,btnDelete].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .medium)
            btn?.borderColor = UIColor.appThemeSilverColor
            btn?.borderWidth = 1.0
            btn?.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
            btn?.cornerRadius = 4
        }
        
        btnEdit.setTitle(getCommonString(key: "Edit_key"), for: .normal)
        btnDelete.setTitle(getCommonString(key: "Delete_key"), for: .normal)
        
         tblInnerCustomDesign.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        if !isEnglish
        {
            self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.img.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [btnEdit,btnDelete].forEach { (btn) in
                btn?.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            lblDesignName.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblDesignName.textAlignment = .right
            
            /*
            [lblCollarStyle,lblCuffs,lblSideLine,lblDesignName].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }*/
            
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Overide Method
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblInnerCustomDesign.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            if tblInnerCustomDesign.contentSize.height <= 68
            {
                self.contrainTblInnerCustomDesignHeight.constant = 68
            }
            else
            {
                self.contrainTblInnerCustomDesignHeight.constant = tblInnerCustomDesign.contentSize.height
            }
        }
    }

    
}
