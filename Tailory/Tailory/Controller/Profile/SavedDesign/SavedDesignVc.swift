//
//  SavedDesignVc.swift
//  Tailory
//
//  Created by Haresh on 20/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire

class SavedDesignVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblSavedDesign: UITableView!
    
    //MARK: - Variable
    
    var intOffset = 0
    var arrayDesign : [JSON] = []
    var upperReferesh = UIRefreshControl()
    var strMessage = ""
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Saved_desings_key"))
    }
    
    override func viewDidLayoutSubviews() {
        self.tblSavedDesign.layoutSubviews()
        self.tblSavedDesign.reloadData()
    }
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        self.tblSavedDesign.register(UINib(nibName: "savedDesignTblCell", bundle: nil), forCellReuseIdentifier: "savedDesignTblCell")
        self.tblSavedDesign.tableFooterView = UIView()
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblSavedDesign.addSubview(upperReferesh)
        
        listOfSavedDesignAPI()
        
    }
    
}


//MARK: - TableView delegate Method

extension SavedDesignVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblSavedDesign
        {
            if arrayDesign.count == 0
            {
                let lbl = UILabel()
                lbl.text = getCommonString(key: "You_have_not_saved_any_designs_key")
//                lbl.text = strMessage
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeBlackColor
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            tableView.backgroundView = nil
            return self.arrayDesign.count
        }
        
       return self.arrayDesign[tableView.tag]["customize_design"].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblSavedDesign
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "savedDesignTblCell") as! savedDesignTblCell
            
            var dict = self.arrayDesign[indexPath.row]
            
            //cell.lblDesignName.text = dict["design_name"].stringValue
            cell.lblDesignName.text = dict["title"].stringValue
            
            cell.img.sd_setShowActivityIndicatorView(true)
            cell.img.sd_setIndicatorStyle(.gray)
            
            cell.img.sd_setImage(with: dict["design_image"].url, placeholderImage: UIImage(named: "ic_tailors_splash_holder"), options: .lowPriority, completed: nil)
            
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteAction(_:)), for: .touchUpInside)
            cell.btnEdit.addTarget(self, action: #selector(btnEditAction(_:)), for: .touchUpInside)
            
            cell.tblInnerCustomDesign.tag = indexPath.row
            cell.tblInnerCustomDesign.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
            cell.tblInnerCustomDesign.delegate = self
            cell.tblInnerCustomDesign.dataSource = self
            cell.tblInnerCustomDesign.reloadData()
            
            cell.tblInnerCustomDesign.layoutIfNeeded()
            cell.tblInnerCustomDesign.layoutSubviews()
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InnerCustomDesignTblCell") as! InnerCustomDesignTblCell
        
        let dict = self.arrayDesign[tableView.tag]["customize_design"][indexPath.row]
//        if isEnglish {
             cell.lblCustomizeDesignTitle.text = dict["style_name"].stringValue + ": "
        /*} else {
            cell.lblCustomizeDesignTitle.text = " : " + dict["style_name"].stringValue 
        }*/
        cell.lblCustomizeDesignValue.text = dict["design_name"].stringValue 

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .gray
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
}

//MARK:- Action Zonr

extension SavedDesignVc {
    @objc func btnDeleteAction(_ sender:UIButton) {
        let dict = arrayDesign[sender.tag]
        print("dict \(dict)")
        setupRemoveAlertController(dict: dict)
    }
    
    @objc func btnEditAction(_ sender:UIButton) {
        let dict = arrayDesign[sender.tag]
        print("dict \(dict)")
        let obj = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
        obj.selectedController = .editCart
        obj.dictCustomize = dict
        obj.handlerUpdateCart = {[weak self] in
            self?.upperRefreshTable()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func setupRemoveAlertController(dict:JSON){
        let alertController = UIAlertController(title: getCommonString(key: "Tailors_key"), message: getCommonString(key: "Are_you_sure_you_want_to_remove_this_item_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.deleteSavedDesign(dict: dict)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}

//MARK: - API calling

extension SavedDesignVc
{
    
    @objc func upperRefreshTable()
    {
        intOffset = 0
        listOfSavedDesignAPI(isShowLoader:false)
    }
    
    func listOfSavedDesignAPI(isShowLoader:Bool = true)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlUser)\(urlMyDesignList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "offset" : String(self.intOffset),
                          "tailor_user_id" : "0"
            ]
            
            
            print("param : \(param)")
            
            if isShowLoader == true {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrayDesign = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblSavedDesign.reloadData()
                            return
                        }
                        
                        if self.intOffset == 0
                        {
                            self.arrayDesign = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrayDesign + aryData
                        
                        self.arrayDesign = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayDesign = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblSavedDesign.reloadData()
                    self.viewDidLayoutSubviews()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func deleteSavedDesign(dict:JSON)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlRemoveMyDesign)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "tbl_trailor_design_id" : dict["tbl_trailor_design_id"].stringValue,
                          "tailor_user_id" : dict["tailor_user_id"].stringValue
            ]
            
            print("param : \(param)")
    
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.upperRefreshTable()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblSavedDesign.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblSavedDesign.contentSize.height).rounded(toPlaces: 2)) - (Double(tblSavedDesign.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                listOfSavedDesignAPI()
            }
        }
    }
}
