//
//  SettingsVc.swift
//  Tailory
//
//  Created by Haresh on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SettingsVc: UIViewController {

    //MARK: - Outlet
    @IBOutlet weak var tblSettings: UITableView!
    
    //MARK: - Variable
    var arraySettings : [JSON] = []
    var notificationOnOff = String()
    var userData = JSON()
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        getUserDetailsData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Settings_key"))
    }
    
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        self.tblSettings.register(UINib(nibName: "ProfileTblCell", bundle: nil), forCellReuseIdentifier: "ProfileTblCell")
        
        self.tblSettings.tableFooterView = UIView()
        
        setArraySettings()
        
        getUserDetailsData()
        
    }
    
    func setArraySettings()
    {
        
        var dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Change_language_key")
        self.arraySettings.append(dict)
        
        if getUserDetail("is_login_guest") as? String == "1" {
            
        }
        else {
            dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Notification_settings_key")
            self.arraySettings.append(dict)
            
            dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Notifications_key")
            self.arraySettings.append(dict)
        }
        
        self.tblSettings.reloadData()
    }
    
}

//MARK: - TableView delegate Method

extension SettingsVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arraySettings.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTblCell") as! ProfileTblCell
        
        cell.lblName.text = self.arraySettings[indexPath.row]["name"].stringValue
        
        if indexPath.row == 1 {
            cell.btnSwitch.isHidden = false
            cell.imgArrow.isHidden = true
            
            if getUserDetail("is_notification") == "0" {
                cell.btnSwitch.isOn = false
            }
            else {
                cell.btnSwitch.isOn = true
            }
            cell.btnSwitch.tag = indexPath.row
            cell.btnSwitch.addTarget(self, action: #selector(btnSwitchAction(_:)), for: .touchUpInside)
        }
        else {
            cell.btnSwitch.isHidden = true
            cell.imgArrow.isHidden = false
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        redirectToController(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    @objc func btnSwitchAction(_ sender: UIButton) {
        switchNotification(isNotificationOnOff: getUserDetail("is_notification") == "0" ? "1" : "0")
    }
}

//MARK: - Redirection

extension SettingsVc {
    
    func redirectToController(index:Int) {
        
        if index == 0 {
            //Change language
            
            let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            vc.selectedController = .fromProfileSection
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if index == 1 {
            //notification setting
            
        }
        else if index == 2 {
            //notification
            
            let vc = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "NotificationVc") as! NotificationVc
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        
    }
}

//MARK: Service
extension SettingsVc {
    
    func getUserDetailsData() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(urlUserDetails)"
            
            print("URL: \(url)")
            
            let param = [
                "user_id" : getUserDetail("user_id"),
                "access_token" : getUserDetail("access_token"),
                "lang" : lang
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let data = json["data"]
                        setValueUserDetail(forKey: "is_notification", forValue: data["is_notification"].stringValue)
                        self.tblSettings.reloadData()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func switchNotification(isNotificationOnOff:String) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlUser)\(urlSwitchNotification)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "is_notification" : isNotificationOnOff]
            
            print("param :\(param)")
            
            self.showLoader()
            CommonService().Service(url: url, param: param) { (respones) in
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        if getUserDetail("is_notification") == "0" {
                            setValueUserDetail(forKey: "is_notification", forValue: "1")
                        }
                        else {
                            setValueUserDetail(forKey: "is_notification", forValue: "0")
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                    self.tblSettings.reloadData()
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
}
