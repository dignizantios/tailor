//
//  TrackOrderVc.swift
//  Tailory
//
//  Created by Haresh on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class TrackOrderVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblTrackOrder: UITableView!
    
    //MARK: - Variable
    
    var arrayTrack : [JSON] = []
    var strOrderID = String()
    var strOrderItemId = String()
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        trackOrderAPICalling()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        setUpNavigationBarWhiteWithTitleAndBack(strTitle: "Order #232343")
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Order_status_key"))
    }
    
    //MARK: - Setup UI
    
    func setupUI() {
        
        self.tblTrackOrder.register(UINib(nibName: "TrackOrderTableCell", bundle: nil), forCellReuseIdentifier: "TrackOrderTableCell")
        self.tblTrackOrder.tableFooterView = UIView()
        setupArray()
    }
    
    
    func setupArray() {
        /*
        var dict = JSON()
        dict["status"].stringValue = "Recived"
        dict["address"].stringValue = "At Dasman store"
        dict["date"].stringValue = "12-03-2019"
        dict["time"].stringValue = "06:00 PM"
        self.arrayTrack.append(dict)
        
        dict = JSON()
        dict["status"].stringValue = "Preparing"
        dict["address"].stringValue = "At Dasman store"
        dict["date"].stringValue = "12-03-2019"
        dict["time"].stringValue = "06:33 PM"
        self.arrayTrack.append(dict)
        
        dict = JSON()
        dict["status"].stringValue = "Delivered"
        dict["address"].stringValue = "At Dasman store"
        dict["date"].stringValue = "12-03-2019"
        dict["time"].stringValue = "06:00 PM"
        self.arrayTrack.append(dict)
        
        self.tblTrackOrder.reloadData()
        */
    }
    
}

//MARK: - TableView delegate Method

extension TrackOrderVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayTrack.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackOrderTableCell") as! TrackOrderTableCell
        
        if indexPath.row == 0 {
            cell.vwUpperLine.isHidden = true
            cell.vwBottom.isHidden = false
//            cell.btnCenter.setBackgroundImage(UIImage(named: "ic_order_time_line_round_select"), for: .normal)
        }
        else if indexPath.row == self.arrayTrack.count - 1 {
            cell.vwUpperLine.isHidden = false
            cell.vwBottom.isHidden = true
//            cell.btnCenter.setBackgroundImage(UIImage(named: "ic_order_time_line_round_unselect"), for: .normal)
        }
//        else {
//            cell.btnCenter.setBackgroundImage(UIImage(named: "ic_order_time_line_round_select"), for: .normal)
//        }
        
        
        var dict = self.arrayTrack[indexPath.row]
        
        cell.lblStatus.text = dict["name"].stringValue
        
        if dict["check"].stringValue == "0" {
            cell.btnCenter.setBackgroundImage(UIImage(named: "ic_order_time_line_round_unselect"), for: .normal)
        }
        else {
            cell.btnCenter.setBackgroundImage(UIImage(named: "ic_order_time_line_round_select"), for: .normal)
        }
        cell.lblDate.text = dict["date"].stringValue
//        cell.lblTime.text = dict["time"].stringValue
        if dict["time"].stringValue != ""{
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm:ss"
            if let timeDate = formatter.date(from: dict["time"].stringValue){
                formatter.dateFormat = "hh:mm a"
                let myString = formatter.string(from: timeDate)
                cell.lblTime.text = myString
            } else {
                cell.lblTime.text = ""
            }
        } else {
            cell.lblTime.text = ""
        }
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
}


//MARK: Services
extension TrackOrderVc {
    
    func trackOrderAPICalling() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlOrder)\(urlTrackOrder)"
            
            print("URL: \(url)")
            
            let param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "lang" : lang,
                         "order_id" : self.strOrderID,
                         "order_item_id": self.strOrderItemId,
                         "timezone" : getCurrentTimeZone()]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
//                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayTrack = []
                        self.arrayTrack = json["data"].arrayValue
//                        print("self.arrayTrack:--",self.arrayTrack)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    self.tblTrackOrder.reloadData()
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
}

