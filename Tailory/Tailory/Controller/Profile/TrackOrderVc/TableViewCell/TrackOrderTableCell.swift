//
//  TrackOrderTableCell.swift
//  Tailory
//
//  Created by Haresh on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TrackOrderTableCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var vwUpperLine: UIView!
    @IBOutlet weak var btnCenter: UIButton!
    @IBOutlet weak var vwBottom: UIView!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAddressReceived: UILabel!
    
    //MARK: - Variable
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [vwUpperLine,vwBottom].forEach { (vw) in
            vw?.backgroundColor = UIColor.appThemeSilverColor
        }
        
        [lblStatus,lblTime].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        [lblDate,lblAddressReceived].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 12, fontname: .medium)
        }
        
        
        if !isEnglish
        {
            
            vwMain.transform = CGAffineTransform(scaleX: -1, y:1)
            [lblStatus,lblAddressReceived].forEach { (lbl) in
                lbl?.textAlignment = .right
                lbl?.transform = CGAffineTransform(scaleX: -1, y:1)
            }
            
            [lblDate,lblTime].forEach { (lbl) in
                lbl?.textAlignment = .left
                lbl?.transform = CGAffineTransform(scaleX: -1, y:1)
            }
            
        }
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
