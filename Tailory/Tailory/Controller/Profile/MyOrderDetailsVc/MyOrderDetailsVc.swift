//
//  MyOrderDetailsVc.swift
//  Tailory
//
//  Created by Haresh on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//


enum checkOrderStatusRedirection {
    case capture
    case success
    case proccessing
}


import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class MyOrderDetailsVc: UIViewController {

    //MARK - Outlet
    
    @IBOutlet weak var lblPaymentTimer: UILabel!
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var lblProducts: UILabel!
    @IBOutlet weak var tblProductList: UITableView!
    @IBOutlet weak var constantHeightTblProductList: NSLayoutConstraint!
    
    @IBOutlet weak var lblDeliveryAddress: UILabel!
    
    @IBOutlet weak var lblMainAddress: UILabel!
    @IBOutlet weak var lblSubAddress: UILabel!
    
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblOrderNumberValue: UILabel!
    
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblOrderStatusValue: UILabel!
    
    @IBOutlet weak var lblOrderedFrom: UILabel!
    @IBOutlet weak var lblOrderedFromValue: UILabel!
    
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderDateValue: UILabel!
    
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblSubtotalValue: UILabel!
    
    @IBOutlet weak var lblDeliveryCharge: UILabel!
    @IBOutlet weak var lblDeliveryChargeValue: UILabel!
    
    @IBOutlet weak var lblTotalPay: UILabel!
    @IBOutlet weak var lblTotalPayValue: UILabel!
    
    @IBOutlet var vwBottomButton: UIStackView!
    @IBOutlet weak var btnReorder: CustomButton!
    @IBOutlet var btnTrackOrder: CustomButton!
    @IBOutlet weak var vwLaundry: UIView!
    
    @IBOutlet weak var lblLandary: UILabel!
    @IBOutlet weak var lblLandaryValue: UILabel!
    
    //MARK: - variable
    
    var dicMyOrderData = JSON()
    var arrayProductList : [JSON] = []
    var selectedFrom = checkOrderStatusRedirection.success
    var strMessage =  String()
    var arrMyOrderDetail : [JSON] = []
    let Captured = "0"
    let Success = "1"
    let Processing = "2"
    var handlerReorderData: () -> Void = {}
    var dictOrderDetail = JSON()
    var dictAddressDetail = JSON()
    var timerPayment:Timer?
    var addOneHourDate = Date()
    
    //MARK - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.tblProductList.register(UINib(nibName: "ProductsTblcell", bundle: nil), forCellReuseIdentifier: "ProductsTblcell")
        
        self.tblProductList.register(UINib(nibName: "ProductsTblcell", bundle: nil), forCellReuseIdentifier: "ProductsTblcell")
        self.tblProductList.register(UINib(nibName: "PaymentTblCell", bundle: nil), forCellReuseIdentifier: "PaymentTblCell")
        self.tblProductList.register(UINib(nibName: "CartTailorTblCell", bundle: nil), forCellReuseIdentifier: "CartTailorTblCell")
        
        setupUI()
        orderDetailsAPICalling()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Order_details_key"))
        tblProductList.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        isOnOrderDetailScreen = true
        strOrderId = self.dicMyOrderData["id"].stringValue
        NotificationCenter.default.addObserver(self, selector: #selector(getOrderDetail), name: .didRefreshOrderDetailScreen, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tblProductList.removeObserver(self, forKeyPath: "contentSize")
        isOnOrderDetailScreen = false
        strOrderId = ""
        timerPayment?.invalidate()
        timerPayment = nil
        
    }
    
    @objc func getOrderDetail(notification:Notification){
        if let json = notification.object as? JSON{
            print("json \(json)")
            self.dicMyOrderData = json
            orderDetailsAPICalling()
        }
    }
    
    //MARK: - SetupUI
    
    func setupUI() {
        
        [lblProducts,lblDeliveryAddress,lblDetails].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        lblMainAddress.textColor = UIColor.appThemeBlackColor
        lblMainAddress.font = themeFont(size: 14, fontname: .roman)
        lblSubAddress.textColor = UIColor.appThemeLightGrayColor
        lblSubAddress.font = themeFont(size: 12, fontname: .roman)
        [lblOrderNumber,lblOrderStatus,lblOrderedFrom,lblOrderDate,lblSubtotal,lblDeliveryCharge,lblTotalPay,lblLandary].forEach { (lbl) in
            
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 14, fontname: .light)
            lbl?.textAlignment = .left
            
        }
        [lblOrderNumberValue,lblOrderStatusValue,lblOrderedFromValue,lblOrderDateValue,lblSubtotalValue,lblDeliveryChargeValue,lblTotalPayValue,lblLandaryValue].forEach { (lbl) in
            
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 14, fontname: .light)
            lbl?.textAlignment = .right
        }
        
        lblPaymentTimer.textColor = UIColor.appThemeCyprusColor
        lblPaymentTimer.textAlignment = .center
        lblPaymentTimer.font = themeFont(size: 15, fontname: .medium)
        
        [btnReorder,btnTrackOrder].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        }
        
        btnReorder.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        self.btnReorder.setTitle(getCommonString(key: "Reorder_key"), for: .normal)
    
        btnTrackOrder.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        self.btnTrackOrder.setTitle(getCommonString(key: "Retry_payment_key"), for: .normal)
        
        lblProducts.text = getCommonString(key: "Products_key")
        lblDeliveryAddress.text = getCommonString(key: "Delivery_address_key")
        lblDetails.text = getCommonString(key: "Details_key")
        
        lblOrderNumber.text = getCommonString(key: "Order_number_key")
        lblOrderStatus.text = getCommonString(key: "Order_status_key")
        lblOrderedFrom.text = getCommonString(key: "Ordered_from_key")
        lblOrderDate.text = getCommonString(key: "Order_date_key")
        lblSubtotal.text = getCommonString(key: "Sub_total_key")
        lblDeliveryCharge.text = getCommonString(key: "Delivery_charge_key")
        lblTotalPay.text = getCommonString(key: "Total_to_pay_key")
        lblLandary.text = getCommonString(key: "Laundry_key")
 
        if !isEnglish
        {
            self.setUpArabicUI()
        }
        
        [btnReorder,btnTrackOrder].forEach { (btn) in
            btn?.isHidden = true
        }
        
    }
    
    func setUpArabicUI()
    {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
    [lblOrderNumber,lblOrderStatus,lblOrderedFrom,lblOrderDate,lblSubtotal,lblDeliveryCharge,lblTotalPay,lblLandary].forEach { (lbl) in
                    lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        [lblOrderNumberValue,lblOrderStatusValue,lblOrderedFromValue,lblOrderDateValue,lblSubtotalValue,lblDeliveryChargeValue,lblTotalPayValue,lblLandaryValue].forEach { (lbl) in
            
            lbl?.textAlignment = .left
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        [lblMainAddress, lblSubAddress, lblDetails, lblDeliveryAddress].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
    }
    
    func setupAddress(dict:JSON) {
        dictAddressDetail = dict
         self.lblMainAddress.text = dict["address_name"].stringValue
         var strSubAddress = "\n\(getCommonString(key: "Floor_key")):\(dict["floor"].stringValue) \n\n"
         strSubAddress += "\(getCommonString(key: "Building_key")):\(dict["building"].stringValue) \n\n"
         strSubAddress += "\(getCommonString(key: "Avenu_key")):\(dict["avenue"].stringValue) \n\n"
         strSubAddress += "\(getCommonString(key: "Street_key")):\(dict["street"].stringValue) \n\n"
         strSubAddress += "\(getCommonString(key: "Block_key")):\(dict["block"].stringValue) \n\n"
         strSubAddress += "\(getCommonString(key: "Postal_code_key")):\(dict["postal_code"].stringValue) \n\n"
         strSubAddress += "\(getCommonString(key: "City_key")):\(dict["city"].stringValue) \n\n"
         strSubAddress += "\(getCommonString(key: "Governorate_key")):\(dict["governorate"].stringValue) \n\n"
         strSubAddress += "\(getCommonString(key: "Country_key")):\(dict["country"].stringValue) "
         self.lblSubAddress.text = strSubAddress
    }
    
    func setupAmountData(dict:JSON) {
        
        if dict["order_status"].stringValue == "9"  {
            lblOrderStatusValue.text = dict["status_name"].stringValue
            lblOrderStatusValue.textColor = UIColor.green
        }else {
            lblOrderStatusValue.textColor = UIColor(red: 112/255, green: 110/255, blue: 220/255, alpha: 1.0)
            lblOrderStatusValue.text = dict["status_name"].stringValue
        }
       /* else if dict["order_status"].stringValue == "0" {
            lblOrderStatusValue.textColor = UIColor(red: 112/255, green: 110/255, blue: 220/255, alpha: 1.0)
            lblOrderStatusValue.text = dict["status_name"].stringValue
        }*/
        
//        lblOrderStatusValue.textColor = UIColor(red: 112/255, green: 110/255, blue: 220/255, alpha: 1.0)
//        lblOrderStatusValue.text = self.dictOrderDetail["status_name"].stringValue
        self.lblOrderNumberValue.text = dict["order_id"].stringValue
//        self.lblOrderStatusValue.text = getCommonString(key: "")
        self.lblOrderedFromValue.text = dict["vendors_name"].stringValue
        self.lblOrderDateValue.text = dict["order_date"].stringValue
        
        self.lblSubtotalValue.text = "\(dict["sub_total_amount"].stringValue) \(getCommonString(key: "KD_key"))"
        self.lblDeliveryChargeValue.text = "\(dict["delivery_charge"].stringValue) \(getCommonString(key: "KD_key"))"
        self.lblTotalPayValue.text = "\(dict["total_amount"].stringValue) \(getCommonString(key: "KD_key"))"
        self.lblLandaryValue.text = dict["laundry"].stringValue + " \(getCommonString(key: "KD_key"))"
        
        if dict["laundry"].stringValue == "0" || dict["laundry"].stringValue == "" || dict["laundry"].stringValue == "0.0"{
            self.vwLaundry.isHidden = true
        }
        
        if dict["order_status"].stringValue == "9" {
            self.btnReorder.isHidden = false
        }
        else {
            self.btnReorder.isHidden = true
        }
        
        if dict["repayment_status"].stringValue == "1" {
            self.btnTrackOrder.isHidden = false
            self.lblPaymentTimer.isHidden = false
        }
        else {
            self.btnTrackOrder.isHidden = true
            self.lblPaymentTimer.isHidden = true
        }
        
        if dict["repayment_status"].stringValue == "1" {
            let expiryDate = stringTodate(Formatter: "yyyy-MM-dd HH:mm:ss", strDate: dict["order_date_time"].stringValue)
            
            let calendar = Calendar.current
            addOneHourDate = calendar.date(byAdding: .hour, value: 100, to: expiryDate)!
            
            if addOneHourDate > Date() {
                timerPayment = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(visibleRetryButton), userInfo: nil, repeats: true)
            } else {
                btnTrackOrder.isHidden = true
                lblPaymentTimer.isHidden = true
            }
        }
    }
    
    //MARK:- Observe Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblProductList.contentSize.height)")
            self.constantHeightTblProductList.constant = tblProductList.contentSize.height
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    @objc func visibleRetryButton() {
        let timeInterval = addOneHourDate.timeIntervalSince(Date())
        if timeInterval <= 0{
            handlerReorderData()
            orderDetailsAPICalling()
            timerPayment?.invalidate()
            timerPayment = nil
            btnTrackOrder.isHidden = true
            lblPaymentTimer.isHidden = true
        } else {
            btnTrackOrder.isHidden = false
            self.lblPaymentTimer.text = "\(timeString(time: timeInterval)) \(getCommonString(key: "remains_to_retry_payment_key"))"
        }
    }
}

//MARK: - IBAction

extension MyOrderDetailsVc {
    
    @IBAction func btnReorderTapped(_ sender: UIButton) {
        
        let OrderID = self.dicMyOrderData["id"].stringValue
        reorderAPICalling(orderId: OrderID)
        
//        if self.dicMyOrderData["order_status"].stringValue == Success {
//            self.reorderAPICalling()
//        }
        
        /*
        if selectedFrom == .capture {
            
        }
        else if selectedFrom == .success {
            //Redirect to card for Reorder
            self.reorderAPICalling()
        }
        else if selectedFrom == .proccessing {
            //Redirect to track order
            
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "TrackOrderVc") as! TrackOrderVc
            
            self.navigationController?.pushViewController(obj, animated: true)
        }
         */
    }
    
    
    @IBAction func btnTrackOrderAction(_ sender: Any) {
        print("order_id:", self.dicMyOrderData)
        getPaymentGateway()
    }

}

//MARK: - TableView delegate DataSource
extension MyOrderDetailsVc : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == self.tblProductList {
            return self.arrMyOrderDetail.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tblProductList {
            return self.arrMyOrderDetail[section]["data"].count
        }
        
        if (self.arrMyOrderDetail[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Tailors_key")) {
            return self.arrMyOrderDetail[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["customize_design"].arrayValue.count
        }
        else if (self.arrMyOrderDetail[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Accessories_key")) {
            return self.arrMyOrderDetail[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["options"].arrayValue.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        if tableView == self.tblProductList {
            
            if (self.arrMyOrderDetail[indexPath.section]["type"].stringValue == getCommonString(key: "Accessories_key")) {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTblcell", for: indexPath) as! ProductsTblcell
                let dict = self.arrMyOrderDetail[indexPath.section]["data"][indexPath.row]
                print("Data:", dict)
                
                if dictOrderDetail["order_status"].stringValue == "9" || dictOrderDetail["repayment_status"].stringValue == "1" || dictOrderDetail["repayment_status"].stringValue == "3"{
                    cell.btnReorder.isHidden = true
                    cell.btnTrackOrder.isHidden = true
                }
                else {
                    cell.btnReorder.isHidden = true
                    cell.btnTrackOrder.isHidden = false
                }
                
                [cell.btnPlus, cell.btnMinus].forEach { (btn) in
                    btn?.isHidden = true
                }
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductQty.font = themeFont(size: 13, fontname: .medium)
                cell.viewFabrics.isHidden = true
                
                let innerArray = self.arrMyOrderDetail[indexPath.section]["data"].arrayValue
                
                if indexPath.row == innerArray.count - 1 {
                    self.tblProductList.separatorStyle = .none
                }
                else {
                    self.tblProductList.separatorStyle = .singleLine
                }
                
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductName.text = dict["product_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblPrice.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                cell.imgProduct.sd_setImage(with: dict["product_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                cell.vwCombineDelivery.isHidden = true
                cell.tblInnerDesign.isHidden = false
                cell.lblFabQuantity.isHidden = true
                
                cell.tblInnerDesign.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                
                cell.btnTrackOrder.tag = indexPath.row
                cell.btnTrackOrder.addTarget(self, action: #selector(btnProductOrderTrackedAction(_:)), for: .touchUpInside)
                
                cell.btnReorder.tag = indexPath.row
                cell.btnReorder.addTarget(self, action: #selector(btnReorderAction(_:)), for: .touchUpInside)
                
                return cell
            }
            else if self.arrMyOrderDetail[indexPath.section]["type"].stringValue == getCommonString(key: "Fabrics_key") {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTblcell", for: indexPath) as! ProductsTblcell
                let dict = self.arrMyOrderDetail[indexPath.section]["data"][indexPath.row]
                print("Data:", dict)
                
                if dictOrderDetail["order_status"].stringValue == "9" || dictOrderDetail["repayment_status"].stringValue == "1" || dictOrderDetail["repayment_status"].stringValue == "3"  {
                    cell.btnReorder.isHidden = true
                    cell.btnTrackOrder.isHidden = true
                }
                else {
                    cell.btnReorder.isHidden = true
                    cell.btnTrackOrder.isHidden = false
                }
                
                [cell.btnPlus, cell.btnMinus].forEach { (btn) in
                    btn?.isHidden = true
                }
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductQty.font = themeFont(size: 13, fontname: .medium)
                cell.viewFabrics.isHidden = true
                
                let innerArray = self.arrMyOrderDetail[indexPath.section]["data"].arrayValue
                
                if indexPath.row == innerArray.count - 1 {
                    self.tblProductList.separatorStyle = .none
                }
                else {
                    self.tblProductList.separatorStyle = .singleLine
                }
                
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductName.text = dict["fabricdesign_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblPrice.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                cell.imgProduct.sd_setImage(with: dict["fabric_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                cell.vwCombineDelivery.isHidden = true
                cell.tblInnerDesign.isHidden = false
                cell.lblFabQuantity.isHidden = true
                
                cell.tblInnerDesign.isHidden = true
                cell.constantTblInnerHeight.constant = 0
                cell.lblFabQuantity.isHidden = false
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                
                cell.lblFabQuantity.textColor = UIColor.appThemeLightGrayColor
                cell.lblFabQuantity.text = "\(dict["quantity"].stringValue) \(getCommonString(key: "Meter_key"))"
                
                cell.btnTrackOrder.tag = indexPath.row
                cell.btnTrackOrder.addTarget(self, action: #selector(btnFabricOrderTrackedAction(_:)), for: .touchUpInside)
                
                cell.btnReorder.tag = indexPath.row
                cell.btnReorder.addTarget(self, action: #selector(btnReorderAction(_:)), for: .touchUpInside)
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartTailorTblCell") as! CartTailorTblCell
                
                let dict = self.arrMyOrderDetail[indexPath.section]["data"][indexPath.row]
                print("Data:", dict)
                
                if dictOrderDetail["order_status"].stringValue == "9" || dictOrderDetail["repayment_status"].stringValue == "1" || dictOrderDetail["repayment_status"].stringValue == "3" {
                    cell.btnReorder.isHidden = true
                    cell.btnTrackOrder.isHidden = true
                }
                else {
                    cell.btnReorder.isHidden = true
                    cell.btnTrackOrder.isHidden = false
                }
                
                let innerArray = self.arrMyOrderDetail[indexPath.section]["data"].arrayValue
                
                if indexPath.row == innerArray.count - 1{
                    self.tblProductList.separatorStyle = .singleLine
                } else {
                    self.tblProductList.separatorStyle = .none
                }
                
                [cell.btnPlus, cell.btnMinus].forEach { (btn) in
                    btn?.isHidden = true
                }
                
                cell.img.sd_setImage(with: dict["design_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                cell.lblDesignName.text = dict["design_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblPriceValue.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                cell.vwEdit.isHidden = true
                cell.vwEditHeight.constant = 0
                cell.vwLaundry.isHidden = true
                cell.vwCombineDelivery.isHidden = true
                
                cell.btnLaundryOutlet.isSelected = dict["is_laundry"].stringValue == "1" ? true : false
                cell.btnLaundryOutlet.tag = indexPath.row
                cell.btnLaundryOutlet.accessibilityValue = "\(indexPath.section)"
                
                cell.tblInnerDesign.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblInnerDesign.isHidden = false
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                
                cell.btnTrackOrder.tag = indexPath.row
                cell.btnTrackOrder.addTarget(self, action: #selector(btnOrderTrackedAction(_:)), for: .touchUpInside)
                
                cell.btnReorder.tag = indexPath.row
                cell.btnReorder.addTarget(self, action: #selector(btnReorderAction(_:)), for: .touchUpInside)
                
                //For fabric hidden
                if dict["is_fabric_added"].stringValue == "1"{
                    cell.vwFabric.isHidden = false
                    let dictFabric = dict["fabrics"][0]
                    cell.lblFabricName.text = dictFabric["fabricdesign_name"].stringValue
                    cell.lblMeterValue.text = "\(dictFabric["quantity"].stringValue) \(getCommonString(key: "Meter_key"))"
                    cell.lblFabricPriceValue.text = dictFabric["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                    cell.imgFabric.sd_setImage(with: dictFabric["fabric_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                }
                else {
                    cell.vwFabric.isHidden = true
                }
                
                return cell
            }
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "InnerCustomDesignTblCell") as! InnerCustomDesignTblCell
            
            if arrMyOrderDetail[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Accessories_key") {
                let dict = self.arrMyOrderDetail[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["options"][indexPath.row]
                if isEnglish {
                    cell.lblCustomizeDesignTitle.text = "\(dict["option_name"].stringValue) : "
                } else {
                    cell.lblCustomizeDesignTitle.text = " : \(dict["option_name"].stringValue)"
                }
                
                cell.lblCustomizeDesignValue.text = dict["option_value_name"].stringValue
                return cell
            }
            if arrMyOrderDetail[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Tailors_key"){
                let dict = arrMyOrderDetail[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["customize_design"][indexPath.row]
                if isEnglish {
                    cell.lblCustomizeDesignTitle.text = "\(dict["style_name"].stringValue) : "
                } else {
                    cell.lblCustomizeDesignTitle.text = " : \(dict["style_name"].stringValue)"
                }
                
                cell.lblCustomizeDesignValue.text = dict["design_name"].stringValue
                return cell
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //set header for
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == self.tblProductList {
            let vw = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)?[0] as? HeaderView
            /*
            if !isEnglish {
                vw?.lblTitleName.transform = CGAffineTransform(scaleX: -1, y: 1)
                vw?.lblTitleName.textAlignment = .right
            }
            */
            vw?.lblTitleName.text = self.arrMyOrderDetail[section]["type"].stringValue
            //            vw?.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255)
            return vw
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.tblProductList {
            return 45
        }
        return 0
    }
    
    @objc func btnProductOrderTrackedAction(_ btn: UIButton) {
        
        //Redirect to track order
        
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "TrackOrderVc") as! TrackOrderVc
        obj.strOrderID = self.dictOrderDetail["order_id"].stringValue
        obj.strOrderItemId = self.dictOrderDetail["products"][btn.tag]["order_item_id"].stringValue
        //            self.dicMyOrderData["id"].stringValue
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    @objc func btnFabricOrderTrackedAction(_ btn: UIButton) {
        
        //Redirect to track order
        
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "TrackOrderVc") as! TrackOrderVc
        obj.strOrderID = self.dictOrderDetail["order_id"].stringValue
        obj.strOrderItemId = self.dictOrderDetail["fabrics"][btn.tag]["order_item_id"].stringValue
        //            self.dicMyOrderData["id"].stringValue
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnOrderTrackedAction(_ btn: UIButton) {
        
        //Redirect to track order
        
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "TrackOrderVc") as! TrackOrderVc
        obj.strOrderID = self.dictOrderDetail["order_id"].stringValue
        obj.strOrderItemId = self.dictOrderDetail["tailors"][btn.tag]["order_item_id"].stringValue
//            self.dicMyOrderData["id"].stringValue
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnReorderAction(_ orderId: UIButton) {
        
    }

}


//MARK: Service Setup
extension MyOrderDetailsVc {
    
    func orderDetailsAPICalling() {
        
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlOrder)\(urlOrderDetails)"
            
            print("URL: \(url)")
            
            let param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "lang" : lang,
                         "id" : dicMyOrderData["id"].stringValue,
                         "address_id" : dicMyOrderData["address_id"].stringValue,
                         "timezone":getCurrentTimeZone()]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        let data = json["data"]
                         self.arrMyOrderDetail = []
                        if data.count == 0 {
                            self.strMessage = data["msg"].stringValue
                        }
                        else {
                            if data["products"].exists() {
                                if data["products"].arrayValue.count != 0 {
                                    var dict = JSON()
                                    dict["type"].stringValue = getCommonString(key: "Accessories_key")
                                    dict["data"] = JSON(data["products"].arrayValue)
                                    self.arrMyOrderDetail.append(dict)
                                }
                            }
                            if data["fabrics"].exists() {
                                if data["fabrics"].arrayValue.count != 0 {
                                    var dict = JSON()
                                    dict["type"].stringValue = getCommonString(key: "Fabrics_key")
                                    dict["data"] = JSON(data["fabrics"].arrayValue)
                                    self.arrMyOrderDetail.append(dict)
                                }
                            }
                            if data["tailors"].exists() {
                                if data["tailors"].arrayValue.count != 0 {
                                    var dict = JSON()
                                    dict["type"].stringValue = getCommonString(key: "Tailors_key")
                                    dict["data"] = JSON(data["tailors"].arrayValue)
                                    self.arrMyOrderDetail.append(dict)
                                }
                            }
                            self.dictOrderDetail = json["data"]
                            self.setupAddress(dict: json["data"]["delivery_address"])
                            self.setupAmountData(dict: json["data"])
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrMyOrderDetail = []
                        self.strMessage = json["msg"].stringValue
                    }
                    print("arrMyOrderDetail : \(self.arrMyOrderDetail)")
                    self.tblProductList.reloadData()
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func reorderAPICalling(orderId: String) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlOrder)\(reorder)"
            print("URL: \(url)")
            
            let param = ["user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "order_id" : orderId,
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                         "is_login_guest" : getUserDetail("is_login_guest") == "1" ? "1" : "0",
                         "lang": lang
            ]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let cartVC = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
                        
                        cartVC.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(cartVC, animated: true)
//                        self.handlerReorderData(json["msg"].stringValue)
//                        self.navigationController?.popViewController(animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func getPaymentGateway() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlPayment)\(urlFatoorahPayment)"
            
            let parameter  = ["InvoiceValue" : dictOrderDetail["total_amount"].stringValue,
                          "CustomerName" : getUserDetail("full_name"),
                          "CustomerBlock" :dictAddressDetail["block"].stringValue,
                          "CustomerStreet":dictAddressDetail["street"].stringValue,
                          "CustomerHouseBuildingNo":dictAddressDetail["building"].stringValue,
                          "CustomerAddress":"\(dictAddressDetail["floor"].stringValue) \(dictAddressDetail["avenue"].stringValue) \(dictAddressDetail["city"].stringValue) \(dictAddressDetail["governorate"].stringValue)",
                          "CountryCodeId":"1",
                          "CustomerMobile":getUserDetail("phone_no"),
                          "CustomerEmail":getUserDetail("email"),
                          "DisplayCurrencyId":"1",
                          "SendInvoiceOption":"2",
                          "CallBackUrl":"\(kCallBackUrl)\(kPaymentTrasactionStatus)",
                          "Language":isEnglish == true ? "2" : "1"]
            
            let dict = JSON(parameter)
            
            let param:[String:String] = ["invoiceCreate":dict.rawString() ?? ""]
            
            
            print("param : \(param)")
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
                        obj.dictPaymentDetail = json["data"]
                        obj.dictSuccess = self.dictOrderDetail
                        obj.selectedPaymentGateway = .fatoorah
                        self.navigationController?.pushViewController(obj, animated: true)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
}

