//
//  ProductsTblcell.swift
//  Tailory
//
//  Created by Haresh on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class ProductsTblcell: UITableViewCell {
    
    //MARK:- Variable Declaration
    
    var handlerCombineDeliveryValue:(Int,Int) -> Void = {_,_ in   }
    var handlerPlusValue:(Int,Int) -> Void = {_,_ in   }
    var handlerMinusValue:(Int,Int) -> Void = {_,_ in   }
    var handlerDeleteValue:(Int,Int) -> Void = {_,_ in   }

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductQty: UILabel!
    
    @IBOutlet var vwTblInnerDesign: UIView!
    @IBOutlet weak var tblInnerDesign: UITableView!
    @IBOutlet weak var constantTblInnerHeight: NSLayoutConstraint!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var vwCombineDelivery: UIView!
    @IBOutlet weak var btnCombineDeliveryOutlet: UIButton!
    
    @IBOutlet weak var lblCombineDelivery: UILabel!
    @IBOutlet weak var lblFabQuantity: UILabel!
    @IBOutlet weak var viewFabrics: UIView!
    
    @IBOutlet weak var lblFabric: UILabel!
    
    @IBOutlet weak var imgFabric: UIImageView!
    @IBOutlet weak var lblFabricName: UILabel!
    @IBOutlet weak var lblMeterValue: UILabel!
    @IBOutlet weak var lblFabricPriceValue: UILabel!
    
    @IBOutlet var vwTrackOrder: UIView!
    @IBOutlet var btnTrackOrder: CustomButton!
    @IBOutlet var btnReorder: CustomButton!
    @IBOutlet weak var switchCombineDeliveryOutlet: UISwitch!
    @IBOutlet weak var lblOutOfStock: UILabel!
    @IBOutlet weak var vwDelete: UIView!
    @IBOutlet weak var btnDeleteOutlet: CustomButton!
    
    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgProduct.layer.cornerRadius = 4
        imgProduct.layer.masksToBounds = true
        
        lblProductName.textColor = UIColor.appThemeBlackColor
        lblProductName.font = themeFont(size: 15, fontname: .medium)
        
        lblProductQty.textColor = UIColor.appThemeLightGrayColor
        lblProductQty.font = themeFont(size: 15, fontname: .light)
        
        lblFabQuantity.textColor = UIColor.appThemeLightGrayColor
        lblFabQuantity.font = themeFont(size: 15, fontname: .light)
        
        lblPrice.textColor = UIColor.appThemeSilverColor
        lblPrice.font = themeFont(size: 13, fontname: .light)
        
        lblCombineDelivery.textColor = UIColor.appThemeBlackColor
        lblCombineDelivery.font = themeFont(size: 15, fontname: .roman)
        lblCombineDelivery.text = getCommonString(key: "Combine_delivery_key")
        
        lblFabric.text = getCommonString(key: "Fabric_key")
        
        tblInnerDesign.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        lblFabricPriceValue.font = themeFont(size: 14, fontname: .medium)
        lblFabricPriceValue.textColor = UIColor.appThemeSilverColor
       
        lblFabricName.font = themeFont(size: 16, fontname: .medium)
        lblFabricName.textColor = UIColor.appThemeBlackColor
        
        lblMeterValue.font = themeFont(size: 14, fontname: .medium)
        lblMeterValue.textColor = UIColor.appThemeLightGrayColor
        
        lblFabric.font = themeFont(size: 15, fontname: .medium)
        lblFabric.textColor = UIColor.appThemeBlackColor
        
        [btnTrackOrder, btnReorder].forEach { (btn) in
            btn?.isHidden = true
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        }
        
        btnDeleteOutlet.titleLabel?.font = UIFont(name: themeFonts.heavy.rawValue, size: 35)
        
        btnTrackOrder.setTitle(getCommonString(key: "Track_order_key"), for: .normal)
        btnReorder.setTitle(getCommonString(key: "Reorder_key"), for: .normal)
        
        lblOutOfStock.font = themeFont(size: 12, fontname: .medium)
        lblOutOfStock.textColor = UIColor.red
        
        if !isEnglish
        {
            self.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
            imgProduct.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblPrice,lblProductName,lblCombineDelivery,lblFabric,lblFabricName,lblMeterValue,lblFabricPriceValue,lblFabQuantity, lblOutOfStock].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
            lblProductQty.transform = CGAffineTransform(scaleX: -1, y: 1)
            btnTrackOrder.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblInnerDesign.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            if tblInnerDesign.contentSize.height <= 30
            {
                self.constantTblInnerHeight.constant = 30
            }
            else
            {
                self.constantTblInnerHeight.constant = tblInnerDesign.contentSize.height
            }
            
        }
    }
    
    //MARK:- Action
    
    @IBAction func switchCombineDelivryAction(_ sender: UIButton) {
        handlerCombineDeliveryValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnMinusAction(_ sender : UIButton){
        handlerMinusValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnPlusAction(_ sender : UIButton){
        handlerPlusValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func switchCombineDeliveryAction(_ sender: UISwitch) {
        handlerCombineDeliveryValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        handlerDeleteValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
}
