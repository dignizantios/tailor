//
//  TrailerFavouriteCollectionCell.swift
//  Tailory
//
//  Created by Haresh on 24/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TrailerFavouriteCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgTrailors: UIImageView!
    @IBOutlet weak var lblTrailorsName: UILabel!
    @IBOutlet weak var lblTrailorsDesign: UILabel!
    @IBOutlet weak var btnFavouritesOutlet: UIButton!
    @IBOutlet weak var lblMinimumPrice: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var vwActivityWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.vwActivityWidth.constant = 0
        self.activityIndicator.isHidden = true
        
        [lblTrailorsName].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .heavy)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        [lblTrailorsDesign].forEach { (lbl) in
            lbl?.font = themeFont(size: 13, fontname: .medium)
            lbl?.textColor = UIColor.appThemeSilverColor
        }
        
        lblMinimumPrice.font = themeFont(size: 12, fontname: .medium)
        lblMinimumPrice.textColor = UIColor.appThemeLightGrayColor
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        imgTrailors.layer.cornerRadius = 5
        imgTrailors.layer.masksToBounds = true
    }
    
    
    func setUpArabicUI() {
        
        [lblTrailorsName,lblTrailorsDesign,lblMinimumPrice].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
        
//        lblMinimumPrice.textAlignment = .left
        
        imgTrailors.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    

}
