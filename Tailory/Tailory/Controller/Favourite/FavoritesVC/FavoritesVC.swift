//
//  FavoritesVC.swift
//  Tailory
//
//  Created by Khushbu on 15/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage

class FavoritesVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblFavourite: UITableView!

    //MARK: - Variable
    var upperReferesh = UIRefreshControl()
    var strMessage = String()
    var tailorOffset: Int = 0
    var fabricOffset: Int = 0
    var productOffset: Int = 0
    var arrayFavoriteData : [JSON] = []
    var dicFavorite = JSON()
    var isTailor = false
    var isFabric = false
    var isProduct = false
    var isTableReload = true
    var isLoadedAll = Int()
    var dictFavoriteData = JSON()
    var isUnFavorite : () -> Void = {}
    var isShowLoater = true
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCartCount()
        self.setUpNavigationBarWithTitleAndCart(strTitle: getCommonString(key: "Favorites_key"))
    }
    
    //MARK: - SetupUI
    
    func setupUI() {
        
        self.tblFavourite.register(UINib(nibName: "FavouritesTblCell", bundle: nil), forCellReuseIdentifier: "FavouritesTblCell")
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblFavourite.addSubview(upperReferesh)
        
        self.tblFavourite.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationFire), name: NSNotification.Name(rawValue: "updateFavouriteList"), object: nil)
        
        if getUserDetail("is_login_guest") == "1" {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginRequiredScreenVc") as! LoginRequiredScreenVc
            obj.checkVC = .favoriteVC
            addChild(obj)
            self.view.addSubview(obj.view)
            obj.didMove(toParent: self)
        }
        else {
            upperRefreshTable()
        }
       
    }
    
}

//MARK: - TableView delegate Method

extension FavoritesVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayFavoriteData.count == 0
        {
            let lbl = UILabel()
//            lbl.text = strMessage
            lbl.text = getCommonString(key: "No_records_found_key")
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        
        return self.arrayFavoriteData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouritesTblCell") as! FavouritesTblCell
        
        if self.arrayFavoriteData.count > 0  {
            
            let dict = self.arrayFavoriteData[indexPath.row]
            
            cell.lblCategoryname.text = dict["type"].stringValue
            
            /*
             if indexPath.row == 0 {
             cell.lblCategoryname.text = "Tailors"
             }
             else {
             cell.lblCategoryname.text = "Accessories"
             }
             */
            
            cell.collectionCategory.tag = indexPath.row
            
            cell.collectionCategory.register(UINib(nibName: "AccessoriesTypeCell", bundle: nil), forCellWithReuseIdentifier: "AccessoriesTypeCell")
            
            cell.collectionCategory.register(UINib(nibName: "TrailerFavouriteCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TrailerFavouriteCollectionCell")
            
            cell.collectionCategory.delegate = self
            cell.collectionCategory.dataSource = self
            cell.collectionCategory.reloadData()
            
            cell.constantCollectionHeight.constant = (tableView.frame.size.width-20)/2

        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

//MARK:- UICollectionView Datasource & Delegate

extension FavoritesVC :  UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
//        let count = self.arrayFavoriteData[collectionView.tag]["list_data"].count
        
        if self.arrayFavoriteData.count == 0 {
            return 0
        }
        return self.arrayFavoriteData[collectionView.tag]["list_data"].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let dict = self.arrayFavoriteData[collectionView.tag]["list_data"][indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrailerFavouriteCollectionCell", for: indexPath) as! TrailerFavouriteCollectionCell
        
        if dict.count > 0 {
            
            if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Tailors_key") {
                
                cell.imgTrailors.sd_setIndicatorStyle(.gray)
                cell.imgTrailors.sd_setImage(with: dict["trailor_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                cell.btnFavouritesOutlet.isSelected = true
                cell.lblTrailorsName.text = dict["trailor_name"].stringValue
                cell.lblTrailorsDesign.text = "\( getCommonString(key: "Available_Design_key")) \(dict["is_available"].stringValue)"
                cell.lblMinimumPrice.text = "\(getCommonString(key: "Minimum_price_key")) \(dict["minimum_price"].stringValue) \(getCommonString(key: "KD_key"))"
                
                cell.btnFavouritesOutlet.accessibilityValue = "\(collectionView.tag)"
                cell.btnFavouritesOutlet.tag = indexPath.row
                cell.btnFavouritesOutlet.addTarget(self, action: #selector(btnFavoriteAction(_:)), for: .touchUpInside)
                
                return cell
            }
            else if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Fabrics_key") {
                
                cell.imgTrailors.sd_setIndicatorStyle(.gray)
                cell.imgTrailors.sd_setImage(with: dict["branch_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                cell.btnFavouritesOutlet.isSelected = true
                cell.lblTrailorsName.text = dict["branch_name"].stringValue
                cell.lblTrailorsDesign.text = "\( getCommonString(key: "Available_Design_key")) \(dict["is_available"].stringValue)"
                cell.lblMinimumPrice.text = "\(getCommonString(key: "Minimum_price_key")) \(dict["minimum_price"].stringValue) \(getCommonString(key: "KD_key"))"
                
                cell.btnFavouritesOutlet.accessibilityValue = "\(collectionView.tag)"
                cell.btnFavouritesOutlet.tag = indexPath.row
                cell.btnFavouritesOutlet.addTarget(self, action: #selector(btnFavoriteAction(_:)), for: .touchUpInside)
                
                return cell
            }
            else if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Accessories_key") {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AccessoriesTypeCell", for: indexPath) as! AccessoriesTypeCell
                
                cell.imgAccessories.sd_setIndicatorStyle(.gray)
                cell.imgAccessories.sd_setImage(with: dict["product_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                cell.btnFavouritesOutlet.isSelected = true
                
                cell.lblAccessriesName.text = dict["product_name"].stringValue
                cell.lblPrice.text = "\(dict["price"].stringValue) \(getCommonString(key: "KD_key"))"
                
                cell.btnFavouritesOutlet.accessibilityValue = "\(collectionView.tag)"
                cell.btnFavouritesOutlet.tag = indexPath.row
                cell.btnFavouritesOutlet.addTarget(self, action: #selector(btnFavoriteAction(_:)), for: .touchUpInside)

                return cell
            }
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (collectionView.frame.size.width-20)/2
        let height = 300
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Tailors_key") {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrailerFavouriteCollectionCell", for: indexPath) as! TrailerFavouriteCollectionCell
            
            if (self.tailorOffset == 0 || self.tailorOffset == -1) {
                self.isTailor = true
            }
            
            if (indexPath.row == self.arrayFavoriteData[collectionView.tag]["list_data"].arrayValue.count - 1) {
                
                if !self.isTailor {
                    self.isTailor = false
                    cell.activityIndicator.isHidden = false
                    cell.vwActivityWidth.constant = 40
                    self.tailorFavoriteList{
                        self.tblFavourite.reloadRows(at: [IndexPath(row: collectionView.tag, section: 0)], with: .none)
                    }
                }
            }
            else {
                cell.activityIndicator.isHidden = true
                cell.vwActivityWidth.constant = 0
            }
        }
        else if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Fabrics_key") {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrailerFavouriteCollectionCell", for: indexPath) as! TrailerFavouriteCollectionCell
            
            if (self.fabricOffset == 0 || self.fabricOffset == -1) {
                self.isFabric = true
            }
            
            if (indexPath.row == self.arrayFavoriteData[collectionView.tag]["list_data"].arrayValue.count - 1) {
                if !self.isFabric {
                    self.isFabric = false
                    cell.activityIndicator.isHidden = false
                    cell.vwActivityWidth.constant = 40
                    self.fabricFavoriteList{
                        self.tblFavourite.reloadRows(at: [IndexPath(row: collectionView.tag, section: 0)], with: .none)
                    }
                }
            }
            else {
                cell.activityIndicator.isHidden = true
                cell.vwActivityWidth.constant = 0
            }
        }
        else if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Accessories_key") {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AccessoriesTypeCell", for: indexPath) as! AccessoriesTypeCell
            
            if (self.productOffset == 0 || self.productOffset == -1) {
                self.isProduct = true
            }
            
            if (indexPath.row == self.arrayFavoriteData[collectionView.tag]["list_data"].arrayValue.count - 1) {
                if !self.isProduct {
                    self.isProduct = false
                    cell.activityIndicator.isHidden = false
                    cell.vwActivityWidth.constant = 40
                    self.productFavoriteList{
                        self.tblFavourite.reloadRows(at: [IndexPath(row: collectionView.tag, section: 0)], with: .none)
                    }
                }
            }
            else {
                cell.activityIndicator.isHidden = true
                cell.vwActivityWidth.constant = 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Tailors_key") {
            
            dictTailorDetail = self.arrayFavoriteData[collectionView.tag]["list_data"][indexPath.row]
            let tailerDetailVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "TailorsDetailVC") as! TailorsDetailVC
            tailerDetailVC.selectedFav = .fromFav
            tailerDetailVC.handlerUpdateTailorDetail = { data in
            
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                
                /*if data["is_favorite"].stringValue == "0"{
                    
                    var removeData = self.arrayFavoriteData[collectionView.tag]["list_data"].arrayValue
                    
                    if removeData.count == 0 {
                        self.arrayFavoriteData.remove(at: collectionView.tag)
                        self.tblFavourite.reloadData()
                    } else {
                        removeData.remove(at: indexPath.row)
                        self.arrayFavoriteData[collectionView.tag]["list_data"] = JSON(removeData)
                    }
                    print("tailorFavArray \(self.arrayFavoriteData)")
                    if self.arrayFavoriteData.count > 0 {
                        collectionView.reloadData()
                    }
                } else {
                    
                    var arrProductFav = self.arrayFavoriteData[collectionView.tag]["list_data"].arrayValue
                    
                    if arrProductFav.count == 0 {
                        var dicFavorite = JSON()
                        dicFavorite["type"].stringValue = getCommonString(key: "Tailors_key")
                        dicFavorite["list_data"] = JSON(data)
                        self.arrayFavoriteData[collectionView.tag] = JSON(dicFavorite)
                    } else {
                        arrProductFav.append(data)
                        self.arrayFavoriteData[collectionView.tag]["list_data"] = JSON(arrProductFav)
                    }
                    print("tailorFavArray \(self.arrayFavoriteData)")
                    UIView.performWithoutAnimation {
                        collectionView.reloadData()
                    }
                }*/
            }
            
            tailerDetailVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(tailerDetailVC, animated: true)

        }
        else if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Fabrics_key") {
            
            dictFabricDetail = self.arrayFavoriteData[collectionView.tag]["list_data"][indexPath.row]
            let tailerDetailVC = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricDetailsVc") as! FabricDetailsVc
            isFromTailor = false
            tailerDetailVC.selectedFav = .fromFav
            tailerDetailVC.handlerUpdateFabricList = { data in
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                
                /*if data["is_favorite"].stringValue != self.arrayFavoriteData[collectionView.tag]["list_data"][indexPath.row]["is_favorite"].stringValue {
                    
                    var removeData = self.arrayFavoriteData[collectionView.tag]["list_data"].arrayValue
//                    removeData.remove(at: indexPath.row)
//                    self.arrayFavoriteData[collectionView.tag]["list_data"] = JSON(removeData)
                    
                    if removeData.count == 0 {
                        self.arrayFavoriteData.remove(at: collectionView.tag)
                        self.tblFavourite.reloadData()
                    } else {
                        removeData.remove(at: indexPath.row)
                        self.arrayFavoriteData[collectionView.tag]["list_data"] = JSON(removeData)
                    }
                    
                    if self.arrayFavoriteData.count > 0 {
                        collectionView.reloadData()
                    }
                }*/
            }
            
            tailerDetailVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(tailerDetailVC, animated: true)

        }
        else if self.arrayFavoriteData[collectionView.tag]["type"].stringValue == getCommonString(key: "Accessories_key") {
            
            let accessories = GlobalVariables.accessoriesStoryboard.instantiateViewController(withIdentifier: "AccessoriesDetailVC") as! AccessoriesDetailVC
            accessories.selectedFav = .fromFav
            accessories.dictAccessories = self.arrayFavoriteData[collectionView.tag]["list_data"][indexPath.row]
            
            
            accessories.handlerUpdateAccessoriesList = { data in
             
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                
                /*if data["is_favorite"].stringValue != self.arrayFavoriteData[collectionView.tag]["list_data"][indexPath.row]["is_favorite"].stringValue {
                    
                    var removeData = self.arrayFavoriteData[collectionView.tag]["list_data"].arrayValue
//                    removeData.remove(at: indexPath.row)
//                    self.arrayFavoriteData[collectionView.tag]["list_data"] = JSON(removeData)
                    
                    if removeData.count == 0 {
                        self.arrayFavoriteData.remove(at: collectionView.tag)
                        self.tblFavourite.reloadData()
                    } else {
                        removeData.remove(at: indexPath.row)
                        self.arrayFavoriteData[collectionView.tag]["list_data"] = JSON(removeData)
                    }
                    
                    if self.arrayFavoriteData.count > 0 {
                        collectionView.reloadData()
                    }
                }*/
                
            }
            
            accessories.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(accessories, animated: true)
        }
    }
}

//MARK: Button Action
extension FavoritesVC {
    
    @objc func btnFavoriteAction(_ sender: UIButton) {
        
        print("Accessibility:", sender.accessibilityValue!)
        print("tag:", sender.tag)
        
        let data = self.arrayFavoriteData[Int(sender.accessibilityValue ?? "0") ?? 0]["list_data"][sender.tag]
        print("Data:", data)
        
        manuallyUpdateFav(section: Int(sender.accessibilityValue ?? "0") ?? 0, row: sender.tag, dict: data)
        self.addFavUnfav(section: Int(sender.accessibilityValue ?? "0") ?? 0, row: sender.tag, dict: data)
    }
    
    func manuallyUpdateFav(section: Int, row: Int, dict: JSON){
        
        if self.arrayFavoriteData[section]["type"].stringValue == getCommonString(key: "Tailors_key") {
            var removeData = self.arrayFavoriteData[section]["list_data"].arrayValue
            removeData.remove(at: row)
            self.arrayFavoriteData[section]["list_data"] = JSON(removeData)
            
            if removeData.count == 0 {
                self.arrayFavoriteData.remove(at: section)
                UIView.performWithoutAnimation {
                    self.tblFavourite.reloadData()
                }
            } else {
                if let tblCell = self.tblFavourite.cellForRow(at: IndexPath(row: section, section: 0)) as? FavouritesTblCell {
                    if self.arrayFavoriteData.count > 0 {
                        tblCell.collectionCategory.reloadData()
                    }
                }
            }
        }
        else if self.arrayFavoriteData[section]["type"].stringValue == getCommonString(key: "Fabrics_key") {
            var removeData = self.arrayFavoriteData[section]["list_data"].arrayValue
            removeData.remove(at: row)
            self.arrayFavoriteData[section]["list_data"] = JSON(removeData)
            
            if removeData.count == 0 {
                self.arrayFavoriteData.remove(at: section)
                UIView.performWithoutAnimation {
                    self.tblFavourite.reloadData()
                }
            } else {
                if let tableCell = self.tblFavourite.cellForRow(at: IndexPath(row: section, section: 0)) as? FavouritesTblCell {
                    if self.arrayFavoriteData.count > 0 {
                        tableCell.collectionCategory.reloadData()
                    }
                }
            }
            
        }
        else if self.arrayFavoriteData[section]["type"].stringValue == getCommonString(key: "Accessories_key") {
            var removeData = self.arrayFavoriteData[section]["list_data"].arrayValue
            removeData.remove(at: row)
            self.arrayFavoriteData[section]["list_data"] = JSON(removeData)
            
            if removeData.count == 0 {
                self.arrayFavoriteData.remove(at: section)
                UIView.performWithoutAnimation {
                    self.tblFavourite.reloadData()
                }
            } else {
                if let tableCell = self.tblFavourite.cellForRow(at: IndexPath(row: section, section: 0)) as? FavouritesTblCell {
                    if self.arrayFavoriteData.count > 0 {
                        
                        if self.arrayFavoriteData[section]["list_data"].arrayValue.count != 0 {
                            tableCell.collectionCategory.reloadData()
                        }
                    }
                }
            }
        }
        
    }
}


//MARK: Services

extension FavoritesVC {
    
    @objc func notificationFire(_ notification: Notification) {
        if notification.object as? String == "0" {
            isShowLoater = false
            upperRefreshTable()
        }
    }
    
    @objc func upperRefreshTable() {
        tailorOffset = 0
        fabricOffset = 0
        productOffset = 0
        isTableReload = true
        self.isLoadedAll = 0
        tailorFavoriteList {}
//        fabricFavoriteList {}
//        productFavoriteList {}
        
    }
    func getCartCount()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlCartCount)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "is_login_guest": getUserDetail("is_login_guest") == "0" ? "0" : "1",
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString]
            
            print("param : \(param)")
            
            //self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                //self.hideLoader()
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].exists(){
                            let data = json["data"].dictionaryValue
                            isCartCount = data["cart_count"]?.intValue ?? 0
                            self.setUpNavigationBarWithTitleAndCart(strTitle: getCommonString(key: "Favorites_key"))
                            
                        }
                        
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        //makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    // makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            //makeToast(strMessage: networkMsg)
        }
    }
    func tailorFavoriteList(completion:@escaping()-> ()) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlUser)\(urlTailorsFavoriteList)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "offset" : String(self.tailorOffset)
                ] as! [String : String]
            
            print("param :\(param)")
            
            if self.tailorOffset == 0 {
                if isShowLoater {
                    self.showLoader()
                }
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.upperReferesh.endRefreshing()

                if (self.tailorOffset == 0) && (self.fabricOffset == 0) && (self.productOffset == 0) {
                    self.arrayFavoriteData = []
                }
                
//                if self.isLoadedAll == 2 {
//                    self.hideLoader()
//                }
                self.isTailor = false
                
                if let json = respones.value {
                    
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        if json["data"].arrayValue.count == 0 {
                            
                            self.hideLoader()
                            self.strMessage = json["msg"].stringValue
                            self.fabricFavoriteList { }
//                            self.tblFavourite.reloadData()
                            return
                        }
                        
                        /*
                        if self.tailorOffset == 0 {
                            self.arrayFavoriteData = []
                        }*/
                        
                        if self.tailorOffset < 0 {
                            return
                        }
                        
                        self.tailorOffset = json["next_offset"].intValue
                        
                        var isTailorAvailable = false
                        
                        for item in 0..<self.arrayFavoriteData.count {
                            
                            var dic = self.arrayFavoriteData[item]
                            if dic["type"].stringValue == getCommonString(key: "Tailors_key") {
                                
                                isTailorAvailable = true
                                var array = dic["list_data"].arrayValue
                                array = array + json["data"].arrayValue
                                dic["list_data"] = JSON(array)
                            }
                            self.arrayFavoriteData[item] = dic
                            completion()
                        }

                        if !isTailorAvailable {
                            
                            let aryData = json["data"].arrayValue
                            
                            var dicFavorite = JSON()    //Products_key
                            dicFavorite["type"].stringValue = getCommonString(key: "Tailors_key")
                            dicFavorite["list_data"] = JSON(aryData)
                            self.arrayFavoriteData.append(dicFavorite)
                            print("ArrayFavorites:", self.arrayFavoriteData)
//                            if self.isLoadedAll == 2 {
//                                self.tblFavourite.reloadData()
//                            }
//                            self.isLoadedAll += 1
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        if getUserDetail("is_login_guest") != "1" {
                            makeToast(strMessage: json["msg"].stringValue)
                        }
//                        makeToast(strMessage: json["msg"].stringValue)
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    print("self.arrayFavoriteData:", self.arrayFavoriteData)
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
                
                self.fabricFavoriteList{}
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func fabricFavoriteList(completion:@escaping()-> ()) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlUser)\(urlFabricsFavoriteList)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "offset" : String(self.fabricOffset)
                ] as! [String : String]
            
            print("param :\(param)")
            
//            if self.fabricOffset == 0 {
//                self.showLoader()
//            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
//                if self.isLoadedAll == 2 {
//                    self.hideLoader()
//                }
                
                self.isFabric = false
                
                if let json = respones.value {
                    
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        if json["data"].arrayValue.count == 0 {
                            
                            self.hideLoader()
                            self.strMessage = json["msg"].stringValue
                            self.productFavoriteList { }
//                            self.tblFavourite.reloadData()
                            return
                        }
                        
                        /*
                        if self.fabricOffset == 0 {
                            self.arrayFavoriteData = []
                        }*/
                        
                        if self.fabricOffset < 0 {
                            return
                        }
                        
                        self.fabricOffset = json["next_offset"].intValue
                        
                        var isFabricAvailable = false
                        
                        for item in 0..<self.arrayFavoriteData.count {
                            
                            var dic = self.arrayFavoriteData[item]
                            if dic["type"].stringValue == getCommonString(key: "Fabrics_key") {
                                /*getCommonString(key: "Tailors_key")
                                  getCommonString(key: "Fabrics_key")*/
                                isFabricAvailable = true
                                var array = dic["list_data"].arrayValue
                                array = array + json["data"].arrayValue
                                dic["list_data"] = JSON(array)
                            }
                            self.arrayFavoriteData[item] = dic
                            completion()
                        }

                        if !isFabricAvailable {
                            
                            let aryData = json["data"].arrayValue
                            
                            var dicFavorite = JSON()    //Products_key
                            dicFavorite["type"].stringValue = getCommonString(key: "Fabrics_key")
                            dicFavorite["list_data"] = JSON(aryData)
                            self.arrayFavoriteData.append(dicFavorite)
                            print("ArrayFavorites:", self.arrayFavoriteData)
                            
//                            if self.isLoadedAll == 2 {
//                                self.tblFavourite.reloadData()
//                            }
//                            self.isLoadedAll += 1
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        if getUserDetail("is_login_guest") != "1" {
                            makeToast(strMessage: json["msg"].stringValue)
                        }
//                        makeToast(strMessage: json["msg"].stringValue)
                        self.strMessage = json["msg"].stringValue
                    }
                    print("self.arrayFavoriteData:", self.arrayFavoriteData)
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
                
                self.productFavoriteList {}
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func productFavoriteList(completion:@escaping()-> ()) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlUser)\(urlProductFavoriteList)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "offset" : String(self.productOffset)
                ] as! [String : String]
            
            print("param :\(param)")
            
//            if self.productOffset == 0 {
//                self.showLoader()
//            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.isShowLoater = false
//                if self.isLoadedAll == 2 {
//                    self.hideLoader()
//                }
                self.isProduct = false
                
                if let json = respones.value {
                    
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        if json["data"].arrayValue.count == 0 {
                            
                            self.hideLoader()
                            self.strMessage = json["msg"].stringValue
                            self.tblFavourite.reloadData()
                            return
                        }
                        
                        /*
                        if self.productOffset == 0 {
                            self.arrayFavoriteData = []
                        }*/
                        
                        if self.productOffset < 0 {
                            return
                        }
                        
                        self.productOffset = json["next_offset"].intValue
                        
                        var isProductAvailable = false
                        for item in 0..<self.arrayFavoriteData.count {
                            
                            var dic = self.arrayFavoriteData[item]
                            if dic["type"].stringValue == getCommonString(key: "Accessories_key") {
                                
                                isProductAvailable = true
                                var array = dic["list_data"].arrayValue
                                array = array + json["data"].arrayValue
                                dic["list_data"] = JSON(array)
                            }
                            self.arrayFavoriteData[item] = dic
                            completion()
                        }

                        if !isProductAvailable {
                            
                            let aryData = json["data"].arrayValue
                            
                            var dicFavorite = JSON()    //Products_key
                            dicFavorite["type"].stringValue = getCommonString(key: "Accessories_key")
                            dicFavorite["list_data"] = JSON(aryData)
                            self.arrayFavoriteData.append(dicFavorite)
                            print("ArrayFavorites:", self.arrayFavoriteData)
//                            if self.isLoadedAll == 2 {
//                                self.tblFavourite.reloadData()
//                            }
//                            self.isLoadedAll += 1
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        if getUserDetail("is_login_guest") != "1" {
                            makeToast(strMessage: json["msg"].stringValue)
                        }
//                        makeToast(strMessage: json["msg"].stringValue)
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    if self.isTableReload {
                        self.isTableReload = false
                        self.tblFavourite.reloadData()
                    }
//                    self.tblFavourite.reloadData()
                    print("self.arrayFavoriteData:", self.arrayFavoriteData)
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addFavUnfav(section: Int, row: Int, dict: JSON) {
        
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlUser)\(urlFavUnfav)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "is_favorite" : "0",
                     "role" : dict["tailor_review_flag"].stringValue,
                     "business_id": dict["tailor_review_flag"].stringValue == "3" ? dict["product_id"].stringValue : dict["business_id"].stringValue]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        /*if self.arrayFavoriteData[section]["type"].stringValue == getCommonString(key: "Tailors_key") {
                            var removeData = self.arrayFavoriteData[section]["list_data"].arrayValue
                            removeData.remove(at: row)
                            self.arrayFavoriteData[section]["list_data"] = JSON(removeData)
                            
                            if removeData.count == 0 {
                                self.arrayFavoriteData.remove(at: section)
                                UIView.performWithoutAnimation {
                                    self.tblFavourite.reloadData()
                                }
                            } else {
                                if let tblCell = self.tblFavourite.cellForRow(at: IndexPath(row: section, section: 0)) as? FavouritesTblCell {
                                    if self.arrayFavoriteData.count > 0 {
                                        tblCell.collectionCategory.reloadData()
                                    }
                                }
                            }
                        }
                        else if self.arrayFavoriteData[section]["type"].stringValue == getCommonString(key: "Fabrics_key") {
                            var removeData = self.arrayFavoriteData[section]["list_data"].arrayValue
                            removeData.remove(at: row)
                            self.arrayFavoriteData[section]["list_data"] = JSON(removeData)
     
                            if removeData.count == 0 {
                                self.arrayFavoriteData.remove(at: section)
                                UIView.performWithoutAnimation {
                                    self.tblFavourite.reloadData()
                                }
                            } else {
                                if let tableCell = self.tblFavourite.cellForRow(at: IndexPath(row: section, section: 0)) as? FavouritesTblCell {
                                    if self.arrayFavoriteData.count > 0 {
                                        tableCell.collectionCategory.reloadData()
                                    }
                                }
                            }

                        }
                        else if self.arrayFavoriteData[section]["type"].stringValue == getCommonString(key: "Accessories_key") {
                            var removeData = self.arrayFavoriteData[section]["list_data"].arrayValue
                            removeData.remove(at: row)
                            self.arrayFavoriteData[section]["list_data"] = JSON(removeData)
                            
                            if removeData.count == 0 {
                                self.arrayFavoriteData.remove(at: section)
                                UIView.performWithoutAnimation {
                                    self.tblFavourite.reloadData()
                                }
                            } else {
                                if let tableCell = self.tblFavourite.cellForRow(at: IndexPath(row: section, section: 0)) as? FavouritesTblCell {
                                    if self.arrayFavoriteData.count > 0 {
                                        
                                        if self.arrayFavoriteData[section]["list_data"].arrayValue.count != 0 {
                                            tableCell.collectionCategory.reloadData()
                                        }
                                    }
                                }
                            }
                        }*/
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
}



/*getCommonString(key: "Tailors_key")
 getCommonString(key: "Fabrics_key")
 getCommonString(key: "Accessories_key")*/
