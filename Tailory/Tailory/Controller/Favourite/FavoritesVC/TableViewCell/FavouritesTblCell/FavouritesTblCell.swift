//
//  TailorsFavouritesTblCell.swift
//  Tailory
//
//  Created by Haresh on 24/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class FavouritesTblCell: UITableViewCell {
    
    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblCategoryname: UILabel!
    @IBOutlet weak var collectionCategory: UICollectionView!
    @IBOutlet weak var constantCollectionHeight: NSLayoutConstraint!
    
    //MARK: - Variable
    
    
    //MARK: - View Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblCategoryname.textColor = UIColor.appThemeLightGrayColor
        lblCategoryname.font = themeFont(size: 18, fontname: .heavy)
        
        if !isEnglish
        {
            lblCategoryname.textAlignment = .right
            lblCategoryname.transform  = CGAffineTransform(scaleX: -1, y: 1)
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
