//
//  FabricsVC.swift
//  Tailory
//
//  Created by Khushbu on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

enum checkOwnFabricOrNot {
    case ownFabric
    case notOwnFabric
}

class FabricsVC: UIViewController {
    
    // MARK: Oulets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwTopHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnMyOwnFabrics: UIButton!
    @IBOutlet weak var lblOr: UILabel!
    @IBOutlet weak var lblSelectFabricFrom: UILabel!
    @IBOutlet weak var tblFabrics: UITableView!
    
    
    // MARK: Variables
    let segmentControllers = SJSegmentedViewController()
    var selectedSegment: SJSegmentTab?
    var selectFabric = checkOwnFabricOrNot.ownFabric
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var arrFabric:[JSON] = []
    var strMessage = String()
    
    var filterData = JSON()
    var sortData = JSON()
    let txtSearch = UITextField()

    // MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
        setUpUI()
        registerXib()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectFabric == .ownFabric {
            self.vwTop.isHidden = true
            self.vwTopHeightConstraint.constant = 0
           /* if isEnglish {
                setUpNavigationBarWithBackHeaderTitleAndRightButtons(strTitle: getCommonString(key: "Fabrics_key"))
            }
            else {
                setUpArabicUI()
                setUpNavigationBarWithBackHeaderTitleAndRightButtonsArabic(strTitle: getCommonString(key: "Fabrics_key"))
            }
            self.navigationBtnSetUp()*/
        }
        else {
            self.vwTop.isHidden = false
            self.vwTopHeightConstraint.constant = 95
            
//            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Fabrics_key"))
            
        }
//        self.setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Fabrics_key"))
        if isEnglish {
            setUpNavigationBarWithBackHeaderTitleAndRightButtons(strTitle: getCommonString(key: "Fabric_title_key"))
        }
        else {
            setUpArabicUI()
            setUpNavigationBarWithBackHeaderTitleAndRightButtonsArabic(strTitle: getCommonString(key: "Fabric_title_key"))
        }
        self.navigationBtnSetUp()
    }
}

//MARK: SetUp
extension FabricsVC {
    
    func setUpUI() {
     
        lblOr.text = getCommonString(key: "OR_key")
        btnMyOwnFabrics.setTitle(getCommonString(key: "I_have_my_own_fabric_key"), for: .normal)
        btnMyOwnFabrics.titleLabel?.font = themeFont(size: 18, fontname: .heavy)
        btnMyOwnFabrics.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        
        [lblSelectFabricFrom].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .medium)
            lbl?.text = getCommonString(key: "Select_fabric_from_key")
        }
        
        if !isEnglish
        {
            setUpArabicUI()
        }
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblFabrics.addSubview(upperReferesh)
        
        tblFabrics.tableFooterView = UIView()
        
        upperRefreshTable()
        
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
    }
    
    func updateFabricList(dict:JSON){
        if let index = arrFabric.index(where: {$0["branch_id"].stringValue == dict["branch_id"].stringValue}) {
            arrFabric[index] = dict
            UIView.performWithoutAnimation {
                self.tblFabrics.reloadData()
            }
//            self.tblFabrics.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
    
    func registerXib() {
        tblFabrics.register(UINib(nibName: "TailorsTableCell", bundle: nil), forCellReuseIdentifier: "TailorsTableCell")
    }
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        btnMyOwnFabrics.transform = CGAffineTransform(scaleX: -1, y: 1)
        lblOr.transform = CGAffineTransform(scaleX: -1, y: 1)
        lblSelectFabricFrom.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
}

//MARK: Navigation Button Setup
extension FabricsVC {
    
    func navigationBtnSetUp() {
        
        let rightSortButton = UIBarButtonItem(image: UIImage(named: "ic_sort"), style: .plain, target: self, action: #selector(btnSortTapped))
        rightSortButton.tintColor = .white
        
        let rightFilterButton = UIBarButtonItem(image: UIImage(named: "ic_filter"), style: .plain, target: self, action: #selector(btnFilterTapped))
        rightFilterButton.tintColor = .white
        
        let rightSearchButton = UIBarButtonItem(image: UIImage(named: "ic_search"), style: .plain, target: self, action: #selector(btnSearchTapped))
        rightSearchButton.tintColor = .white
        
        if isEnglish {
            navigationItem.rightBarButtonItems = [rightSearchButton, rightFilterButton, rightSortButton]
        }
        else {
            navigationItem.leftBarButtonItems = [rightSearchButton, rightFilterButton, rightSortButton]
        }
        
    }
    
    @objc func btnSortTapped() {
        let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SortingVC") as! SortingVC
        objc.dictSortingData = self.sortData
        objc.handlerSort = { data in
            self.sortData = data!
            self.intOffset = 0
            self.getFabricList(search: self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        }
        objc.modalPresentationStyle = .overCurrentContext
        objc.modalTransitionStyle = .crossDissolve
        self.present(objc, animated: true, completion: nil)
    }
    
    @objc func btnFilterTapped() {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.isTrailors = false
        obj.dictFilterData = self.filterData
        obj.handlerFilter = { data in
            self.filterData = data!
            self.intOffset = 0
            self.getFabricList(search: self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnSearchTapped() {
        
        let vw = UIView()
        vw.frame = CGRect(x: 0, y: 0, width: self.navigationController?.navigationBar.frame.size.width ?? 375 - 200, height: 35)
        
        let img = UIImageView()
        img.frame = CGRect(x: 5, y: 8, width: vw.frame.size.height-16, height: vw.frame.size.height-16)
        img.image = UIImage(named: "ic_search")
        img.tintColor = .white
        vw.addSubview(img)
        txtSearch.returnKeyType = .search
        txtSearch.frame = CGRect(x: img.frame.size.width+10, y: 0, width: vw.frame.size.width-img.frame.size.width-135, height: 35)
        txtSearch.becomeFirstResponder()
        txtSearch.textColor = UIColor.black
        txtSearch.backgroundColor = .clear
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                          NSAttributedString.Key.font : themeFont(size: 14, fontname: .medium)]
        txtSearch.attributedPlaceholder = NSAttributedString(string: getCommonString(key: "Search_here_key"), attributes: attributes)
        txtSearch.font = themeFont(size: 14, fontname: .medium)
        vw.addSubview(txtSearch)
        
        vw.layer.cornerRadius = 5
        vw.layer.masksToBounds = true
        vw.backgroundColor = UIColor(red: 211/255, green: 212/255, blue: 211/255, alpha: 1.0)
        self.navigationItem.titleView = vw
        
        let btnClear = UIBarButtonItem(title: getCommonString(key: "Clear_key"), style: .plain, target: self, action: #selector(btnClearAction))
        btnClear.tintColor = .white
        
        if isEnglish {
            let leftButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow"), style: .plain, target: self, action: #selector(backButtonAction))
            leftButton.tintColor = UIColor.white
            navigationItem.hidesBackButton = true
            
            navigationItem.leftBarButtonItems = []
            navigationItem.leftBarButtonItem = leftButton
            
            navigationItem.rightBarButtonItems = []
            navigationItem.rightBarButtonItem = btnClear
        }
        else {
            
            vw.transform = CGAffineTransform(scaleX: -1, y: 1)
            img.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtSearch.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtSearch.textAlignment = .right
            
            let leftButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonAction))
            leftButton.tintColor = UIColor.white
            navigationItem.hidesBackButton = true
            navigationItem.rightBarButtonItems = []
            navigationItem.rightBarButtonItem = leftButton
            
            navigationItem.leftBarButtonItems = []
            navigationItem.leftBarButtonItem = btnClear
        }
    }
    
    @objc func btnClearAction() {
        if txtSearch.text != "" {
            self.txtSearch.text = ""
            self.upperRefreshTable()
        } else {
            self.view.endEditing(true)
            if isEnglish {
                setUpNavigationBarWithBackHeaderTitleAndRightButtons(strTitle: getCommonString(key: "Fabric_title_key"))
            }
            else {
                setUpNavigationBarWithBackHeaderTitleAndRightButtonsArabic(strTitle: getCommonString(key: "Fabric_title_key"))
            }
            self.navigationBtnSetUp()
        }
    }
    
    @objc func backButtonAction() {
        btnClearAction()
    }
}

//MARK: TextField Delegate
extension FabricsVC : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField:UITextField){
        if textField == txtSearch {
            self.intOffset = 0
            self.getFabricList(search: textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",isComeFromSearch: true)
//            self.viewWillAppear(true)
//            self.view.endEditing(true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("Clicked")
        if textField == self.txtSearch {
            self.intOffset = 0
            self.getFabricList(search: textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
            self.viewWillAppear(true)
            self.view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        /*
         let searchText = textField.text ?? "" + string
         if searchText.count >= 3 {
         self.arrTailorList = self.arraySearchData.filter({ (data) -> Bool in
         return data["trailor_name"].stringValue == searchText
         })
         self.tblTailors.reloadData()
         }
         else {
         self.arrTailorList = self.arraySearchData
         }
         self.tblTailors.reloadData()
         */
        return true
    }
    
}

//MARK: TableView Delegate/Datasource
extension FabricsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrFabric.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            
            if !isEnglish {
                lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrFabric.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TailorsTableCell", for: indexPath) as! TailorsTableCell
        cell.vwAvailabelDesign.isHidden = true
        cell.btnFavorite.tag = indexPath.row
        cell.btnFavorite.addTarget(self, action: #selector(btnFavoriteSelection(_:)), for: .touchUpInside)
        cell.vwStatus.isHidden = true
        cell.setupFabricData(dict: arrFabric[indexPath.row])
        cell.lblStatus.text = getCommonString(key: "Unavailable_key")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
     //   fabricsDetailVC()
        dictFabricDetail = arrFabric[indexPath.row]
        
        let obj = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricDetailsVc") as! FabricDetailsVc
        obj.handlerUpdateFabricList = {[weak self] dict in
            self?.updateFabricList(dict: dict)
        }
        
        if selectFabric == .ownFabric // Fabric
        {
            isFromTailor = false
        }
        else if selectFabric == .notOwnFabric // Tailor&Fabric
        {
            isFromTailor = true
        }
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    
    func fabricsDetailVC() {
        
        let detailVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "TailorsDetailVC")
        
        let availableVC = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "AvailableFabricsVC")
        availableVC.title = getCommonString(key: "Available_fabrics_key")
        
        let priceListVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "PriceListVC")
        priceListVC.title = getCommonString(key: "Price_list_key")
        
        let moreInfoVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "MoreInfoVC")
        moreInfoVC.title = getCommonString(key: "More_info_key")
        
        segmentControllers.headerViewController = detailVC
        segmentControllers.segmentControllers = [availableVC, priceListVC, moreInfoVC]
        
        segmentControllers.headerViewHeight = 305
        segmentControllers.segmentViewHeight = 50.0
        segmentControllers.selectedSegmentViewHeight = 2.0
        segmentControllers.showsHorizontalScrollIndicator = false
        segmentControllers.showsVerticalScrollIndicator = false
        segmentControllers.segmentBounces = false
        
        segmentControllers.segmentTitleColor = UIColor.appThemeBlackColor
        segmentControllers.segmentSelectedTitleColor = UIColor.appThemeSilverColor
        segmentControllers.selectedSegmentViewColor = UIColor.appThemeSilverColor
        
        if !isEnglish {
            segmentControllers.segmentControllers = [moreInfoVC, priceListVC, availableVC]
            segmentControllers.setSelectedSegmentAt(1, animated: true)
        }        
        
        navigationController?.pushViewController(segmentControllers, animated: true)
    }
}

//MARK: - Laundry Delegate method
 
 extension FabricsVC: delegateWantLaundryPopup
 {
    
    func yesForLaundry() {
        var dict = objCartModel.dictProductJson["tailor"]
        dict["is_fabric_added"].stringValue = enumIsAddedFabric.notAddedFabric.rawValue
        dict["quantity"].stringValue = objCartModel.quantity
        dict["is_laundry"].stringValue = enumIsAddedLaudry.addedLaudry.rawValue
        objCartModel.dictProductJson["tailor"] = dict
        redirectToAddCart()
//        addProductToCart()
//        checkFabricAdded()
        // call API
    }
    
    func noForLaundry() {
        var dict = objCartModel.dictProductJson["tailor"]
        dict["is_fabric_added"].stringValue = enumIsAddedFabric.notAddedFabric.rawValue
        dict["quantity"].stringValue = objCartModel.quantity
        dict["is_laundry"].stringValue = enumIsAddedLaudry.notAddedLaudry.rawValue
        objCartModel.dictProductJson["tailor"] = dict
        redirectToAddCart()
//        addProductToCart()
//        checkFabricAdded()
        // call API
    }
    
    func openLaundryPopupmMenu()
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LaundryPopupVc") as! LaundryPopupVc
        obj.delegateWantLaundry = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
    }
    
 }


//MARK: Button Action
extension FabricsVC {
    
    @IBAction func btnMyOwnFabricsAction(_ sender: Any) {
        let dict = objCartModel.dictProductJson["tailor"]
        if dict["measurement_type"].stringValue == "3" || dict["measurement_type"].stringValue == "4" {
            self.openLaundryPopupmMenu()
        } else {
            let alertController = UIAlertController(title: getCommonString(key: "Note_key"), message: getCommonString(key: "The_date_of_picking_up_the_fabric_will_be_the_same_as_specified_here_key"), preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: getCommonString(key: "Ok_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                self.openLaundryPopupmMenu()
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func redirectToAddCart(){
        var dict = objCartModel.dictProductJson["tailor"]
        dict["is_fabric_added"].stringValue = enumIsAddedFabric.notAddedFabric.rawValue
        dict["fabrics"] = []
        dict["quantity"].stringValue = objCartModel.quantity
//        dict["is_laundry"].stringValue = enumIsAddedLaudry.notAddedLaudry.rawValue
        
        // call API
        if dict["measurement_type"].stringValue == "3" || dict["measurement_type"].stringValue == "4"{
            objCartModel.dictProductJson["tailor"] = dict
            print("final dict \(objCartModel.dictProductJson)")
            let obj = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyAppoitmentVC") as! MyAppoitmentVC
            self.navigationController?.pushViewController(obj, animated: true)
        } else{
            dict["is_exist_fabric_pickup_address_id"].stringValue = dict["is_exist_address_id"].stringValue
            dict["fabric_pickup_address_id"].stringValue = dict["address_id"].stringValue
            dict["fabrics_pickup_time"].stringValue = dict["time"].stringValue
            dict["fabrics_pickup_date"].stringValue = dict["date"].stringValue
            //            dict["fabrics_pickup_address"] = dict["address"]
            if getUserDetail("is_login_guest") == "1" {
                dict["fabrics_pickup_address"] = guestDictAddress
                dict["fabric_pickup_address_id"].stringValue = ""
                dict["is_exist_fabric_pickup_address_id"] = "0"
            }
            else {
                dict["fabrics_pickup_address"] = dict["address"]
            }
            dict["fabrics_notes"] = dict["extra_notes"]
            objCartModel.dictProductJson["tailor"] = dict
            print("final dict \(objCartModel.dictProductJson)")
            addProductToCart()
        }
    }
    
    @objc func btnFavoriteSelection(_ sender: UIButton) {
        
        if getUserDetail("is_login_guest") == "1" {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
        }
        else {
            var dict = arrFabric[sender.tag]
            if dict["is_favorite"].stringValue == "0"{
                dict["is_favorite"].stringValue = "1"
                addFavUnfav(status: "1", tag: sender.tag, dict: dict)
            } else {
                 dict["is_favorite"].stringValue = "0"
                addFavUnfav(status: "0", tag: sender.tag, dict: dict)
            }
            self.arrFabric[sender.tag] = dict
            UIView.performWithoutAnimation {
                self.tblFabrics.reloadData()
            }
        }
        /*
        let dict = arrFabric[sender.tag]
        if dict["is_favorite"].stringValue == "0"{
            addFavUnfav(status: "1", tag: sender.tag, dict: dict)
        } else {
            addFavUnfav(status: "0", tag: sender.tag, dict: dict)
        }
         */
    }
}


//MARK: - Product add to cart Delegate method

extension FabricsVC: delegateProductAddCartPopup
{
    func redirectToGoToCart() {

        self.dismiss(animated: false, completion: nil)
        self.cartTapped()
        
//        self.navigationController?.popToRootViewController(animated: false)
        
        /*
        if isEnglish
        {
            appdelgate.tailerTabbarVC.selectedIndex = 3
        }
        else
        {
            appdelgate.tailerTabbarVC.selectedIndex = 1
        }
        */
    }
    
    func redirectToAddMore() {
        
        for vc in self.navigationController!.viewControllers {
            if let myVC = vc as? TailorsDetailVC {
                self.navigationController?.popToViewController(myVC, animated: true)
            }
        }
        
//        self.navigationController?.popToRootViewController(animated: false)
        
        /*
        if isEnglish
        {
            appdelgate.tailerTabbarVC.selectedIndex = 3
        }
        else
        {
            appdelgate.tailerTabbarVC.selectedIndex = 1
        }*/
        
    }
    
}

//MARK:- Service

extension FabricsVC
{
    @objc func upperRefreshTable()
    {
        intOffset = 0
        getFabricList(search: self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
    }
    
    func getFabricList(search:String = "",isComeFromSearch:Bool=false)
    {
//        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlFabric)\(urlFabricList)"

            var param: [String : String] = [:]
            /*if self.filterData.isEmpty && self.sortData.isEmpty {
                param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                    "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)",
                    "search" : search
                ]
            }
            else if !self.filterData.isEmpty && self.sortData.isEmpty {
                param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                    "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)",
                    "is_available" : self.filterData["is_available"].stringValue,
                    "min_price" : self.filterData["min_price"].stringValue,
                    "max_price" : self.filterData["max_price"].stringValue,
                    "min_distance" : self.filterData["min_distance"].stringValue,
                    "max_distance" : self.filterData["max_distance"].stringValue,
                    "star_rating" : self.filterData["star_rating"].stringValue,
                    "search" : search
                ]
            }
            else if self.filterData.isEmpty && !self.sortData.isEmpty {
                param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                    "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)",
                    "sort" : self.sortData["sort"].stringValue,
                    "search" : search
                ]
            }
            else {*/
                param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                    "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)",
                    "is_available" : self.filterData["is_available"].stringValue,
                    "min_price" : self.filterData["min_price"].stringValue,
                    "max_price" : self.filterData["max_price"].stringValue,
                    "min_distance" : self.filterData["min_distance"].stringValue,
                    "max_distance" : self.filterData["max_distance"].stringValue,
                    "sort" : self.sortData["sort"].stringValue,
                    "search" : search,
                    "star_rating" : self.filterData["star_rating"].stringValue
                ]
           // }
            
            print("param : \(param)")
            
            if isComeFromSearch == false{
                self.view.endEditing(true)
                if intOffset == 0 {
                    self.showLoader()
                }
            }
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.upperReferesh.endRefreshing()
                            self.arrFabric = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblFabrics.tableFooterView = UIView()
                            self.tblFabrics.reloadData()
                            return
                        }
                        
                        if self.intOffset == 0
                        {
                            self.arrFabric = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrFabric + aryData
                        
                        self.arrFabric = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        self.arrFabric = []
                        self.strMessage = json["msg"].stringValue
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                    self.tblFabrics.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addFavUnfav(status:String,tag:Int,dict:JSON)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlFavUnfav)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "is_favorite" : status,
                     "role" : dict["tailor_review_flag"].stringValue,
                     "business_id":dict["business_id"].stringValue]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
//                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                        /*var dictData = dict
                        dictData["is_favorite"].stringValue = status
                        self.arrFabric[tag] = dictData
//                        self.tblFabrics.reloadRows(at: [IndexPath(row: tag, section: 0)], with: .none)
                        self.tblFabrics.reloadData()*/
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func addProductToCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlAddToCart)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                     "category_id" : objCartModel.category_id,
                     "business_id" : objCartModel.business_id,
                     "quantity": objCartModel.quantity,
                     "product_cart_json": JSON(objCartModel.dictProductJson).rawString(),
                     "is_login_guest":getUserDetail("is_login_guest") == "1" ? "1" : "0"
                ] as! [String : String]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
//                        makeToast(strMessage: json["msg"].stringValue)
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProductAddToCartVc") as! ProductAddToCartVc
                        obj.isSaveForLater = false
                        obj.delegateAddCartPopup = self
                        obj.modalPresentationStyle = .overCurrentContext
                        obj.modalTransitionStyle = .crossDissolve
                        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblFabrics.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblFabrics.contentSize.height).rounded(toPlaces: 2)) - (Double(tblFabrics.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                getFabricList(search: self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
            }
        }
    }
}
