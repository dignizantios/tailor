//
//  FabricDetailsVc.swift
//  Tailory
//
//  Created by Haresh on 26/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import MXSegmentedPager
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class FabricDetailsVc: MXSegmentedPagerController {
    
    @IBOutlet var vwHeader: UIView!
    @IBOutlet weak var collectionImg: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var lblTailorTitle: UILabel!
    @IBOutlet weak var lblDesignAvailable: UILabel!
    @IBOutlet weak var vwStarRating: HCSStarRatingView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var btnReviews: UIButton!
    @IBOutlet weak var lblMinimumPrice: UILabel!
    @IBOutlet weak var lblMinimumPriceValue: UILabel!
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var btnFavorites: UIButton!    
    @IBOutlet weak var vwAvailabelDesign: UIView!
    @IBOutlet weak var btnIhaveOwnFabricOutlet: UIButton!
    @IBOutlet weak var vwHaveOwnFabric: UIView!
    
    //MARK: Variables
    var isStar = false
    var isFavorites = false
    var vwProgressHeaderValue = CGFloat()
    var handlerUpdateFabricList : (JSON) -> Void = {_ in}
    var selectedFav = isComeFromFav.notFromFav
    var vwAddCart = UIView()
    var isHaveOwnFabric = Bool()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        setUpSegment()
        registerXib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        vwAddCart = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.size.height - 70, width: UIScreen.main.bounds.size.width, height: 70))
        vwAddCart.backgroundColor = UIColor.white
        let btn = UIButton()
        btn.frame = CGRect(x: 20, y: 0, width: vwAddCart.frame.width - 40, height: 50)
        btn.addTarget(self, action: #selector(btnAddProductAction), for: .touchUpInside)
        btn.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btn.setTitle(getCommonString(key: "Add_to_cart_key"), for: .normal)
        vwAddCart.addSubview(btn)
//        self.view.addSubview(vwAddCart)
//        self.view.bringSubviewToFront(vwAddCart)
        vwAddCart.isHidden = false
        getFabricDetail()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: "")
        //        setUpNavigationBarWhiteWithTitleAndBack(strTitle: "", isBackButtonShow: false)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        if self.vwProgressHeaderValue == 0
        {
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.appThemeCyprusColor
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.appThemeCyprusColor
            }
            self.navigationController?.navigationBar.backgroundColor = UIColor.appThemeCyprusColor
            self.setHeaderTitle(title: dictFabricDetail["branch_name"].stringValue)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(vwAddCartButton), name: .manageAddCartButton, object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !isEnglish {
            self.segmentedPager.pager.showPage(at: 2, animated: false)
        }
        
    }    
    
    
    @objc func btnAddProductAction(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addToCartFabricDetail"), object: nil)
    }
    
    @objc func vwAddCartButton(notification:Notification){
//        if let obj = notification.object as? String{
//            if obj == "1" && strFabricAvailabel == "0" {
//                vwAddCart.isHidden = false
//                self.view.bringSubviewToFront(vwAddCart)
//            } else {
//                vwAddCart.isHidden = true
//            }
//        }
    }
    
}

//MARK: SetUp
extension FabricDetailsVc {
    
    func setUpUI() {
        
        btnIhaveOwnFabricOutlet.setTitle(getCommonString(key: "I_have_my_own_fabric_key"), for: .normal)
        btnIhaveOwnFabricOutlet.titleLabel?.font = themeFont(size: 18, fontname: .heavy)
        btnIhaveOwnFabricOutlet.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        
        if !isHaveOwnFabric{
            vwHaveOwnFabric.isHidden = true
        }
        
        if !isFromTailor{
            objCartModel = CartModel()
            objCartModel.category_id = enumCategoryId.fabrics.rawValue
            objCartModel.business_id = dictFabricDetail["branch_id"].stringValue
        }
        
        [lblTailorTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        [lblDesignAvailable, lblReviews].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeNobelColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [lblMinimumPrice, lblMinimumPriceValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeCyprusColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        vwAvailabelDesign.isHidden = true
        
        //        lblMinimumPrice.text = getCommonString(key: "Minimum_price_key")
        
    }
    
    func setUpSegment() {
        
        segmentedPager.backgroundColor = .white
        
        // Parallax Header
        segmentedPager.parallaxHeader.view = vwHeader
        segmentedPager.parallaxHeader.mode = .fill
        segmentedPager.parallaxHeader.height = 300
        
        if statusbarHeight < 21 {
            segmentedPager.parallaxHeader.minimumHeight = 66
        }
        else {
            segmentedPager.parallaxHeader.minimumHeight = 90
        }
        
        segmentedPager.segmentedControl.selectionIndicatorLocation = .down
        segmentedPager.segmentedControl.backgroundColor = .white
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.appThemeSilverColor]
        
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "AvenirLTStd-Heavy", size: 13.0)!]
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.appThemeSilverColor
        
        segmentedPager.segmentedControl.selectionStyle = .fullWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorHeight = 2
        
    }
    
    func registerXib() {
        
        collectionImg.register(UINib(nibName: "TailorsDetailCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TailorsDetailCollectionCell")
    }
    
    func setUpArabicUI() {
        self.vwHeader.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [lblTailorTitle].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblDesignAvailable, lblReviews].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblMinimumPrice, lblMinimumPriceValue].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        btnIhaveOwnFabricOutlet.contentHorizontalAlignment = .right
        btnIhaveOwnFabricOutlet.transform = CGAffineTransform(scaleX: -1, y: 1)
        
    }
    
    func setupData(dict:JSON) {
        
        lblTailorTitle.text = dict["branch_name"].stringValue
        vwStarRating.value = CGFloat(dict["avg_rate"].floatValue)
        lblReviews.text = "(\(dict["total_rating"].stringValue) \(getCommonString(key: "Review_key")))"
        lblDesignAvailable.text = "\(getCommonString(key: "Available_fabric_key")) \(dict["available_fabrics"].stringValue)"
        lblMinimumPrice.text = "\(getCommonString(key: "Minimum_price_key"))"
        lblMinimumPriceValue.text = "\(dict["minimum_price"].stringValue) \(getCommonString(key: "KD_key"))"
        if dict["is_favorite"].stringValue == "1"{
            btnFavorites.isSelected = true
        } else {
            btnFavorites.isSelected = false
        }
        if dict["is_review"].stringValue == "1"{
            btnStar.isSelected = true
        } else {
            btnStar.isSelected = false
        }
        pageController.numberOfPages = dict["branch_images"].arrayValue.count
        
        if dict["is_available"].stringValue != "1" {
            self.view.addSubview(vwAddCart)
            self.view.bringSubviewToFront(vwAddCart)
        }
        
        self.collectionImg.reloadData()
    }
    
    func redirectToAddCart(){
        var dict = objCartModel.dictProductJson["tailor"]
        dict["is_fabric_added"].stringValue = enumIsAddedFabric.notAddedFabric.rawValue
        dict["fabrics"] = []
        dict["quantity"].stringValue = objCartModel.quantity
        dict["is_laundry"].stringValue = enumIsAddedLaudry.notAddedLaudry.rawValue
        
        // call API
        if dict["measurement_type"].stringValue == "3" || dict["measurement_type"].stringValue == "4"{
            objCartModel.dictProductJson["tailor"] = dict
            print("final dict \(objCartModel.dictProductJson)")
            let obj = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyAppoitmentVC") as! MyAppoitmentVC
            self.navigationController?.pushViewController(obj, animated: true)
        } else{
            dict["is_exist_fabric_pickup_address_id"].stringValue = dict["is_exist_address_id"].stringValue
            dict["fabric_pickup_address_id"].stringValue = dict["address_id"].stringValue
            dict["fabrics_pickup_time"].stringValue = dict["time"].stringValue
            dict["fabrics_pickup_date"].stringValue = dict["date"].stringValue
            //            dict["fabrics_pickup_address"] = dict["address"]
            if getUserDetail("is_login_guest") == "1" {
                dict["fabrics_pickup_address"] = guestDictAddress
                dict["fabric_pickup_address_id"].stringValue = ""
                dict["is_exist_fabric_pickup_address_id"] = "0"
            }
            else {
                dict["fabrics_pickup_address"] = dict["address"]
            }
            dict["fabrics_notes"] = dict["extra_notes"]
            objCartModel.dictProductJson["tailor"] = dict
            print("final dict \(objCartModel.dictProductJson)")
            addProductToCart()
        }
    }
}

//MARK: Button Action
extension FabricDetailsVc {
    
    @IBAction func btnStarAction(_ sender: Any) {
        if getUserDetail("is_login_guest") == "1" {            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
            return
        }
        if dictFabricDetail["is_review"].stringValue == "1"{
            return
        }
        let rateTailorVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "RateTailorVC") as! RateTailorVC
        rateTailorVC.selectedController = .comeFromFabric
        rateTailorVC.handlerUpdateTailorDetail = {[weak self] in
            self?.getFabricDetail(isUpdate: true)
        }
        self.navigationController?.pushViewController(rateTailorVC, animated: true)
    }
    
    @IBAction func btnFavoritesAction(_ sender: Any) {
        
        if getUserDetail("is_login_guest") == "1" {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
        }
        else {
            if dictFabricDetail["is_favorite"].stringValue == "0"{
                dictFabricDetail["is_favorite"].stringValue = "1"
                setupData(dict: dictFabricDetail)
                addFavUnfav(status: "1")
            } else {
                dictFabricDetail["is_favorite"].stringValue = "0"
                setupData(dict: dictFabricDetail)
                addFavUnfav(status: "0")
            }
            self.handlerUpdateFabricList(dictFabricDetail)
        }
        /*
        if dictFabricDetail["is_favorite"].stringValue == "0"{
            addFavUnfav(status: "1")
        } else {
            addFavUnfav(status: "0")
        }
         */
    }
    
    @IBAction func btnReviewsAction(_ sender: Any) {        
        let reviewsVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "ReviewsVC") as! ReviewsVC
        reviewsVC.selectedParentController = .fabric
        self.navigationController?.pushViewController(reviewsVC, animated: true)
    }
    
    @IBAction func btnOwnFabricAction(_ sender:UIButton){
        let alertController = UIAlertController(title: getCommonString(key: "Tailory_key"), message: getCommonString(key: "The_date_of_picking_up_the_fabric_will_be_the_same_as_specified_here_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Ok_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.redirectToAddCart()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}

//MARK: collection Datasource/Delegate
extension FabricDetailsVc : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  dictFabricDetail["branch_images"].arrayValue.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TailorsDetailCollectionCell", for: indexPath) as! TailorsDetailCollectionCell
        let arrFabriceImage = dictFabricDetail["branch_images"].arrayValue
        let dict = arrFabriceImage[indexPath.row]
        cell.imgTailors.sd_setImage(with: dict["branch_image"].url, placeholderImage: UIImage(named: "ic_reviews_user_profile_splash_holder"), options: .lowPriority, completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

//MARK: ScrollView Delegate
extension FabricDetailsVc : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageController.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
}


//MARK: Segment Method
extension FabricDetailsVc {
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        if isEnglish
        {
            return [getCommonString(key: "Available_fabrics_key"),getCommonString(key: "More_info_key"),getCommonString(key: "Size_chart_key")][index]
        }
        else
        {
            return [getCommonString(key: "Size_chart_key"),getCommonString(key: "More_info_key"),getCommonString(key: "Available_fabrics_key")][index]
        }
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
        print("Progress: \(parallaxHeader.progress)")
        
        self.vwProgressHeaderValue = parallaxHeader.progress
        
        if parallaxHeader.progress <= 0.40 {
            
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.appThemeCyprusColor
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.appThemeCyprusColor
            }
            self.navigationController?.navigationBar.backgroundColor = UIColor.appThemeCyprusColor
            self.setHeaderTitle(title: dictFabricDetail["branch_name"].stringValue)
            
        }
        else {
           if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.clear
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.clear
            }
            self.navigationController?.navigationBar.backgroundColor = UIColor.clear
            self.setHeaderTitle(title: "")
        }
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, segueIdentifierForPageAt index: Int) -> String {
        
        if isEnglish
        {
            return ["mx_page_0","mx_page_1","mx_page_2"][index]
            
        }
        else
        {
            return ["mx_page_2","mx_page_1","mx_page_0"][index]
        }
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWith index: Int) {
        print("index \(index)")
        if isEnglish {
            if index == 0{
                self.vwAddCart.isHidden = false
            } else {
                self.vwAddCart.isHidden = true
            }
        } else {
            if index == 2{
                self.vwAddCart.isHidden = false
            } else {
                self.vwAddCart.isHidden = true
            }
        }
        
    }
    
    //MARK: - Set Header Title
    
    func setHeaderTitle(title:String)
    {
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = title
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 17, fontname: .heavy)
        
        self.navigationItem.titleView = HeaderLabel
    }
}

//MARK: - Product add to cart Delegate method

extension FabricDetailsVc: delegateProductAddCartPopup
{
    func redirectToGoToCart() {

        self.dismiss(animated: false, completion: nil)
        self.cartTapped()
        
//        self.navigationController?.popToRootViewController(animated: false)
        
        /*
        if isEnglish
        {
            appdelgate.tailerTabbarVC.selectedIndex = 3
        }
        else
        {
            appdelgate.tailerTabbarVC.selectedIndex = 1
        }
        */
    }
    
    func redirectToAddMore() {
        
        for vc in self.navigationController!.viewControllers {
            if let myVC = vc as? TailorsDetailVC {
                self.navigationController?.popToViewController(myVC, animated: true)
            }
        }
        
//        self.navigationController?.popToRootViewController(animated: false)
        
        /*
        if isEnglish
        {
            appdelgate.tailerTabbarVC.selectedIndex = 3
        }
        else
        {
            appdelgate.tailerTabbarVC.selectedIndex = 1
        }*/
        
    }
    
}

extension FabricDetailsVc {
    
    func getFabricDetail(isUpdate:Bool = false)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlFabric)\(urlFabricDetail)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "branch_id" : dictFabricDetail["branch_id"].stringValue
                    ]
            
            
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        dictFabricDetail = json["data"]
                        strFabricAvailabel = dictFabricDetail["is_available"].stringValue
                        self.setupData(dict: json["data"])
                        if isUpdate {
                            self.handlerUpdateFabricList(dictFabricDetail)
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func addFavUnfav(status:String)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlFavUnfav)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "is_favorite" : status,
                     "role" : dictFabricDetail["tailor_review_flag"].stringValue,
                     "business_id":dictFabricDetail["business_id"].stringValue]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
//                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if self.selectedFav == .notFromFav {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                        }
//                        dictFabricDetail["is_favorite"].stringValue = status
//                        self.handlerUpdateFabricList(dictFabricDetail)
//                        self.setupData(dict: dictFabricDetail)
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addProductToCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlAddToCart)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                     "category_id" : objCartModel.category_id,
                     "business_id" : objCartModel.business_id,
                     "quantity": objCartModel.quantity,
                     "product_cart_json": JSON(objCartModel.dictProductJson).rawString(),
                     "is_login_guest":getUserDetail("is_login_guest") == "1" ? "1" : "0"
                ] as! [String : String]
            
            print("param :\(param)")
            
            //            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        //                        makeToast(strMessage: json["msg"].stringValue)
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProductAddToCartVc") as! ProductAddToCartVc
                        obj.isSaveForLater = false
                        obj.delegateAddCartPopup = self
                        obj.modalPresentationStyle = .overCurrentContext
                        obj.modalTransitionStyle = .crossDissolve
                        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}

