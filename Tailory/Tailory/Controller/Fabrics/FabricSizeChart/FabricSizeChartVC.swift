//
//  FabricSizeChartVC.swift
//  Tailory
//
//  Created by Jaydeep on 23/10/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class FabricSizeChartVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblComingSoon: UILabel!
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblComingSoon.text = getCommonString(key: "Coming_soon_key")
        
        [lblComingSoon].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeSilverColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
    }

}
