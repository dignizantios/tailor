//
//  FabricInfoVc.swift
//  
//
//  Created by Haresh on 26/07/19.
//

import UIKit
import WebKit

class FabricMoreInfoVc: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblMoreInfoData: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        NotificationCenter.default.post(name: .manageAddCartButton, object: "1")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpUi()
    }
    
}

//MARK: SetUp
extension FabricMoreInfoVc {
    
    func setUpUi() {
        [lblMoreInfoData].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
//         self.lblMoreInfoData.attributedText = dictFabricDetail["description"].stringValue.htmlToAttributedString
        let webView = WKWebView()
        webView.scrollView.delegate = self
        webView.isMultipleTouchEnabled = false
        webView.frame = self.vwMain.frame
        webView.loadHTMLString(dictFabricDetail["description"].stringValue, baseURL: nil)
        self.vwMain.addSubview(webView)
        /*
        if !isEnglish {
            webView.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
         */
    }
    
    func setUpArabicUI() {
//        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [lblMoreInfoData].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
    }
}

//MARK: - UIScrollViewDelegate
extension FabricMoreInfoVc : UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}
