//
//  FabricMeasurmentVC.swift
//  Tailory
//
//  Created by Khushbu on 24/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class FabricMeasurmentVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblMeters: UILabel!
    @IBOutlet weak var txtMeterWant: CustomTextField!
    @IBOutlet weak var btnAddToCart: CustomButton!
    
    
    //MARK: Variables
    
    
    
    //MARK: Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        setUpArabicUI()
        navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Measurment_key"))
    }
}

//MARK: SetUp
extension FabricMeasurmentVC {
    
    func setUpUI() {
        
        [lblMeters].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
            lbl?.text = getCommonString(key: "Meters_key")
        }
        
        [txtMeterWant].forEach { (txt) in
            txt?.textColor = UIColor.appThemeLightGrayColor
            txt?.font = themeFont(size: 15, fontname: .heavy)
            txt?.placeholder = getCommonString(key: "How_many_meters_you_want_key")
        }
        
        [btnAddToCart].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
            btn?.setTitle(getCommonString(key: "Add_to_cart_key"), for: .normal)
        }
        
        addDoneButtonOnKeyboard(textfield: txtMeterWant)
    }
    
    func setUpArabicUI() {
        
        if !isEnglish {
            self.view.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblMeters].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
            [txtMeterWant].forEach { (txt) in
                txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
                txt?.textAlignment = .right
            }
            
            [btnAddToCart].forEach { (btn) in
                btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
        }
    }
}


//MARK: Button Action
extension FabricMeasurmentVC {
    
    @IBAction func btnAddToCartAction(_ sender: Any) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProductAddToCartVc") as! ProductAddToCartVc
        obj.isCheckVC = .fabrics
        obj.delegateAddCartPopup = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
        
        
    }

}

//MARK: - Product add to cart Delegate method

extension FabricMeasurmentVC: delegateProductAddCartPopup
{
    func redirectToGoToCart() {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LaundryPopupVc") as! LaundryPopupVc
        obj.delegateWantLaundry = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
        
    }
    
    func redirectToAddMore() {
        
    }
    
}

//MARK: - Laundry Delegate method

extension FabricMeasurmentVC: delegateWantLaundryPopup
{
    
    func yesForLaundry() {
        
        self.navigationController?.popToRootViewController(animated: false)
        
        if isEnglish
        {
            appdelgate.tailerTabbarVC.selectedIndex = 3
        }
        else
        {
            appdelgate.tailerTabbarVC.selectedIndex = 1
        }
        
    }
    
    func noForLaundry() {
        
        self.navigationController?.popToRootViewController(animated: false)
        
        if isEnglish
        {
            appdelgate.tailerTabbarVC.selectedIndex = 3
        }
        else
        {
            appdelgate.tailerTabbarVC.selectedIndex = 1
        }
    }
    
}
