 //
//  AvailableFabricsVC.swift
//  Tailory
//
//  Created by Khushbu on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire
import DropDown
import ImageSlideshow

class AvailableFabricsVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblAvailableFabrics: UITableView!
    @IBOutlet weak var btnCountinue: CustomButton!
    
    
    //MARK: Variables
//     var arrayData : [JSON] = []
    var arrAvailabelFabric :[JSON] = []
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var strMessage = String()
    var dictSelectedFabric = JSON()
    var sliderShow = ImageSlideshow()
    
    //MARK: Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
//        setUpData()
        setUpArabicUI()
        registerXib()
//        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "<#T##String#>"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(btnCountinueAction(_:)), name: NSNotification.Name(rawValue: "addToCartFabricDetail"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "addToCartFabricDetail"), object: nil)
    }
    
}


//MARK: SetUp
extension AvailableFabricsVC {
    
    func setUpUI() {
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblAvailableFabrics.addSubview(upperReferesh)
        
        tblAvailableFabrics.tableFooterView = UIView()
        
        /*[btnCountinue].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
            btn?.setTitle(getCommonString(key: "Add_to_cart_key"), for: .normal)
        }
        
        btnCountinue.isHidden = true*/
        
        upperRefreshTable()
    }

    
    func registerXib() {
        
        tblAvailableFabrics.register(UINib(nibName: "AvailableFabricsTableCell", bundle: nil), forCellReuseIdentifier: "AvailableFabricsTableCell")
    }
    
    func setUpArabicUI() {
        
        if !isEnglish {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            btnCountinue.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
}


//MARK: Button Action
extension AvailableFabricsVC {
    
    @IBAction func btnCountinueAction(_ sender: Any) {
        /*
        let fabricVC = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricMeasurmentVC") as! FabricMeasurmentVC
        
        self.navigationController?.pushViewController(fabricVC, animated: true)
         */
        
        if isFromTailor == true  // Tailor&Fabric
        {
            if dictSelectedFabric["cart_item"].stringValue == "0" || dictSelectedFabric["cart_item"].stringValue == "" || dictSelectedFabric["is_selected"].stringValue == "0"{
                makeToast(strMessage: getCommonString(key: "Please_add_some_item_in_fabric_key"))
                return
            }
            var arrSelectedFabric:[JSON] = []
            var dict = objCartModel.dictProductJson["tailor"]
            if dict["is_fabric_added"].stringValue == "0" {
                dict["time"].stringValue = ""
                dict["date"].stringValue = ""
            }            
            dict["fabrics_pickup_date"].stringValue = ""
            dict["fabrics_pickup_time"].stringValue = ""
            var dictFabric = JSON()
            dictFabric["tbl_fabric_style_id"].stringValue = dictSelectedFabric["tbl_fabric_style_id"].stringValue
            dictFabric["tbl_fabric_design_id"].stringValue = dictSelectedFabric["tbl_fabric_design_id"].stringValue
            dictFabric["fabricdesign_name"].stringValue = dictSelectedFabric["fabricdesign_name"].stringValue
            dictFabric["price"].stringValue = dictSelectedFabric["price"].stringValue
            dictFabric["branch_id"].stringValue = dictFabricDetail["branch_id"].stringValue
            dictFabric["quantity"].stringValue = dictSelectedFabric["cart_item"].stringValue
            dictFabric["is_diff_branch"].stringValue = isAlreadyData
            arrSelectedFabric.append(dictFabric)
            dict["fabrics"] = JSON(arrSelectedFabric)
            objCartModel.dictProductJson["tailor"] = dict
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LaundryPopupVc") as! LaundryPopupVc
            obj.delegateWantLaundry = self
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
        }
        else  // Only Fabric
        {
//            self.openAddToCartMethod()
            if dictSelectedFabric["cart_item"].stringValue == "0" || dictSelectedFabric["cart_item"].stringValue == "" || dictSelectedFabric["is_selected"].stringValue == "0"{
                makeToast(strMessage: getCommonString(key: "Please_select_at_least_one_fabric_key"))
                return
            }
            objCartModel.quantity = dictSelectedFabric["cart_item"].stringValue
            var arrSelectedFabric:[JSON] = []
            var dict = JSON()
            dict["tbl_fabric_style_id"].stringValue = dictSelectedFabric["tbl_fabric_style_id"].stringValue
            dict["tbl_fabric_design_id"].stringValue = dictSelectedFabric["tbl_fabric_design_id"].stringValue
            dict["fabricdesign_name"].stringValue = dictSelectedFabric["fabricdesign_name"].stringValue
            dict["price"].stringValue = dictSelectedFabric["price"].stringValue
            dict["branch_id"].stringValue = dictFabricDetail["branch_id"].stringValue
            dict["quantity"].stringValue = dictSelectedFabric["cart_item"].stringValue
            dict["is_diff_branch"].stringValue = isAlreadyData
            arrSelectedFabric.append(dict)
            objCartModel.dictProductJson["fabrics"] = JSON(arrSelectedFabric)
            print("final \(objCartModel.dictProductJson)")
            addProductToCart()
//             checkFabricAdded()
        }
        
    }
    
    @IBAction func btnAddCartAction(_ sender:UIButton){
        
        let tag = Int(sender.title(for: .disabled) ?? "0")!
        
        for i in 0..<self.arrAvailabelFabric[tag]["designs"][sender.tag].count {
            var dict = self.arrAvailabelFabric[tag]["designs"][i]
            dict["is_selected"].stringValue = "0"
            self.arrAvailabelFabric[tag]["designs"][i] = dict
        }
        self.arrAvailabelFabric[tag]["designs"][sender.tag]["is_selected"].stringValue = "1"
        var dict = self.arrAvailabelFabric[tag]["designs"][sender.tag]
        var item = dict["cart_item"].intValue
        item += 1
        dict["cart_item"].intValue = item
        self.arrAvailabelFabric[tag]["designs"][sender.tag] = dict
        dictSelectedFabric = self.arrAvailabelFabric[tag]["designs"][sender.tag]
        tblAvailableFabrics.reloadRows(at: [IndexPath(row: 0, section: tag)], with: .none)
  
    }
    
    @IBAction func btnMinusCartAction(_ sender:UIButton){
        
        let tag = Int(sender.title(for: .disabled) ?? "0")!
        
        for i in 0..<self.arrAvailabelFabric[tag]["designs"][sender.tag].count {
            var dict = self.arrAvailabelFabric[tag]["designs"][i]
            dict["is_selected"].stringValue = "0"
            self.arrAvailabelFabric[tag]["designs"][i] = dict
        }
        self.arrAvailabelFabric[tag]["designs"][sender.tag]["is_selected"].stringValue = "1"
        
        var dict = self.arrAvailabelFabric[tag]["designs"][sender.tag]
        var item = dict["cart_item"].intValue
        if item == 1{
            dictSelectedFabric = self.arrAvailabelFabric[tag]["designs"][sender.tag]
            tblAvailableFabrics.reloadRows(at: [IndexPath(row: 0, section: tag)], with: .none)
            return
        }
        item -= 1
        dict["cart_item"].intValue = item
        self.arrAvailabelFabric[tag]["designs"][sender.tag] = dict
        dictSelectedFabric = self.arrAvailabelFabric[tag]["designs"][sender.tag]
        tblAvailableFabrics.reloadRows(at: [IndexPath(row: 0, section: tag)], with: .none)
    }

}

 //MARK: - Product add to cart Delegate method
 
 extension AvailableFabricsVC: delegateProductAddCartPopup
 {
    func redirectToGoToCart() {
        
        self.dismiss(animated: false, completion: nil)
        self.cartTapped()
        
//        self.navigationController?.popToRootViewController(animated: false)

//        if isEnglish
//        {
//            appdelgate.tailerTabbarVC.selectedIndex = 3
//        }
//        else
//        {
//            appdelgate.tailerTabbarVC.selectedIndex = 1
//        }
        
    }
    
    func redirectToAddMore() {
        
        if isFromTailor {
            for vc in self.navigationController!.viewControllers {
                if let myVC = vc as? TailorsDetailVC {
                    self.navigationController?.popToViewController(myVC, animated: true)
                }
            }
        } else {
          
//            self.navigationController?.popToRootViewController(animated: false)
        }
        
//        if isEnglish
//        {
//            appdelgate.tailerTabbarVC.selectedIndex = 3
//        }
//        else
//        {
//            appdelgate.tailerTabbarVC.selectedIndex = 1
//        }

    }
    
 }
 
 //MARK: - Laundry Delegate method
 
 extension AvailableFabricsVC: delegateWantLaundryPopup
 {
    
    func yesForLaundry() {
        var dict = objCartModel.dictProductJson["tailor"]
        dict["is_fabric_added"].stringValue = enumIsAddedFabric.addedFabric.rawValue
        dict["quantity"].stringValue = objCartModel.quantity
        dict["is_laundry"].stringValue = enumIsAddedLaudry.addedLaudry.rawValue
        objCartModel.dictProductJson["tailor"] = dict
        addProductToCart()
//        checkFabricAdded()
        // call API
    }
    
    func noForLaundry() {
        var dict = objCartModel.dictProductJson["tailor"]
        dict["is_fabric_added"].stringValue = enumIsAddedFabric.addedFabric.rawValue
        dict["quantity"].stringValue = objCartModel.quantity
        dict["is_laundry"].stringValue = enumIsAddedLaudry.notAddedLaudry.rawValue
        objCartModel.dictProductJson["tailor"] = dict
        addProductToCart()
//        checkFabricAdded()
        // call API
    }
    
    func openAddToCartMethod()
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProductAddToCartVc") as! ProductAddToCartVc
        
        if isFromTailor == false {
            obj.isCheckVC = .fabrics
        }
        
        obj.delegateAddCartPopup = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
    }
    
 }
 

 

//MARK: TableView Datasource/Delegate
extension AvailableFabricsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if arrAvailabelFabric.count == 0{
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            btnCountinue.isHidden = true
            return 0
        }
        btnCountinue.isHidden = false
        tableView.backgroundView = nil
        return arrAvailabelFabric.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrAvailabelFabric[section]["is_selected"].stringValue == "0" {
            return 0
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableFabricsTableCell", for: indexPath) as! AvailableFabricsTableCell
        cell.vwOfFooter.isHidden = true
        cell.widthOfFooter.constant = 0
        cell.collectionAvailableFabrics.register(UINib(nibName: "AvailableFabricsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AvailableFabricsCollectionCell")
        cell.collectionAvailableFabrics.tag = (indexPath.section * 1000) + indexPath.row
        cell.collectionAvailableFabrics.delegate = self
        cell.collectionAvailableFabrics.dataSource = self
        cell.collectionAvailableFabrics.reloadData()
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = AvailableFabricsHeaderView.instanceFromNib() as?  AvailableFabricsHeaderView
        
        headerView?.lblTitle.text = arrAvailabelFabric[section]["style_name"].stringValue
        headerView?.btnSectionSelected.tag = section
        headerView?.setUpUI()
        headerView?.btnSectionSelected.addTarget(self, action: #selector(sectionTapped(_:)), for: .touchUpInside)

        if arrAvailabelFabric[section]["is_selected"] == "0" {
            headerView?.btnImgArrow.isSelected = false
        }
        else {
            headerView?.btnImgArrow.isSelected = true
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    @objc func sectionTapped(_ sender: UIButton) {
        if arrAvailabelFabric[sender.tag]["is_selected"] == "1" {
            return
        }
        for i in 0..<arrAvailabelFabric.count {
            arrAvailabelFabric[i]["is_selected"] = "0"
            tblAvailableFabrics.reloadSections(IndexSet(arrayLiteral: i), with: .automatic)
        }
        arrAvailabelFabric[sender.tag]["is_selected"] = "1"
        tblAvailableFabrics.reloadSections(IndexSet(arrayLiteral: sender.tag), with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
}


//MARK: CollectionView Datasource/Delegate
extension AvailableFabricsVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let row = collectionView.tag / 1000
        print("row \(row)")
        return arrAvailabelFabric[row]["designs"].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvailableFabricsCollectionCell", for: indexPath) as! AvailableFabricsCollectionCell
        let row = collectionView.tag / 1000
        
        let dict = arrAvailabelFabric[row]["designs"][indexPath.row]
        cell.lblTitle.text = dict["fabricdesign_name"].stringValue
        cell.imgTitle.sd_setImage(with: dict["fabric_design_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
        sliderShow.setImageInputs([SDWebImageSource(url: dict["fabric_design_image"].url!, placeholder: UIImage(named: "ic_taylor_details_more_info_splash_holder"))])
        cell.btnEnlargeImage.addTarget(self, action: #selector(presentEnlargedProductImage), for: .touchUpInside)
        cell.lblPrice.text =  "\(dict["price"].stringValue) \(getCommonString(key: "KD_key"))"
        
        if dict["is_selected"].stringValue != "0" {
            cell.imgTitle.layer.borderColor = UIColor.appThemeSilverColor.cgColor
            cell.imgTitle.layer.borderWidth = 3
        }
        else {
            cell.imgTitle.layer.borderWidth = 0
        }
        
        //--- Available stock       
        var strArrAvailabelStock:[String] = []
        if dict["available_stock"].doubleValue > 100 {
            for i in stride(from: 0.5, through: 100, by: 0.5){
                if !isEnglish {
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .decimal
                    formatter.maximumFractionDigits = 2
                    formatter.locale = Locale(identifier: "ar")
                    if let ar = formatter.string(for: i) {
                        strArrAvailabelStock.append("\(ar)")
                    }
                }
                else {
                    strArrAvailabelStock.append("\(i)")
                }
            }
        }
        else {
            for i in stride(from: 0.5, through: dict["available_stock"].doubleValue, by: 0.5){
                if !isEnglish {
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .decimal
                    formatter.maximumFractionDigits = 2
                    formatter.locale = Locale(identifier: "ar")
                    if let ar = formatter.string(for: i) {
                        strArrAvailabelStock.append("\(ar)")
                    }
                }
                else {
                    strArrAvailabelStock.append("\(i)")
                }
            }
        }
        print("strArrAvailabelStock \(strArrAvailabelStock)")
        
        if dict["available_stock"].doubleValue <= 0{
            cell.lblOutOfStock.isHidden = false
        } else {
            cell.lblOutOfStock.isHidden = true
        }
        
        cell.quantityDD.dataSource = strArrAvailabelStock
        cell.handlorQuantityDD = {
            self.configureDropdown(dropdown: cell.quantityDD, sender: cell.btnQuantityDD)
            cell.quantityDD.direction = .bottom
            cell.quantityDD.width = cell.btnQuantityDD.frame.width + 10
            cell.quantityDD.show()
        }
        
        cell.lblCartItem.text = dict["cart_item"].stringValue
        
        cell.quantityDD.selectionAction = { (index: Int, item: String) in
            
            for i in 0..<self.arrAvailabelFabric[row]["designs"][indexPath.row].count
            {
                var dict = self.arrAvailabelFabric[row]["designs"][i]
                dict["is_selected"].stringValue = "0"
                self.arrAvailabelFabric[row]["designs"][i] = dict
            }
            print(item)
            self.arrAvailabelFabric[row]["designs"][indexPath.row]["cart_item"].stringValue = item
            self.arrAvailabelFabric[row]["designs"][indexPath.row]["is_selected"].stringValue = "1"
            print("Array:", self.arrAvailabelFabric[row]["designs"])
            self.dictSelectedFabric = self.arrAvailabelFabric[row]["designs"][indexPath.row]
            collectionView.reloadData()
            self.checkFabricAdded()
        }
        
        cell.btnMinusOutlet.setTitle("\(row)", for: .disabled)
        cell.btnAddOutlet.setTitle("\(row)", for: .disabled)
        
        cell.btnAddOutlet.tag = indexPath.row
        cell.btnMinusOutlet.tag = indexPath.row
//        cell.lblCartItem.text = dict["cart_item"].stringValue
        cell.btnAddOutlet.addTarget(self, action: #selector(btnAddCartAction(_:)), for: .touchUpInside)
        cell.btnMinusOutlet.addTarget(self, action: #selector(btnMinusCartAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func formatCurrency(value: Double) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale(identifier: "fa_IR")
        let result = formatter.string(from: value as NSNumber)
        return result!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let row = collectionView.tag / 1000
        let dict = arrAvailabelFabric[row]["designs"][indexPath.row]
        if dict["available_stock"].intValue <= 0{
            return
        }
        for i in 0..<self.arrAvailabelFabric[row]["designs"][indexPath.row].count
        {
            var dict = self.arrAvailabelFabric[row]["designs"][i]
            dict["is_selected"].stringValue = "0"
            self.arrAvailabelFabric[row]["designs"][i] = dict
        }
        self.arrAvailabelFabric[row]["designs"][indexPath.row]["is_selected"].stringValue = "1"
        dictSelectedFabric = self.arrAvailabelFabric[row]["designs"][indexPath.row]
        collectionView.reloadData()
        checkFabricAdded()
//        sliderShow.presentFullScreenController(from: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastSectionIndex = collectionView.numberOfSections - 1
        let lastRowIndex = collectionView.numberOfItems(inSection: lastSectionIndex) - 1
//        let row = collectionView.tag % 1000
        let section = collectionView.tag / 1000
        let intFabricsPageOffset = self.arrAvailabelFabric[section]["next_offset"].intValue
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intFabricsPageOffset != -1 && intFabricsPageOffset != 0{
            print("this is the last cell")
            if let cell = tblAvailableFabrics.cellForRow(at: IndexPath(row: 0, section: section)) as? AvailableFabricsTableCell{
                cell.widthOfFooter.constant = 40
                cell.vwOfFooter.isHidden = false
                cell.activityIndicatorview.startAnimating()
                cell.activityIndicatorview.tintColor = .black
                cell.activityIndicatorview.hidesWhenStopped = true
                getExtraFabricsDesign(tag: section, currentIndex: lastRowIndex)
            }
        } else {
            if let cell = tblAvailableFabrics.cellForRow(at: IndexPath(item: 0, section: section)) as? AvailableFabricsTableCell {
                 cell.widthOfFooter.constant = 0
                 cell.vwOfFooter.isHidden = true
                 cell.activityIndicatorview.stopAnimating()
            }
        }
    }
    
    @objc func presentEnlargedProductImage()
    {
        self.sliderShow.presentFullScreenController(from: self)
    }
}
 
 //MARK:- Service
 extension AvailableFabricsVC {
    
    @objc func upperRefreshTable()
    {
        intOffset = 0
        getAvailabelFabrics()
    }
    
    func getAvailabelFabrics()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlFabric)\(urlFabricStyleList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "offset" : String(self.intOffset),
                          "branch_id" : dictFabricDetail["branch_id"].stringValue
            ]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrAvailabelFabric = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblAvailableFabrics.reloadData()
                            NotificationCenter.default.post(name: .manageAddCartButton, object: "0")
                            return
                        }
                        NotificationCenter.default.post(name: .manageAddCartButton, object: "1")
                        if self.intOffset == 0
                        {
                            self.arrAvailabelFabric = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        for i in 0..<aryData.count{
                            var dict = aryData[i]
                            dict["is_selected"].stringValue = "0"
                            if i == 0 {
                                dict["is_selected"].stringValue = "1"
                            }
                            var arrDesign = dict["designs"].arrayValue
                            for j in 0..<arrDesign.count{
                                var dictDesign = arrDesign[j]
                                dictDesign["is_selected"] = "0"
                                dictDesign["cart_item"].stringValue = "0.5"
                                arrDesign[j] = dictDesign
                            }
                            dict["designs"] = JSON(arrDesign)
                            aryData[i] = dict
                        }
                        aryData = self.arrAvailabelFabric + aryData
                        
                        self.arrAvailabelFabric = aryData
    
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrAvailabelFabric = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblAvailableFabrics.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func getExtraFabricsDesign(tag:Int,currentIndex:Int)
    {
        self.view.endEditing(true)
        var intFabricsPageOffset = self.arrAvailabelFabric[tag]["next_offset"].intValue
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlFabric)\(urlFabricStyleDesign)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "offset" : String(intFabricsPageOffset),
                          "branch_id" : dictFabricDetail["branch_id"].stringValue,
                          "tbl_fabric_style_id" : self.arrAvailabelFabric[tag]["tbl_fabric_style_id"].stringValue]
            
            
            print("param : \(param)")
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        intFabricsPageOffset = self.arrAvailabelFabric[tag]["next_offset"].intValue
                        
                        if intFabricsPageOffset < 0
                        {
                            return
                        }
                        
                        self.arrAvailabelFabric[tag]["next_offset"] = JSON(json["next_offset"])
                        var arrDesign = self.arrAvailabelFabric[tag]["designs"].arrayValue
                        var arrNewDesign = json["data"].arrayValue
                        for j in 0..<arrNewDesign.count{
                            var dictDesign = arrNewDesign[j]
                            dictDesign["is_selected"] = "0"
                            dictDesign["cart_item"].stringValue = "0.5"
                            arrNewDesign[j] = dictDesign
                        }
                        arrDesign = arrDesign + arrNewDesign
                        self.arrAvailabelFabric[tag]["designs"] = JSON(arrDesign)
                        self.tblAvailableFabrics.reloadRows(at: [IndexPath(row: 0, section: tag)], with: .none)
                        if let cell = self.tblAvailableFabrics.cellForRow(at: IndexPath(item: 0, section: tag)) as? AvailableFabricsTableCell {
                            cell.widthOfFooter.constant = 0
                            cell.vwOfFooter.isHidden = true
                            cell.activityIndicatorview.stopAnimating()
                            cell.collectionAvailableFabrics.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at: .centeredHorizontally, animated: true)
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrAvailabelFabric = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblAvailableFabrics.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addProductToCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlAddToCart)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                     "category_id" : objCartModel.category_id,
                     "business_id" : objCartModel.business_id,
                     "quantity": objCartModel.quantity,
                     "is_login_guest": getUserDetail("is_login_guest") == "0" ? "0" : "1",
                     "product_cart_json": JSON(objCartModel.dictProductJson).rawString()
                ] as! [String : String]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
//                        makeToast(strMessage: json["msg"].stringValue)
                        self.openAddToCartMethod()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblAvailableFabrics.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblAvailableFabrics.contentSize.height).rounded(toPlaces: 2)) - (Double(tblAvailableFabrics.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                getAvailabelFabrics()
            }
        }
    }
    
    func checkFabricAdded()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
           
            let url = "\(urlUser)\(urlCheckCartItem)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "category_id" : enumCategoryId.fabrics.rawValue,
                          "business_id" : dictFabricDetail["business_id"].stringValue,
                          "branch_id": dictFabricDetail["branch_id"].stringValue
//                          "is_fabric_added": ""
            ]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
//                self.showLoader()
            }
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
//                        self.addProductToCart()
                        //self.sliderShow.presentFullScreenController(from: self)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                       self.showAlert(msg: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func showAlert(msg:String){
        let alertController = UIAlertController(title: getCommonString(key: "Tailory_key"), message: msg, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            isAlreadyData = "1"
            //self.sliderShow.presentFullScreenController(from: self)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
            isAlreadyData = "0"
            //self.sliderShow.presentFullScreenController(from: self)
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
 }
