//
//  AvailableFabricsCollectionCell.swift
//  Tailory
//
//  Created by Khushbu on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import DropDown

class AvailableFabricsCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblMeterTitle: UILabel!
    @IBOutlet weak var btnMinusOutlet: UIButton!
    @IBOutlet weak var btnAddOutlet: UIButton!
    @IBOutlet weak var lblCartItem: UILabel!
    @IBOutlet weak var vwCart: UIView!
    @IBOutlet var btnQuantityDD: UIButton!
    @IBOutlet weak var lblOutOfStock: UILabel!
    @IBOutlet weak var btnEnlargeImage: UIButton!

    //MARK: Variables
    var handlorQuantityDD:() -> Void = {}
    var quantityDD = DropDown()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        lblPrice.textColor = UIColor.appThemeSilverColor
        lblPrice.font = themeFont(size: 10, fontname: .light)
        
        lblMeterTitle.textColor = UIColor.appThemeSilverColor
        lblMeterTitle.font = themeFont(size: 10, fontname: .light)
        lblMeterTitle.text = getCommonString(key: "Meter_key")
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 16, fontname: .book)
        }
        
        lblOutOfStock.textColor = UIColor.red
        lblOutOfStock.font = themeFont(size: 15, fontname: .heavy)
        lblOutOfStock.text = getCommonString(key: "Out_of_stock_key")
        
        lblCartItem.textColor = UIColor.appThemeBlackColor
        lblCartItem.font = themeFont(size: 15, fontname: .heavy)
        
        [imgTitle].forEach { (img) in
            img?.clipsToBounds = true
            img?.layer.cornerRadius = 5
        }
    }

    
    func setUpArabicUI() {
        
        [lblTitle,lblPrice, lblCartItem, lblMeterTitle].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
//         vwCart.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [imgTitle].forEach { (img) in
            img?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    @IBAction func btnQuantityAction(_ sender: Any) {
        handlorQuantityDD()
    }
}
