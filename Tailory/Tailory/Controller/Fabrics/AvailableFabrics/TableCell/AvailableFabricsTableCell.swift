//
//  AvailableFabricsTableCell.swift
//  Tailory
//
//  Created by Khushbu on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class AvailableFabricsTableCell: UITableViewCell {

    
    //MARK: Outlets
    @IBOutlet weak var collectionAvailableFabrics: UICollectionView!
    @IBOutlet weak var vwOfFooter: UIView!
    @IBOutlet weak var widthOfFooter: NSLayoutConstraint!
    @IBOutlet weak var activityIndicatorview: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
