//
//  AvailableFabricsHeaderView.swift
//  Tailory
//
//  Created by Khushbu on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class AvailableFabricsHeaderView: UIView {

    //MARK: Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnImgArrow: UIButton!
    @IBOutlet weak var btnSectionSelected: UIButton!

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "AvailableFabricsHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AvailableFabricsHeaderView
    }
    
    
    func setUpUI() {
        if !isEnglish {
            setUpArabicUI()
        }
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
    }
    
    func setUpArabicUI() {
        [lblTitle].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
}
