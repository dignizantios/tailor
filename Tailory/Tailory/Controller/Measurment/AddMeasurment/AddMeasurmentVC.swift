//
//  AddMeasurmentVC.swift
//  Tailory
//
//  Created by Khushbu on 23/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

enum measurmentForEditOrAdd
{
    case edit
    case add
    case cart
}



class AddMeasurmentVC: UIViewController {

    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtTitle: CustomTextField!
    @IBOutlet weak var lblChest: UILabel!
    @IBOutlet weak var txtChest: CustomTextField!
    @IBOutlet weak var lblAnkle: UILabel!
    @IBOutlet weak var txtAnkle: CustomTextField!
    @IBOutlet weak var lblArm: UILabel!
    @IBOutlet weak var txtArm: CustomTextField!
    @IBOutlet weak var lblSholder: UILabel!
    @IBOutlet weak var txtSholder: CustomTextField!
    @IBOutlet weak var lblNeck: UILabel!
    @IBOutlet weak var txtNeck: CustomTextField!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var txtHeight: CustomTextField!
    @IBOutlet weak var lblWaist: UILabel!
    @IBOutlet weak var txtWaist: CustomTextField!
    @IBOutlet weak var lblThight: UILabel!
    @IBOutlet weak var txtThight: CustomTextField!
    @IBOutlet weak var lblBack: UILabel!
    @IBOutlet weak var txtBack: CustomTextField!
    @IBOutlet weak var lblExtraNotes: UILabel!
    @IBOutlet weak var txtVwExtraNotes: CustomTextview!
    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var lblDefaultMeasurment: UILabel!
    @IBOutlet var vwAddMeasurment: UIView!
    @IBOutlet weak var btnSave: CustomButton!
    
    //MARK: Variables
    
    var selectedController = measurmentForEditOrAdd.add
    var dictMeasurment = JSON()
    var strSelectedAsDefault = "0"
    var handlerMyMeasurment:() -> Void = {}
    var dictCart = JSON()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpArabicUI()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedController == .add || selectedController == .cart
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Add_measurement_key"))

        }
        else if selectedController == .edit
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Edit_measurement_key"))
        }
        
    }
    
}


//MARK: SetUp
extension AddMeasurmentVC {
    
    func setUpUI() {
        
        [lblTitle,lblChest, lblAnkle, lblArm, lblSholder, lblNeck, lblHeight, lblWaist, lblThight, lblBack, lblExtraNotes, lblDefaultMeasurment].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .medium)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        [txtTitle,txtChest, txtAnkle, txtArm, txtSholder, txtNeck, txtHeight, txtWaist, txtThight, txtBack].forEach { (txt) in
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.textColor = UIColor.appThemeLightGrayColor
            txt?.keyboardType = .numberPad
            txt?.placeholder = getCommonString(key: "in_cm_key")
        }
        
        [txtChest, txtAnkle, txtArm, txtSholder, txtNeck, txtHeight, txtWaist, txtThight, txtBack].forEach { (txt) in
            addDoneButtonOnKeyboard(textfield: txt)
        }        
        
        txtTitle.keyboardType = .default
        txtTitle.placeholder = getCommonString(key: "Enter_here_key")
        
        lblTitle.text = "\(getCommonString(key: "Title_key")) *"
        lblChest.text = "\(getCommonString(key: "Chest_key")) *"
        lblAnkle.text = "\(getCommonString(key: "Ankle_key")) *"
        lblArm.text = "\(getCommonString(key: "Arm_key")) *"
        lblSholder.text = "\(getCommonString(key: "Sholders_key")) *"
        lblNeck.text = "\(getCommonString(key: "Neck_key")) *"
        lblHeight.text = getCommonString(key: "Height_key")
        lblWaist.text = getCommonString(key: "Waist_key")
        lblThight.text = getCommonString(key: "Thight_key")
        lblBack.text = getCommonString(key: "Back_key")
        lblExtraNotes.text = getCommonString(key: "Extra_notes_key")
        lblDefaultMeasurment.text = getCommonString(key: "Set_as_default_measurment_key")
        
        txtVwExtraNotes.textColor = UIColor.appThemeLightGrayColor
        txtVwExtraNotes.font = themeFont(size: 15, fontname: .medium)
        
        [btnSave].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
            btn?.setTitle(getCommonString(key: "Save_key"), for: .normal)
        }
        
        if selectedController == .edit || selectedController == .cart{
            setupEditData()
        }
        
        if getUserDetail("is_login_guest") == "1" {
            self.vwAddMeasurment.isHidden = true
        }
        else {
            self.vwAddMeasurment.isHidden = false
        }
    }
    
    func setUpArabicUI() {
        
        if !isEnglish {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblTitle,lblChest, lblAnkle, lblArm, lblSholder, lblNeck, lblHeight, lblWaist, lblThight, lblBack, lblExtraNotes, lblDefaultMeasurment].forEach { (lbl) in
                lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            [txtTitle,txtChest, txtAnkle, txtArm, txtSholder, txtNeck, txtHeight, txtWaist, txtThight, txtBack].forEach { (txt) in
                txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
                txt?.textAlignment = .right
            }
            
            txtVwExtraNotes.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtVwExtraNotes.textAlignment = .right
            
            btnSelection.transform = CGAffineTransform(scaleX: -1, y: 1)
            btnSave.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func setupEditData(){
        self.txtTitle.text = dictMeasurment["title"].stringValue
        self.txtChest.text = dictMeasurment["chest"].stringValue
        self.txtAnkle.text = dictMeasurment["ankle"].stringValue
        self.txtArm.text   = dictMeasurment["arm"].stringValue
        self.txtSholder.text = dictMeasurment["sholder"].stringValue
        self.txtNeck.text = dictMeasurment["neck"].stringValue
        self.txtHeight.text = dictMeasurment["height"].stringValue
        self.txtWaist.text = dictMeasurment["waist"].stringValue
        self.txtThight.text = dictMeasurment["thight"].stringValue
        self.txtBack.text = dictMeasurment["back"].stringValue
        self.txtVwExtraNotes.text = dictMeasurment["notes"].stringValue
        self.btnSelection.isSelected = dictMeasurment["is_default"].stringValue == "0" ? false : true
    }
    
}


//MARK: Button Action
extension AddMeasurmentVC {
    
    @IBAction func btnSelectionAction(_ sender: Any) {
        
        if btnSelection.isSelected == true {
            btnSelection.isSelected = false
            strSelectedAsDefault = "0"
        }
        else {
            btnSelection.isSelected = true
            strSelectedAsDefault = "1"
        }
    }
    
    @IBAction func btnSave(_ sender: Any) {
        
        if (txtTitle.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_title_key"))
        }
        else if (txtChest.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_chest_key"))
        }
        else if (txtAnkle.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_ankle_key"))
        }
        else if (txtArm.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_arm_key"))
        }
        else if (txtSholder.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_shoulder_key"))
        }
        else if (txtNeck.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_neck_key"))
        }
        else if (txtHeight.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_height_key"))
        }
        else if (txtWaist.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_waist_key"))
        }
        else if (txtThight.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_thigh_key"))
        }
        else if (txtBack.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_back_key"))
        }
        else
        {
            if getUserDetail("is_login_guest") == "0" {
                self.addAndEditMeasurmentDetail()
            } else {
                var dict = objCartModel.dictProductJson["tailor"]
                dict["measurement"] = toCreateJsonForMeasurement()
                objCartModel.dictProductJson["tailor"] = dict
                isForGuestRedirect()
            }
            
        }
//        else if (txtVwExtraNotes.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
//        {
//            makeToast(strMessage: getCommonString(key: "Please_enter_extra_notes_key"))
//        }
    }
    
}


//MARK: TextField Delegate
extension AddMeasurmentVC:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For mobile numer validation
        if textField != txtTitle {
            let allowedCharacters = CharacterSet(charactersIn:".0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }

}


//MARK: TextView Delegate
extension AddMeasurmentVC {
    func toCreateJsonForMeasurement() -> JSON {
        var dict = JSON()
        dict["title"].stringValue =  txtTitle.text ?? ""
        dict["chest"].stringValue = txtChest.text ?? ""
        dict["ankle"].stringValue = txtAnkle.text ?? ""
        dict["arm"].stringValue = txtArm.text ?? ""
        dict["sholder"].stringValue = txtSholder.text ?? ""
        dict["neck"].stringValue = txtNeck.text ?? ""
        dict["height"].stringValue  =  txtHeight.text ?? ""
        dict["waist"].stringValue = txtWaist.text ?? ""
        dict["thight"].stringValue = txtThight.text ?? ""
        dict["back"].stringValue = txtBack.text ?? ""
        dict["notes"].stringValue = txtVwExtraNotes.text ?? ""
        return dict
    }
}

//MARK: - API calling

extension AddMeasurmentVC {
    
    func isForGuestRedirect(){
        if self.selectedController == .cart {
            var dict = objCartModel.dictProductJson["tailor"]
            if dict["is_fabric_added"].stringValue == "0" {
                let obj = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyAppoitmentVC") as! MyAppoitmentVC
                obj.selectedController = .comeFromCart
                obj.dictCart = dictCart
                obj.handlerUpdateCart = {[weak self] in
                    self?.handlerMyMeasurment()
                    self?.navigationController?.popViewController(animated: false)
                }
                self.navigationController?.pushViewController(obj, animated: true)
            } else {
                updateMeasurementForCart()
            }
        } else {
            if dictFabricDetail["branch_id"].intValue != 0 {
                let obj = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricDetailsVc") as! FabricDetailsVc
                isFromTailor = true
                self.navigationController?.pushViewController(obj, animated: true)
            } else {
                let fabriceVC = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricsVC") as! FabricsVC
                fabriceVC.selectFabric = .notOwnFabric
                self.navigationController?.pushViewController(fabriceVC, animated: true)
            }
        }
    }
    
    func addAndEditMeasurmentDetail()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlAddEditMeasurment)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param =
                        [
                        "lang" : lang,
                         "user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "title" : txtTitle.text ?? "",
                         "chest" : txtChest.text ?? "",
                         "ankle" : txtAnkle.text ?? "",
                         "arm" : txtArm.text ?? "",
                         "sholder" : txtSholder.text ?? "",
                         "neck" : txtNeck.text ?? "",
                         "height" : txtHeight.text ?? "",
                         "waist" : txtWaist.text ?? "",
                         "thight" : txtThight.text ?? "",
                         "back" : txtBack.text ?? "",
                         "notes" : txtVwExtraNotes.text ?? "",
                         "is_default" : btnSelection.isSelected ? "1" : "0"
                        ]
            
            if selectedController == .edit
            {
                param["tbl_measurement_id"] = dictMeasurment["tbl_measurement_id"].stringValue // 0 for add -- 1 for update
            }
            else if selectedController == .add  || selectedController == .cart {
                param["tbl_measurement_id"] = "0" // 0 for add -- 1 for update
            }
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.handlerMyMeasurment()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strNotLogin
                    {
                        if getUserDetail("is_login_guest") == "1" {
                            
                        }
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    func updateMeasurementForCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlEditMeasurement)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "cart_id":dictCart["cart_id"].stringValue,
                          "product_cart_json": JSON(objCartModel.dictProductJson).rawString()
                ] as! [String : String]
            
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.handlerMyMeasurment()
                        self.navigationController?.popViewController(animated: true)
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
}
