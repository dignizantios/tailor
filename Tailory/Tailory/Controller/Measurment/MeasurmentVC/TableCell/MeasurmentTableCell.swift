//
//  MeasurmentTableCell.swift
//  Tailory
//
//  Created by Khushbu on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class MeasurmentTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        setUpUi()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUpUi() {
        
        vwMain.cornerRadius = 5
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeSilverColor
            lbl?.font = themeFont(size: 14, fontname: .medium)
        }
        
        imgTitle.clipsToBounds = true
        imgTitle.layer.cornerRadius = 5
    }
    
    func setUpArabicUI() {
        
        [lblTitle].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [imgTitle].forEach { (img) in
            img?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }

    }
    
}
