//
//  MeasurmentVC.swift
//  Tailory
//
//  Created by Khushbu on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire

enum checkParentControllerForMeasurment
{
    case comeFromCustomize
    case comeFromCart
    case comeFromAlreadyAdded
}


class MeasurmentVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblMeasurment: UITableView!
    
    //MARK: Variables
    var arrayData : [JSON] = []
    var selectedController = checkParentControllerForMeasurment.comeFromCustomize
    var dictCartOrder = JSON()
    var handlerUpdateCart:() -> Void = {}
    var dictAlreadyMeasurementData = JSON()
    var dictMeasurement = JSON()
    var isAlreadyMeasurement = Bool()
    
    //MARK:- viewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        if !isEnglish {
            setUpArabicUI()
        }
        setUpData()
        setUpUI()
        registerXib()
        
        if selectedController == .comeFromCustomize {
            checkMeasurmentData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Measurment_key"))
    }
    
}

//MARK: SetUp
extension MeasurmentVC {
    
    func setUpUI() {
        
        tblMeasurment.tableFooterView = UIView()
    }
    
    func setUpData() {
        
        print("tailor data \(objCartModel.dictProductJson)")
        var dicValue = JSON()
        dicValue["title"].stringValue = getCommonString(key: "Request_someone_to_take_mymeasurment_key")
        dicValue["measurement_type"] = "1"
        dicValue["img"] = "ic_measurement_icon"
        arrayData.append(dicValue)
        
        dicValue = JSON()
        dicValue["title"].stringValue = getCommonString(key: "Request_someone_to_take_my_Dishdashah_key")
        dicValue["measurement_type"] = "2"
        dicValue["img"] = "ic_delivery_icon"
        arrayData.append(dicValue)
        
        dicValue = JSON()
        dicValue["title"].stringValue = getCommonString(key: "My_measurment_key")
        dicValue["measurement_type"] = "3"
        dicValue["img"] = "ic__my_measurement_icon"
        arrayData.append(dicValue)
        
        dicValue = JSON()
        dicValue["title"].stringValue = getCommonString(key: "Tailors_knows_me_key")
        dicValue["measurement_type"] = "4"
        dicValue["img"] = "ic_hand shake_icon"
        arrayData.append(dicValue)
        
        print("ArrayData:", arrayData)
    }
    
    func registerXib() {
        
        tblMeasurment.register(UINib(nibName: "MeasurmentTableCell", bundle: nil), forCellReuseIdentifier: "MeasurmentTableCell")
    }
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
}

//MARK: - TableView Delegate and DataSource

extension MeasurmentVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MeasurmentTableCell", for: indexPath) as! MeasurmentTableCell
        
        let dict = arrayData[indexPath.row]
        
        cell.lblTitle.text = dict["title"].stringValue
        cell.imgTitle.image = UIImage(named: dict["img"].stringValue)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
//        var dictMeasurement = arrayData[indexPath.row]
        /*for i in 0..<arrAlreadyMeasurementData.count {
            let dict = arrAlreadyMeasurementData[i]
            let dictData = arrayData[indexPath.row]
            if dict["measurement_type"].stringValue == dictData["measurement_type"].stringValue {
                isAlreadyMeasurement = true
                dictMeasurement = dict
                break
            }
        }*/
        dictMeasurement = arrayData[indexPath.row]
        if isAlreadyMeasurement {
            print("dictMeasurement \(dictMeasurement)")            
            setupDataWithMeasurement()
            selectedController = .comeFromAlreadyAdded
        } else {
            setupDataWithoutMeasurement()
        }
        self.checkAlreadyAddedFabric(row: indexPath.row)
    }
    
    func setupDataWithMeasurement(){
        print("dictMeasurement \(dictAlreadyMeasurementData)")
        var dict = objCartModel.dictProductJson["tailor"]
        dict["date"].stringValue = dictAlreadyMeasurementData["date"].stringValue
        dict["time"].stringValue = dictAlreadyMeasurementData["time"].stringValue
        dict["extra_notes"].stringValue = dictAlreadyMeasurementData["extra_notes"].stringValue
        dict["is_exist_address_id"].stringValue = dictAlreadyMeasurementData["is_exist_address_id"].stringValue
        dict["address_id"].stringValue = dictAlreadyMeasurementData["address_id"].stringValue
        dict["address"] = dictAlreadyMeasurementData["address"].count == 0 ? JSON() : dictAlreadyMeasurementData["address"]
        dict["tailor_reference_id"].stringValue = dictAlreadyMeasurementData["tailor_reference_id"].stringValue
        dict["tailor_reference_notes"].stringValue = dictAlreadyMeasurementData["tailor_reference_notes"].stringValue
        dict["tbl_measurement_id"].stringValue = dictAlreadyMeasurementData["tbl_measurement_id"].stringValue
        dict["is_exist_measurement_id"].stringValue = dictAlreadyMeasurementData["is_exist_measurement_id"].stringValue
        dict["measurement"] = dictAlreadyMeasurementData["measurement"].count == 0 ? JSON() : dictAlreadyMeasurementData["measurement"]
        dict["fabrics_pickup_date"].stringValue =  dictAlreadyMeasurementData["fabrics_pickup_date"].stringValue
        dict["fabrics_notes"].stringValue =  dictAlreadyMeasurementData["fabrics_notes"].stringValue
        dict["is_exist_fabric_pickup_address_id"].stringValue = dictAlreadyMeasurementData["is_exist_fabric_pickup_address_id"].stringValue
        dict["fabric_pickup_address_id"].stringValue = dictAlreadyMeasurementData["fabric_pickup_address_id"].stringValue
        dict["fabrics_pickup_address"] = dictAlreadyMeasurementData["fabrics_pickup_address"].count == 0 ? JSON() : dictAlreadyMeasurementData["fabrics_pickup_address"]
        dict["fabrics_pickup_time"].stringValue = dictAlreadyMeasurementData["fabrics_pickup_time"].stringValue
        dict["measurement_type"].stringValue = dictMeasurement["measurement_type"].stringValue
        objCartModel.dictProductJson["tailor"] = dict
    }
    
    func setupDataWithoutMeasurement(){
        var dict = objCartModel.dictProductJson["tailor"]
        dict["date"].stringValue = ""
        dict["time"].stringValue = ""
        dict["extra_notes"].stringValue =  ""
        dict["is_exist_address_id"].stringValue = enumExistAddress.isNotExistAddress.rawValue
        dict["address_id"].stringValue = ""
        dict["address"] = JSON()
        dict["tailor_reference_id"].stringValue = ""
        dict["tailor_reference_notes"].stringValue = ""
        dict["tbl_measurement_id"].stringValue = ""
        dict["is_exist_measurement_id"].stringValue = enumExistMeasurement.isNotExistMeasurement.rawValue
        dict["measurement"] = JSON()
        dict["fabrics_pickup_date"].stringValue = ""
        dict["fabrics_notes"].stringValue =  ""
        dict["is_exist_fabric_pickup_address_id"].stringValue = dict["is_exist_address_id"].stringValue
        dict["fabric_pickup_address_id"].stringValue = ""
        dict["fabrics_pickup_address"] = JSON()
        dict["fabrics_pickup_time"].stringValue = ""
        dict["measurement_type"].stringValue = dictMeasurement["measurement_type"].stringValue
        objCartModel.dictProductJson["tailor"] = dict
    }
}

//MARK: - Redirection method

extension MeasurmentVC
{
    
    func redirectToController(index:Int)
    {
        if index == 0 {
            let selectDateVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "SelectPreferDateVC") as! SelectPreferDateVC
            selectDateVC.selectedController = selectedController
            selectDateVC.dictFabricDetailAPI = dictFabricDetail
            if selectedController == .comeFromAlreadyAdded {
                print("dictMeasurement \(dictMeasurement)")
                selectDateVC.dictAlreadyAdded = dictAlreadyMeasurementData
            }
            if selectedController == .comeFromCart{
                selectDateVC.dictCart = dictCartOrder
                selectDateVC.handlerUpdateCart = {[weak self] in
                    self?.handlerUpdateCart()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        self?.navigationController?.popViewController(animated: false)
                    })
                }
            }
            self.navigationController?.pushViewController(selectDateVC, animated: false)
        }
        else if index == 1 {
            let selectDateVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "SelectPreferDateVC") as! SelectPreferDateVC
            selectDateVC.selectedController = selectedController
            selectDateVC.dictFabricDetailAPI = dictFabricDetail
            if selectedController == .comeFromAlreadyAdded {
                print("dictMeasurement \(dictMeasurement)")
                selectDateVC.dictAlreadyAdded = dictAlreadyMeasurementData
            }
            if selectedController == .comeFromCart{
                selectDateVC.dictCart = dictCartOrder
                selectDateVC.handlerUpdateCart = {[weak self] in
                    self?.handlerUpdateCart()
                    DispatchQueue.main.async {
                        self?.navigationController?.popViewController(animated: false)
                    }
//                    self?.navigationController?.popViewController(animated: true)
                }
            }
            self.navigationController?.pushViewController(selectDateVC, animated: false)
        }
        else if index == 2 {
            if getUserDetail("is_login_guest") == "1" {
                let obj = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "AddMeasurmentVC") as! AddMeasurmentVC
                if selectedController == .comeFromCart{
                    obj.selectedController = .cart
                    obj.dictCart = dictCartOrder
                    obj.dictMeasurment = dictCartOrder["tailor_json"]["tailor"]["measurement"]
                } else if selectedController == .comeFromCustomize {
                   obj.selectedController = .add
                }
                obj.handlerMyMeasurment = {[weak self] in
//                    self?.upperRefreshTable()
                    self?.handlerUpdateCart()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        self?.navigationController?.popViewController(animated: false)
                    })
                }
                self.navigationController?.pushViewController(obj, animated: false)
            }
            else {
                let myMeasurment = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyMeasurmentVC") as! MyMeasurmentVC
                myMeasurment.dictFabricDetailAPI = dictFabricDetail
                if selectedController == .comeFromCart{
                    myMeasurment.selectedController = .fromCart
                    myMeasurment.dictCart = dictCartOrder
                    myMeasurment.handlerUpdateCart = {[weak self] in
                        self?.handlerUpdateCart()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                            self?.navigationController?.popViewController(animated: false)
                        })
                    }
                }
                else {
                    myMeasurment.selectedController = .fromTailor
                }
                self.navigationController?.pushViewController(myMeasurment, animated: false)

            }
            /*
            let myMeasurment = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyMeasurmentVC") as! MyMeasurmentVC
            if selectedController == .comeFromCart{
                myMeasurment.selectedController = .fromCart
                myMeasurment.dictCart = dictCartOrder
                myMeasurment.handlerUpdateCart = {[weak self] in
                    self?.handlerUpdateCart()
                    self?.navigationController?.popViewController(animated: true)
                }
            } else {
                myMeasurment.selectedController = .fromTailor
            }
            self.navigationController?.pushViewController(myMeasurment, animated: true)
            */
        }
        else if index == 3 {
            let referenceNumberVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "ReferenceNumberVC") as! ReferenceNumberVC
            referenceNumberVC.dictFabricDetailAPI = dictFabricDetail
            if selectedController == .comeFromCart{
                referenceNumberVC.selectedController = .fromCart
                referenceNumberVC.dictCart = dictCartOrder
                referenceNumberVC.handlerUpdateCart = {[weak self] in
                    self?.handlerUpdateCart()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                        self?.navigationController?.popViewController(animated: false)
                    })
//                    self?.navigationController?.popViewController(animated: true)
                }
            } else {
                referenceNumberVC.selectedController = .fromTailor
            }
            self.navigationController?.pushViewController(referenceNumberVC, animated: true)
        }
    }
}

//MARK:- API

extension MeasurmentVC {
    
    func checkAlreadyAddedFabric(row:Int)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
           
            let url = "\(urlUser)\(urlCheckSameFabric)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "tbl_trailor_id" : objCartModel.dictProductJson["tailor"]["tbl_trailor_id"].stringValue
            ]
            
            
            
            print("param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        dictFabricDetail = json["data"]
                        self.redirectToController(index: row)

                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        dictFabricDetail = json["data"]
                        self.redirectToController(index: row)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func checkMeasurmentData()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
           
            let url = "\(urlUser)\(urlCheckMeasurementData)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "is_login_guest" : getUserDetail("is_login_guest"),
                          "business_id":objCartModel.business_id,
                          "date_time":DateToString(Formatter: "yyyy-MM-dd HH:mm", date: Date())
            ]
            
            print("param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.dictAlreadyMeasurementData = json["data"]
                        self.isAlreadyMeasurement = true
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                       
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}
