//
//  ReferenceNumberVC.swift
//  Tailory
//
//  Created by Khushbu on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class ReferenceNumberVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblPleaseEnterNumber: UILabel!
    @IBOutlet weak var lblReferNumber: UILabel!
    @IBOutlet weak var txtReferenceNumber: CustomTextField!
    @IBOutlet weak var lblExtraNotes: UILabel!
    @IBOutlet weak var txtVwExtraNotes: CustomTextview!
    @IBOutlet weak var btnCountinue: UIButton!
    
    //MARK: Variables    
   
    var dictCart = JSON()
    var selectedController = checkForParentControllerOfMyMeasurment.fromTailor
    var handlerUpdateCart:() -> Void = {}
    var dictFabricDetailAPI = JSON()
    
    //MARK:- ViewLife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        setUpData()
        setUpArabicUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Reference_number_key"))
    }
}


//MARK: SetUp
extension ReferenceNumberVC {
    
    func setUpUI() {
        
        [lblPleaseEnterNumber].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .medium)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        [lblReferNumber, lblExtraNotes].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .medium)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        [txtVwExtraNotes].forEach { (txt) in
            txt?.textColor = UIColor.appThemeLightGrayColor
            txt?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [txtReferenceNumber].forEach { (txt) in
            txt?.font = themeFont(size: 15, fontname: .medium)
            txt?.textColor = UIColor.appThemeLightGrayColor
        }
        
        addDoneButtonOnKeyboard(textfield: txtReferenceNumber)
        
        lblPleaseEnterNumber.text = getCommonString(key: "Please_enter_your_reference_number_key")
        lblReferNumber.text = getCommonString(key: "Reference_number_key")
        lblExtraNotes.text = getCommonString(key: "Extra_notes_key")
        
        btnCountinue.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnCountinue.setTitle(getCommonString(key: "Countinue_key"), for: .normal)
    }
    
    func setUpArabicUI() {
        
        if !isEnglish {
            vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [lblPleaseEnterNumber].forEach { (lbl) in
                lbl?.textAlignment = .right
                lbl?.transform  = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            [lblReferNumber, lblExtraNotes].forEach { (lbl) in
                lbl?.textAlignment = .right
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            [txtReferenceNumber].forEach { (txt) in
                txtReferenceNumber.transform = CGAffineTransform(scaleX: -1, y: 1)
                txt?.textAlignment = .right
            }
            
            [txtVwExtraNotes].forEach { (txt) in
                txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
                txt?.textAlignment = .right
            }
            
            btnCountinue.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func setUpData() {
        
        if selectedController == .fromCart {
            print("DicData:-\(dictCart)")
            
            var dict = dictCart["tailor_json"]["tailor"]
            
            self.txtReferenceNumber.text = dict["tailor_reference_id"].stringValue
            self.txtVwExtraNotes.text = dict["tailor_reference_notes"].stringValue
        }
    }
}


//MARK: Button Action
extension ReferenceNumberVC {
    
    @IBAction func btnCountinueAction(_ sender: Any) {
        
        if (self.txtReferenceNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: getCommonString(key: "Please_enter_reference_number_key"))
        }
//        else if (self.txtVwExtraNotes.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
//            makeToast(strMessage: getCommonString(key: "Please_enter_note_key"))
//        }
        else {
            var dict = objCartModel.dictProductJson["tailor"]
            dict["tailor_reference_id"].stringValue = self.txtReferenceNumber.text ?? ""
            dict["tailor_reference_notes"].stringValue = self.txtVwExtraNotes.text ?? ""
            objCartModel.dictProductJson["tailor"] = dict
            if selectedController == .fromCart {
                dict = objCartModel.dictProductJson["tailor"]
                if dict["is_fabric_added"].stringValue == "0" {
                    let obj = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyAppoitmentVC") as! MyAppoitmentVC
                    obj.selectedController = .comeFromCart
                    obj.dictCart = dictCart
                    obj.handlerUpdateCart = {[weak self] in
                        self?.handlerUpdateCart()
                        self?.navigationController?.popViewController(animated: false)
                    }
                    self.navigationController?.pushViewController(obj, animated: true)
                } else {
                    updateMeasurementForCart()
                }
//                updateMeasurementForCart()
            } else {
                dictFabricDetail = dictFabricDetailAPI
                if dictFabricDetail["branch_id"].intValue != 0 {
                    let obj = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricDetailsVc") as! FabricDetailsVc
                    obj.isHaveOwnFabric = true
                    isFromTailor = true
                    self.navigationController?.pushViewController(obj, animated: true)
                } else {
                    let fabriceVC = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricsVC") as! FabricsVC
                                   fabriceVC.selectFabric = .notOwnFabric
                                   self.navigationController?.pushViewController(fabriceVC, animated: true)
                }
            }
        }
    }
}


//MARK:
extension ReferenceNumberVC {
    func updateMeasurementForCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlEditMeasurement)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "cart_id":dictCart["cart_id"].stringValue,
                          "product_cart_json": JSON(objCartModel.dictProductJson).rawString()
                ] as! [String : String]
            
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.handlerUpdateCart()
                        self.navigationController?.popViewController(animated: true)
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}
