//
//  SelectPreferDateVC.swift
//  Tailory
//
//  Created by Khushbu on 20/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
//import FSCalendar_Persian
import Alamofire
import AlamofireSwiftyJSON

class SelectPreferDateVC: UIViewController {

    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var vwCalendar: FSCalendar!
    @IBOutlet weak var btnCalendarLeft: UIButton!
    @IBOutlet weak var btnCalendarRight: UIButton!
    @IBOutlet weak var lblPleaseSelectTime: UILabel!
    @IBOutlet weak var collectionSelectTime: UICollectionView!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var lblDefaultAddress: UILabel!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblAddressTitleValue: UILabel!
    @IBOutlet weak var lblExtraNotes: UILabel!
    @IBOutlet weak var txtVwExtraNotes: UITextView!
    @IBOutlet weak var btnCountinue: CustomButton!
    @IBOutlet weak var btnSelectAddressOutlet: UIButton!
    @IBOutlet weak var heightOfCollectionSelect: NSLayoutConstraint!
    @IBOutlet weak var lblInfoMessage: UILabel!
    
    //MARK: Variables
    var arrDate: [JSON] = []
    var startDate = false
    var dictAddress = JSON()
    var currentPage: Date?
    private lazy var today: Date = {
        return Date()
    }()
    
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    var selectedController = checkParentControllerForMeasurment.comeFromCustomize
    var dictCart = JSON()
    var handlerUpdateCart:() -> Void = {}
    var dictFabricDetailAPI = JSON()
    var dictAlreadyAdded = JSON()
    
    //MARK: Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setUpCalendar()
        setUpUI()
        registerXib()
        
        if getUserDetail("is_login_guest") != "1" {
            self.defaultAddress()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Select_prefer_date_key"))
        collectionSelectTime.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        collectionSelectTime.removeObserver(self, forKeyPath: "contentSize")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
            print("contentSize:= \(collectionSelectTime.contentSize.height)")
            if collectionSelectTime.contentSize.height == 0{
                self.heightOfCollectionSelect.constant = 110
            } else{
                self.heightOfCollectionSelect.constant = collectionSelectTime.contentSize.height
            }
        }
    }
}

//MARK: SetUp
extension SelectPreferDateVC {
    
    func setUpUI() {
        
//        self.defaultAddress()
        
        [vwCalendar].forEach { (vw) in
            vw?.layer.borderColor = UIColor.appThemelightBlueColor.cgColor
            vw?.layer.borderWidth = 2
            vw?.layer.cornerRadius = 5
        }
        
        [lblPleaseSelectTime, lblDefaultAddress].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeCyprusColor
            lbl?.font = themeFont(size: 18, fontname: .book)
        }
        
        [lblInfoMessage].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeCyprusColor
            lbl?.font = themeFont(size: 14, fontname: .book)
        }
        
        [lblAddressTitle, lblExtraNotes].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 17, fontname: .heavy)
        }
        
        [lblAddressTitleValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeNobelColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [txtVwExtraNotes].forEach { (txt) in
            txt?.textColor = UIColor.appThemeLightGrayColor
            txt?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [btnChange].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 13, fontname: .medium)
        }
        
        btnCountinue.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        
        lblInfoMessage.text = getCommonString(key: "The_date_of_picking_up_the_fabric_will_be_the_same_as_specified_here_key")
        
        lblPleaseSelectTime.text = getCommonString(key: "Please_select_time_key")
        btnChange.setTitle(getCommonString(key: "Change_key"), for: .normal)
        lblDefaultAddress.text = getCommonString(key: "Default_address_key")
        lblAddressTitle.text = getCommonString(key: "Address_title_key")
        lblExtraNotes.text = getCommonString(key: "Extra_notes_key")
        btnCountinue.setTitle(getCommonString(key: "Countinue_key"), for: .normal)
        
        setUpData()
        if getUserDetail("is_login_guest") == "1" {
            self.btnChange.isHidden = true
            self.lblAddressTitleValue.text = getCommonString(key: "Please_select_address_key")
        }
        if selectedController == .comeFromCart {
            
            if getUserDetail("is_login_guest") == "1" {
                self.btnChange.isHidden = false
                setupAddress(dict: dictCart["tailor_json"]["tailor"]["address"])
            }
            
            if dictCart["tailor_json"]["tailor"]["date"].stringValue != ""{
                self.vwCalendar.select(stringTodate(Formatter: "yyyy-MM-dd", strDate: dictCart["tailor_json"]["tailor"]["date"].stringValue))
            } else {
                self.vwCalendar.select(Date())
            }
            
            self.txtVwExtraNotes.text = dictCart["tailor_json"]["tailor"]["extra_notes"].stringValue
//            self.lblExtraNotes.isHidden = self.txtVwExtraNotes.text.isEmpty == true ? true : false
            
            let strCurrentDate = DateToString(Formatter: "yyyy-MM-dd", date: Date())
            let dtCurrent = stringTodate(Formatter: "yyyy-MM-dd", strDate: strCurrentDate)
            
            let dtSelected = self.vwCalendar.selectedDate
            if dtSelected! <= dtCurrent {
                arrDate = setupCurrentTime()
            } else {
                arrDate = setupTime()
            }
            
            if let index = arrDate.index(where: {$0["time"].stringValue == dictCart["tailor_json"]["tailor"]["time"].stringValue}) {
                var dictTime = arrDate[index]
                dictTime["is_selected"].stringValue  = "1"
                arrDate[index] = dictTime
                self.collectionSelectTime.reloadData()
            }
        } else if selectedController == .comeFromAlreadyAdded {
            print("dictAlreadyAdded \(dictAlreadyAdded)")
            if dictAlreadyAdded["date"].stringValue != ""{
                self.vwCalendar.select(stringTodate(Formatter: "yyyy-MM-dd", strDate: dictAlreadyAdded["date"].stringValue))
            } else {
                self.vwCalendar.select(Date())
            }
            
            self.vwCalendar.isUserInteractionEnabled = false
            self.collectionSelectTime.isUserInteractionEnabled = false
            self.btnChange.isUserInteractionEnabled = false
            self.btnCalendarLeft.isUserInteractionEnabled = false
            self.btnCalendarRight.isUserInteractionEnabled = false            
            self.setupAddress(dict: dictAlreadyAdded["address"])
            
            self.txtVwExtraNotes.text = dictAlreadyAdded["extra_notes"].stringValue
//            self.lblExtraNotes.isHidden = self.txtVwExtraNotes.text.isEmpty == true ? true : false
            
            let strCurrentDate = DateToString(Formatter: "yyyy-MM-dd", date: Date())
            let dtCurrent = stringTodate(Formatter: "yyyy-MM-dd", strDate: strCurrentDate)
            
            let dtSelected = self.vwCalendar.selectedDate
            if dtSelected! <= dtCurrent {
                arrDate = setupCurrentTime()
            } else {
                arrDate = setupTime()
            }
            
            if let index = arrDate.index(where: {$0["time"].stringValue == dictAlreadyAdded["time"].stringValue}) {
                var dictTime = arrDate[index]
                dictTime["is_selected"].stringValue  = "1"
                arrDate[index] = dictTime
                self.collectionSelectTime.reloadData()
            }
        }
       
    }
    
    func setUpData() {
        arrDate = []
        arrDate = setupCurrentTime()
        self.collectionSelectTime.reloadData()
    }
    
    
    func registerXib() {
        collectionSelectTime.register(UINib(nibName: "SelectDateCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SelectDateCollectionCell")
    }
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        [vwCalendar].forEach { (vw) in
            vw?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblPleaseSelectTime, lblDefaultAddress].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblAddressTitle, lblExtraNotes].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblAddressTitleValue,lblInfoMessage].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
        
        [btnChange].forEach { (btn) in
            btn?.transform  = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [btnCountinue].forEach { (btn) in
            btn?.transform  = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [txtVwExtraNotes].forEach { (txt) in
            txt?.transform = CGAffineTransform(scaleX: -1, y: 1)
            txt?.textAlignment = .right
        }
        
        txtVwExtraNotes.transform = CGAffineTransform(scaleX: -1, y: 1)
        txtVwExtraNotes.textAlignment = .right
    }
    
    func setupAddress(dict:JSON) {
        if !dict["address_name"].exists(){
            self.btnSelectAddressOutlet.isUserInteractionEnabled = true
            self.lblAddressTitleValue.text = ""
            self.lblAddressTitle.text = getCommonString(key: "Please_tap_to_enter_address_key")
            return
        }
        self.btnSelectAddressOutlet.isUserInteractionEnabled = false
        self.dictAddress = dict
        self.lblAddressTitle.text = dict["address_name"].stringValue
        var strSubAddress = "\n\(getCommonString(key: "Floor_key")):\(dict["floor"].stringValue), \n\n"
        strSubAddress += "\(getCommonString(key: "Building_key")):\(dict["building"].stringValue), \n\n"
        strSubAddress += "\(getCommonString(key: "Avenu_key")):\(dict["avenue"].stringValue), \n\n"
        strSubAddress += "\(getCommonString(key: "Street_key")):\(dict["street"].stringValue), \n\n"
        strSubAddress += "\(getCommonString(key: "Block_key")):\(dict["block"].stringValue), \n\n"
        strSubAddress += "\(getCommonString(key: "Postal_code_key")):\(dict["postal_code"].stringValue), \n\n"
        strSubAddress += "\(getCommonString(key: "City_key")):\(dict["city"].stringValue), \n\n"
        strSubAddress += "\(getCommonString(key: "Governorate_key")):\(dict["governorate"].stringValue), \n\n"
        strSubAddress += "\(getCommonString(key: "Country_key")):\(dict["country"].stringValue). "
        self.lblAddressTitleValue.text = strSubAddress
    }
}


//MARK: Button Action
extension SelectPreferDateVC {
    
    @IBAction func btnChangeAction(_ sender: Any) {
        
        if getUserDetail("is_login_guest") == "1" {
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SelectLocationVc") as! SelectLocationVc
            obj.handlerAddNewAddress = {[weak self] dict in
                self?.setupAddress(dict: dict)
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else {
            
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVc") as! MyAddressVc
            obj.selectedController = .fromTailor
            obj.dictAddress = dictAddress
            obj.handlerUpdateAddress = {[weak self] dict in
                self?.setupAddress(dict: dict)
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
        /*
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVc") as! MyAddressVc
        obj.selectedController = .fromTailor
        obj.dictAddress = dictAddress
        obj.handlerUpdateAddress = {[weak self] dict in
            self?.setupAddress(dict: dict)
        }
        self.navigationController?.pushViewController(obj, animated: true)
        */
    }
    
    @IBAction func btnCountinueAction(_ sender: Any) {
        dictFabricDetail = dictFabricDetailAPI
        var dict = objCartModel.dictProductJson["tailor"]
        let strSelectedDate = DateToString(Formatter: "yyyy-MM-dd", date: self.vwCalendar.selectedDate ?? Date())
        var isAnySelected = false
        for i in 0..<arrDate.count{
            var dictDate = arrDate[i]
            if dictDate["is_selected"].stringValue == "1"{
                isAnySelected = true
                dict["time"].stringValue = "\(dictDate["time"].stringValue)"
                break
            }
        }
        if !isAnySelected{
            makeToast(strMessage: getCommonString(key: "Please_select_time_key"))
            return
        }
        dict["date"].stringValue = strSelectedDate
        dict["extra_notes"].stringValue =  self.txtVwExtraNotes.text ?? ""
        dict["is_exist_address_id"].stringValue = getUserDetail("is_login_guest") == "1" ? enumExistAddress.isNotExistAddress.rawValue : enumExistAddress.existAddress.rawValue
        dict["address_id"].stringValue = dictAddress["address_id"].stringValue
        dict["address"] = getUserDetail("is_login_guest") == "1" ? dictAddress : JSON()
        dict["tailor_reference_id"].stringValue = ""
        dict["tailor_reference_notes"].stringValue = ""
        objCartModel.dictProductJson["tailor"] = dict
        if self.lblAddressTitleValue.text == getCommonString(key: "Please_select_address_key") {
            makeToast(strMessage: getCommonString(key: "Please_select_address_key"))
            return
        }
        if selectedController == .comeFromCart {
            dict = objCartModel.dictProductJson["tailor"]
            dict["is_exist_fabric_pickup_address_id"].stringValue = dict["is_exist_address_id"].stringValue
            dict["fabric_pickup_address_id"].stringValue = dict["address_id"].stringValue
            dict["fabrics_pickup_time"].stringValue = dict["time"].stringValue
            dict["fabrics_pickup_date"].stringValue = dict["date"].stringValue
            dict["fabrics_pickup_address"] = dict["address"]
            dict["fabrics_notes"] = dict["extra_notes"]
            objCartModel.dictProductJson["tailor"] = dict
            updateMeasurementForCart()
        } else {
            if dictFabricDetail["branch_id"].intValue != 0 {
                let obj = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricDetailsVc") as! FabricDetailsVc
                isFromTailor = true
                obj.isHaveOwnFabric = true
                self.navigationController?.pushViewController(obj, animated: true)
            } else{
                let fabriceVC = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricsVC") as! FabricsVC
                           fabriceVC.selectFabric = .notOwnFabric
                           self.navigationController?.pushViewController(fabriceVC, animated: true)
            }
        }
        
    }
    
    @IBAction func btnCalendarLeftAction(_ sender: Any) {
         self.moveCurrentPage(moveUp: false)
    }
    
    @IBAction func btnCalendarRightAction(_ sender: Any) {
        self.moveCurrentPage(moveUp: true)
    }
    
    @IBAction func btnSelectAddressAction(_ sender:UIButton){
        
        if getUserDetail("is_login_guest") == "1" {
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SelectLocationVc") as! SelectLocationVc
            obj.handlerAddNewAddress = {[weak self] dict in
                self?.setupAddress(dict: dict)
                if dict != nil {
                    self?.btnChange.isHidden = false
                }
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else {
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVc") as! MyAddressVc
            obj.selectedController = .fromTailor
            obj.dictAddress = dictAddress
            obj.handlerUpdateAddress = {[weak self] dict in
                self?.setupAddress(dict: dict)
            }
            self.navigationController?.pushViewController(obj, animated: true)

        }
        
        
        /*
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVc") as! MyAddressVc
        obj.selectedController = .fromTailor
        obj.dictAddress = dictAddress
        obj.handlerUpdateAddress = {[weak self] dict in
            self?.setupAddress(dict: dict)
        }
        self.navigationController?.pushViewController(obj, animated: true)
        */
        
    }
    
    func setupTime() -> [JSON]{
        var arrTime:[JSON] = []
        for i in 10..<20{
            var dict = JSON()
            dict["time"].stringValue = "\(i):00"
            dict["is_selected"].stringValue = "0"
            arrTime.append(dict)
            
            dict = JSON()
            dict["time"].stringValue = "\(i):30"
            dict["is_selected"].stringValue = "0"
            arrTime.append(dict)
        }
        return arrTime
    }
    
    func setupCurrentTime() -> [JSON]{
        var arrTime:[JSON] = []
        let date = Calendar.current.dateComponents([.minute,.hour], from: Date())
        var startTime = 0
        if date.hour! >= 10 && date.hour! <= 19{
            if date.minute! <= 30{
                startTime = date.hour!
            } else {
                startTime = date.hour! + 1
            }
            
            for i in startTime..<20{
                var dict = JSON()
                if startTime == i{
                    if date.minute! <= 30{
                        dict = JSON()
                        dict["time"].stringValue = "\(i):30"
                        dict["is_selected"].stringValue = "0"
                        arrTime.append(dict)
                        print("\(i):30")
                    } else {
                        dict = JSON()
                        dict["time"].stringValue = "\(i):00"
                        dict["is_selected"].stringValue = "0"
                        arrTime.append(dict)
                        
                        dict = JSON()
                        dict["time"].stringValue = "\(i):30"
                        dict["is_selected"].stringValue = "0"
                        arrTime.append(dict)
                        print("\(i):00")
                        print("\(i):30")
                    }
                } else {
                    dict = JSON()
                    dict["time"].stringValue = "\(i):00"
                    dict["is_selected"].stringValue = "0"
                    arrTime.append(dict)
                    
                    dict = JSON()
                    dict["time"].stringValue = "\(i):30"
                    dict["is_selected"].stringValue = "0"
                    arrTime.append(dict)
                    
                    print("\(i):00")
                    print("\(i):30")
                }
            }
        } else {
            print("Time slot is not available")
        }
        return arrTime
    }
}


//MARK: Collection Delegate/Datasource
extension SelectPreferDateVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrDate.count == 0 {
            let lbl = UILabel()
            lbl.text = getCommonString(key: "Time_slot_is_not_availabel_key")
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = collectionView.center
            collectionView.backgroundView = lbl
            if !isEnglish {
                lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            return 0
        }
        collectionView.backgroundView = nil
        return arrDate.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectDateCollectionCell", for: indexPath) as! SelectDateCollectionCell
        let dict = arrDate[indexPath.row]
        cell.lblTime.text = dict["time"].stringValue
        
        if dict["is_selected"].stringValue == "1"{
            cell.lblTime.backgroundColor = UIColor.appThemeCyprusColor
            cell.lblTime.textColor = UIColor.white
        } else {
            cell.lblTime.backgroundColor = UIColor.lightGray
            cell.lblTime.textColor = UIColor.appThemeCyprusColor
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0..<arrDate.count {
            var dict = arrDate[i]
            dict["is_selected"].stringValue = "0"
            arrDate[i] = dict
        }
        arrDate[indexPath.row]["is_selected"].stringValue = "1"
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/3, height: 60)
    }
}


//MARK: TextView Datasource/Delegate
extension SelectPreferDateVC : UITextViewDelegate {
    
//    func textViewDidChange(_ textView: UITextView) {
//
//        if textView == txtVwExtraNotes {
//
//            lblExtraNotes.isHidden = textView.text == "" ? false : true
//        }
//    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        
//        if text == "\n" {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
    
}

//MARK: SetUpCalendar
extension SelectPreferDateVC : FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func setUpCalendar() {
        vwCalendar.checkEnglish = true
        vwCalendar.delegate = self
        vwCalendar.dataSource = self
        vwCalendar.clipsToBounds = true
        vwCalendar.allowsMultipleSelection = false
        vwCalendar.appearance.todayColor = UIColor.appThemeSilverColor
        vwCalendar.firstWeekday = 1
        vwCalendar.appearance.selectionColor = UIColor.appThemeCyprusColor
        vwCalendar.placeholderType = .none
        vwCalendar.appearance.eventDefaultColor = UIColor.black
        vwCalendar.appearance.eventSelectionColor = UIColor.white
        if !isEnglish {
            self.vwCalendar.transform = CGAffineTransform(scaleX: -1, y: 1)
//            "FSCalendar.m"
//            "FSCalendarWeekdayView.m"
//            "FSCalendarHeaderView.h"
            vwCalendar.checkEnglish = false
            vwCalendar.locale = NSLocale.init(localeIdentifier: "ar") as Locale
//            vwCalendar.calendarIdentifier = NSCalendar.Identifier.islamic.rawValue
        }
    }
    
    
    func showArabicSubTitle(date: Date!) -> String! {
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd"
        
        var calendarDate = dateFormater.string(from: date as Date)
        
        let characters = Array(calendarDate.characters)
        
        let substituteArabic = ["0":"٠", "1":"١", "2":"٢", "3":"٣", "4":"٤", "5":"٥", "6":"٦", "7":"٧", "8":"٨", "9":"٩"]
        var arabicDate =  ""
        
        for i in characters {
            if let subs = substituteArabic[String(i)] {
                arabicDate += subs
            } else {
                arabicDate += String(i)
            }
        }
        
        return arabicDate
    }
    
    func showEnglishSubTitle(date: Date!) -> String! {
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd"
        
        var calendarDate = dateFormater.string(from: date as Date)
        
        let characters = Array(calendarDate.characters)
        
        let substituteArabic = ["٠":"0", "١":"1", "٢":"2", "٣":"3", "٤":"4", "٥":"5", "٦":"6", "٧":"7", "٨":"8", "٩":"9"]
        var arabicDate =  ""
        
        for i in characters {
            if let subs = substituteArabic[String(i)] {
                arabicDate += subs
            } else {
                arabicDate += String(i)
            }
        }
        
        return arabicDate
    }
    
    //MARK: - FSCalendarDelegate
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
        return !isEnglish ? self.showArabicSubTitle(date: date) : showEnglishSubTitle(date: date)
    }
    
   
    func moveCurrentPage(moveUp: Bool) {
        
        var calendar = Calendar.current
        if !isEnglish{
            calendar = Calendar(identifier: .islamic)
        }
        
        var dateComponents = DateComponents()
        
        dateComponents.month = moveUp ? 1 : -1
        
        self.currentPage = calendar.date(byAdding: dateComponents, to: self.currentPage ?? Date())
        
        self.vwCalendar.setCurrentPage(self.currentPage!, animated: true)
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
       
        let todayDate = Date()
        let tDate = minimumDate(yourDate:todayDate)
        print("tDate-->",tDate)
        print("date-->",date)
        print("todayDate-->",todayDate)
        if date == tDate {
            return true
        }
        else if date.compare(Date()) == .orderedAscending {
            return false
        }
        else {
            return true
        }
    }
    
    
    ///---- Selection cell radious set swift
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderRadiusFor date: Date) -> CGFloat {
        
        for i in 1...31 {
            if [i].contains((self.gregorian.component(.day, from: date))) {
                return 0.0
            }
        }
        return 1.0
    }
        
    func minimumDate(yourDate:Date) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if !isEnglish {
            formatter.locale = Locale(identifier: "ar")
        }
        let myString = formatter.string(from: Date())
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "yyyy-MM-dd"
        if !isEnglish {
            formatter.locale = Locale(identifier: "ar")
        }
        let strCurrentDate = formatter.string(from: yourDate!)
        return formatter.date(from: strCurrentDate)!
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        let todayDate = Date()
        let tDate = minimumDate(yourDate:todayDate)
        if date == tDate {
            return UIColor.white
        }
        else if date.compare(Date()) == .orderedAscending {
            return UIColor.appThemeLightGrayColor
        }
        else {
            return UIColor.appThemeCyprusColor
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let strSelectedDate = DateToString(Formatter: "yyyy-MM-dd", date: date)
        let dtSelected = stringTodate(Formatter: "yyyy-MM-dd", strDate: strSelectedDate)
        
        let strCurrentDate = DateToString(Formatter: "yyyy-MM-dd", date: Date())
        let dtCurrent = stringTodate(Formatter: "yyyy-MM-dd", strDate: strCurrentDate)
        
        if dtSelected <= dtCurrent {
            arrDate = setupCurrentTime()
        } else {
            arrDate = setupTime()
        }
        collectionSelectTime.reloadData()
    }
    
}


//MARK: - API calling

extension SelectPreferDateVC
{
    
    func defaultAddress()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlMyDefaultAddress)"
            
            var param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token")
            ]
            
            if selectedController == .comeFromCart{
                param["address_id"] = dictCart["tailor_json"]["tailor"]["address_id"].stringValue
            } else if selectedController == .comeFromAlreadyAdded {
                print("dictAlreadyAdded \(dictAlreadyAdded)")
                if dictAlreadyAdded["date"].stringValue != ""{
                    param["address_id"] = dictAlreadyAdded["address_id"].stringValue
                }
            }
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let dict = json["data"]
                        
                        if dict["address_name"].exists()
                        {
                            self.btnChange.isHidden = false
                            self.btnSelectAddressOutlet.isUserInteractionEnabled = false
                            self.lblAddressTitleValue.isUserInteractionEnabled = false
                            self.setupAddress(dict: dict)
                        }
                        else
                        {
                            self.btnChange.isHidden = true
                            self.lblAddressTitleValue.text = ""
                            self.lblAddressTitle.text = getCommonString(key: "Please_tap_to_enter_address_key")
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    @objc func handleTap(sender:UITapGestureRecognizer)
    {
        print("address title value")
        
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVc") as! MyAddressVc
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    func updateMeasurementForCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlEditMeasurement)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "cart_id":dictCart["cart_id"].stringValue,
                          "product_cart_json": JSON(objCartModel.dictProductJson).rawString()
            ] as! [String : String]
            
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.handlerUpdateCart()
                        self.navigationController?.popViewController(animated: true)
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
}
