//
//  SelectDateCollectionCell.swift
//  Tailory
//
//  Created by Khushbu on 20/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class SelectDateCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        self.lblTime.font = themeFont(size: 15, fontname: .medium)
    }

    func setUpArabicUI() {        
        self.lblTime.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
}
