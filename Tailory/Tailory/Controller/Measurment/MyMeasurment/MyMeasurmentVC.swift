//
//  MyMeasurmentVC.swift
//  Tailory
//
//  Created by Khushbu on 22/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON


enum checkForParentControllerOfMyMeasurment {
    case fromTailor
    case fromProfile
    case fromCart
}


class MyMeasurmentVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblMyMeasurment: UITableView!
    @IBOutlet weak var btnCountinue: CustomButton!
    @IBOutlet var vwBottomBtn: UIStackView!
    
    //MARK: - Variable
    
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var arrMeasurementList:[JSON] = []
    var strMessage = String()
    var dictCart = JSON()
    var selectedController = checkForParentControllerOfMyMeasurment.fromProfile
    var handlerUpdateCart:() -> Void = {}
    var dictFabricDetailAPI = JSON()
    
    //MARK: Action
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        registerXib()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "My_measurement_key"))
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_my_measurement_add_new_detail_plush"), style: .plain, target: self, action: #selector(addMeasurment))
        rightButton.tintColor = UIColor.white
        
        if isEnglish
        {
            self.navigationItem.rightBarButtonItem = rightButton
        }
        else
        {
            self.tblMyMeasurment.semanticContentAttribute = .forceRightToLeft
            self.navigationItem.leftBarButtonItem = rightButton
        }

    }
    
    @objc func addMeasurment()
    {
        let obj = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "AddMeasurmentVC") as! AddMeasurmentVC
        obj.selectedController = .add
        obj.handlerMyMeasurment = {[weak self] in
            self?.upperRefreshTable()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}


//MARK: SetUp
extension MyMeasurmentVC {
    
    func setUpUI() {
      
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblMyMeasurment.addSubview(upperReferesh)
        self.tblMyMeasurment.tableFooterView = UIView()
        
        [btnCountinue].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
            btn?.setTitle(getCommonString(key: "Countinue_key"), for: .normal)
        }
        
        if selectedController == .fromProfile {
            self.btnCountinue.isHidden = true
            self.vwBottomBtn.isHidden = true
        }
        
        upperRefreshTable()
    }
    
    func registerXib() {
        tblMyMeasurment.register(UINib(nibName: "MyAddressTblCell", bundle: nil), forCellReuseIdentifier: "MyAddressTblCell")
    }
    
    
}

//MARK: TableView Delegate/Datasource
extension MyMeasurmentVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrMeasurementList.count == 0
        {
            let lbl = UILabel()
            lbl.text = getCommonString(key: "Please_add_your_measurements_key")
//            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrMeasurementList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAddressTblCell", for: indexPath) as! MyAddressTblCell
        
        cell.btnEdit.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        
        cell.btnEdit.addTarget(self, action: #selector(btnEditMeasurmentTapped), for: .touchUpInside)
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteTapped), for: .touchUpInside)
        
        let dict = arrMeasurementList[indexPath.row]
        cell.lblMainAddress.text = dict["title"].stringValue
        
        var strSubMeasurment = "\n\(getCommonString(key: "Chest_key")) : \(dict["chest"].stringValue) \n\n"
        strSubMeasurment += "\(getCommonString(key: "Ankle_key")) : \(dict["ankle"].stringValue) \n\n"
        strSubMeasurment += "\(getCommonString(key: "Arm_key")) : \(dict["arm"].stringValue) \n\n"
        strSubMeasurment += "\(getCommonString(key: "Sholders_key")) : \(dict["sholder"].stringValue) \n\n"
        strSubMeasurment += "\(getCommonString(key: "Neck_key")) : \(dict["neck"].stringValue) \n\n"
        strSubMeasurment += "\(getCommonString(key: "Height_key")) : \(dict["height"].stringValue) \n\n"
        strSubMeasurment += "\(getCommonString(key: "Waist_key")) : \(dict["waist"].stringValue) \n\n"
        strSubMeasurment += "\(getCommonString(key: "Thight_key")) : \(dict["thight"].stringValue) \n\n"
        strSubMeasurment += "\(getCommonString(key: "Back_key")) : \(dict["back"].stringValue) "
        cell.lblSubAddress.text = strSubMeasurment
        
        
        if selectedController == .fromTailor || selectedController == .fromCart
        {
            cell.vwRadioButton.isHidden = false
            
            if dict["is_default"].stringValue == "0"
            {
                cell.btnRadioBtn.isSelected = false
            }
            else
            {
                cell.btnRadioBtn.isSelected = true
            }
            
        }
        else if selectedController == .fromProfile
        {
            cell.vwRadioButton.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedController == .fromTailor || selectedController == .fromCart
        {
            for i in 0..<self.arrMeasurementList.count
            {
                self.arrMeasurementList[i]["is_default"].stringValue = "0"
            }
            self.arrMeasurementList[indexPath.row]["is_default"].stringValue = "1"
            self.tblMyMeasurment.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    
    ///--- Swipe to delete row
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: getCommonString(key: "Delete_key")) { (action, index) in
            let alertController = UIAlertController(title: getCommonString(key: "My_measurement_key"), message: getCommonString(key: "Are_you_sure_want_to_delete_key"), preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                print("Yes")
                let dict = self.arrMeasurementList[indexPath.row]
                self.removeMeasurement(measurementId: dict["tbl_measurement_id"].stringValue)
            }
            let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        delete.backgroundColor = .red
        return [delete]
    }
    
}

//MARK: Button Action
extension MyMeasurmentVC {
    
    @IBAction func btnCountinueAction(_ sender: Any) {
        var isAnySelection = false
        var dictMesurement = JSON()
        for i in 0..<self.arrMeasurementList.count{
            if self.arrMeasurementList[i]["is_default"].stringValue == "1"{
                isAnySelection = true
                dictMesurement = self.arrMeasurementList[i]
            }
        }
        if isAnySelection == false{
            makeToast(strMessage: getCommonString(key: "Please_select_measurement_key"))
            return
        }
        
        var dict = objCartModel.dictProductJson["tailor"]
        dict["tbl_measurement_id"].stringValue = dictMesurement["tbl_measurement_id"].stringValue
        dict["is_exist_measurement_id"].stringValue = enumExistMeasurement.existMesurement.rawValue
        dict["measurement"] = JSON()
        objCartModel.dictProductJson["tailor"] = dict
        
        if selectedController == .fromCart{
            dict = objCartModel.dictProductJson["tailor"]
            if dict["is_fabric_added"].stringValue == "0" {
                let obj = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MyAppoitmentVC") as! MyAppoitmentVC
                obj.dictCart = dictCart
                obj.selectedController = .comeFromCart
                obj.handlerUpdateCart = {[weak self] in
                    self?.handlerUpdateCart()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                        self?.navigationController?.popViewController(animated: false)
                    })
                }
                self.navigationController?.pushViewController(obj, animated: true)
            } else {
                 updateMeasurementForCart()
            }
        } else {
            dictFabricDetail = dictFabricDetailAPI
            if dictFabricDetail["branch_id"].intValue != 0 {
                let obj = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricDetailsVc") as! FabricDetailsVc
                isFromTailor = true
                obj.isHaveOwnFabric = true
                self.navigationController?.pushViewController(obj, animated: true)
            } else {
                let fabriceVC = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricsVC") as! FabricsVC
                fabriceVC.selectFabric = .notOwnFabric
                self.navigationController?.pushViewController(fabriceVC, animated: true)
            }
        }
    }

    @objc func btnEditMeasurmentTapped(_ sender:UIButton)
    {
        let obj = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "AddMeasurmentVC") as! AddMeasurmentVC
        obj.selectedController = .edit
        obj.dictMeasurment = arrMeasurementList[sender.tag]
        obj.handlerMyMeasurment = {[weak self] in
            self?.upperRefreshTable()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnDeleteTapped(_ sender:UIButton)
    {
        let alertController = UIAlertController(title: getCommonString(key: "Tailors_key"), message: getCommonString(key: "Are_you_sure_you_want_to_remove_measurement_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            let dict = self.arrMeasurementList[sender.tag]
            self.removeMeasurement(measurementId: dict["tbl_measurement_id"].stringValue)
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

//MARK: - API calling

extension MyMeasurmentVC
{
    @objc func upperRefreshTable()
    {
        intOffset = 0
        getMyMeasurementList()
    }
    
    func getMyMeasurementList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlUser)\(urlMyMeasurement)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "offset" : String(self.intOffset)]
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrMeasurementList = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.btnCountinue.isHidden = true
                            self.tblMyMeasurment.reloadData()
                            return
                        }
                        self.btnCountinue.isHidden = false
                        if self.intOffset == 0
                        {
                            self.arrMeasurementList = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrMeasurementList + aryData
                        self.arrMeasurementList = aryData
                        
                        if self.selectedController == .fromCart{
                            if let index = self.arrMeasurementList.index(where: {$0["tbl_measurement_id"].stringValue == self.dictCart["tailor_json"]["tailor"]["tbl_measurement_id"].stringValue}) {
                                var dictMeasurement = self.arrMeasurementList[index]
                                dictMeasurement["is_default"].stringValue = "1"
                                self.arrMeasurementList[index] = dictMeasurement
                                self.tblMyMeasurment.reloadData()
                            }
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrMeasurementList = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblMyMeasurment.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func removeMeasurement(measurementId:String)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlRemoveMeasurement)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "tbl_measurement_id" : measurementId]
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.upperRefreshTable()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func updateMeasurementForCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlEditMeasurement)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "cart_id":dictCart["cart_id"].stringValue,
                          "product_cart_json": JSON(objCartModel.dictProductJson).rawString()
                ] as! [String : String]
            
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.handlerUpdateCart()
                        self.navigationController?.popViewController(animated: false)
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblMyMeasurment.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblMyMeasurment.contentSize.height).rounded(toPlaces: 2)) - (Double(tblMyMeasurment.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                getMyMeasurementList()
            }
        }
    }
}
