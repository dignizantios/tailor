//
//  AvailableDesignsVC.swift
//  Tailory
//
//  Created by Khushbu on 16/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire

class AvailableDesignsVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var collectionDesignType: UICollectionView!
    @IBOutlet weak var btnCustomizes: CustomButton!
    @IBOutlet weak var btnMyDesigns: CustomButton!
    @IBOutlet var vwBtnsBottom: UIStackView!
    @IBOutlet weak var lblOr: UILabel!
    @IBOutlet weak var btnChooseDefaultDesign: CustomButton!
    @IBOutlet weak var lblWhatisDefaultDesign: UILabel!
    @IBOutlet weak var btnWhatsDefaultDesign: UIButton!
    @IBOutlet weak var vwFooter: UIView!
    @IBOutlet weak var widthOfVwPaginated: NSLayoutConstraint!
    @IBOutlet weak var activityIndicatorFooterview: UIActivityIndicatorView!
    @IBOutlet var vwBtnsavedLater: UIView!
    @IBOutlet var btnSavedForLater: CustomButton!
    
    //MARK: Variables
    var arrAvailabelDesign :[JSON] = []
    var intOffset:Int = 0
    var strMessage = String()
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        RegisterXib()
        
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
}

//MARK: SetUpUI
extension AvailableDesignsVC {
    
    func setUpUI() {
        
        [btnCustomizes].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: .appThemeSilverColor)
        }
        
        [btnMyDesigns, btnSavedForLater].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: .white)
            btn?.borderColor = UIColor.appThemeSilverColor
            btn?.borderWidth = 1
            btn?.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        }
        
        [btnChooseDefaultDesign].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: .appThemeCyprusColor)
            btn?.titleLabel?.textColor = UIColor.white
            
        }
        
        [lblOr].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .heavy)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        [lblWhatisDefaultDesign].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .medium)
            lbl?.textColor = UIColor.appThemeCyprusColor
        }
        
        if getUserDetail("is_login_guest") == "1" {
            btnMyDesigns.isHidden = true
            vwBtnsavedLater.isHidden = true
        }
        else {
            btnMyDesigns.isHidden = false
            vwBtnsavedLater.isHidden = false
        }
            
        
        
        btnCustomizes.setTitle(getCommonString(key: "Customize_key"), for: .normal)
//        btnCustomizes.setTitle(getCommonString(key: "Customized_key"), for: .normal)
        btnMyDesigns.setTitle(getCommonString(key: "My_designs_key"), for: .normal)
        btnSavedForLater.setTitle(getCommonString(key: "Saved_for_later_key"), for: .normal)
        
        lblOr.text = getCommonString(key: "OR_key")
        btnChooseDefaultDesign.setTitle(getCommonString(key: "Choose_the_default_design_key"), for: .normal)
        lblWhatisDefaultDesign.text = getCommonString(key: "What_is_the_default_design_key")
        
         self.upperRefreshCollection()
    }

    func RegisterXib() {
        
        collectionDesignType.register(UINib(nibName: "AvailableDesignsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "AvailableDesignsCollectionCell")
    }
    
    func setUpArabicUI() {
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [btnCustomizes, btnSavedForLater].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [btnMyDesigns].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [btnChooseDefaultDesign].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
            
        }
        
        [lblOr].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblWhatisDefaultDesign].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}

//MARK: Button Action
extension AvailableDesignsVC {
    
    @IBAction func btnCustomizesAction(_ sender: Any) {
        var dictSelectedDesign = JSON()
        for i in 0..<arrAvailabelDesign.count {
            if arrAvailabelDesign[i]["is_selected"].stringValue == "1"{
                dictSelectedDesign = arrAvailabelDesign[i]
            }
        }
        if dictSelectedDesign["cart_item"].stringValue == "0" {
            makeToast(strMessage: getCommonString(key: "Please_add_atlest_one_quantity_key"))
            return
        }
        if dictSelectedDesign.count == 0{
            makeToast(strMessage: getCommonString(key: "Please_select_design_to_customize_key"))
            return
        }
        objCartModel.quantity = dictSelectedDesign["cart_item"].stringValue
        var dict = JSON()
        dict["tbl_trailor_id"].stringValue = dictTailorDetail["tbl_trailor_id"].stringValue
        dict["tailor_user_id"].stringValue = dictTailorDetail["tailor_user_id"].stringValue
        dict["tbl_trailor_design_id"].stringValue = dictSelectedDesign["tbl_trailor_design_id"].stringValue
        dict["price"].stringValue = dictSelectedDesign["price"].stringValue
        dict["is_my_design"].stringValue = enumCustomDesign.addCustomDesign.rawValue
        dict["tbl_my_design_id"].stringValue = ""
        dict["is_diff_tailor"].stringValue = isAlreadyData
        objCartModel.dictProductJson["tailor"] = dict
        
        let customizeVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
        customizeVC.selectedController = .comeFromTailorDetails
        customizeVC.dictCustomize = dictSelectedDesign
        self.navigationController?.pushViewController(customizeVC, animated: true)
    }
    
    @IBAction func btnMyDesigmAction(_ sender: Any) {
        
        let obj = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "MyDesignVc") as! MyDesignVc
        
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnChooseDefaultDesignAction(_ sender: Any) {
        
    }
    
    @IBAction func btnWhatisDefaultDesignAction(_ sender: Any) {
        
    }
    
    @IBAction func btnAddCartAction(_ sender:UIButton){
        for i in 0..<self.arrAvailabelDesign.count
        {
            var dict = self.arrAvailabelDesign[i]
            dict["is_selected"].stringValue = "0"
            self.arrAvailabelDesign[i] = dict
        }
        self.arrAvailabelDesign[sender.tag]["is_selected"].stringValue = "1"
        
        var dict = self.arrAvailabelDesign[sender.tag]
        var item = dict["cart_item"].intValue
        item += 1
        dict["cart_item"].intValue = item
        self.arrAvailabelDesign[sender.tag] = dict
        collectionDesignType.reloadData()
    }
    
    @IBAction func btnMinusCartAction(_ sender:UIButton){
        for i in 0..<self.arrAvailabelDesign.count
        {
            var dict = self.arrAvailabelDesign[i]
            dict["is_selected"].stringValue = "0"
            self.arrAvailabelDesign[i] = dict
        }
        self.arrAvailabelDesign[sender.tag]["is_selected"].stringValue = "1"
        
        var dict = self.arrAvailabelDesign[sender.tag]
        var item = dict["cart_item"].intValue
        if item == 0{
            self.arrAvailabelDesign[sender.tag]["is_selected"].stringValue = "0"
            collectionDesignType.reloadData()
            return
        }
        item -= 1
        dict["cart_item"].intValue = item
        self.arrAvailabelDesign[sender.tag] = dict
        collectionDesignType.reloadData()
    }
    
    @IBAction func btnSavedForLaterAction(_ sender: Any) {
        
        let objc = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "SavedForLaterVC") as! SavedForLaterVC
        
        self.navigationController?.pushViewController(objc, animated: false)
    }
    
    @IBAction func btnAddAction(_ sender: UIButton) {
        if self.arrAvailabelDesign[sender.tag]["is_selected"].stringValue == "0"{
           checkTailorAdded()
        }
        for i in 0..<self.arrAvailabelDesign.count
        {
            var dict = self.arrAvailabelDesign[i]
            dict["is_selected"].stringValue = "0"
            self.arrAvailabelDesign[i] = dict
        }
        self.arrAvailabelDesign[sender.tag]["is_selected"].stringValue = "1"
        collectionDesignType.reloadData()
    }

}

//MARK: Collection Datasource/Delegate
extension AvailableDesignsVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.arrAvailabelDesign.count == 0 {
            let lbl = UILabel()
            lbl.text = self.strMessage
            lbl.textAlignment = .center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = collectionView.center
            collectionView.backgroundView = lbl
            
            if !isEnglish {
                lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            self.vwBtnsBottom.isHidden = true
            self.btnSavedForLater.isHidden = true
            self.widthOfVwPaginated.constant = 0
            self.vwFooter.isHidden = true
            self.activityIndicatorFooterview.stopAnimating()
            
            return 0
        }
//        self.btnSavedForLater.isHidden = false
//        self.vwBtnsBottom.isHidden = false
//        collectionView.backgroundView = nil
        
        if dictTailorDetail["is_available"].stringValue == "1" {
            self.vwBtnsBottom.isHidden = true
//            self.btnCustomizes.isHidden = true
            self.vwBtnsavedLater.isHidden = true
        }
        else {
            self.btnSavedForLater.isHidden = false
            self.vwBtnsBottom.isHidden = false
        }
        collectionView.backgroundView = nil
        return arrAvailabelDesign.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvailableDesignsCollectionCell", for: indexPath) as! AvailableDesignsCollectionCell
        
        let dict = arrAvailabelDesign[indexPath.row]
        
        cell.lblTitle.text = dict["design_name"].stringValue
        cell.imgPic.sd_setImage(with: dict["design_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
        cell.lblPrice.text =  "\(dict["price"].stringValue) \(getCommonString(key: "KD_key"))"
        
        if dict["is_selected"].stringValue != "0" {
            cell.btnAdd.isHidden = true
            cell.vwCart.isHidden = true
            cell.imgPic.layer.borderColor = UIColor.appThemeSilverColor.cgColor
            cell.imgPic.layer.borderWidth = 3
        }
        else {
            cell.btnAdd.isHidden = true
            cell.vwCart.isHidden = true
            cell.imgPic.layer.borderWidth = 0
        }
        
        /*
        if dict["cart_item"].intValue == 0 {
            cell.lblTotalItem.isHidden = true
        }
        else {
            cell.lblTotalItem.isHidden = false
        }*/
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(btnAddAction(_:)), for: .touchUpInside)
        
        cell.btnAddOutlet.tag = indexPath.row
        cell.btnMinusOutlet.tag = indexPath.row
        
        cell.lblTotalItem.text = dict["cart_item"].stringValue
        cell.btnAddOutlet.addTarget(self, action: #selector(btnAddCartAction(_:)), for: .touchUpInside)
        cell.btnMinusOutlet.addTarget(self, action: #selector(btnMinusCartAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.arrAvailabelDesign[indexPath.row]["is_selected"].stringValue == "0"{
            checkTailorAdded()
        }
        for i in 0..<self.arrAvailabelDesign.count
        {
            var dict = self.arrAvailabelDesign[i]
            dict["is_selected"].stringValue = "0"
            self.arrAvailabelDesign[i] = dict
        }
        
        self.arrAvailabelDesign[indexPath.row]["is_selected"].stringValue = "1"
  
        collectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastSectionIndex = collectionView.numberOfSections - 1
        let lastRowIndex = collectionView.numberOfItems(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
             print("this is the last cell")
            self.widthOfVwPaginated.constant = 40
            self.vwFooter.isHidden = false
            self.activityIndicatorFooterview.startAnimating()
            self.activityIndicatorFooterview.tintColor = .black
            self.activityIndicatorFooterview.hidesWhenStopped = true
            getAvailabelDesign()
        } else {
            self.widthOfVwPaginated.constant = 0
            self.vwFooter.isHidden = true
            self.activityIndicatorFooterview.stopAnimating()
        }
    }
}

//MARK:- Service
extension AvailableDesignsVC {
    
    @objc func upperRefreshCollection()
    {
        intOffset = 0
        getAvailabelDesign()
    }
    
    func getAvailabelDesign()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            if self.intOffset < 0
            {
//                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlTrailor)\(urlTailorDesignList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "tailor_user_id" : dictTailorDetail["tailor_user_id"].stringValue,
                          "tbl_trailor_id" : dictTailorDetail["tbl_trailor_id"].stringValue
            ]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
//                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {                        
                        if json["data"].arrayValue.count == 0 {
                            self.arrAvailabelDesign = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.collectionDesignType.reloadData()
                            return
                        }
                    
                        if self.intOffset == 0
                        {
                            self.arrAvailabelDesign = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        for i in 0..<aryData.count{
                            var dict = aryData[i]
                            dict["is_selected"].stringValue = "0"
                            dict["cart_item"].stringValue = "1"
                            aryData[i] = dict
                        }
                        aryData = self.arrAvailabelDesign + aryData
                        
                        self.arrAvailabelDesign = aryData
                        
//                        self.widthOfVwPaginated.constant = 0
//                        self.vwFooter.isHidden = true
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        //                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrAvailabelDesign = []
//                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.collectionDesignType.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func checkTailorAdded()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
           
            let url = "\(urlUser)\(urlCheckCartItem)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "category_id" : enumCategoryId.tailor.rawValue,
                          "business_id" : dictTailorDetail["business_id"].stringValue,
                          "is_fabric_added": ""
            ]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
       
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                       self.showAlert(msg: json["msg"].stringValue)
                    }
                    self.collectionDesignType.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    //MARK:- Alert
    
    func showAlert(msg:String){
        let alertController = UIAlertController(title: getCommonString(key: "Tailory_key"), message: msg, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
          isAlreadyData = "1"
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
            isAlreadyData = "0"
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}
