//
//  AvailableDesignsCollectionCell.swift
//  Tailory
//
//  Created by Khushbu on 17/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class AvailableDesignsCollectionCell: UICollectionViewCell {

    
    //MARK: Outlets
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTotalItem: UILabel!
    @IBOutlet weak var btnMinusOutlet: UIButton!
    @IBOutlet weak var btnAddOutlet: UIButton!
    @IBOutlet weak var vwCart: UIView!
    @IBOutlet var btnAdd: CustomButton!
    
    //MARK:- Outlet ZOne
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        lblPrice.textColor = UIColor.appThemeSilverColor
        lblPrice.font = themeFont(size: 10, fontname: .light)
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        lblTotalItem.textColor = UIColor.appThemeBlackColor
        lblTotalItem.font = themeFont(size: 15, fontname: .heavy)
        
        [imgPic].forEach { (img) in
            img?.clipsToBounds = true
            img?.layer.cornerRadius = 5
        }
        
        btnAdd.setTitle(getCommonString(key: "Add_key"), for: .normal)
        btnAdd.titleLabel?.font = themeFont(size: 12, fontname: .light)
    }
    
    func setUpArabicUI() {
        [lblTitle,lblPrice, lblTotalItem].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        btnAdd.transform = CGAffineTransform(scaleX: -1, y: 1)
//        vwCart.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [imgPic].forEach { (img) in
            img?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }

}
