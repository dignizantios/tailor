//
//  MoreInfoVC.swift
//  Tailory
//
//  Created by Khushbu on 16/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import WebKit

class MoreInfoVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblMoreInfoData: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUpUi()
    }
    
}

//MARK: - UIScrollViewDelegate
extension MoreInfoVC : UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}

//MARK: SetUp
extension MoreInfoVC {
    
    func setUpUi() {
        [lblMoreInfoData].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        let webView = WKWebView()
        webView.frame = self.vwMain.frame
        webView.scrollView.delegate = self
        webView.isMultipleTouchEnabled = false
        webView.loadHTMLString(dictTailorDetail["description"].stringValue, baseURL: nil)
        self.view.addSubview(webView)
//        self.lblMoreInfoData.attributedText = dictTailorDetail["description"].stringValue.htmlToAttributedString
    }
    
    func setUpArabicUI() {
//        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [lblMoreInfoData].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
    }
}
