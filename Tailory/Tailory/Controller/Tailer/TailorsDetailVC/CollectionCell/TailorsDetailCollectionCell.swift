//
//  TailorsDetailCollectionCell.swift
//  Tailory
//
//  Created by Khushbu on 17/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class TailorsDetailCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgTailors: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
    func setUpArabicUI() {
        imgTailors.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    func setupCell(dict:JSON){
        imgTailors.sd_setImage(with: dict["tailory_image"].url, placeholderImage: UIImage(named: "ic_reviews_user_profile_splash_holder"), options: .lowPriority, completed: nil)
    }

}
