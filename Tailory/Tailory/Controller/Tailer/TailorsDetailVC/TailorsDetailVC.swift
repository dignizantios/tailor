//
//  TailorsDetailVC.swift
//  Tailory
//
//  Created by Khushbu on 16/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import MXSegmentedPager
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class TailorsDetailVC: MXSegmentedPagerController {

    @IBOutlet var vwHeader: UIView!
    @IBOutlet weak var collectionImg: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var lblTailorTitle: UILabel!
    @IBOutlet weak var lblDesignAvailable: UILabel!
    @IBOutlet weak var vwStarRating: HCSStarRatingView!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var btnReviews: UIButton!
    @IBOutlet weak var lblMinimumPrice: UILabel!
    @IBOutlet weak var lblMinimumPriceValue: UILabel!
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var btnFavorites: UIButton!
    
    
    //MARK: Variables
    var isStar = false
    var isFavorites = false
    var selectedFav = isComeFromFav.notFromFav
    var vwProgressHeaderValue = CGFloat()
    var handlerUpdateTailorDetail : (JSON) -> Void = {_ in}
   
//    var dictTailorDetail = JSON()
    
    //MARK:- ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        setUpSegment()
        registerXib()
        
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {

        setUpNavigationBarWhiteWithTitleAndBack(strTitle: "")
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
        if self.vwProgressHeaderValue == 0
        {
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.appThemeCyprusColor
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.appThemeCyprusColor
            }
            
            self.navigationController?.navigationBar.backgroundColor = UIColor.appThemeCyprusColor
            self.setHeaderTitle(title: dictTailorDetail["trailor_name"].stringValue)
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !isEnglish {
            self.segmentedPager.pager.showPage(at: 1, animated: false)
        }
    }
    
}

//MARK: SetUp
extension TailorsDetailVC {
    
    func setUpUI() {
        
        objCartModel = CartModel()
        objCartModel.category_id = enumCategoryId.tailor.rawValue
        objCartModel.business_id = dictTailorDetail["tbl_trailor_id"].stringValue
                        
        [lblTailorTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        [lblDesignAvailable, lblReviews].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeNobelColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [lblMinimumPrice, lblMinimumPriceValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeCyprusColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        lblMinimumPrice.text = getCommonString(key: "Minimum_price_key")
        
        getTailorDetail()
//        setupData(dict: dictTailorDetail)
    }
    
    func setUpSegment() {
        
        segmentedPager.backgroundColor = .white
        
        // Parallax Header
        segmentedPager.parallaxHeader.view = vwHeader
        segmentedPager.parallaxHeader.mode = .fill
        segmentedPager.parallaxHeader.height = 300
        
        if statusbarHeight < 21 {
            segmentedPager.parallaxHeader.minimumHeight = 66
        }
        else {
            segmentedPager.parallaxHeader.minimumHeight = 90
        }
        
        segmentedPager.segmentedControl.selectionIndicatorLocation = .down
        segmentedPager.segmentedControl.backgroundColor = .white
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.appThemeSilverColor]
        
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "AvenirLTStd-Heavy", size: 13.0)!]
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.appThemeSilverColor
        
        segmentedPager.segmentedControl.selectionStyle = .fullWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorHeight = 2
        
    }
    
    func registerXib() {
        
        collectionImg.register(UINib(nibName: "TailorsDetailCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TailorsDetailCollectionCell")
    }
    
    func setUpArabicUI() {
        self.vwHeader.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        
        [lblTailorTitle].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblDesignAvailable, lblReviews].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblMinimumPrice, lblMinimumPriceValue].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
    }
    
    func setupData(dict:JSON) {
        
        lblTailorTitle.text = dict["trailor_name"].stringValue
        vwStarRating.value = CGFloat(dict["avg_rate"].floatValue)
        lblReviews.text = "(\(dict["total_rating"].stringValue) \(getCommonString(key: "Review_key")))"
        lblDesignAvailable.text = "\(getCommonString(key: "Available_Design_key")) \(dict["available_designs"].stringValue)"
        lblMinimumPriceValue.text = "\(dict["minimum_price"].stringValue) \(getCommonString(key: "KD_key"))"
        if dict["is_favorite"].stringValue == "1"{
            btnFavorites.isSelected = true
        } else {
            btnFavorites.isSelected = false
        }
        if dict["is_review"].stringValue == "1"{
            btnStar.isSelected = true
        } else {
            btnStar.isSelected = false
        }
        pageController.numberOfPages = dictTailorDetail["trailor_images"].arrayValue.count
        
    }
}

//MARK: Button Action
extension TailorsDetailVC {
    
    @IBAction func btnStarAction(_ sender: Any) {
        
        if getUserDetail("is_login_guest") == "1" {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
            return
        }
        
        if dictTailorDetail["is_review"].stringValue == "1" {
            return
        }        
        let rateTailorVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "RateTailorVC") as! RateTailorVC
        rateTailorVC.selectedController = .comeFromTailor
        rateTailorVC.handlerUpdateTailorDetail = {[weak self] in
            self?.getTailorDetail(isUpdate: true)
        }
        self.navigationController?.pushViewController(rateTailorVC, animated: true)
    }
    
    @IBAction func btnFavoritesAction(_ sender: UIButton) {
        
        if getUserDetail("is_login_guest") == "1" {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
        }
        else {
            if dictTailorDetail["is_favorite"].stringValue == "0"{
                dictTailorDetail["is_favorite"].stringValue = "1"
                setupData(dict: dictTailorDetail)
                addFavUnfav(status: "1")
            } else {
                dictTailorDetail["is_favorite"].stringValue = "0"
                setupData(dict: dictTailorDetail)
                addFavUnfav(status: "0")
            }
            self.handlerUpdateTailorDetail(dictTailorDetail)
        }
        /*
        if dictTailorDetail["is_favorite"].stringValue == "0"{
            addFavUnfav(status: "1")
        } else {
            addFavUnfav(status: "0")
        }
         */
    }
    
    @IBAction func btnReviewsAction(_ sender: Any) {
       
        let reviewsVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "ReviewsVC") as! ReviewsVC
        reviewsVC.selectedParentController = .tailor
        self.navigationController?.pushViewController(reviewsVC, animated: true)
    }

}

//MARK: collection Datasource/Delegate
extension TailorsDetailVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dictTailorDetail["trailor_images"].arrayValue.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TailorsDetailCollectionCell", for: indexPath) as! TailorsDetailCollectionCell
        let arrTailorImage = dictTailorDetail["trailor_images"].arrayValue
        let dict = arrTailorImage[indexPath.row]
        cell.setupCell(dict: dict)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

//MARK: ScrollView Delegate
extension TailorsDetailVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageController.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

}


//MARK: Segment Method
extension TailorsDetailVC {
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        if isEnglish
        {
            return [getCommonString(key: "Available_designs_key"),getCommonString(key: "More_info_key")][index]
        }
        else
        {
            return [getCommonString(key: "More_info_key"),getCommonString(key: "Available_designs_key")][index]
        }
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didScrollWith parallaxHeader: MXParallaxHeader) {
        
        print("Progress: \(parallaxHeader.progress)")
        
        self.vwProgressHeaderValue = parallaxHeader.progress
        if parallaxHeader.progress <= 0.40 {
            
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.appThemeCyprusColor
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.appThemeCyprusColor
            }
            
            self.navigationController?.navigationBar.backgroundColor = UIColor.appThemeCyprusColor
            self.setHeaderTitle(title: dictTailorDetail["trailor_name"].stringValue)
    
        }
        else {
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.clear
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.clear
            }
            self.navigationController?.navigationBar.backgroundColor = UIColor.clear
            self.setHeaderTitle(title: "")
        }
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, segueIdentifierForPageAt index: Int) -> String {
        
        if isEnglish
        {
            return ["mx_page_0","mx_page_1"][index]
            
        }
        else
        {
            return ["mx_page_1","mx_page_0"][index]
        }
        
    }
    
    
    func setHeaderTitle(title:String)
    {
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = title
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 17, fontname: .heavy)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
}
//MARK:- Service

extension TailorsDetailVC {
    
    func getTailorDetail(isUpdate:Bool = false)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlTrailor)\(urlTailorDetail)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                    "user_id" : getUserDetail("user_id"),
                    "access_token" : getUserDetail("access_token"),
                    "tbl_trailor_id" : dictTailorDetail["tbl_trailor_id"].stringValue,
                    "tailor_user_id" : dictTailorDetail["tailor_user_id"].stringValue]
            
            
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.setupData(dict: json["data"])
                        dictTailorDetail = json["data"]
                        if isUpdate {
                            self.handlerUpdateTailorDetail(dictTailorDetail)
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addFavUnfav(status:String)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlFavUnfav)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "is_favorite" : status,
                     "role" : dictTailorDetail["tailor_review_flag"].stringValue,
                     "business_id":dictTailorDetail["business_id"].stringValue]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if self.selectedFav == .notFromFav {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                        }
//                        dictTailorDetail["is_favorite"].stringValue = status
//                        self.handlerUpdateTailorDetail(dictTailorDetail)
//                        self.setupData(dict: dictTailorDetail)
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
}
