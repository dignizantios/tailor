//
//  PriceListVC.swift
//  Tailory
//
//  Created by Khushbu on 16/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON

class PriceListVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblPriceList: UITableView!
    
    //MARK: Variables
    var arrayData : [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !isEnglish {
            setUpArabicUI()
        }
        
        registerXib()
        tblPriceList.tableFooterView = UIView()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpData()
    }
    
}

//MARK: SetUp
extension PriceListVC {
    
    func setUpUI() {
        
    }
    
    func setUpArabicUI() {
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    func setUpData() {
        
        ///-----For Classic
        var dicTitle = JSON()
        dicTitle["title"] = "Classic"
        dicTitle["selected"] = "0"
        
        var arrayValue = [JSON]()
        var dicValue = JSON()
        dicValue["type"] = "Summer"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
       
        dicValue = JSON()
        dicValue["type"] = "Winter"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
       
        dicValue = JSON()
        dicValue["type"] = "Two-piece"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
      
        dicValue = JSON()
        dicValue["type"] = "Three-piece"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        dicTitle["value"] = JSON(arrayValue)
        
//        print("Value:", arrayValue)
        arrayData.append(dicTitle)
        
        ///--- For Modern
        dicTitle = JSON()
        dicTitle["title"] = "Modern"
        dicTitle["selected"] = "0"
        
        arrayValue = [JSON]()
        dicValue = JSON()
        dicValue["type"] = "Summer"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        
        dicValue = JSON()
        dicValue["type"] = "Winter"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        
        dicValue = JSON()
        dicValue["type"] = "Two-piece"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        
        dicValue = JSON()
        dicValue["type"] = "Three-piece"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        dicTitle["value"] = JSON(arrayValue)
        
        arrayData.append(dicTitle)
        
        ///-----For Omani
        dicTitle = JSON()
        dicTitle["title"] = "Omani"
        dicTitle["selected"] = "0"
        
        arrayValue = [JSON]()
        dicValue = JSON()
        dicValue["type"] = "Summer"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        
        dicValue = JSON()
        dicValue["type"] = "Winter"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        
        dicValue = JSON()
        dicValue["type"] = "Two-piece"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        
        dicValue = JSON()
        dicValue["type"] = "Three-piece"
        dicValue["normal"] = "10.000 KD"
        dicValue["express"] = "10.000 KD"
        arrayValue.append(dicValue)
        dicTitle["value"] = JSON(arrayValue)
        arrayData.append(dicTitle)
        
//        print("ArrayData:", arrayData)
    }
    
    func registerXib() {
        
        tblPriceList.register(UINib(nibName: "PriceListTableCell", bundle: nil), forCellReuseIdentifier: "PriceListTableCell")
        tblPriceList.register(UINib(nibName: "PriceListHeaderTableCell", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "PriceListHeaderTableCell")
        
        tblPriceList.register(UINib(nibName: "PriceListHeaderTableCell", bundle: nil), forCellReuseIdentifier: "PriceListHeaderTableCell")
    }
    
}

//MARK:
extension PriceListVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrayData[section]["selected"].stringValue == "0" {
            return 0
        }
        else {
            return arrayData[section]["value"].count+1
        }
//        return arrayData[section]["value"].count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let rowCell = tableView.dequeueReusableCell(withIdentifier: "PriceListTableCell", for: indexPath) as! PriceListTableCell
        
        let dic = arrayData[indexPath.section]["value"][indexPath.row-1]
        
        if indexPath.row == 0 {
            rowCell.lblType.text = ""
            rowCell.lblNormal.text = getCommonString(key: "Normal_key")
            rowCell.lblExpress.text = getCommonString(key: "Express_key")
        }
        else {
            rowCell.lblType.text = dic["type"].stringValue
            rowCell.lblNormal.text = dic["normal"].stringValue
            rowCell.lblExpress.text = dic["express"].stringValue
        }
        
        return rowCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "PriceListHeaderTableCell") as! PriceListHeaderTableCell
        
        headerCell.lblTitle.text = arrayData[section]["title"].stringValue
        headerCell.btnSectionSelected.tag = section
        headerCell.btnSectionSelected.addTarget(self, action: #selector(sectionTapped(_:)), for: .touchUpInside)
        
        if arrayData[section]["selected"] == "0" {
            headerCell.btnImgArrow.isSelected = false
        }
        else {
            headerCell.btnImgArrow.isSelected = true
        }
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @objc func sectionTapped(_ sender: UIButton) {
        
        if arrayData[sender.tag]["selected"] == "1" {
            arrayData[sender.tag]["selected"] = "0"
        }
        else {
            for i in 0..<arrayData.count {
                arrayData[i]["selected"] = "0"
            }
            arrayData[sender.tag]["selected"] = "1"
        }
        tblPriceList.reloadData()
    }
    
}

//MARK:
extension PriceListVC {
    
}

//MARK:
extension PriceListVC {
    
}
