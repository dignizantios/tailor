//
//  PriceListHeaderTableCell.swift
//  Tailory
//
//  Created by Khushbu on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class PriceListHeaderTableCell: UITableViewHeaderFooterView {

    //MARK: Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnImgArrow: UIButton!
    @IBOutlet weak var btnSectionSelected: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        if !isEnglish {
            setUpArabicUI()
        }
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.font = themeFont(size: 18, fontname: .heavy)
        }
    }
    
    func setUpArabicUI() {
        [lblTitle].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}
