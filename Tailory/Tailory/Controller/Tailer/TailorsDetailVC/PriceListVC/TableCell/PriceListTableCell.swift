//
//  PriceListTableCell.swift
//  Tailory
//
//  Created by Khushbu on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class PriceListTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblNormal: UILabel!
    @IBOutlet weak var lblExpress: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        [lblType, lblNormal, lblExpress].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpArabicUI() {
        [lblType, lblNormal, lblExpress].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}
