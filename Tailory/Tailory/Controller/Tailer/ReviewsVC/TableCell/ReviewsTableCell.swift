//
//  ReviewsTableCell.swift
//  Tailory
//
//  Created by Khushbu on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SwiftyJSON

class ReviewsTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        if !isEnglish {
            setUpArabicUI()
        }
        setUpUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpUI() {
        
        [imgTitle].forEach { (img) in
            img?.clipsToBounds = true
            img?.layer.cornerRadius = (img?.frame.height)!/2
        }
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 17, fontname: .medium)
        }
        
        [lblDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 13, fontname: .medium)
        }
    }
    
    func setUpArabicUI() {
        
        [imgTitle].forEach { (img) in
            img?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblTitle].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblDescription].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func setupData(dict:JSON){
        lblTitle.text = dict["name"].stringValue
        lblDescription.text =  dict["review"].stringValue
        imgTitle.sd_setImage(with: dict["profile_image"].url, placeholderImage: UIImage(named: "ic_tailors_splash_holder"), options: .lowPriority, completed: nil)
        vwRating.value = dict["rate_star"].stringValue == "0" ? 0 : CGFloat(dict["rate_star"].floatValue)
    }
}
