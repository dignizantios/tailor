//
//  ReviewsVC.swift
//  Tailory
//
//  Created by Khushbu on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import SDWebImage

enum checkReviewParentController:String {
    case fabric = "2"
    case tailor = "1"
}

class ReviewsVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var arrReviewList:[JSON] = []
    var strMessage = String()
    var selectedParentController = checkReviewParentController.tailor    
    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblReviews: UITableView!
    
    //MARK:- ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !isEnglish {
            setUpArabicUI()
        }
        
        registerXib()
        
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblReviews.addSubview(upperReferesh)
        tblReviews.tableFooterView = UIView()
        upperRefreshTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Reviews_key"))
    }
    
}

//MARK: SetUp
extension ReviewsVC {
    
    func setUpArabicUI() {
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
}

//MARK: RegisterXib
extension ReviewsVC {
    
    func registerXib() {
        
        tblReviews.register(UINib(nibName: "ReviewsTableCell", bundle: nil), forCellReuseIdentifier: "ReviewsTableCell")
    }
}

//MARK: TableView Datasource/Delegate
extension ReviewsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrReviewList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            if !isEnglish {
                lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            return 0
        }
        tableView.backgroundView = nil
        return arrReviewList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewsTableCell", for: indexPath) as! ReviewsTableCell
        
        let dict = arrReviewList[indexPath.row]
        cell.setupData(dict: dict)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
}

//MARK:- Service

extension ReviewsVC
{
    @objc func upperRefreshTable()
    {
        intOffset = 0
        getReviewList()
    }
    
    func getReviewList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlTrailor)\(urlReviewList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "business_id" : selectedParentController == .tailor ? dictTailorDetail["business_id"].stringValue : dictFabricDetail["business_id"].stringValue,
                "tailor_review_flag" : "\(selectedParentController.rawValue)"
            ]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrReviewList = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblReviews.reloadData()
                            return
                        }
                        if self.intOffset == 0
                        {
                            self.arrReviewList = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrReviewList + aryData
                        
                        self.arrReviewList = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        self.arrReviewList = []
                        self.strMessage = json["msg"].stringValue
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                    self.tblReviews.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblReviews.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblReviews.contentSize.height).rounded(toPlaces: 2)) - (Double(tblReviews.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                getReviewList()
            }
        }
    }
}
