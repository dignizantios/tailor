//
//  AccessoriesDetailCell.swift
//  Tailory
//
//  Created by Jaydeep on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class AccessoriesDetailCell: UITableViewCell {
    
    //MARK:- Varibale Declaration
    
    var handlerTextfield:(String) -> Void = {str in}
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwTextView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtField: UITextField!
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish{
            setUpArabicUI()
        }
        
        lblTitle.font = themeFont(size: 15, fontname: .heavy)
        lblTitle.textColor = UIColor.black
        
        txtField.font = themeFont(size: 15, fontname: .medium)
        txtField.textColor = UIColor.black
        
        txtView.font = themeFont(size: 15, fontname: .medium)
        txtView.textColor = UIColor.black
        
        lblPlaceholder.font = themeFont(size: 15, fontname: .medium)
        lblPlaceholder.textColor = UIColor.black
        
        txtField.addTarget(self, action: #selector(textFiedlDidChange(_:)), for: .editingChanged)
        
        txtField.placeholder = getCommonString(key: "Enter_here_key")
        lblPlaceholder.text = getCommonString(key: "Enter_here_key")
        
        txtView.contentInset = UIEdgeInsets(top: -6, left: -1, bottom: 0, right: 0)
        
        txtView.delegate = self
        txtField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpArabicUI() {
        
        [lblTitle,lblPlaceholder].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
        
        txtView?.transform = CGAffineTransform(scaleX: -1, y: 1)
        txtView?.textAlignment = .right
        
        txtField?.transform = CGAffineTransform(scaleX: -1, y: 1)
        txtField?.textAlignment = .right
    }
}

//MARK:- Textfield Delegate

extension AccessoriesDetailCell:UITextFieldDelegate{
    @objc func textFiedlDidChange(_ textField:UITextField){
        handlerTextfield(textField.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK:- Textfield Delegate

extension AccessoriesDetailCell:UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "" {
            lblPlaceholder.isHidden = false
        } else {
            lblPlaceholder.isHidden = true
        }
        handlerTextfield(textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
