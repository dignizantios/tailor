//
//  SelectionCell.swift
//  Tailory
//
//  Created by Jaydeep on 28/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class SelectionCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnRadioOutlet: UIButton!
    @IBOutlet weak var btnCheckOutlet: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        if !isEnglish{
            setUpArabicUI()
        }
        
        lblTitle.font = themeFont(size: 15, fontname: .medium)
        lblTitle.textColor = UIColor.black
       
    }
    
    func setUpArabicUI() {
        
        [lblTitle].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
    }

}
