//
//  SelectCell.swift
//  Tailory
//
//  Created by Jaydeep on 28/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class SelectCell: UITableViewCell {
    
    //MARK:-Variable Declaration
    
    var handlerTextfield:() -> Void = {}
    
    //MARK:- Outlet Zone
    @IBOutlet weak var txtSelect: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- ViewLife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        if !isEnglish{
            setUpArabicUI()
        }
        
        lblTitle.font = themeFont(size: 15, fontname: .heavy)
        lblTitle.textColor = UIColor.black
        
        txtSelect.font = themeFont(size: 13, fontname: .medium)
        txtSelect.textColor = UIColor.black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpArabicUI() {
        
        [lblTitle].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
        
        [txtSelect].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
    }
}

extension SelectCell:UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        handlerTextfield()
        return false
    }
}
