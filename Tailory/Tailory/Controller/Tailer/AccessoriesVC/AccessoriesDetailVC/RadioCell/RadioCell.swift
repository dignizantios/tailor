//
//  RadioCell.swift
//  Tailory
//
//  Created by Jaydeep on 28/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class RadioCell: UITableViewCell {
    
    //MARK:- Variable Declaration
    var handlerCollectionHeight:(CGFloat) -> Void = {_ in}
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionRadio: UICollectionView!
    @IBOutlet weak var heightOfCollection: NSLayoutConstraint!
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if !isEnglish{
            setUpArabicUI()
        }
        
        lblTitle.font = themeFont(size: 15, fontname: .heavy)
        lblTitle.textColor = UIColor.black
        
        collectionRadio.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUpArabicUI() {
        [lblTitle].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UICollectionView {
            print("contentSize:= \(collectionRadio.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            self.heightOfCollection.constant = collectionRadio.collectionViewLayout.collectionViewContentSize.height
            handlerCollectionHeight(collectionRadio.contentSize.height)
        }
    }
    
}
