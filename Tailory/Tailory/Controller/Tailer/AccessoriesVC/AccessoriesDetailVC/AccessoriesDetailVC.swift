//
//  AccessoriesDetailVCViewController.swift
//  Tailory
//
//  Created by Jaydeep on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import SDWebImage
import DropDown
import WebKit

class AccessoriesDetailVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrAccessoriesDetail:[JSON] = []
    var dictAccessories = JSON()
    var dictAccessoriesDetail = JSON()
    var handlerUpdateAccessoriesList : (JSON) -> Void = {_ in}
    var sizeDD = DropDown()
    var selectedFav = isComeFromFav.notFromFav
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var imgAccessories: UIImageView!
    @IBOutlet weak var lblAccessoriesName: UILabel!
    @IBOutlet weak var lblAccessoriesPrice: UILabel!
    @IBOutlet weak var lblAccessoriesSKU: UILabel!
    @IBOutlet weak var lblAccessoriesDescription: UILabel!
    @IBOutlet weak var btnAddFavouriteOutlet: CustomButton!
    @IBOutlet weak var lblFavourite: UILabel!
    @IBOutlet weak var btnShareOutlet: CustomButton!
    @IBOutlet weak var lblShareTitle: UILabel!
    @IBOutlet weak var tblAccessoriesDetail: UITableView!
    @IBOutlet weak var btnAddToCartOutlet: UIButton!
    @IBOutlet weak var heightOfTblAccessoriesDetail: NSLayoutConstraint!
    @IBOutlet weak var collectionImg: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var wkWebviewDetail: WKWebView!
    @IBOutlet weak var heightOfWebview: NSLayoutConstraint!
    @IBOutlet var vwWebView: UIView!
    @IBOutlet var vwWebViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCartItem: UILabel!
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        registerXib()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblAccessoriesDetail.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        wkWebviewDetail.scrollView.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: "")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.shadowImage = UIImage()
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblAccessoriesDetail.removeObserver(self, forKeyPath: "contentSize")
        wkWebviewDetail.scrollView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblAccessoriesDetail.contentSize.height)")
            self.heightOfTblAccessoriesDetail.constant = tblAccessoriesDetail.contentSize.height
        }
        if object is WKWebView {
            print("contentSize WKWebView := \(tblAccessoriesDetail.contentSize.height)")
            self.heightOfWebview.constant = self.wkWebviewDetail.scrollView.contentSize.height
        }
    }

    //MARK:- Setup UI
    
    func setupUI(){
        
        [lblAccessoriesName,lblAccessoriesPrice,lblCartItem].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = themeFont(size: 16, fontname: .heavy)
        }
        
        [lblAccessoriesSKU].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 13, fontname: .medium)
        }
        
        [lblFavourite,lblShareTitle].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = themeFont(size: 14, fontname: .medium)
        }
        
        lblFavourite.text = getCommonString(key: "Add_to_favorites_key")
        lblShareTitle.text = getCommonString(key: "Share_this_product_key")
        lblCartItem.text = "1"
        
        wkWebviewDetail.navigationDelegate = self

        
//        btnAddFavouriteOutlet.backgroundColor = UIColor.appThemeSilverColor
//        btnShareOutlet.backgroundColor = UIColor.appThemeSilverColor
        
        btnAddToCartOutlet.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnAddToCartOutlet.setTitle(getCommonString(key: "Add_to_cart_key"), for: .normal)
        
        tblAccessoriesDetail.register(UINib(nibName: "AccessoriesDetailCell", bundle: nil), forCellReuseIdentifier: "AccessoriesDetailCell")
        tblAccessoriesDetail.register(UINib(nibName: "SelectCell", bundle: nil), forCellReuseIdentifier: "SelectCell")
        tblAccessoriesDetail.register(UINib(nibName: "RadioCell", bundle: nil), forCellReuseIdentifier: "RadioCell")
       
        getAccessoriesDetail()
        
        if !isEnglish {
            setupArabicUI()
        }
        
    }
    
    func setupData(dict:JSON) {
        lblAccessoriesName.text = dict["product_name"].stringValue
        lblAccessoriesPrice.text =  "\(dict["price"].stringValue) \(getCommonString(key: "KD_key"))"
        lblAccessoriesSKU.text = "\(getCommonString(key: "SKU_key")): \(dict["sku"].stringValue)"
        wkWebviewDetail.scrollView.isScrollEnabled = false
        wkWebviewDetail.loadHTMLString(dict["description"].stringValue, baseURL: nil)
        wkWebviewDetail.sizeToFit()
        
        if dict["is_favorite"].stringValue == "0"{
           btnAddFavouriteOutlet.isSelected = false
        } else {
           btnAddFavouriteOutlet.isSelected = true
        }
        
        arrAccessoriesDetail = dict["options"].arrayValue
        
        for i in 0..<arrAccessoriesDetail.count{
            var dict = arrAccessoriesDetail[i]
            var arrOptionValue = dict["option_value"].arrayValue
            for j in 0..<arrOptionValue.count{
                var dictInner = arrOptionValue[j]
                dictInner["isSelected"].stringValue = "0"
                arrOptionValue[j] = dictInner
            }
            dict["option_value"] = JSON(arrOptionValue)
            arrAccessoriesDetail[i] = dict
        }
        print("arrAccessoriesDetail \(arrAccessoriesDetail)")
        pageController.numberOfPages = dict["product_images"].arrayValue.count
        self.collectionImg.reloadData()
        self.tblAccessoriesDetail.reloadData()
        
    }
    
    func setupArabicUI(){
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [lblAccessoriesName,lblAccessoriesSKU,lblCartItem].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblAccessoriesPrice].forEach { (lbl) in
            lbl?.textAlignment = .left
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblShareTitle,lblFavourite].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        btnAddToCartOutlet.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    func registerXib() {
        
        collectionImg.register(UINib(nibName: "TailorsDetailCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TailorsDetailCollectionCell")
    }
    
}

//MARK:- WKWebview Delegate
extension AccessoriesDetailVC:WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.documentElement.scrollHeight") { (height, error) in
            print("wkWebviewDetail height:", height as! CGFloat)
            self.heightOfWebview.constant = height as! CGFloat
            let contentSize = webView.scrollView.contentSize
            let viewSize = webView.bounds.size
            let zoomScale = viewSize.width/contentSize.width
            webView.scrollView.minimumZoomScale = zoomScale
            webView.scrollView.maximumZoomScale = zoomScale
            webView.scrollView.zoomScale = zoomScale
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        }
    }
   
}

//MARK: ScrollView Delegate
extension AccessoriesDetailVC : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionImg {
            pageController.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
    }
    
}


//MARK: - IBAction

extension AccessoriesDetailVC
{
    func addToCartValidation() {
//        print("arrAccessoriesDetail \(arrAccessoriesDetail)")
        var arrOption:[JSON] = []
        for i in 0..<arrAccessoriesDetail.count{
            let dict = arrAccessoriesDetail[i]
//            print("dict \(dict)")
            if dict["is_type"].stringValue == accessoriesDetailType.check.rawValue{
                let arrOptionValue = dict["option_value"].arrayValue
                var arrCheck : [JSON] = []
                for j in 0..<arrOptionValue.count{
                    let dictOption = arrOptionValue[j]
                    if dictOption["isSelected"].stringValue == "1"{
                        var dictCheck = JSON()
                        dictCheck["product_option_value_id"].stringValue = dictOption["product_option_value_id"].stringValue
                        dictCheck["price"].stringValue = dictOption["price"].stringValue
                        dictCheck["option_value_name"].stringValue = dictOption["option_value_name"].stringValue
                        arrCheck.append(dictCheck)
                    }
                }
                if arrCheck.count == 0{
                    makeToast(strMessage: "\(getCommonString(key: "Please_select_at_least_one_key")) \(dict["option_name"].stringValue)")
                    return
                }
                var dictOption = JSON()
                dictOption["check_multiple"] = JSON(arrCheck)
                dictOption["product_option_id"].stringValue = dict["product_option_id"].stringValue
                dictOption["option_name"].stringValue = dict["option_name"].stringValue
                dictOption["comment"].stringValue = ""
                dictOption["is_type"].stringValue = dict["is_type"].stringValue
                dictOption["quantity"].stringValue = "1"
                arrOption.append(dictOption)
            } else if dict["is_type"].stringValue == accessoriesDetailType.radio.rawValue{
                let arrOptionValue = dict["option_value"].arrayValue
                var dictCheck = JSON()
                for j in 0..<arrOptionValue.count{
                    let dictOption = arrOptionValue[j]
                    if dictOption["isSelected"].stringValue == "1"{
                        dictCheck["product_option_value_id"].stringValue = dictOption["product_option_value_id"].stringValue
                        dictCheck["price"].stringValue = dictOption["price"].stringValue
                        dictCheck["option_value_name"].stringValue = dictOption["option_value_name"].stringValue
                        dictCheck["quantity"].stringValue = dictOption["quantity"].stringValue
                        break
                    }
                }
                if dictCheck.count == 0{
                    makeToast(strMessage: "\(getCommonString(key: "Please_select_at_least_one_key")) \(dict["option_name"].stringValue)")
                    return
                }
                var dictOption = JSON()
                dictOption["check_multiple"] = []
                dictOption["product_option_id"].stringValue = dict["product_option_id"].stringValue
                dictOption["option_name"].stringValue = dict["option_name"].stringValue
                dictOption["comment"].stringValue = ""
                dictOption["is_type"].stringValue = dict["is_type"].stringValue
                dictOption["product_option_value_id"].stringValue = dictCheck["product_option_value_id"].stringValue
                dictOption["price"].stringValue = dictCheck["price"].stringValue
                dictOption["option_value_name"].stringValue = dictCheck["option_value_name"].stringValue
                dictOption["quantity"].stringValue = dictCheck["quantity"].stringValue
                arrOption.append(dictOption)
            } else {
                if dict["is_type"].stringValue == accessoriesDetailType.text.rawValue ||  dict["is_type"].stringValue == accessoriesDetailType.textarea.rawValue{
                    if dict["comment"].stringValue == "" {
                        makeToast(strMessage: "\(getCommonString(key: "Please_enter_key")) \(dict["option_name"].stringValue)")
                        return
                    }
                    else {
                        var strMin = String()
                        var strMax = String()
                        for i in 0..<dict["validate"].arrayValue.count {
                            if dict["validate"][i]["id"].stringValue == "1" {
                                strMin = dict["validate"][i]["min_value"].stringValue
                            }
                            else if  dict["validate"][i]["id"].stringValue == "2" {
                                strMax = dict["validate"][i]["max_value"].stringValue
                            }
                        }
                        if dict["validate"].arrayValue.count != 0 {
                            if (Double(dict["comment"].stringValue) ?? 0) < (Double(strMin) ?? 0) || (Double(dict["comment"].stringValue) ?? 0) > (Double(strMax) ?? 0) {
                                makeToast(strMessage: "\(getCommonString(key: "Please_enter_length_between_key")) \(strMin) - \(strMax)")
                                return
                            }
                        }
                        print("Min:\(strMin), Max:\(strMax)")
                    }
                } else if dict["is_type"].stringValue == accessoriesDetailType.select.rawValue {
                    if dict["option_value_name"].stringValue == ""{
                        makeToast(strMessage: "\(getCommonString(key: "Please_select_key")) \(dict["option_name"].stringValue)")
                        return
                    }
                }
                
                
                
                /*if dict["is_type"].stringValue == accessoriesDetailType.text.rawValue {
                 let arrValidate = dict["validate"].arrayValue
                 for i in 0..<arrValidate.count{
                 let dictValidate = arrValidate[i]
                 if dictValidate["id"].stringValue == accessoriesValidParam.min.rawValue {
                 if dict["comment"].stringValue.isNumeric == false{
                 makeToast(strMessage: getCommonString(key: "Please_enter_only_numeric_value_key"))
                 return
                 }
                 let strValue = dict["comment"].doubleValue
                 if strValue < dictValidate["min_value"].doubleValue{
                 var strValidation = dict["option_name"].stringValue
                 strValidation += " \(getCommonString(key: "should_be_minimum_key"))"
                 strValidation += " \(dictValidate["min_value"].stringValue)"
                 makeToast(strMessage: strValidation)
                 return
                 }
                 } else if dictValidate["id"].stringValue == accessoriesValidParam.max.rawValue {
                 let strValue = dict["comment"].doubleValue
                 if strValue > dictValidate["max_value"].doubleValue{
                 var strValidation = dict["option_name"].stringValue
                 strValidation += " \(getCommonString(key: "should_be_maximum_key"))"
                 strValidation += " \(dictValidate["max_value"].stringValue)"
                 makeToast(strMessage: strValidation)
                 return
                 }
                 }
                 }
                 }*/
//                print("dict \(dict)")
                var dictOption = JSON()
                dictOption["check_multiple"] = []
                dictOption["product_option_id"].stringValue = dict["product_option_id"].stringValue
                dictOption["option_name"].stringValue = dict["option_name"].stringValue
                dictOption["comment"].stringValue = dict["comment"].stringValue
                dictOption["is_type"].stringValue = dict["is_type"].stringValue
                dictOption["product_option_value_id"].stringValue = dict["product_option_value_id"].stringValue
                dictOption["price"].stringValue = dict["price"].stringValue
                dictOption["option_value_name"].stringValue = dict["option_value_name"].stringValue
                dictOption["quantity"].stringValue = dict["quantity"].stringValue
                arrOption.append(dictOption)
            }
        }
//        print("arrOption \(arrOption)")
        
        var dictAccessories = JSON()
        dictAccessories["accessory_category_id"].stringValue = dictAccessoriesDetail["accessory_category_id"].stringValue
        dictAccessories["product_id"].stringValue = dictAccessoriesDetail["product_id"].stringValue
        dictAccessories["product_name"].stringValue = dictAccessoriesDetail["product_name"].stringValue
        dictAccessories["store_id"].stringValue = dictAccessoriesDetail["store_id"].stringValue
        dictAccessories["quantity"].stringValue = self.lblCartItem.text ?? ""
        dictAccessories["price"].stringValue = dictAccessoriesDetail["price"].stringValue
        dictAccessories["options"] = JSON(arrOption)
        
        objCartModel = CartModel()
        objCartModel.dictProductJson["accessories"] = dictAccessories
        
        print("final \(objCartModel.dictProductJson)")
        
        if arrOption.count < arrAccessoriesDetail.count{
            makeToast(strMessage: "Please fill up all detail")
            return
        }
        addProductToCart()
        
    }
    
    @IBAction func btnAddToCartTapped(_ sender: UIButton) {
        
        addToCartValidation()
        
        /*
//        print("arrAccessoriesDetail \(arrAccessoriesDetail)")
        var arrOption:[JSON] = []
        for i in 0..<arrAccessoriesDetail.count{
            let dict = arrAccessoriesDetail[i]
            if dict["is_type"].stringValue == accessoriesDetailType.check.rawValue{
                let arrOptionValue = dict["option_value"].arrayValue
                var arrCheck : [JSON] = []
                for j in 0..<arrOptionValue.count{
                    let dictOption = arrOptionValue[j]
                    if dictOption["isSelected"].stringValue == "1"{
                        var dictCheck = JSON()
                        dictCheck["product_option_value_id"].stringValue = dictOption["product_option_value_id"].stringValue
                        dictCheck["price"].stringValue = dictOption["price"].stringValue
                        dictCheck["option_value_name"].stringValue = dictOption["option_value_name"].stringValue
                        arrCheck.append(dictCheck)
                    }
                }
                if arrCheck.count == 0{
                    makeToast(strMessage: "\(getCommonString(key: "Please_select_at_least_one_key")) \(dict["option_name"].stringValue)")
                    return
                }
                var dictOption = JSON()
                dictOption["check_multiple"] = JSON(arrCheck)
                dictOption["product_option_id"].stringValue = dict["product_option_id"].stringValue
                dictOption["option_name"].stringValue = dict["option_name"].stringValue
                dictOption["comment"].stringValue = ""
                dictOption["is_type"].stringValue = dict["is_type"].stringValue
                dictOption["quantity"].stringValue = "1"
                arrOption.append(dictOption)
            } else if dict["is_type"].stringValue == accessoriesDetailType.radio.rawValue{
                let arrOptionValue = dict["option_value"].arrayValue
                var dictCheck = JSON()
                for j in 0..<arrOptionValue.count{
                    let dictOption = arrOptionValue[j]
                    if dictOption["isSelected"].stringValue == "1"{
                        dictCheck["product_option_value_id"].stringValue = dictOption["product_option_value_id"].stringValue
                        dictCheck["price"].stringValue = dictOption["price"].stringValue
                        dictCheck["option_value_name"].stringValue = dictOption["option_value_name"].stringValue
                        dictCheck["quantity"].stringValue = dictOption["quantity"].stringValue
                        break
                    }
                }
                if dictCheck.count == 0{
                    makeToast(strMessage: "\(getCommonString(key: "Please_select_at_least_one_key")) \(dict["option_name"].stringValue)")
                    return
                }
                var dictOption = JSON()
                dictOption["check_multiple"] = []
                dictOption["product_option_id"].stringValue = dict["product_option_id"].stringValue
                dictOption["option_name"].stringValue = dict["option_name"].stringValue
                dictOption["comment"].stringValue = ""
                dictOption["is_type"].stringValue = dict["is_type"].stringValue
                dictOption["product_option_value_id"].stringValue = dictCheck["product_option_value_id"].stringValue
                dictOption["price"].stringValue = dictCheck["price"].stringValue
                dictOption["option_value_name"].stringValue = dictCheck["option_value_name"].stringValue
                dictOption["quantity"].stringValue = dictCheck["quantity"].stringValue
                arrOption.append(dictOption)
            } else {
                if dict["is_type"].stringValue == accessoriesDetailType.text.rawValue ||  dict["is_type"].stringValue == accessoriesDetailType.textarea.rawValue{
                    if dict["comment"].stringValue == "" {
                        makeToast(strMessage: "\(getCommonString(key: "Please_enter_key")) \(dict["option_name"].stringValue)")
                        return
                    }
                } else if dict["is_type"].stringValue == accessoriesDetailType.select.rawValue {
                    if dict["option_value_name"].stringValue == ""{
                        makeToast(strMessage: "\(getCommonString(key: "Please_select_key")) \(dict["option_name"].stringValue)")
                        return
                    }
                }
                /*if dict["is_type"].stringValue == accessoriesDetailType.text.rawValue {
                    let arrValidate = dict["validate"].arrayValue
                    for i in 0..<arrValidate.count{
                        let dictValidate = arrValidate[i]
                         if dictValidate["id"].stringValue == accessoriesValidParam.min.rawValue {
                            if dict["comment"].stringValue.isNumeric == false{
                                makeToast(strMessage: getCommonString(key: "Please_enter_only_numeric_value_key"))
                                return
                            }
                            let strValue = dict["comment"].doubleValue
                            if strValue < dictValidate["min_value"].doubleValue{
                                var strValidation = dict["option_name"].stringValue
                                strValidation += " \(getCommonString(key: "should_be_minimum_key"))"
                                strValidation += " \(dictValidate["min_value"].stringValue)"
                                makeToast(strMessage: strValidation)
                                return
                            }
                         } else if dictValidate["id"].stringValue == accessoriesValidParam.max.rawValue {
                            let strValue = dict["comment"].doubleValue
                            if strValue > dictValidate["max_value"].doubleValue{
                                var strValidation = dict["option_name"].stringValue
                                strValidation += " \(getCommonString(key: "should_be_maximum_key"))"
                                strValidation += " \(dictValidate["max_value"].stringValue)"
                                makeToast(strMessage: strValidation)
                                return
                            }
                        }
                    }
                }*/
                var dictOption = JSON()
                dictOption["check_multiple"] = []
                dictOption["product_option_id"].stringValue = dict["product_option_id"].stringValue
                dictOption["option_name"].stringValue = dict["option_name"].stringValue
                dictOption["comment"].stringValue = dict["comment"].stringValue
                dictOption["is_type"].stringValue = dict["is_type"].stringValue
                dictOption["product_option_value_id"].stringValue = dict["product_option_value_id"].stringValue
                dictOption["price"].stringValue = dict["price"].stringValue
                dictOption["option_value_name"].stringValue = dict["option_value_name"].stringValue
                dictOption["quantity"].stringValue = dict["quantity"].stringValue
                arrOption.append(dictOption)
            }
        }
//        print("arrOption \(arrOption)")
        
        var dictAccessories = JSON()
        dictAccessories["accessory_category_id"].stringValue = dictAccessoriesDetail["accessory_category_id"].stringValue
        dictAccessories["product_id"].stringValue = dictAccessoriesDetail["product_id"].stringValue
        dictAccessories["product_name"].stringValue = dictAccessoriesDetail["product_name"].stringValue
        dictAccessories["store_id"].stringValue = dictAccessoriesDetail["store_id"].stringValue
        dictAccessories["quantity"].stringValue = self.lblCartItem.text ?? ""
        dictAccessories["price"].stringValue = dictAccessoriesDetail["price"].stringValue
        dictAccessories["options"] = JSON(arrOption)
        
        objCartModel = CartModel()
        objCartModel.dictProductJson["accessories"] = dictAccessories
        
        print("final \(objCartModel.dictProductJson)")
        
        if arrOption.count < arrAccessoriesDetail.count{
            makeToast(strMessage: "Please fill up all detail")
            return
        }
        addProductToCart()
        */
    }
    
    @IBAction func btnShareAction(_ sender:UIButton){
        //Set the default sharing message.
        let message = getCommonString(key: "Product_share_msg_key")
        //Set the link to share.
        if let link = NSURL(string: "http://google.com") {
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.print, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.saveToCameraRoll, UIActivity.ActivityType.postToFlickr, UIActivity.ActivityType.postToVimeo, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.message, UIActivity.ActivityType.mail, UIActivity.ActivityType.openInIBooks]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnFavAction(_ sender:UIButton){
        
        if getUserDetail("is_login_guest") == "1" {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
        }
        else {
            if dictAccessoriesDetail["is_favorite"].stringValue == "0"{
                addFavUnfav(status: "1")
            } else {
                addFavUnfav(status: "0")
            }
        }
        /*
        if dictAccessoriesDetail["is_favorite"].stringValue == "0"{
            addFavUnfav(status: "1")
        } else {
            addFavUnfav(status: "0")
        }
        */
    }
    
    @IBAction func btnAddAction(_ sender:UIButton){
        var totalItem = Int(self.lblCartItem.text!)!
        totalItem += 1
        
        /*if self.dictAccessoriesDetail["quantity"].intValue < totalItem {
            makeToast(strMessage: getCommonString(key: "No_More_item_add_key"))
            return
        }*/
        
        self.lblCartItem.text = "\(totalItem)"
    }
    
    @IBAction func btnMinusAction(_ sender:UIButton){
        var totalItem = Int(self.lblCartItem.text!)!
        totalItem -= 1
        if totalItem < 1{
            return
        }
        self.lblCartItem.text = "\(totalItem)"
    }
}

//MARK: collection Datasource/Delegate
extension AccessoriesDetailVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionImg{
            return dictAccessoriesDetail["product_images"].arrayValue.count
        }
        return arrAccessoriesDetail[collectionView.tag]["option_value"].arrayValue.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionImg {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TailorsDetailCollectionCell", for: indexPath) as! TailorsDetailCollectionCell
            let arrAccessoriesImage = dictAccessoriesDetail["product_images"].arrayValue
            let dict = arrAccessoriesImage[indexPath.row]
            cell.imgTailors.sd_setImage(with: dict["product_image"].url, placeholderImage: UIImage(named: "ic_reviews_user_profile_splash_holder"), options: .lowPriority, completed: nil)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectionCell", for: indexPath) as! SelectionCell
        let arrOption = arrAccessoriesDetail[collectionView.tag]["option_value"].arrayValue
        let type = arrAccessoriesDetail[collectionView.tag]["is_type"].stringValue
        let dict = arrOption[indexPath.row]
        if type == accessoriesDetailType.radio.rawValue{
            cell.btnCheckOutlet.isHidden = true
            if dict["isSelected"].stringValue == "0"{
                cell.btnRadioOutlet.isSelected = false
            } else {
                cell.btnRadioOutlet.isSelected = true
            }
        } else {
            cell.btnRadioOutlet.isHidden = true
            if dict["isSelected"].stringValue == "0"{
                cell.btnCheckOutlet.isSelected = false
            } else {
                cell.btnCheckOutlet.isSelected = true
            }
        }
        cell.lblTitle.text = dict["option_value_name"].stringValue
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionImg{
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
        return CGSize(width: collectionView.frame.size.width/2, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView != collectionImg {
            var arrOption = arrAccessoriesDetail[collectionView.tag]["option_value"].arrayValue
            let type = arrAccessoriesDetail[collectionView.tag]["is_type"].stringValue
            if type == accessoriesDetailType.radio.rawValue{
                for i in 0..<arrOption.count{
                    var dict = arrOption[i]
                    dict["isSelected"] = "0"
                    arrOption[i] = dict
                }
                arrOption[indexPath.row]["isSelected"] = "1"
            } else {
                var dict = arrOption[indexPath.row]
                if dict["isSelected"].stringValue == "0"{
                    dict["isSelected"].stringValue = "1"
                } else {
                    dict["isSelected"].stringValue = "0"
                }
                arrOption[indexPath.row] = dict
            }
            arrAccessoriesDetail[collectionView.tag]["option_value"] = JSON(arrOption)
            collectionView.reloadData()
        }
    }
    
}

//MARK:- Tableview Datasource and Delegate

extension AccessoriesDetailVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAccessoriesDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var dict = arrAccessoriesDetail[indexPath.row]
      
        if dict["is_type"].stringValue == accessoriesDetailType.select.rawValue {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "SelectCell") as! SelectCell
            cell.selectionStyle = .none
            cell.lblTitle.text = dict["option_name"].stringValue
            cell.txtSelect.text = getCommonString(key: "Select_key")
            let arrOptionValue = dict["option_value"].arrayValue
            sizeDD.dataSource = arrOptionValue.map(({$0["option_value_name"].stringValue }))
            cell.handlerTextfield = {[weak self] in
                self?.configureDropdown(dropdown: (self?.sizeDD)!, sender: cell.txtSelect)
                self?.sizeDD.show()
            }
            sizeDD.selectionAction = { (index: Int, item: String) in
                cell.txtSelect.text = item
                if let index = arrOptionValue.index(where: {$0["option_value_name"].stringValue == item}) {
                    dict["product_option_value_id"].stringValue = arrOptionValue[index]["product_option_value_id"].stringValue
                    dict["quantity"].stringValue = arrOptionValue[index]["quantity"].stringValue
                    dict["price"].stringValue = arrOptionValue[index]["price"].stringValue
                }
                dict["option_value_name"].stringValue = item
                dict["comment"].stringValue = ""
                dict["check_multiple"] = []
                self.arrAccessoriesDetail[indexPath.row] = dict
            }
            return cell
        } else if dict["is_type"].stringValue == accessoriesDetailType.radio.rawValue || dict["is_type"].stringValue == accessoriesDetailType.check.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RadioCell") as! RadioCell
            cell.lblTitle.text = dict["option_name"].stringValue
            cell.selectionStyle = .none
            cell.collectionRadio.register(UINib(nibName: "SelectionCell", bundle: nil), forCellWithReuseIdentifier: "SelectionCell")
            cell.collectionRadio.tag = indexPath.row
            cell.collectionRadio.delegate = self
            cell.collectionRadio.dataSource = self
            DispatchQueue.main.async {
                cell.collectionRadio.reloadData()
            }
            cell.contentView.layoutIfNeeded()
            cell.contentView.layoutSubviews()
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccessoriesDetailCell") as! AccessoriesDetailCell
        cell.lblTitle.text = dict["option_name"].stringValue
        if dict["is_type"].stringValue == accessoriesDetailType.text.rawValue{
            cell.txtField.isHidden = false
            cell.vwTextView.isHidden = true
            cell.txtView.isHidden = true
            cell.lblPlaceholder.isHidden = true
            let arrValidate = dict["validate"].arrayValue
            for i in 0..<arrValidate.count{
                if arrValidate[i]["id"].stringValue == accessoriesValidParam.alphabets.rawValue{
                    cell.txtField.keyboardType = .alphabet
                    break
                } else if arrValidate[i]["id"].stringValue == accessoriesValidParam.decimal.rawValue {
                    cell.txtField.keyboardType = .decimalPad
                    self.addDoneButtonOnKeyboard(textfield: cell.txtField)
                    break
                } else if arrValidate[i]["id"].stringValue == accessoriesValidParam.numeric.rawValue {
                    cell.txtField.keyboardType = .numberPad
                    self.addDoneButtonOnKeyboard(textfield: cell.txtField)
                    break
                }
            }
        } else {
            cell.txtField.isHidden = true
            cell.vwTextView.isHidden = false
            cell.txtView.isHidden = false
            cell.lblPlaceholder.isHidden = false

        }
        cell.handlerTextfield = {[weak self] str in
            if dict["is_type"].stringValue == accessoriesDetailType.text.rawValue{
                cell.txtView.isHidden = true
                cell.txtField.text = str
            } else {
                cell.txtField.isHidden = true
                cell.txtView.text = str
            }
            dict["comment"].stringValue = str.trimmingCharacters(in: .whitespacesAndNewlines)
            dict["product_option_value_id"].stringValue = ""
            dict["option_value_name"].stringValue = ""
            dict["quantity"].stringValue = ""
            dict["price"].stringValue = ""
            dict["check_multiple"] = []
            self?.arrAccessoriesDetail[indexPath.row] = dict
        }
        return cell
    }
}



//MARK: - Product add to cart Delegate method

extension AccessoriesDetailVC: delegateProductAddCartPopup
{
    func redirectToGoToCart() {
        
        self.dismiss(animated: false, completion: nil)
        self.cartTapped()
        
//        self.navigationController?.popToRootViewController(animated: false)
        
    }
    
    func redirectToAddMore() {
         self.navigationController?.popToRootViewController(animated: false)
    }
    
}

//MARK:- Service

extension AccessoriesDetailVC {
    
    func getAccessoriesDetail()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlAccessories)\(urlAccessoriesDetail)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "product_id" : dictAccessories["product_id"].stringValue
                     ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.dictAccessoriesDetail = json["data"]
                        self.setupData(dict: json["data"])
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addProductToCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlAddToCart)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                     "category_id" : enumCategoryId.accessories.rawValue,
                     "business_id" : dictAccessoriesDetail["store_id"].stringValue,
                     "quantity": self.lblCartItem.text ?? "",
                     "product_cart_json": JSON(objCartModel.dictProductJson).rawString(),
                     "is_login_guest":getUserDetail("is_login_guest") == "1" ? "1" : "0"
                ] as! [String : String]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProductAddToCartVc") as! ProductAddToCartVc
                        obj.delegateAddCartPopup = self
                        obj.modalPresentationStyle = .overCurrentContext
                        obj.modalTransitionStyle = .crossDissolve
                        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addFavUnfav(status:String)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlFavUnfav)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "is_favorite" : status,
                     "role" : dictAccessoriesDetail["tailor_review_flag"].stringValue,
                     "business_id":dictAccessoriesDetail["product_id"].stringValue]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if self.selectedFav == .notFromFav {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                        }
                        
                        self.dictAccessoriesDetail["is_favorite"].stringValue = status
                        self.handlerUpdateAccessoriesList(self.dictAccessoriesDetail)
                        if self.dictAccessoriesDetail["is_favorite"].stringValue == "0"{
                            self.btnAddFavouriteOutlet.isSelected = false
                        } else {
                            self.btnAddFavouriteOutlet.isSelected = true
                        }
//                        self.setupData(dict: self.dictAccessoriesDetail)
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}

