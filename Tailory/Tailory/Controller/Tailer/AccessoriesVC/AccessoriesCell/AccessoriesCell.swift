//
//  AccessoriesCell.swift
//  Tailory
//
//  Created by Jaydeep on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class AccessoriesCell: UITableViewCell {
    
    //MARK:- Variable Declaration
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblAccessoriesName: UILabel!
    @IBOutlet weak var imgAccessories: UIImageView!
    
    
    //MARK:- View Life Cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if !isEnglish {
            setUpArabicUI()
        }
        
        [lblAccessoriesName].forEach { (lbl) in
            lbl?.font = themeFont(size: 22, fontname: .heavy)
            lbl?.textColor = .white
        }        
        
    }
    
    override func draw(_ rect: CGRect) {
        imgAccessories.layer.cornerRadius = 5
        imgAccessories.clipsToBounds = true
        imgAccessories.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpArabicUI() {
        
        lblAccessoriesName.transform = CGAffineTransform(scaleX: -1, y: 1)
        imgAccessories.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
}
