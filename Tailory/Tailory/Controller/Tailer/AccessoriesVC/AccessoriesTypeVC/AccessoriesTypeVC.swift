//
//  AccessoriesTypeVC.swift
//  Tailory
//
//  Created by Jaydeep on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import SDWebImage

class AccessoriesTypeVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dict = JSON()
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var strMessage = String()
    var arrProductList:[JSON] = []
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var collectionAccessoriesType: UICollectionView!
     @IBOutlet weak var vwMain: UIView!
    
    //MARK:- ViewLifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        registerXib()
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: dict["accessory_category_name"].stringValue)
    }
    
    func setUpUI() {
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        collectionAccessoriesType.addSubview(upperReferesh)
//        tblAccessories.tableFooterView = UIView()
        
        upperRefreshTable()
    
    }
    
    func registerXib() {
        
        self.collectionAccessoriesType.register(UINib(nibName: "AccessoriesTypeCell", bundle: nil), forCellWithReuseIdentifier: "AccessoriesTypeCell")
    }
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    @IBAction func btnFavAction(_ sender:UIButton){
        
        if getUserDetail("is_login_guest") as? String == "1" {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
        }
        else {
            let dict = arrProductList[sender.tag]
            if dict["is_favorite"].stringValue == "0"{
                addFavUnfav(status: "1", tag: sender.tag, dict: dict)
            } else {
                addFavUnfav(status: "0", tag: sender.tag, dict: dict)
            }
        }
        /*
        let dict = arrProductList[sender.tag]
        if dict["is_favorite"].stringValue == "0"{
            addFavUnfav(status: "1", tag: sender.tag, dict: dict)
        } else {
            addFavUnfav(status: "0", tag: sender.tag, dict: dict)
        }
         */
    }
    
    func updateAccessoriesList(dict:JSON){
        print("Data: \(dict)")
        if let index = arrProductList.index(where: {$0["product_id"].stringValue == dict["product_id"].stringValue}) {
            arrProductList[index]["is_favorite"] = dict["is_favorite"]
            self.collectionAccessoriesType.reloadItems(at: [IndexPath(item: index, section: 0)])
        }
    }
    

}

//MARK:- UICollectionView Datasource & Delegate

extension AccessoriesTypeVC :  UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrProductList.count == 0 {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = collectionView.center
            collectionView.backgroundView = lbl
            return 0
        }
        collectionView.backgroundView = nil
        return arrProductList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AccessoriesTypeCell", for: indexPath) as! AccessoriesTypeCell
        let dict = arrProductList[indexPath.row]
        cell.imgAccessories.sd_setImage(with: dict["product_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
        cell.lblAccessriesName.text = dict["product_name"].stringValue
        cell.lblAccessoriesDesciption.text = ""
//        cell.lblAccessoriesDesciption.attributedText = dict["description"].stringValue.htmlToAttributedString
        if dict["is_favorite"].stringValue == "0" {
            cell.btnFavouritesOutlet.isSelected = false
        } else {
            cell.btnFavouritesOutlet.isSelected = true
        }
        cell.lblPrice.text = "\(dict["price"].stringValue) \(getCommonString(key: "KD_key"))"
        cell.btnFavouritesOutlet.tag = indexPath.row
        cell.btnFavouritesOutlet.addTarget(self, action: #selector(btnFavAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width-20)/2
        let height = 300
        
        return CGSize(width: width, height: width+20)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("Select Data: \(arrProductList[indexPath.row])")
        let obj =  GlobalVariables.accessoriesStoryboard.instantiateViewController(withIdentifier: "AccessoriesDetailVC") as! AccessoriesDetailVC
        obj.dictAccessories = arrProductList[indexPath.row]
        obj.handlerUpdateAccessoriesList = {[weak self] dict in
            self?.updateAccessoriesList(dict: dict)
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

extension AccessoriesTypeVC {
    
    @objc func upperRefreshTable()
    {
        intOffset = 0
        getProductList()
    }
    
    func getProductList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlAccessories)\(urlAccessoriesProductList)"
            
            let param  = ["lang" : lang,
                          "offset" : String(self.intOffset),
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "accessory_category_id":dict["accessory_category_id"].stringValue]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrProductList = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.collectionAccessoriesType.reloadData()
                            return
                        }
                        
                        if self.intOffset == 0
                        {
                            self.arrProductList = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrProductList + aryData
                        
                        self.arrProductList = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrProductList = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.collectionAccessoriesType.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addFavUnfav(status:String,tag:Int,dict:JSON)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlFavUnfav)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "is_favorite" : status,
                     "role" : dict["tailor_review_flag"].stringValue,
                     "business_id":dict["product_id"].stringValue]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                        var dictData = dict
                        dictData["is_favorite"].stringValue = status
                        self.arrProductList[tag] = dictData
                        self.collectionAccessoriesType.reloadItems(at: [IndexPath(item: tag, section: 0)])
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(collectionAccessoriesType.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(collectionAccessoriesType.contentSize.height).rounded(toPlaces: 2)) - (Double(collectionAccessoriesType.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                getProductList()
            }
        }
    }
    
}
