//
//  AccessoriesTypeCell.swift
//  Tailory
//
//  Created by Jaydeep on 18/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class AccessoriesTypeCell: UICollectionViewCell {
    
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var imgAccessories: UIImageView!
    @IBOutlet weak var lblAccessriesName: UILabel!
    @IBOutlet weak var lblAccessoriesDesciption: UILabel!
    @IBOutlet weak var btnFavouritesOutlet: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var vwActivityWidth: NSLayoutConstraint!
    
    //MARK:- ViewLife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.vwActivityWidth.constant = 0
        self.activityIndicator.isHidden = true
        
        [lblAccessriesName].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .heavy)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        [lblPrice].forEach { (lbl) in
            lbl?.font = themeFont(size: 13, fontname: .heavy)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        lblAccessoriesDesciption.font = themeFont(size: 13, fontname: .medium)
        lblAccessoriesDesciption.textColor = UIColor.lightGray
        
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
    override func draw(_ rect: CGRect) {
        imgAccessories.layer.cornerRadius = 5
        imgAccessories.layer.masksToBounds = true
    }
    
    func setUpArabicUI() {
        
        [lblAccessriesName,lblPrice,lblAccessoriesDesciption].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            lbl?.textAlignment = .right
        }
        
        lblPrice.textAlignment = .left
        
        imgAccessories.transform = CGAffineTransform(scaleX: -1, y: 1)
    }

}
