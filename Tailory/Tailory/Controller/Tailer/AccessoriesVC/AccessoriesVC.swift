//
//  AccessoriesVC.swift
//  Tailory
//
//  Created by Khushbu on 15/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import SDWebImage

class AccessoriesVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblAccessories: UITableView!
    
    //MARK: Variables
    var arrAccessories: [JSON] = []
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var strMessage = String()
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        setUpUI()
        registerXib()
        
        if !isEnglish {
            setUpArabicUI()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Accessories_key"))
    }
    
}

//MARK: SetUpUI
extension AccessoriesVC {

    
    func setUpUI() {
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblAccessories.addSubview(upperReferesh)
        
        tblAccessories.tableFooterView = UIView()
        
        upperRefreshTable()
    }
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
}

//MARK: Register Xib
extension AccessoriesVC {
    
    func registerXib() {
        
        self.tblAccessories.register(UINib(nibName: "AccessoriesCell", bundle: nil), forCellReuseIdentifier: "AccessoriesCell")
    }
}

//MARK: TableView Datasource/Delegate
extension AccessoriesVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrAccessories.count == 0 {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrAccessories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccessoriesCell", for: indexPath) as! AccessoriesCell
        
        let dict = arrAccessories[indexPath.row]
        
        cell.lblAccessoriesName.text = dict["accessory_category_name"].stringValue
        cell.imgAccessories.sd_setImage(with: dict["accessory_category_image"].url, placeholderImage: UIImage(named: "ic_tailors_splash_holder"), options: .lowPriority, completed: nil)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        let obj = GlobalVariables.accessoriesStoryboard.instantiateViewController(withIdentifier: "AccessoriesTypeVC") as! AccessoriesTypeVC
        obj.hidesBottomBarWhenPushed = true
        obj.dict = arrAccessories[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
   
}

extension AccessoriesVC {
    
    @objc func upperRefreshTable()
    {
        intOffset = 0
        getAccessoriesList()
    }
    
    func getAccessoriesList()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlAccessories)\(urlAccessoriesList)"
            
            let param  = ["lang" : lang,
                          "offset" : String(self.intOffset)]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrAccessories = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblAccessories.reloadData()
                            return
                        }
                        
                        if self.intOffset == 0
                        {
                            self.arrAccessories = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrAccessories + aryData
                        
                        self.arrAccessories = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrAccessories = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblAccessories.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblAccessories.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblAccessories.contentSize.height).rounded(toPlaces: 2)) - (Double(tblAccessories.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                getAccessoriesList()
            }
        }
    }
    
}
