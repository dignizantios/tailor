//
//  HomeTableCell.swift
//  Tailory
//
//  Created by Khushbu on 16/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class HomeTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgTitleBG: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblComingSoon: UILabel!
    @IBOutlet weak var vwBg: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        [lblTitle,lblComingSoon].forEach { (lbl) in
            lbl?.font = themeFont(size: 27, fontname: .light)
            lbl?.textColor = .white
        }
    }

    func setUpArabicUI() {
        
        lblTitle.transform = CGAffineTransform(scaleX: -1, y: 1)
        lblComingSoon.transform = CGAffineTransform(scaleX: -1, y: 1)
        imgTitleBG.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
