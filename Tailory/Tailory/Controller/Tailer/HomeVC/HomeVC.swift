//
//  HomeVC.swift
//  Tailory
//
//  Created by Khushbu on 15/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import SDWebImage

class HomeVC: UIViewController {

    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblHome: UITableView!
    
    //MARK: Variables
    var arrayTblData : [JSON] = []
    var strMessage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        printFonts()
        getCategoryList()
        setUpUI()
        registerXib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCartCount()
        getLaundryPrice()
        self.setUpNavigationBarWithTitleAndCart(strTitle: getCommonString(key: "Home_key"))
        isAlreadyData = "0"
    }

}

//MARK: SetUpUI
extension HomeVC {
    
    func setUpUI() {
        
        self.tblHome.tableFooterView = UIView()
    }
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
}

//MARK: Register Xib
extension HomeVC {
    
    func registerXib() {
        
        self.tblHome.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
    }
}

//MARK: TableView Datasource/Delegate
extension HomeVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayTblData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell", for: indexPath) as! HomeTableCell
        
        let dict = arrayTblData[indexPath.row]
        
        if dict["is_block"].stringValue == "1"{
            cell.vwBg.isHidden = false            
            cell.lblComingSoon.isHidden = false
            cell.lblComingSoon.text = getCommonString(key: "Coming_soon_key")
        } else {
            cell.vwBg.isHidden = true
            cell.lblComingSoon.isHidden = true
        }
       
        cell.lblTitle.text = dict["category_name"].stringValue
        cell.imgTitleBG.sd_setImage(with: dict["category_image"].url, placeholderImage: UIImage(named: "ic_accessories_screen_splash_holder"), options: .lowPriority, completed: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = arrayTblData[indexPath.row]
        
        if dict["is_block"].stringValue == "1"{
            return
        }
        objCartModel = CartModel()
        strFabricAvailabel = ""
        if indexPath.row == 0 {
            
            let tailorsVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "TailorsVC") as! TailorsVC
            
            tailorsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(tailorsVC, animated: true)
        }
        else if indexPath.row == 1 {
            
            let fabricVC = GlobalVariables.fabricsStoryboard.instantiateViewController(withIdentifier: "FabricsVC") as! FabricsVC
            
            fabricVC.selectFabric = .ownFabric
            fabricVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(fabricVC, animated: true)
        }
        else if indexPath.row == 2
        {
            let accessories = GlobalVariables.accessoriesStoryboard.instantiateViewController(withIdentifier: "AccessoriesVC") as! AccessoriesVC
            
            self.navigationController?.pushViewController(accessories, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }
}
//MARK: - API calling

extension HomeVC
{
    func getCartCount()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlCartCount)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "is_login_guest": getUserDetail("is_login_guest") == "0" ? "0" : "1",
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString]
            
            print("param : \(param)")
            
            //self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                //self.hideLoader()
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].exists(){
                            let data = json["data"].dictionaryValue
                            isCartCount = data["cart_count"]?.intValue ?? 0
                            self.setUpNavigationBarWithTitleAndCart(strTitle: getCommonString(key: "Home_key"))
                            
                        }
                        
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        //makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                   // makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            //makeToast(strMessage: networkMsg)
        }
    }
    
    
    func getLaundryPrice()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlTrailor)\(urlLuandryPrice)"
            
            let param  = ["lang" : lang]
            
            print("param : \(param)")
            
            //self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                //self.hideLoader()
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].exists(){
                            let data = json["data"].stringValue
                            Defaults.removeObject(forKey: "LaundryPrice")
                            Defaults.set(data, forKey: "LaundryPrice")
                            Defaults.synchronize()
                            
                        }
                        
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        //makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    // makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            //makeToast(strMessage: networkMsg)
        }
    }
    
    func getCategoryList()
    {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlTrailor)\(urlCategoryList)"
            
            let param  = ["lang" : lang,
                          "timezone":getCurrentTimeZone()]
            
            
            print("param : \(param)")
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        if data.count == 0{
                            self.arrayTblData = []
                            self.strMessage = json["msg"].stringValue
                        } else {
                            self.arrayTblData = []
                            self.arrayTblData = json["data"].arrayValue
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayTblData = []
                        self.strMessage = json["msg"].stringValue
                    }
    
                    self.tblHome.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}

