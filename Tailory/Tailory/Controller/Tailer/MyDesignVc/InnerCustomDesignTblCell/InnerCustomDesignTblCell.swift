//
//  InnerCustomDesignTblCell.swift
//  Tailory
//
//  Created by Haresh on 04/09/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class InnerCustomDesignTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblCustomizeDesignTitle: UILabel!
    
    @IBOutlet weak var lblCustomizeDesignValue: UILabel!
    
    //MARK: - VIew life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblCustomizeDesignTitle.font = themeFont(size: 15, fontname: .medium)
        lblCustomizeDesignValue.font = themeFont(size: 15, fontname: .medium)

        lblCustomizeDesignTitle.textColor = UIColor.appThemeBlackColor
        lblCustomizeDesignValue.textColor = UIColor.appThemeLightGrayColor
        
        if !isEnglish
        {
            
            [lblCustomizeDesignTitle,lblCustomizeDesignValue].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
