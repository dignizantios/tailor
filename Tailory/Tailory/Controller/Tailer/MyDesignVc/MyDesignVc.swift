//
//  MyDesignVc.swift
//  Tailory
//
//  Created by Haresh on 27/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class MyDesignVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblMyDesign: UITableView! // Tag = 100
    
    //MARK: - Variable
    
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var arrayDesign : [JSON] = []
    var strMessage = String()
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "My_design_key"))
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Saved_desings_key"))        
        self.tblMyDesign.layoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLayoutSubviews() {
        self.tblMyDesign.layoutSubviews()
        self.tblMyDesign.reloadData()
    }
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        
        self.tblMyDesign.register(UINib(nibName: "MyDesignTableCell", bundle: nil), forCellReuseIdentifier: "MyDesignTableCell")
        
        self.tblMyDesign.tableFooterView = UIView()
//        self.tblMyDesign.tag = 100
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblMyDesign.addSubview(upperReferesh)
        
        upperRefreshTable()
        
    }
    
}

//MARK: - TableView delegate Method

extension MyDesignVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblMyDesign // Main table
        {
            if arrayDesign.count == 0
            {
                let lbl = UILabel()
                lbl.text = strMessage
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeBlackColor
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            tableView.backgroundView = nil
            return self.arrayDesign.count
        }

        return self.arrayDesign[tableView.tag]["customize_design"].count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblMyDesign
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyDesignTableCell") as! MyDesignTableCell
            
            let dict = self.arrayDesign[indexPath.row]
            
    //        cell.lblDesignName.text = dict["design_name"].stringValue
            cell.lblDesignName.text = dict["title"].stringValue
            
            cell.img.sd_setShowActivityIndicatorView(true)
            cell.img.sd_setIndicatorStyle(.gray)

            cell.img.sd_setImage(with: dict["design_image"].url, placeholderImage: UIImage(named: "ic_tailors_splash_holder"), options: .lowPriority, completed: nil)
            
            cell.btnEdit.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
//            cell.btnRadioSelection.tag = indexPath.row
            
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteAction(_:)), for: .touchUpInside)
            cell.btnEdit.addTarget(self, action: #selector(btnEditAction(_:)), for: .touchUpInside)
//            cell.btnRadioSelection.addTarget(self, action: #selector(btnRadionSelectionAction(_:)), for: .touchUpInside)
            
            if dict["is_selected"].stringValue == "0" {
                cell.btnRadioSelection.isSelected = false
            }
            else {
                cell.btnRadioSelection.isSelected = true
            }
            
            cell.tblInnerCustomDesign.tag = indexPath.row
            cell.tblInnerCustomDesign.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
            cell.tblInnerCustomDesign.delegate = self
            cell.tblInnerCustomDesign.dataSource = self
            cell.tblInnerCustomDesign.reloadData()
            cell.tblInnerCustomDesign.layoutIfNeeded()
            cell.tblInnerCustomDesign.layoutSubviews()
            return cell

        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "InnerCustomDesignTblCell") as! InnerCustomDesignTblCell
        
        var dict = self.arrayDesign[tableView.tag]["customize_design"][indexPath.row]
        cell.lblCustomizeDesignTitle.text = dict["style_name"].stringValue + ": "
        cell.lblCustomizeDesignValue.text = dict["design_name"].stringValue

        
       // cell.lblCustomizeDesign.addDifferenceColorinLabel(str1: "\(dict["style_name"].stringValue): ", color1: UIColor.appThemeBlackColor, str2: dict["design_name"].stringValue, color2: UIColor.appThemeLightGrayColor)
        
        return cell

    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrayDesign[indexPath.row]["is_selected"].stringValue == "1" {
            redirerctMeasurement(indexPath: indexPath)
        } else {
             checkTailorAdded(indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .gray
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    
    @objc func btnRadionSelectionAction(_ sender: UIButton) {
        
    }
    
    func redirerctMeasurement(indexPath:IndexPath){
        var dict = JSON()
        let dictDesign = arrayDesign[indexPath.row]
        dict["tbl_trailor_id"].stringValue = dictTailorDetail["tbl_trailor_id"].stringValue
        dict["tailor_user_id"].stringValue = dictTailorDetail["tailor_user_id"].stringValue
        dict["tbl_trailor_design_id"].stringValue = dictDesign["tbl_trailor_design_id"].stringValue
        dict["price"].stringValue = dictDesign["price"].stringValue
        dict["is_my_design"].stringValue = enumCustomDesign.addMyDesign.rawValue
        dict["tbl_my_design_id"].stringValue = dictDesign["tbl_my_design_id"].stringValue
        dict["customize"] = []
        dict["is_diff_tailor"].stringValue = isAlreadyData
        objCartModel.quantity = "1"
        objCartModel.dictProductJson["tailor"] = dict
    
        for i in 0..<self.arrayDesign.count {
            var data = self.arrayDesign[i]
            data["is_selected"].stringValue = "0"
            self.arrayDesign[i] = data
        }
        
        var dictData = self.arrayDesign[indexPath.row]
        dictData["is_selected"].stringValue = "1"
        self.arrayDesign[indexPath.row] = dictData
        self.tblMyDesign.reloadData()
        
        let measurmentVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MeasurmentVC") as! MeasurmentVC
        self.navigationController?.pushViewController(measurmentVC, animated: true)
    }
}

//MARK:- Action Zonr

extension MyDesignVc {
    @objc func btnDeleteAction(_ sender:UIButton) {
        let dict = arrayDesign[sender.tag]
        print("dict \(dict)")
        setupRemoveAlertController(dict: dict)
    }
    
    func setupRemoveAlertController(dict:JSON){
        let alertController = UIAlertController(title: getCommonString(key: "Tailors_key"), message: getCommonString(key: "Are_you_sure_you_want_to_remove_this_item_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.deleteSavedDesign(dict: dict)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func btnEditAction(_ sender:UIButton) {
        let dict = arrayDesign[sender.tag]
        print("dict \(dict)")
        let obj = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
        obj.selectedController = .editCart
        obj.dictCustomize = dict
        obj.handlerUpdateCart = {[weak self] in
            self?.upperRefreshTable()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
}


//MARK: - API calling

extension MyDesignVc
{
    
    @objc func upperRefreshTable()
    {
        intOffset = 0
        listOfMyDesignAPI()
    }
    
    func listOfMyDesignAPI()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlUser)\(urlMyDesignList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "offset" : String(self.intOffset),
                          "tailor_user_id":dictTailorDetail["tailor_user_id"].stringValue
                        ]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrayDesign = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblMyDesign.reloadData()
                            return
                        }
                        
                        if self.intOffset == 0
                        {
                            self.arrayDesign = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        
                        for i in 0..<aryData.count {
                            var dict = aryData[i]
                            dict["is_selected"] = "0"
                            aryData[i] = dict
                            print("aryData:", aryData)
                        }
                        aryData = self.arrayDesign + aryData
                        
                        self.arrayDesign = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayDesign = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblMyDesign.reloadData()
                    self.viewDidLayoutSubviews()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func deleteSavedDesign(dict:JSON)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlRemoveMyDesign)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "tbl_trailor_design_id" : dict["tbl_trailor_design_id"].stringValue,
                          "tailor_user_id" : dict["tailor_user_id"].stringValue
            ]
            
            print("param : \(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.upperRefreshTable()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func checkTailorAdded(indexPath:IndexPath){
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlCheckCartItem)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "category_id" : enumCategoryId.tailor.rawValue,
                          "business_id" : dictTailorDetail["business_id"].stringValue]
            
            
            print("param : \(param)")
            
            /*if intOffset == 0 {
                self.showLoader()
            }*/
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.redirerctMeasurement(indexPath: indexPath)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        self.showAlert(msg: json["msg"].stringValue, indexPath: indexPath)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func showAlert(msg:String,indexPath:IndexPath){
            let alertController = UIAlertController(title: getCommonString(key: "Tailory_key"), message: msg, preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                isAlreadyData = "1"
                self.redirerctMeasurement(indexPath: indexPath)
            }
            let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
                isAlreadyData = "0"
                self.redirerctMeasurement(indexPath: indexPath)
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblMyDesign.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblMyDesign.contentSize.height).rounded(toPlaces: 2)) - (Double(tblMyDesign.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                listOfMyDesignAPI()
            }
        }
    }
}
