//
//  MyDesignTableCell.swift
//  Tailory
//
//  Created by Haresh on 27/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class MyDesignTableCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet var btnRadioSelection: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDesignName: UILabel!
    
    @IBOutlet weak var tblInnerCustomDesign: UITableView!
    @IBOutlet weak var contrainTblInnerCustomDesignHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblCollarStyle: UILabel!
    @IBOutlet weak var lblCuffs: UILabel!
    @IBOutlet weak var lblSideLine: UILabel!
    
    @IBOutlet weak var btnEdit: CustomButton!
    @IBOutlet weak var btnDelete: CustomButton!

    @IBOutlet var vwEdit: CustomView!
    @IBOutlet var imgEdit: UIButton!
    @IBOutlet var lblEdit: UILabel!
    
    @IBOutlet var vwDelete: CustomView!
    @IBOutlet var imgDelete: UIButton!
    @IBOutlet var lblDelete: UILabel!
    @IBOutlet weak var lblDesignTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        img.layer.cornerRadius = 5.0
        img.layer.masksToBounds = true
        
        lblDesignName.font = themeFont(size: 16, fontname: .medium)
        lblDesignName.textColor = UIColor.appThemeBlackColor
        
        //lblDesignTitle.font = themeFont(size: 16, fontname: .medium)
      //  lblDesignTitle.textColor = UIColor.appThemeBlackColor
        
//        tblInnerCustomDesign.tag = 101
        tblInnerCustomDesign.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        btnRadioSelection.isUserInteractionEnabled = false
        
        lblEdit.text = getCommonString(key: "Edit_key")
        lblDelete.text = getCommonString(key: "Delete_key")
        
        [lblEdit, lblDelete].forEach { (lbl) in
            lbl?.font = themeFont(size: 13, fontname: .medium)
            lbl?.textColor = UIColor.appThemeSilverColor
        }
        
        /*
        [lblCollarStyle,lblCuffs,lblSideLine].forEach { (lbl) in
            lbl?.font = themeFont(size: 13, fontname: .light)
        }*/
        
        if !isEnglish
        {
            self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.img.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            lblDesignName.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblDesignName.textAlignment = .right
            
//            lblDesignTitle.transform = CGAffineTransform(scaleX: -1, y: 1)
//            lblDesignTitle.textAlignment = .right
            
            [lblEdit, lblDelete].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            [imgEdit, imgDelete].forEach { (img) in
                img?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            /*
            [lblCollarStyle,lblCuffs,lblSideLine,lblDesignName].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            */
        }

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblInnerCustomDesign.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
            if tblInnerCustomDesign.contentSize.height <= 68
            {
                self.contrainTblInnerCustomDesignHeight.constant = 68
            }
            else
            {
                self.contrainTblInnerCustomDesignHeight.constant = tblInnerCustomDesign.contentSize.height
                
            }
            
        }
    }

    
}
