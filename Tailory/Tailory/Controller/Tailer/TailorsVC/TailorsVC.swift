//
//  TailerVC.swift
//  Tailory
//
//  Created by Khushbu on 16/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class TailorsVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblTailors: UITableView!
    
    //MARK: Variables
    let segmentControllers = SJSegmentedViewController()
    var selectedSegment: SJSegmentTab?
    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var arrTailorList:[JSON] = []
    var strMessage = String()
    let txtSearch = UITextField()
    var arraySearchData:[JSON] = []
    var filterData = JSON()
    var sortData = JSON()

    
    //MARK:- ViewLifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        registerXib()
        setUpArabicUI()
        txtSearch.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        dictTailorDetail = JSON()
        
        if isEnglish {
            setUpNavigationBarWithBackHeaderTitleAndRightButtons(strTitle: getCommonString(key: "Tailors_key"))
        }
        else {
            setUpNavigationBarWithBackHeaderTitleAndRightButtonsArabic(strTitle: getCommonString(key: "Tailors_key"))
        }
        self.navigationBtnSetUp()
    }
    
}

//MARK: SetUpUI
extension TailorsVC {
    
    func setUpUI() {
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblTailors.addSubview(upperReferesh)
        
        tblTailors.tableFooterView = UIView()
        
        upperRefreshTable()
        
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func registerXib() {
        
        tblTailors.register(UINib(nibName: "TailorsTableCell", bundle: nil), forCellReuseIdentifier: "TailorsTableCell")
    }
    
    func setUpArabicUI() {
        
        if !isEnglish {
            
            self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func updateTailorList(dict:JSON){
        if let index = arrTailorList.index(where: {$0["tailor_user_id"].stringValue == dict["tailor_user_id"].stringValue}) {
            arrTailorList[index] = dict
            UIView.performWithoutAnimation {
                self.tblTailors.reloadData()
            }
            
//            self.tblTailors.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }

}

//MARK: Navigation Button Setup
extension TailorsVC {
    
    func navigationBtnSetUp() {
        
        let rightSortButton = UIBarButtonItem(image: UIImage(named: "ic_sort"), style: .plain, target: self, action: #selector(btnSortTapped))
        rightSortButton.tintColor = .white
        
        let rightFilterButton = UIBarButtonItem(image: UIImage(named: "ic_filter"), style: .plain, target: self, action: #selector(btnFilterTapped))
        rightFilterButton.tintColor = .white
        
        let rightSearchButton = UIBarButtonItem(image: UIImage(named: "ic_search"), style: .plain, target: self, action: #selector(btnSearchTapped))
        rightSearchButton.tintColor = .white
        
        if isEnglish {
            navigationItem.rightBarButtonItems = [rightSearchButton, rightFilterButton, rightSortButton]
        }
        else {
            navigationItem.leftBarButtonItems = [rightSearchButton, rightFilterButton, rightSortButton ]
        }
        
    }
    
    @objc func btnSortTapped() {
        
        let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "SortingVC") as! SortingVC
        objc.dictSortingData = self.sortData
        objc.handlerSort = { data in
            self.sortData = data!
            self.intOffset = 0
            self.listOfTailorAPI(search: self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        }
        objc.modalPresentationStyle = .overCurrentContext
        objc.modalTransitionStyle = .crossDissolve
        self.present(objc, animated: true, completion: nil)
    }
    
    @objc func btnFilterTapped() {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.dictFilterData = self.filterData
        obj.handlerFilter = { data in
            self.filterData = data!
            self.intOffset = 0
            self.listOfTailorAPI(search: self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnSearchTapped() {
        
        let vw = UIView()
        vw.frame = CGRect(x: 0, y: 0, width: self.navigationController?.navigationBar.frame.size.width ?? 375 - 200, height: 35)
        
        let img = UIImageView()
        img.frame = CGRect(x: 5, y: 8, width: vw.frame.size.height-16, height: vw.frame.size.height-16)
        img.image = UIImage(named: "ic_search")
        img.tintColor = .white
        vw.addSubview(img)
        txtSearch.returnKeyType = .search
        txtSearch.frame = CGRect(x: img.frame.size.width+10, y: 0, width: vw.frame.size.width-img.frame.size.width-135, height: 35)
        txtSearch.becomeFirstResponder()
        txtSearch.textColor = UIColor.black
        txtSearch.backgroundColor = .clear
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                          NSAttributedString.Key.font : themeFont(size: 14, fontname: .medium)]
        txtSearch.attributedPlaceholder = NSAttributedString(string: getCommonString(key: "Search_here_key"), attributes: attributes)
        txtSearch.font = themeFont(size: 14, fontname: .medium)
        vw.addSubview(txtSearch)
        
        vw.layer.cornerRadius = 5
        vw.layer.masksToBounds = true
        vw.backgroundColor = UIColor(red: 211/255, green: 212/255, blue: 211/255, alpha: 1.0)
        self.navigationItem.titleView = vw
        
        let btnClear = UIBarButtonItem(title: getCommonString(key: "Clear_key"), style: .plain, target: self, action: #selector(btnClearAction))
        btnClear.tintColor = .white
        
        
        if isEnglish {
            let leftButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow"), style: .plain, target: self, action: #selector(backButtonAction))
            leftButton.tintColor = UIColor.white
            navigationItem.hidesBackButton = true
            
            navigationItem.leftBarButtonItems = []
            navigationItem.leftBarButtonItem = leftButton
            
            navigationItem.rightBarButtonItems = []
            navigationItem.rightBarButtonItem = btnClear
        }
        else {
            
            vw.transform = CGAffineTransform(scaleX: -1, y: 1)
            img.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtSearch.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtSearch.textAlignment = .right
            
            let leftButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonAction))
            leftButton.tintColor = UIColor.white
            navigationItem.hidesBackButton = true
            navigationItem.rightBarButtonItems = []
            navigationItem.rightBarButtonItem = leftButton
            
            navigationItem.leftBarButtonItems = []
            navigationItem.leftBarButtonItem = btnClear
        }
    }
    
    @objc func btnClearAction() {
        if txtSearch.text != "" {
            self.txtSearch.text = ""
            self.upperRefreshTable()
        } else {
            self.view.endEditing(true)
            if isEnglish {
                setUpNavigationBarWithBackHeaderTitleAndRightButtons(strTitle: getCommonString(key: "Tailors_key"))
            }
            else {
                setUpNavigationBarWithBackHeaderTitleAndRightButtonsArabic(strTitle: getCommonString(key: "Tailors_key"))
            }
            self.navigationBtnSetUp()
        }
    }
    
    @objc func backButtonAction() {
        btnClearAction()
    }
    
}


//MARK: TableView Delegate/Datasource
extension TailorsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrTailorList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlackColor
            lbl.center = tableView.center
            
            if !isEnglish {
                lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrTailorList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TailorsTableCell", for: indexPath) as! TailorsTableCell
        
        cell.btnFavorite.tag = indexPath.row
        cell.btnFavorite.addTarget(self, action: #selector(btnFavoriteSelection(_:)), for: .touchUpInside)
       
//        cell.vwStatus.isHidden = true
        
        let dict = arrTailorList[indexPath.row]
        cell.setupData(dict: dict)
        /*if indexPath.row == 1 {
            cell.vwStatus.isHidden = false
            cell.isUserInteractionEnabled = false
            cell.contentView.alpha = 0.5
        }
        else {
            cell.vwStatus.isHidden = true
        }*/
        
        cell.lblStatus.text = getCommonString(key: "Busy_key")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        tailoryDetailVC()
        dictTailorDetail = arrTailorList[indexPath.row]
        
//        if dictTailorDetail["is_available"].stringValue == "1" {
//            return
//        }
        
        let tailerDetailVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "TailorsDetailVC") as! TailorsDetailVC
        tailerDetailVC.handlerUpdateTailorDetail = {[weak self] dict in
            self?.updateTailorList(dict: dict)
        }
        self.navigationController?.pushViewController(tailerDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
            // print("this is the last cell")
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.color = .black
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            
        } else {
            tableView.tableFooterView?.isHidden = true
            tableView.tableFooterView = nil
        }
    }
    
    @objc func btnFavoriteSelection(_ sender: UIButton) {        
        if getUserDetail("is_login_guest") == "1" {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
        }
        else {
            var dict = arrTailorList[sender.tag]
            if dict["is_favorite"].stringValue == "0"{
                dict["is_favorite"].stringValue = "1"
                addFavUnfav(status: "1", tag: sender.tag, dict: dict)
            } else {
                 dict["is_favorite"].stringValue = "0"
                addFavUnfav(status: "0", tag: sender.tag, dict: dict)
            }
            self.arrTailorList[sender.tag] = dict
            UIView.performWithoutAnimation {
                self.tblTailors.reloadData()
            }           
        }
        /*
        let dict = arrTailorList[sender.tag]
        if dict["is_favorite"].stringValue == "0"{
            addFavUnfav(status: "1", tag: sender.tag, dict: dict)
        } else {
            addFavUnfav(status: "0", tag: sender.tag, dict: dict)
        }
         */
    }
    
    func tailoryDetailVC() {
        
        let detailVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "TailorsDetailVC")
        
        let availableVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "AvailableDesignsVC")
        availableVC.title = getCommonString(key: "Available_designs_key")
        
        let priceListVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "PriceListVC")
        priceListVC.title = getCommonString(key: "Price_list_key")
        
        let moreInfoVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "MoreInfoVC")
        moreInfoVC.title = getCommonString(key: "More_info_key")
        
        segmentControllers.headerViewController = detailVC
        segmentControllers.segmentControllers = [availableVC, priceListVC, moreInfoVC]
        
        segmentControllers.headerViewHeight = 305
        segmentControllers.segmentViewHeight = 50.0
        segmentControllers.selectedSegmentViewHeight = 2.0
        segmentControllers.showsHorizontalScrollIndicator = false
        segmentControllers.showsVerticalScrollIndicator = false
        segmentControllers.segmentBounces = false
        
        segmentControllers.segmentTitleColor = UIColor.appThemeBlackColor
        segmentControllers.segmentSelectedTitleColor = UIColor.appThemeSilverColor
        segmentControllers.selectedSegmentViewColor = UIColor.appThemeSilverColor
        
        if !isEnglish {
            segmentControllers.segmentControllers = [moreInfoVC, priceListVC, availableVC]
            segmentControllers.setSelectedSegmentAt(1, animated: true)
        }
        navigationController?.pushViewController(segmentControllers, animated: true)
    }
    
}

//MARK: TextField Delegate
extension TailorsVC : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField:UITextField){
        if textField == txtSearch {
            self.intOffset = 0
            self.listOfTailorAPI(search: textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",isComeFromSearch: true)
//            self.viewWillAppear(true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("Clicked")
        if textField == self.txtSearch {
            self.self.intOffset = 0
            self.listOfTailorAPI(search: textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
            self.viewWillAppear(true)
            self.view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        /*
        let searchText = textField.text ?? "" + string
        if searchText.count >= 3 {
            self.arrTailorList = self.arraySearchData.filter({ (data) -> Bool in
                return data["trailor_name"].stringValue == searchText
            })
            self.tblTailors.reloadData()
        }
        else {
            self.arrTailorList = self.arraySearchData
        }
        self.tblTailors.reloadData()
        */
        return true
    }
    
}

extension TailorsVC
{
    @objc func upperRefreshTable()
    {
        intOffset = 0
        self.listOfTailorAPI(search: self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
    }
    
    func listOfTailorAPI(search:String = "",isComeFromSearch:Bool=false)
    {
//        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            if self.intOffset < 0
            {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlTrailor)\(urlTrailorList)"
            var param: [String : String] = [:]
            /*if self.filterData.isEmpty && self.sortData.isEmpty {
                param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                    "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)",
                    "is_available" : self.filterData["is_available"].stringValue,
                    "min_price" : self.filterData["min_price"].stringValue,
                    "max_price" : self.filterData["max_price"].stringValue,
                    "min_distance" : self.filterData["min_distance"].stringValue,
                    "max_distance" : self.filterData["max_distance"].stringValue,
                    "star_rating" : self.filterData["star_rating"].stringValue,
                    "search" : search
                ]
            }
            else if !self.filterData.isEmpty && self.sortData.isEmpty {
                param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                    "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)",
                    "is_available" : self.filterData["is_available"].stringValue,
                    "min_price" : self.filterData["min_price"].stringValue,
                    "max_price" : self.filterData["max_price"].stringValue,
                    "min_distance" : self.filterData["min_distance"].stringValue,
                    "max_distance" : self.filterData["max_distance"].stringValue,
                    "star_rating" : self.filterData["star_rating"].stringValue,
                    "search" : search
                ]
            }
            else if self.filterData.isEmpty && !self.sortData.isEmpty {
                param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                    "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)",
                    "sort" : self.sortData["sort"].stringValue,
                    "search" : search
                ]
            }
            else {*/
                print(self.filterData)
                print(self.sortData)
                param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "timezone" : getCurrentTimeZone(),
                          "offset" : String(self.intOffset),
                          "current_latitude" : "\(userCurrentLocation?.coordinate.latitude ?? 0.0)",
                    "current_longitude" : "\(userCurrentLocation?.coordinate.longitude ?? 0.0)",
                    "is_available" : self.filterData["is_available"].stringValue,
                    "min_price" : self.filterData["min_price"].stringValue,
                    "max_price" : self.filterData["max_price"].stringValue,
                    "min_distance" : self.filterData["min_distance"].stringValue,
                    "max_distance" : self.filterData["max_distance"].stringValue,
                    "sort" : self.sortData["sort"].stringValue,
                    "search" : search,
                    "star_rating" : self.filterData["star_rating"].stringValue
                ]
//            }
            print("param : \(param)")
            
            if isComeFromSearch == false{
                self.view.endEditing(true)
                if intOffset == 0 {
                   self.showLoader()
                }
            }
  
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.upperReferesh.endRefreshing()
                            self.arrTailorList = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblTailors.tableFooterView = UIView()
                            self.tblTailors.reloadData()
                            return
                        }
                        
                        if self.intOffset == 0
                        {
                            self.arrTailorList = []
                        }
                        
                        if self.intOffset < 0
                        {
                            return
                        }
                        
                        self.intOffset = json["next_offset"].intValue
                        
                        var aryData = json["data"].arrayValue
                        aryData = self.arrTailorList + aryData
                        
                        self.arrTailorList = aryData
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrTailorList = []
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblTailors.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addFavUnfav(status:String,tag:Int,dict:JSON)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlUser)\(urlFavUnfav)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param = ["lang" : lang,
                     "user_id" : getUserDetail("user_id"),
                     "access_token" : getUserDetail("access_token"),
                     "is_favorite" : status,
                     "role" : dict["tailor_review_flag"].stringValue,
                     "business_id":dict["business_id"].stringValue]
            
            print("param :\(param)")
            
//            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateFavouriteList"), object: "0")
                        /*var dictData = dict
                        dictData["is_favorite"].stringValue = status
                        self.arrTailorList[tag] = dictData*/
                        /*self.tblTailors.beginUpdates()
                        self.tblTailors.reloadRows(at: [IndexPath(row: tag, section: 0)], with: .automatic)
                        self.tblTailors.endUpdates()*/
//                        self.tblTailors.reloadData()
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strNotLogin {
                        
                    }
                }
                else {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.intOffset != -1 && self.intOffset != 0
        {
            if (Double(tblTailors.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblTailors.contentSize.height).rounded(toPlaces: 2)) - (Double(tblTailors.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2))
            {
                self.listOfTailorAPI(search: self.txtSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
            }
        }
    }
}



