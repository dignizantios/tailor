//
//  TailorsTableCell.swift
//  Tailory
//
//  Created by Khushbu on 16/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class TailorsTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var imgTailor: UIImageView!
    @IBOutlet weak var imgTailorLogo: UIImageView!
    @IBOutlet weak var lblTailerTitle: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var vwStarRatting: HCSStarRatingView!
    @IBOutlet weak var lblTotalReviews: UILabel!
    @IBOutlet weak var lblAvailableDesign: UILabel!
    @IBOutlet weak var lblAvailableDesignValue: UILabel!
    @IBOutlet weak var lblMinimumPrice: UILabel!
    @IBOutlet weak var lblMinimumPriceValue: UILabel!
    @IBOutlet weak var vwStatus: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblStatusHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vwImageOuter: UIView!
    @IBOutlet weak var vwAvailabelDesign: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
        
        setUpUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func draw(_ rect: CGRect) {
        imgTailor.clipsToBounds = true
        
        vwImageOuter.roundCorners([.topLeft,.topRight], radius: 5)
        vwImageOuter.clipsToBounds = true
    }
    
    func setUpUI() {
     
        [lblTailerTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        [lblAvailableDesign, lblAvailableDesignValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeSilverColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [lblTotalReviews, lblMinimumPrice, lblMinimumPriceValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeNobelColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [lblStatus].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeCyprusColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        lblAvailableDesign.text = getCommonString(key: "Available_Design_key")
        lblMinimumPrice.text = getCommonString(key: "Minimum_price_key")
//        lblStatus.text = getCommonString(key: "Unavailable_key")
        
        imgTailorLogo.layer.cornerRadius = imgTailorLogo.frame.height/2
        imgTailorLogo.clipsToBounds = true
        imgTailorLogo.backgroundColor = .white
        self.vwMain.alpha = 1.0
    }
    
    func setUpArabicUI() {
        
        self.imgTailor.transform = CGAffineTransform(scaleX: -1, y: 1)
        self.imgTailorLogo.transform = CGAffineTransform(scaleX: -1, y: 1)
        btnFavorite.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [lblTailerTitle].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblAvailableDesign, lblAvailableDesignValue].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblTotalReviews, lblMinimumPrice, lblMinimumPriceValue].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblStatus].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func setupData(dict:JSON) {
        
        imgTailor.sd_setImage(with: dict["trailor_image"].url, placeholderImage: UIImage(named: "ic_tailors_splash_holder"), options: .lowPriority, completed: nil)
        imgTailorLogo.sd_setImage(with: dict["trailor_logo"].url, placeholderImage: UIImage(named: "ic_reviews_user_profile_splash_holder"), options: .lowPriority, completed: nil)
        lblTailerTitle.text = dict["trailor_name"].stringValue
        vwStarRatting.value = CGFloat(dict["avg_rate"].floatValue)
        lblTotalReviews.text = "(\(dict["total_rating"].stringValue) \(getCommonString(key: "Review_key")))"
        lblAvailableDesignValue.text = dict["available_designs"].stringValue
        lblMinimumPriceValue.text = "\(dict["minimum_price"].stringValue) \(getCommonString(key: "KD_key"))"
        
        self.vwMain.alpha = 1.0
        if dict["is_available"].stringValue == "1" {
            self.vwMain.alpha = 0.65
            vwStatus.isHidden = false
        }
        else {
            vwStatus.isHidden = true
        }
        
        if dict["is_favorite"].stringValue == "1"{
            btnFavorite.isSelected = true
        } else {
            btnFavorite.isSelected = false
        }
    }
    
    func setupFabricData(dict:JSON) {
        
        self.vwMain.alpha = 1.0
        if dict["is_available"].stringValue == "1" {
            self.vwMain.alpha = 0.65
            vwStatus.isHidden = false
        }
        else {
            vwStatus.isHidden = true
        }
        
        imgTailor.sd_setImage(with: dict["branch_image"].url, placeholderImage: UIImage(named: "ic_tailors_splash_holder"), options: .lowPriority, completed: nil)
        imgTailorLogo.sd_setImage(with: dict["branch_logo"].url, placeholderImage: UIImage(named: "ic_reviews_user_profile_splash_holder"), options: .lowPriority, completed: nil)
        lblTailerTitle.text = dict["branch_name"].stringValue
        vwStarRatting.value = dict["avg_rate"].stringValue == "0" ? 0 : CGFloat(dict["avg_rate"].floatValue)
        lblTotalReviews.text = "(\(dict["total_rating"].stringValue) \(getCommonString(key: "Review_key")))"
        lblAvailableDesignValue.text = dict["available_fabrics"].stringValue
        lblMinimumPriceValue.text = "\(dict["minimum_price"].stringValue) \(getCommonString(key: "KD_key"))"
        if dict["is_favorite"].stringValue == "1"{
            btnFavorite.isSelected = true
        } else {
            btnFavorite.isSelected = false
        }
    }
    
}
