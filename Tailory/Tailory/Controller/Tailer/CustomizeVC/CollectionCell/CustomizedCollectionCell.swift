//
//  CustomizedCollectionCell.swift
//  Tailory
//
//  Created by Khushbu on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class CustomizedCollectionCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !isEnglish {
            setUpArabicUI()
        }
    }
    
    func setUpUI() {
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeSilverColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
    }
    
    func setUpArabicUI() {
        [lblTitle].forEach { (lbl) in
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [imgTitle].forEach { (img) in
            img?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }

}
