//
//  customizedHeaderTableCell.swift
//  Tailory
//
//  Created by Khushbu on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class customizedHeaderView: UIView {

    //MARK: Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSelection: UIButton!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "customizedHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! customizedHeaderView
    }
    
    func setUpUI() {
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeSilverColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        if !isEnglish {
            
            [lblTitle].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
        }
    }
    
}
