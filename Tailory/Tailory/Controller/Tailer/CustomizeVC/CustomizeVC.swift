//
//  CustomizeVC.swift
//  Tailory
//
//  Created by Khushbu on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import SDWebImage

enum checkParentForCustomize
{
    case comeFromTailorDetails
    case comeFromCart
    case editCart
}


class CustomizeVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblCustomized: UITableView!
    @IBOutlet weak var btnSaveToMyProfile: UIButton!
    @IBOutlet weak var btnCountinue: CustomButton!
    @IBOutlet weak var vwSaveToProfile: UIView!
    
    
    //MARK: Variables
    var arrayCustomize : [JSON] = []
    var selectedController = checkParentForCustomize.comeFromTailorDetails
    var dictCustomize = JSON()
//    var intOffset:Int = 0
    var upperReferesh = UIRefreshControl()
    var currentIndex = 1
    var handlerUpdateCart:() -> Void = {}
    
    //MARK:- ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !isEnglish {
            setUpArabicUI()
        }

        setUpUI()
        registerXib()
        self.navigationController?.navigationBar.isHidden = false

        /*
        if getUserDetail("is_login_guest") == "1" {
            self.btnSaveToMyProfile.isHidden = true
        }
         */
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedController == .comeFromTailorDetails
        {
            self.vwSaveToProfile.isHidden = false
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Customize_key"))
//            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Customize_key"))
            btnCountinue.setTitle(getCommonString(key: "Countinue_key"), for: .normal)
        }
        else if selectedController == .comeFromCart || selectedController == .editCart
        {
            self.vwSaveToProfile.isHidden = true
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Edit_design_key"))
            btnCountinue.setTitle(getCommonString(key: "Save_key"), for: .normal)
        }
    }
    
    override func backButtonTapped() {
        if currentIndex == 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            currentIndex -= 1
            hideBottomButton()
            self.tblCustomized.reloadData()
        }
    }
}


//MARK: setUp
extension CustomizeVC {
    
    func setUpUI() {
        
        [btnCountinue].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: .appThemeCyprusColor)
        }
        
        btnSaveToMyProfile.setupThemeButtonUI(backColor: UIColor.white)
        btnSaveToMyProfile.setTitle(getCommonString(key: "Save_to_my_profile_key"), for: .normal)
        btnSaveToMyProfile.setTitleColor(UIColor.appThemeCyprusColor, for: .normal)
        
//        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
//        tblCustomized.addSubview(upperReferesh)
        
        tblCustomized.tableFooterView = UIView()
        
        if selectedController == .comeFromCart {
            getDeisgnFromCart()
        } else if selectedController == .comeFromTailorDetails {
            upperRefreshTable()
        } else if selectedController == .editCart{
            getEditCustomizeDesign()
        }
    }
    
    func registerXib() {
        
        tblCustomized.register(UINib(nibName: "customizedTableCell", bundle: nil), forCellReuseIdentifier: "customizedTableCell")
    }
    
    
    func setUpArabicUI() {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [btnCountinue,btnSaveToMyProfile].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    func hideBottomButton(){
//        if arrayCustomize.count == currentIndex{
            if selectedController == .comeFromCart || selectedController == .editCart{
                self.vwSaveToProfile.isHidden = true
                btnCountinue.setTitle(getCommonString(key: "Done_key"), for: .normal)
            } else {
                self.vwSaveToProfile.isHidden = false
                btnCountinue.setTitle(getCommonString(key: "Countinue_key"), for: .normal)
            }
       /* } else {
            self.vwSaveToProfile.isHidden = true
            btnCountinue.setTitle(getCommonString(key: "Next_key"), for: .normal)
        }*/
    }
    
}

//MARK: Button Action
extension CustomizeVC {
    
    @IBAction func btnCountinueAction(_ sender: Any) {
        
        if selectedController == .comeFromTailorDetails
        {
            /*if arrayCustomize.count != currentIndex{
                currentIndex += 1
                hideBottomButton()
                self.tblCustomized.reloadData()
            } else{
                var dict = objCartModel.dictProductJson
                var arrayPassSavedData : [JSON] = []
                arrayPassSavedData = getCustimzeArray()
                dict["tailor"]["customize"] = JSON(arrayPassSavedData)                
                objCartModel.dictProductJson = dict
                let measurmentVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MeasurmentVC") as! MeasurmentVC
                measurmentVC.selectedController = .comeFromCustomize
                self.navigationController?.pushViewController(measurmentVC, animated: false)
            }*/
            for i in 0..<self.arrayCustomize.count {
                let arrayList = self.arrayCustomize[i]["list"].arrayValue
                
                if arrayList.contains(where: { (json) -> Bool in
                    if json["is_default"].stringValue == "1" {
                        return true
                    }
                    return false
                }) {
                    print("Selected at: \(i)")
                }
                else {
                    print("Not Selected at: \(i)")
                    makeToast(strMessage: "\(getCommonString(key: "Please_select_key")) \(self.arrayCustomize[i]["style_name"].stringValue)")
                    return
                }
            }
            
            var dict = objCartModel.dictProductJson
            var arrayPassSavedData : [JSON] = []
            arrayPassSavedData = getCustimzeArray()
            dict["tailor"]["customize"] = JSON(arrayPassSavedData)
            objCartModel.dictProductJson = dict
            let measurmentVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MeasurmentVC") as! MeasurmentVC
            measurmentVC.selectedController = .comeFromCustomize
            self.navigationController?.pushViewController(measurmentVC, animated: false)
        }
        else if selectedController == .comeFromCart
        {
            print("Come From Cart,Save Tapped")
           /* if arrayCustomize.count != currentIndex{
                currentIndex += 1
                hideBottomButton()
                self.tblCustomized.reloadData()
            } else{*/
                editDesignToCart()
//            }
        } else if  selectedController == .editCart {
            /*if arrayCustomize.count != currentIndex{
                currentIndex += 1
                hideBottomButton()
                self.tblCustomized.reloadData()
            } else {*/
                
                self.btnSaveToMyProfileTapped(self.btnSaveToMyProfile)
           // }
        }
    }
    
    @IBAction func btnSaveToMyProfileTapped(_ sender: UIButton) {
        
        for i in 0..<self.arrayCustomize.count {
            let arrayList = self.arrayCustomize[i]["list"].arrayValue
            
            if arrayList.contains(where: { (json) -> Bool in
                if json["is_default"].stringValue == "1" {
                    return true
                }
                return false
            }) {
                print("Selected at: \(i)")
            }
            else {
                print("Not Selected at: \(i)")
                makeToast(strMessage: "\(getCommonString(key: "Please_select_key")) \(self.arrayCustomize[i]["style_name"].stringValue)")
//                makeToast(strMessage: "Please select \(self.arrayCustomize[i]["style_name"].stringValue)")
                return
            }
        }
        
        if getUserDetail("is_login_guest") == "1" {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            obj.handlerIsGuest = {
                let objc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                
                objc.selectedController = .fromGuesdUserDetailPopup
                self.navigationController?.pushViewController(objc, animated: false)
            }
            self.present(obj, animated: false, completion: nil)
        }
        else {
            print("arrayCustomize:\(arrayCustomize)")
            var arrayPassSavedData : [JSON] = []
            arrayPassSavedData = getCustimzeArray()
            print("arraySavedData:\(arrayPassSavedData)")
            let obj = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "AddDesignNameVC") as! AddDesignNameVC
            if selectedController == .editCart {
                obj.strTitle = dictCustomize["title"].stringValue
                obj.selectedAddDesign = .edit
            }
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.handlerAddDesignName = {[weak self] name in
                self?.saveToMyProfileAPICalling(name:name,stringSavedData: JSON(arrayPassSavedData).rawString() ?? "")
            }
            self.present(obj, animated: false, completion: nil)
        }
        /*
         print("arrayCustomize:\(arrayCustomize)")
        var arrayPassSavedData : [JSON] = []
        arrayPassSavedData = getCustimzeArray()
        print("arraySavedData:\(arrayPassSavedData)")
        self.saveToMyProfileAPICalling(stringSavedData: JSON(arrayPassSavedData).rawString() ?? "")
         */
    }
    
    func getCustimzeArray() -> [JSON]{
        var arrayPassSavedData : [JSON] = []
        for i in 0..<arrayCustomize.count
        {
            var arrayList = arrayCustomize[i]["list"].arrayValue
            for j in 0..<arrayList.count
            {
                if arrayList[j]["is_default"].stringValue == "1"
                {
                    var dictSaveToProfile = JSON()
                    dictSaveToProfile["tbl_style_id"].stringValue = arrayList[j]["tbl_style_id"].stringValue
                    dictSaveToProfile["tbl_design_style_id"].stringValue = arrayList[j]["tbl_design_style_id"].stringValue
                    arrayPassSavedData.append(dictSaveToProfile)
                    break
                }
            }
        }
        return arrayPassSavedData
    }
    
}

//MARK: TableView Datasource/delegate
extension CustomizeVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if arrayCustomize.count == 0{
            return 0
        }
        return self.arrayCustomize.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customizedTableCell", for: indexPath) as! customizedTableCell
        
        cell.collectionCustomized.tag = indexPath.section
        cell.collectionCustomized.register(UINib(nibName: "CustomizedCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CustomizedCollectionCell")
        cell.collectionCustomized.delegate = self
        cell.collectionCustomized.dataSource = self
        cell.collectionCustomized.reloadData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = customizedHeaderView.instanceFromNib() as? customizedHeaderView
        
//        let dict = arrayCustomize[currentIndex - 1]
        let dict = arrayCustomize[section]
        
        view?.lblTitle.text = dict["style_name"].stringValue.uppercased()
        view?.setUpUI()
        view?.btnSelection.tag = section
//        view?.btnSelection.addTarget(self, action: #selector(btnSelectionAction(_:)), for: .touchUpInside)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    @objc func btnSelectionAction(_ sender: UIButton) {
        print("Index:", sender.tag)
        
        for i in 0..<arrayCustomize.count{
            arrayCustomize[i]["is_selected"] = "0"
        }
        arrayCustomize[sender.tag]["is_selected"] = "1"
        self.tblCustomized.reloadData()
        
    }
}

//MARK: Collection DataSource/Delegate
extension CustomizeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCustomize[collectionView.tag]["list"].count
//        return arrayCustomize[currentIndex - 1]["list"].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomizedCollectionCell", for: indexPath) as! CustomizedCollectionCell
        
//        let dict = arrayCustomize[currentIndex - 1]["list"].arrayValue[indexPath.row]
        let dict = arrayCustomize[collectionView.tag]["list"].arrayValue[indexPath.row]
        
        cell.imgTitle.sd_setImage(with: dict["style_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
        cell.lblTitle.text = dict["style_name"].stringValue
        
        if dict["is_default"].stringValue == "1"{
            cell.imgTitle.layer.borderColor = UIColor.appThemeSilverColor.cgColor
            cell.imgTitle.layer.borderWidth = 3
            cell.imgTitle.layer.cornerRadius = 5
            cell.imgTitle.clipsToBounds = true
        } else {
            cell.imgTitle.layer.borderWidth = 0
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var arrInner = arrayCustomize[collectionView.tag]["list"].arrayValue
//        var arrInner = arrayCustomize[currentIndex - 1]["list"].arrayValue
        
        for i in 0..<arrInner.count{
            var dict = arrInner[i]
            dict["is_default"].stringValue = "0"
            arrInner[i] = dict
        }
//        arrayCustomize[currentIndex - 1]["list"] = JSON(arrInner)
//        arrayCustomize[currentIndex - 1]["list"][indexPath.row]["is_default"].stringValue = "1"
        arrayCustomize[collectionView.tag]["list"] = JSON(arrInner)
        arrayCustomize[collectionView.tag]["list"][indexPath.row]["is_default"].stringValue = "1"
        collectionView.reloadData()
    }
    
}

//MARK:- Service
extension CustomizeVC {
    
    @objc func upperRefreshTable()
    {
//        intOffset = 0
        getCustomizeDesign()
    }
    
    func getCustomizeDesign()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlTrailor)\(urlTailorCustomizeList)"
            
            let param = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "tbl_trailor_design_id" : dictCustomize["tbl_trailor_design_id"].stringValue,
                          "tailor_user_id" : dictTailorDetail["tailor_user_id"].stringValue
            ]
            
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
             //   self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrayCustomize = json["data"].arrayValue
//                            self.strMessage = json["msg"].stringValue
                            self.tblCustomized.reloadData()
                            return
                        }
                        
                        var arrayData : [JSON] = []
                        arrayData = json["data"].arrayValue
                        if self.selectedController == .comeFromTailorDetails {
                            for i in 0..<arrayData.count {
                                var arrayList : [JSON] = []
                                arrayList = arrayData[i]["list"].arrayValue
                                for j in 0..<arrayList.count {
                                    var dict = arrayList[j]
                                    dict["is_default"].stringValue = "0"
                                    arrayList[j] = dict
                                }
                                arrayData[i]["list"] = JSON(arrayList)
                                print("ArrayData:", arrayData[i])
                            }
                            self.arrayCustomize = []
                            self.arrayCustomize = arrayData
//                            self.arrayCustomize = json["data"].arrayValue
                        }
                        else {
                            self.arrayCustomize = []
//                            self.arrayCustomize = arrayData
                            self.arrayCustomize = json["data"].arrayValue
                        }
                        
                       /* self.arrayCustomize = []
                        self.arrayCustomize = arrayData
                        self.arrayCustomize = json["data"].arrayValue */
                        self.hideBottomButton()
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayCustomize = []
                        //                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblCustomized.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func saveToMyProfileAPICalling(name:String,stringSavedData:String)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlTrailor)\(urlSetDesignToProfile)"
            
            var param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "tbl_trailor_design_id" : dictCustomize["tbl_trailor_design_id"].stringValue,
                          "my_design" : stringSavedData,
                          "title":name
            ]
            
            if selectedController == .comeFromTailorDetails {
                param["tailor_user_id"] = dictTailorDetail["tailor_user_id"].stringValue
            } else if selectedController == .editCart{
                 param["tailor_user_id"] = dictCustomize["tailor_user_id"].stringValue
            }
            
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
           //     self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        if self.selectedController == .editCart {
                            self.handlerUpdateCart()
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayCustomize = []
                        //                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblCustomized.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    func getDeisgnFromCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlGetDesignToCart)"
            
            var arrCustomizeDesign =  dictCustomize["customize_design"].arrayValue
            var arrPostCustmizaDesign:[JSON] = []
            for i in 0..<arrCustomizeDesign.count {
                var dict = JSON()
                dict["tbl_style_id"].stringValue = arrCustomizeDesign[i]["tbl_style_id"].stringValue
                dict["tbl_design_style_id"].stringValue = arrCustomizeDesign[i]["tbl_design_style_id"].stringValue
                arrPostCustmizaDesign.append(dict)
            }
            
            let param:[String:String]  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "tbl_trailor_design_id" : dictCustomize["tbl_trailor_design_id"].stringValue,
                          "tailor_user_id" : dictCustomize["tailor_user_id"].stringValue,
                          "customize_design" : JSON(arrPostCustmizaDesign).rawString() ?? ""]
            
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                //   self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrayCustomize = json["data"].arrayValue
                            self.tblCustomized.reloadData()
                            return
                        }
                        self.arrayCustomize = []
                        self.arrayCustomize = json["data"].arrayValue
                        self.hideBottomButton()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayCustomize = []
                    }
                    self.tblCustomized.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    func editDesignToCart()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlEditDesignCart)"
            
            var param:[String:String]  = ["lang" : lang,
                                          "user_id" : getUserDetail("user_id"),
                                          "access_token" : getUserDetail("access_token"),
                                          "tbl_trailor_design_id" : dictCustomize["tbl_trailor_design_id"].stringValue,
                                          "tailor_user_id" : dictCustomize["tailor_user_id"].stringValue,
                                          "is_my_design":dictCustomize["is_my_design"].stringValue,
                                          "tbl_my_design_id":dictCustomize["tbl_my_design_id"].stringValue,
                                          "cart_id":dictCustomize["cart_id"].stringValue]
            
            if dictCustomize["is_my_design"].stringValue == "0"{
                var arrayPassSavedData : [JSON] = []
                arrayPassSavedData = getCustimzeArray()
                var dictTailorJson = dictCustomize["tailor_json"]["tailor"]
                dictTailorJson["customize"] = JSON(arrayPassSavedData)
                dictCustomize["tailor_json"]["tailor"] = dictTailorJson
                print("dictTailorJson \(dictCustomize["tailor_json"])")
                param["product_cart_json"] = (dictCustomize["tailor_json"]).rawString()
            } else {
                var arrayPassSavedData : [JSON] = []
                arrayPassSavedData = getCustimzeArray()
                param["customize_design"] = JSON(arrayPassSavedData).rawString()
            }
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                //   self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.handlerUpdateCart()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    self.tblCustomized.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func getEditCustomizeDesign()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlCustomizeDesign)"
            
            var param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "tbl_trailor_design_id" : dictCustomize["tbl_trailor_design_id"].stringValue,
                          "tailor_user_id" : dictCustomize["tailor_user_id"].stringValue
            ]
            
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                //   self.upperReferesh.endRefreshing()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].arrayValue.count == 0 {
                            self.arrayCustomize = json["data"].arrayValue
                            self.tblCustomized.reloadData()
                            return
                        }
                        self.arrayCustomize = []
                        self.arrayCustomize = json["data"].arrayValue
                        self.hideBottomButton()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayCustomize = []
                        //                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblCustomized.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
  
}
