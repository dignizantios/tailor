//
//  RateTailorVC.swift
//  Tailory
//
//  Created by Khushbu on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON


enum CheckParentForRatting:String {
    case comeFromFabric = "2"
    case comeFromTailor = "1"
}



class RateTailorVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwDescription: UIView!
    @IBOutlet weak var txtVwDescription: UITextView!
    @IBOutlet weak var btnRate: CustomButton!
    @IBOutlet weak var vwRatingStar: HCSStarRatingView!
    
    //MARK: - Variable
    
    var selectedController = CheckParentForRatting.comeFromTailor
    var handlerUpdateTailorDetail : () -> Void = { }
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !isEnglish {
            setUpArabicUI()
        }
        setUpUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedController == .comeFromTailor
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Rate_tailor_key"))
        }
        else if selectedController == .comeFromFabric
        {
            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Rate_fabric_key"))
        }
        
        
    }
    
}

//MARK: setUp
extension RateTailorVC {
    
    func setUpUI() {
        
        [lblTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .medium)
        }
        
        [btnRate].forEach { (btn) in
            btn?.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .medium)
        }
        
        [vwDescription].forEach { (vw) in
            vw?.clipsToBounds = true
            vwDescription.layer.cornerRadius = 5
        }
        
        [txtVwDescription].forEach { (txtVw) in
            txtVw?.font = themeFont(size: 15, fontname: .medium)
            txtVw?.textColor = UIColor.appThemeBlackColor
        }
        
        lblTitle.text = getCommonString(key: "Write_your_review_key")
        btnRate.setTitle(getCommonString(key: "Rate_key"), for: .normal)
    }
    
    func setUpArabicUI() {
        
        vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [lblTitle].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [btnRate].forEach { (btn) in
            btn?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [txtVwDescription].forEach { (txtVw) in
            txtVw?.transform = CGAffineTransform(scaleX: -1, y: 1)
            txtVw?.textAlignment = .right
        }
        
        lblTitle.text = getCommonString(key: "Write_your_review_key")
        btnRate.setTitle(getCommonString(key: "Rate_key"), for: .normal)
    }
}

//MARK: Button Action
extension RateTailorVC {
    
    @IBAction func btnRateAction(_ sender: Any) {
        
        if getUserDetail("is_login_guest") as? String == "1" {
            
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotLoginRequiredVC") as! ForgotLoginRequiredVC
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: false, completion: nil)
        }
        else {
            if vwRatingStar.value < 0 {
                makeToast(strMessage: getCommonString(key: "Please_give_at_least_one_review_key"))
            } else if (txtVwDescription.text?.trimmingCharacters(in: .whitespaces))!.isEmpty {
                makeToast(strMessage: getCommonString(key: "Please_enter_comment_key"))
            } else {
                addReview()
            }
        }
    }
}

//MARK: - API calling

extension RateTailorVC {
    
    func addReview()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlTrailor)\(urlGiveRate)"
            
            print("URL: \(url)")
            
            var param = [String:String]()
            
            param =
                [
                    "lang" : lang,
                    "user_id" : getUserDetail("user_id"),
                    "access_token" : getUserDetail("access_token"),
                    "business_id" : selectedController == .comeFromTailor ? dictTailorDetail["business_id"].stringValue : dictFabricDetail["business_id"].stringValue,
                    "tailor_review_flag" : selectedController == .comeFromTailor ? dictTailorDetail["tailor_review_flag"].stringValue : dictFabricDetail["tailor_review_flag"].stringValue,
                    "rate_star" : "\(vwRatingStar.value)",
                    "comment" : txtVwDescription.text ?? "",
                    "timezone" : getCurrentTimeZone()
                    
            ]
            
            
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.handlerUpdateTailorDetail()
                        makeToast(strMessage: json["msg"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
}
