//
//  SavedForLaterVC.swift
//  Tailory
//
//  Created by Khushbu on 30/09/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import SDWebImage


class SavedForLaterVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet var tblSaveForLaters: UITableView!
    @IBOutlet var vwIndicatorHeight: NSLayoutConstraint!
    @IBOutlet var indicatorActivity: UIActivityIndicatorView!
    
    
    //MARK: Variables
    var intOffset:Int = 0
    var strMessage = String()
    var arrSavedLater : [JSON] = []
    var upperReferesh = UIRefreshControl()
    var isDiffeStore = "0"
    
    
    //MARK: Controller Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Saved_for_later_key"))
    }
    
}

//MARK: SetUpUI
extension SavedForLaterVC {
    
    func setUpUI() {
        
        self.tblSaveForLaters.register(UINib(nibName: "SaveForLaterTableCell", bundle: nil), forCellReuseIdentifier: "SaveForLaterTableCell")
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblSaveForLaters.addSubview(upperReferesh)
        
        tblSaveForLaters.tableFooterView = UIView()
        
        upperRefreshTable()
        
        if !isEnglish {
            setUpArabic()
        }
    }
    
    func setUpArabic() {
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    @objc func removeAlert(_ sender: UIButton) {
        
        let index = sender.tag
        print("Index: \(index)")
        
        let alrt = UIAlertController(title: "", message: getCommonString(key: "Are_you_sure_want_to_delete_key"), preferredStyle: .alert)
        
        let no = UIAlertAction(title: getCommonString(key: "No_key"), style: .cancel) { _ in
            print("No")
        }
        let yes = UIAlertAction(title: getCommonString(key: "Yes_key"), style: .default) { _ in
            print("Yes")
            self.removeSaveLater(index)
        }
        
        if isEnglish {
            alrt.addAction(no)
            alrt.addAction(yes)
        }
        else {
            alrt.addAction(yes)
            alrt.addAction(no)
        }
        self.present(alrt, animated: true, completion: nil)
    }
    
}

//MARK: Tableview Delegate/Datasource
extension SavedForLaterVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblSaveForLaters {
            
            if arrSavedLater.count == 0 {
                
                let lbl = UILabel()
                lbl.text = strMessage
                if !isEnglish {
                    lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                }
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeBlackColor
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            tableView.backgroundView = nil
            
            return self.arrSavedLater.count
        }
        return self.arrSavedLater[tableView.tag]["customize_design"].arrayValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblSaveForLaters {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SaveForLaterTableCell", for: indexPath) as! SaveForLaterTableCell
            
            let dict = self.arrSavedLater[indexPath.row]
            
            if dict["measurement_type"].stringValue == "1" || dict["measurement_type"].stringValue == "2" {
                
                cell.vwPreferDate.isHidden = false
                cell.vwSelectTime.isHidden = false
                cell.vwTest.isHidden = false
                cell.vwextraNotes.isHidden = false
                cell.vwRefrenceNumber.isHidden = true
                
                cell.lblQuantity.text = dict["quantity"].stringValue
                cell.lblPreferDateValue.text = dict["date"].stringValue
                cell.lblSelectTimeValue.text = dict["time"].stringValue
                cell.lblExtraNotesValue.text = dict["extra_notes"].stringValue
                
                var strSubAddress = "\n\(getCommonString(key: "Floor_key")):\(dict["address"]["floor"].stringValue), \n\n"
                strSubAddress += "\(getCommonString(key: "Building_key")):\(dict["address"]["building"].stringValue), \n\n"
                strSubAddress += "\(getCommonString(key: "Avenu_key")):\(dict["address"]["avenue"].stringValue), \n\n"
                strSubAddress += "\(getCommonString(key: "Street_key")):\(dict["address"]["street"].stringValue), \n\n"
                strSubAddress += "\(getCommonString(key: "Block_key")):\(dict["address"]["block"].stringValue), \n\n"
                strSubAddress += "\(getCommonString(key: "Postal_code_key")):\(dict["address"]["postal_code"].stringValue), \n\n"
                strSubAddress += "\(getCommonString(key: "City_key")):\(dict["address"]["city"].stringValue), \n\n"
                strSubAddress += "\(getCommonString(key: "Governorate_key")):\(dict["address"]["governorate"].stringValue) \n\n"
                strSubAddress += "\(getCommonString(key: "Country_key")):\(dict["address"]["country"].stringValue). "
                cell.lblTestValue.text = strSubAddress
                
                if dict["measurement_type"].stringValue == "1" {
                    cell.lblProductType.text = getCommonString(key: "Request_someone_to_take_mymeasurment_key")
                }
                else {
                    cell.lblProductType.text = getCommonString(key: "Request_someone_to_take_my_Dishdashah_key")
                }
                
                if dict["extra_notes"].stringValue == "" {
                    cell.vwextraNotes.isHidden = true
                }
                else {
                    cell.vwextraNotes.isHidden = false
                }
                
                cell.tblProduct.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblProduct.tag = indexPath.row
                cell.tblProduct.delegate = self
                cell.tblProduct.dataSource = self
                cell.tblProduct.reloadData()
                cell.tblProduct.layoutIfNeeded()
                cell.tblProduct.layoutSubviews()
                
            }
            else if dict["measurement_type"].stringValue == "3" {
                
                cell.vwPreferDate.isHidden = true
                cell.vwSelectTime.isHidden = true
                cell.vwTest.isHidden = false
                cell.vwextraNotes.isHidden = true
                cell.vwRefrenceNumber.isHidden = true
                
                var strSubMeasurment = "\n\(getCommonString(key: "Chest_key")):\(dict["measurement"]["chest"].stringValue), \n\n"
                strSubMeasurment += "\(getCommonString(key: "Ankle_key")):\(dict["measurement"]["ankle"].stringValue), \n\n"
                strSubMeasurment += "\(getCommonString(key: "Arm_key")):\(dict["measurement"]["arm"].stringValue), \n\n"
                strSubMeasurment += "\(getCommonString(key: "Sholders_key")):\(dict["measurement"]["sholder"].stringValue), \n\n"
                strSubMeasurment += "\(getCommonString(key: "Neck_key")):\(dict["measurement"]["neck"].stringValue), \n\n"
                strSubMeasurment += "\(getCommonString(key: "Height_key")):\(dict["measurement"]["height"].stringValue), \n\n"
                strSubMeasurment += "\(getCommonString(key: "Waist_key")):\(dict["measurement"]["waist"].stringValue), \n\n"
                strSubMeasurment += "\(getCommonString(key: "Thight_key")):\(dict["measurement"]["thight"].stringValue), \n\n"
                strSubMeasurment += "\(getCommonString(key: "Back_key")):\(dict["measurement"]["back"].stringValue). "
                cell.lblTestValue.text = strSubMeasurment
                
                cell.lblQuantity.text = dict["quantity"].stringValue
                cell.lblProductType.text = getCommonString(key: "My_measurment_key")
                
                cell.tblProduct.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblProduct.tag = indexPath.row
                cell.tblProduct.delegate = self
                cell.tblProduct.dataSource = self
                cell.tblProduct.reloadData()
                cell.tblProduct.layoutIfNeeded()
                cell.tblProduct.layoutSubviews()
                
            }
            else if dict["measurement_type"].stringValue == "4" {
                
                cell.vwPreferDate.isHidden = true
                cell.vwSelectTime.isHidden = true
                cell.vwTest.isHidden = true
                cell.vwextraNotes.isHidden = false
                cell.vwRefrenceNumber.isHidden = false
                
                cell.lblProductType.text = getCommonString(key: "Tailors_knows_me_key")
                cell.lblQuantity.text = dict["quantity"].stringValue
                
                if dict["extra_notes"].stringValue == "" {
                    cell.vwextraNotes.isHidden = true
                }
                else {
                    cell.vwextraNotes.isHidden = false
                }
                
                cell.tblProduct.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblProduct.tag = indexPath.row
                cell.tblProduct.delegate = self
                cell.tblProduct.dataSource = self
                cell.tblProduct.reloadData()
                cell.tblProduct.layoutIfNeeded()
                cell.tblProduct.layoutSubviews()
            }
            
            cell.btnAddToCart.tag = indexPath.row
            cell.btnAddToCart.addTarget(self, action: #selector(addToCart(_:)), for: .touchUpInside)
            
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(removeAlert(_:)), for: .touchUpInside)
            
            cell.lblProductName.text = dict["design_name"].stringValue
            cell.imgProduct.sd_setImage(with: dict["design_image"].url, placeholderImage: UIImage(named: "ic_reviews_user_profile_splash_holder"), options: .lowPriority, completed: nil)
            
            
            cell.btnPlus.tag = indexPath.row
            cell.handlerPlusValue = {[weak self] row in
                
                self?.arrSavedLater[row]["quantity"].intValue += 1
                tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
            }
            
            cell.btnMinus.tag = indexPath.row
            cell.btnMinus.accessibilityValue = "\(indexPath.section)"
            cell.handlerMinusValue = {[weak self] row in
                if self?.arrSavedLater[row]["quantity"].intValue == 1 {
                    return
                }
                self?.arrSavedLater[row]["quantity"].intValue -= 1
                tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
            }
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InnerCustomDesignTblCell") as! InnerCustomDesignTblCell
            
            let dict = self.arrSavedLater[tableView.tag]["customize_design"][indexPath.row]
            if isEnglish {
                cell.lblCustomizeDesignTitle.text = "\(dict["style_name"].stringValue) : "
            } else {
                cell.lblCustomizeDesignTitle.text = " : \(dict["style_name"].stringValue)"
            }
            
            cell.lblCustomizeDesignValue.text = dict["design_name"].stringValue


            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == self.tblSaveForLaters {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && intOffset != -1 && self.intOffset != 0{
                // print("this is the last cell")
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.color = .black
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                tableView.tableFooterView = spinner
                tableView.tableFooterView?.isHidden = false
                
            } else {
                tableView.tableFooterView?.isHidden = true
                tableView.tableFooterView = nil
            }
        }
    }
    
    //MARK:- Scrollview delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.intOffset != -1 && self.intOffset != 0 {
            if (Double(tblSaveForLaters.contentOffset.y).rounded(toPlaces: 2)) >= Double(((Double(tblSaveForLaters.contentSize.height).rounded(toPlaces: 2)) - (Double(tblSaveForLaters.bounds.size.height).rounded(toPlaces: 2))).rounded(toPlaces: 2)) {
                mySaveLaterList()
            }
        }
    }
    
}

//MARK: Services
extension SavedForLaterVC {
    
    @objc func upperRefreshTable() {
        intOffset = 0
        mySaveLaterList()
    }
    
    func mySaveLaterList() {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            if self.intOffset < 0 {
                self.upperReferesh.endRefreshing()
                return
            }
            
            let url = "\(urlUser)\(urlMySaveLaterList)"
            
            let param = ["lang" : lang,
                         "user_id" : getUserDetail("user_id"),
                         "access_token" : getUserDetail("access_token"),
                         "offset":String(self.intOffset),
                         "tbl_trailor_id":dictTailorDetail["tbl_trailor_id"].stringValue
            ]
            
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                self.upperReferesh.endRefreshing()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        if json["data"].arrayValue.count == 0 {
                            self.arrSavedLater = json["data"].arrayValue
                            self.strMessage = json["msg"].stringValue
                            self.tblSaveForLaters.reloadData()
                            return
                        }
                        if self.intOffset == 0 {
                            self.arrSavedLater = []
                        }
                        if self.intOffset < 0 {
                            return
                        }
                        
                        var arrData = json["data"].arrayValue
                        arrData = self.arrSavedLater + arrData
                        self.arrSavedLater = arrData
                        
                        self.intOffset = json["next_offset"].intValue
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.strMessage = json["msg"].stringValue
                    }
                    
                    self.tblSaveForLaters.reloadData()
                }
                else {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    @objc func addToCart(_ sender: UIButton) {
        checkTailorAdded(tag: sender.tag)
    }
    
    func addCart(_ sender:Int){
        let date = self.DateToString(Formatter: "yyyy-MM-dd HH:mm:ss", date: Date())
        print("Date:", date)
        
        print("Add Data:- \(self.arrSavedLater[sender])")
        
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            let url = "\(urlUser)\(urlAddToCartBySavedLater)"
            
            var param  = ["user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "item_id":self.arrSavedLater[sender]["item_id"].stringValue,
                          "lang":lang,
                          "device_token":Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "quantity":self.arrSavedLater[sender]["quantity"].stringValue,
                          "timezone":getCurrentTimeZone(),
                          "date_time":date,
                          "is_diff_tailor":isDiffeStore
            ]
            let dict =  arrSavedLater[sender]
            if dict["is_fabric_added"].stringValue == "1"{
                param["branch_id"] = dict["fabrics"][0]["branch_id"].stringValue
            }
            
            print("param : \(param)")
            
            //            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                //                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        
                        //                        self.arrSavedLater.remove(at: sender.tag)
                        //                        self.tblSaveForLaters.reloadData()
                        
                        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ProductAddToCartVc") as! ProductAddToCartVc
                        obj.delegateAddCartPopup = self
                        obj.modalPresentationStyle = .overCurrentContext
                        obj.modalTransitionStyle = .crossDissolve
                        appdelgate.window?.rootViewController?.present(obj, animated: false, completion: nil)
                        
                        //                        self.tblSaveForLaters.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.strMessage = json["msg"].stringValue
                    }
                }
                else {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    
    func removeSaveLater(_ sender: Int) {

        print("Remove Data:- \(self.arrSavedLater[sender])")
        
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            
            
            let url = "\(urlUser)\(urlRemoveSavedItems)"
            let param  = ["user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "item_id":self.arrSavedLater[sender]["item_id"].stringValue,
                          "lang" : lang
                          ]
            
            print("param : \(param)")
                self.showLoader()
 
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        self.arrSavedLater.remove(at: sender)
                        self.tblSaveForLaters.reloadData()
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.strMessage = json["msg"].stringValue
                    }
                }
                else {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func checkTailorAdded(tag:Int)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
           
            let url = "\(urlUser)\(urlCheckCartItem)"
            
            var param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "category_id" : enumCategoryId.tailor.rawValue
            ]
            
            let dict =  arrSavedLater[tag]
            if dict["is_fabric_added"].stringValue == "1"{
                param["branch_id"] = dict["fabrics"][0]["branch_id"].stringValue
            }
            param["business_id"] = dict["tbl_trailor_id"].stringValue
            
            print("param : \(param)")
            
            if intOffset == 0 {
                self.showLoader()
            }
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.addCart(tag)
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                       self.showAlert(msg: json["msg"].stringValue,tag: tag)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    //MARK:- Alert
    
    func showAlert(msg:String,tag:Int){
        let alertController = UIAlertController(title: getCommonString(key: "Tailory_key"), message: msg, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
//          isAlreadyData = "1"
            self.isDiffeStore = "1"
            self.addCart(tag)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
            self.isDiffeStore = "0"
            self.addCart(tag)
//            isAlreadyData = "0"
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension SavedForLaterVC : delegateProductAddCartPopup {
    
    func redirectToGoToCart() {
        self.cartTapped()
        
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func redirectToAddMore() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
}
