//
//  SaveForLaterTableCell.swift
//  Tailory
//
//  Created by Khushbu on 30/09/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class SaveForLaterTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var tblProduct: UITableView!
    
    @IBOutlet var lblProductType: UILabel!
    
    @IBOutlet var vwPreferDate: UIView!
    @IBOutlet var lblPreferDateTitle: UILabel!
    @IBOutlet var lblPreferDateValue: UILabel!
    
    @IBOutlet var vwSelectTime: UIView!
    @IBOutlet var lblSelectTimeTitle: UILabel!
    @IBOutlet var lblSelectTimeValue: UILabel!
    
    @IBOutlet var vwTest: UIView!
    @IBOutlet var lblTestTitle: UILabel!
    @IBOutlet var lblTestValue: UILabel!
    
    @IBOutlet var vwRefrenceNumber: UIView!
    @IBOutlet var lblReferenceNumberTitle: UILabel!
    @IBOutlet var lblReferenceNumberValue: UILabel!
    
    @IBOutlet var vwextraNotes: UIView!
    @IBOutlet var lblExtraNotesTitle: UILabel!
    @IBOutlet var lblExtraNotesValue: UILabel!
    
    @IBOutlet var vwQuantity: UIView!
    @IBOutlet var btnMinus: UIButton!
    @IBOutlet var lblQuantity: UILabel!
    @IBOutlet var btnPlus: UIButton!
    
    @IBOutlet var btnAddToCart: CustomButton!
    
    @IBOutlet var vwDelete: CustomView!
    @IBOutlet var lblDelete: UILabel!
    @IBOutlet var btnDelete: UIButton!
    
    //MARK: Variables
    var handlerPlusValue:(Int) -> Void = {_ in   }
    var handlerMinusValue:(Int) -> Void = {_ in   }
    
    @IBOutlet weak var constantTblInnerHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        imgProduct.layer.cornerRadius = 4
        imgProduct.layer.masksToBounds = true
        
        [lblProductName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .medium)
        }
        
        [lblPreferDateTitle, lblSelectTimeTitle, lblTestTitle, lblReferenceNumberTitle, lblExtraNotesTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [lblPreferDateValue, lblSelectTimeValue, lblTestValue, lblReferenceNumberValue, lblExtraNotesValue].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 14, fontname: .light)
        }
        
        [lblProductType].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .medium)
        }
        
        [lblQuantity].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .medium)
        }
        
        [lblDelete].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeSilverColor
            lbl?.font = themeFont(size: 12, fontname: .medium)
        }
        
        lblPreferDateTitle.text = "Date"
        lblSelectTimeTitle.text = "Time"
        lblTestTitle.text = getCommonString(key: "Test_key")
        lblReferenceNumberTitle.text = getCommonString(key: "Reference_number_key")
        lblExtraNotesTitle.text = getCommonString(key: "Extra_notes_key")
        lblDelete.text = getCommonString(key: "Delete_key")
        
        
        tblProduct.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        if !isEnglish {
            setUpArabic()
        }
    }
    
    func setUpArabic() {
        imgProduct.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        [lblProductName, lblPreferDateTitle, lblSelectTimeTitle, lblTestTitle, lblReferenceNumberTitle, lblExtraNotesTitle, lblPreferDateValue, lblSelectTimeValue, lblTestValue, lblReferenceNumberValue, lblExtraNotesValue, lblProductType, lblQuantity].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        vwDelete.transform = CGAffineTransform(scaleX: -1, y: 1)
        btnAddToCart.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
//            print("contentSize:= \(tblProduct.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
//            self.constantTblInnerHeight.constant = tblProduct.contentSize.height
            
            if tblProduct.contentSize.height <= 69
            {
                self.constantTblInnerHeight.constant = 69
            }
            else
            {
                self.constantTblInnerHeight.constant = tblProduct.contentSize.height
            }
            //            }
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func btnMinusAction(_ sender : UIButton){
        handlerMinusValue(sender.tag)
    }
    
    @IBAction func btnPlusAction(_ sender : UIButton){
        handlerPlusValue(sender.tag)
    }
}
