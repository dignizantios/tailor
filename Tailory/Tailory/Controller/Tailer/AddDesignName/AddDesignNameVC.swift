//
//  AddDesignNameVC.swift
//  Tailory
//
//  Created by Jaydeep on 11/01/20.
//  Copyright © 2020 YASH. All rights reserved.
//

import UIKit

class AddDesignNameVC: UIViewController {
    
    //MARK:- Variable Declration
    
    var handlerAddDesignName:(String) -> Void = {_ in}
    var selectedAddDesign = isComeFromEditDesign.add
    var strTitle = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtTitleName: UITextField!
    @IBOutlet weak var btnAddoutlet: UIButton!
    @IBOutlet weak var btnCancelOutlet: UIButton!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    //MARK:- SetupUI
    
    func setupUI(){
        
        lblTitle.text = getCommonString(key: "Add_Design_key")
        lblTitle.textColor = .appThemeBlackColor
        lblTitle.font = themeFont(size: 19, fontname: .heavy)
        
        txtTitleName.placeholder = getCommonString(key: "Enter_here_key")
        txtTitleName.textColor = .appThemeBlackColor
        txtTitleName.font = themeFont(size: 15, fontname: .heavy)
        txtTitleName.layer.borderColor = UIColor.appThemeCyprusColor.cgColor
        txtTitleName.layer.borderWidth = 1.0
        
        btnCancelOutlet.setupThemeButtonUI(backColor: UIColor.white)
        btnCancelOutlet.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        btnCancelOutlet.layer.borderColor = UIColor.appThemeSilverColor.cgColor
        btnCancelOutlet.layer.borderWidth = 1.0
        btnCancelOutlet.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        
        btnAddoutlet.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        btnAddoutlet.setTitle(getCommonString(key: "Add_key"), for: .normal)
        
        if selectedAddDesign == .edit {
            txtTitleName.text = strTitle
            btnAddoutlet.setTitle(getCommonString(key: "Edit_key"), for: .normal)
            lblTitle.text = getCommonString(key: "Edit_design_key")
        }
    }

}

//MARK:- Action Zone

extension AddDesignNameVC {
    
    @IBAction func btnAddDesignNameAction(_ sender: UIButton) {
        if (txtTitleName.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)! {
            makeToast(strMessage: getCommonString(key: "Please_enter_design_name_key"))
            return
        }
        handlerAddDesignName(txtTitleName.text ?? "")
        self.dismiss(animated: false, completion: nil)
    }
}
