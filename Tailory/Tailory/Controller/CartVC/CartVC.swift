//
//  CartVC.swift
//  Tailory
//
//  Created by Khushbu on 15/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire

class CartVC: UIViewController {

    //MARK : - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var tblCart: UITableView!
    @IBOutlet weak var btnContinue: CustomButton!
    @IBOutlet var vwFooter: UIView!
    @IBOutlet weak var lblFooterLaundry: UILabel!
    @IBOutlet weak var switchFooterlandry: UISwitch!
    @IBOutlet var vwBottom: UIView!
    
    //MARK: - Variable
    
    var arrCartList : [JSON] = []
    var strMessage =  String()
    var upperReferesh = UIRefreshControl()
    var isEditMode = Bool()
    var btnEditOutlet = UIButton()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Cart_key"))
        btnEditOutlet = UIButton(type: .custom)
        btnEditOutlet.setTitle(getCommonString(key: "Edit_key"), for: .normal)
        btnEditOutlet.setTitle(getCommonString(key: "Done_key"), for: .selected)
        btnEditOutlet.titleLabel?.font = themeFont(size: 17, fontname: .heavy)
        btnEditOutlet.contentHorizontalAlignment = .right
        btnEditOutlet.frame = CGRect(x: 0, y: 0, width: 70, height: 25)
        btnEditOutlet.addTarget(self, action: #selector(btnClickEditAction(_:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnEditOutlet)
        if isEnglish {
            self.navigationItem.rightBarButtonItem = barButton
        } else {
            self.navigationItem.leftBarButtonItem = barButton
        }
        
        if isEnglish {
            let backButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped))
            backButton.tintColor = UIColor.white
            
            let homeButton = UIBarButtonItem(image: UIImage(named: "ic_my_home"), style: .plain, target: self, action: #selector(backToHomeAction))
            homeButton.tintColor = UIColor.white
            
            self.navigationItem.leftBarButtonItems = [backButton,homeButton]
        } else {
            let backButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonTapped))
            backButton.tintColor = UIColor.white
            
            let homeButton = UIBarButtonItem(image: UIImage(named: "ic_my_home"), style: .plain, target: self, action: #selector(backToHomeAction))
            homeButton.tintColor = UIColor.white
            
            self.navigationItem.rightBarButtonItems = [backButton,homeButton]
    
        }
        
        self.tblCart.layoutSubviews()
    }
    
    @objc override func backToHomeAction(){
        appdelgate.tailerTabbarVC.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func setupUI()
    {
        self.tblCart.reloadData()
        self.tblCart.register(UINib(nibName: "ProductsTblcell", bundle: nil), forCellReuseIdentifier: "ProductsTblcell")
        self.tblCart.register(UINib(nibName: "CartTailorTblCell", bundle: nil), forCellReuseIdentifier: "CartTailorTblCell")
        
        btnContinue.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnContinue.setTitle(getCommonString(key: "Continue_key"), for: .normal)
//        btnContinue.setTitle("Continue", for: .normal)

        lblFooterLaundry.text = getCommonString(key: "Laundry_key")
        lblFooterLaundry.textColor = UIColor.appThemeBlackColor
        lblFooterLaundry.font = themeFont(size: 15, fontname: .roman)
        
        if !isEnglish
        {
           setupArabicUI()
        }
        
        upperReferesh.addTarget(self, action: #selector(upperRefreshTable), for: .valueChanged)
        tblCart.addSubview(upperReferesh)
        
        tblCart.tableFooterView = UIView()
        
        upperRefreshTable()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tblCart.layoutSubviews()
    }
    
    func setupArabicUI()
    {
//        vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        tblCart.semanticContentAttribute = .forceRightToLeft
//        btnContinue.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
        lblFooterLaundry.transform = CGAffineTransform(scaleX: -1, y: 1)
        lblFooterLaundry.textAlignment = .right
    }
    
    func setupRemoveAlertController(section:Int,row:Int,dict:JSON){
        let alertController = UIAlertController(title: getCommonString(key: "Tailors_key"), message: getCommonString(key: "Are_you_sure_you_want_to_remove_this_item_remove_from_cart_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.addRemoveItem(section: section, row: row, dict: dict)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func btnClickEditAction(_ sender:UIButton){
        if btnEditOutlet.isSelected {
            isEditMode = false
            btnEditOutlet.isSelected = false
        } else {
            isEditMode = true
            btnEditOutlet.isSelected = true
        }
        UIView.performWithoutAnimation {
            self.tblCart.reloadData()
        }
    }
    
}

//MARK: - IBAction

extension CartVC
{
    @IBAction func btnContinueTapped(_ sender: UIButton) {
        
        for i in 0..<arrCartList.count {
            let dict = arrCartList[i]
            if dict["type"].stringValue == getCommonString(key: "Accessories_key") {
                let arrInner = dict["data"].arrayValue
                if arrInner.contains(where: { (json) -> Bool in
                     if json["available_stock"].intValue <= 0{
                         return false
                     } else {
                         let qun = json["quantity"].intValue
                         if qun > json["available_stock"].intValue {
                            return false
                         } else {
                             return true
                         }
                     }
                }) {
                    
                } else {
                    makeToast(strMessage: getCommonString(key: "Your_cart_few_items_out_of_stock_key"))
                    return
                }
            } else if dict["type"].stringValue == getCommonString(key: "Fabrics_key") {
                let arrInner = dict["data"].arrayValue
                if arrInner.contains(where: { (json) -> Bool in
                     if json["available_stock"].floatValue <= 0{
                         return false
                     } else {
                         let qun = json["quantity"].floatValue
                         if qun > json["available_stock"].floatValue {
                            return false
                         } else {
                             return true
                         }
                     }
                }) {
                    
                } else {
                    makeToast(strMessage: getCommonString(key: "Your_cart_few_items_out_of_stock_key"))
                    return
                }
            } else {
                let arrInner = dict["data"].arrayValue
                if arrInner.contains(where: { (json) -> Bool in
                    if json["is_fabric_added"].stringValue == "1"{
                        let dictFabric = json["fabrics"][0]
                        if  dictFabric["available_stock"].floatValue <= 0{
                            return false
                        } else {
                            let qun = dictFabric["quantity"].floatValue
                            if qun > dictFabric["available_stock"].floatValue {
                               return false
                            } else {
                                return true
                            }
                        }
                    }
                    return true
                }) {
                    
                } else {
                    makeToast(strMessage: getCommonString(key: "Your_cart_few_items_out_of_stock_key"))
                    return
                }
            }
        }
        
        if getUserDetail("is_login_guest") == "1" {
            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "GuestPopupVc") as! GuestPopupVc
            obj.delegateGuestPopup = self
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            appdelgate.window?.rootViewController?.present(obj, animated: true, completion: nil)
        }
        else {
            redirectToContinue()
        }
        
        /*let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "GuestPopupVc") as! GuestPopupVc
        obj.delegateGuestPopup = self
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        appdelgate.window?.rootViewController?.present(obj, animated: true, completion: nil)*/
//        redirectToContinue()
    }
    
    //Cell method
    
    @objc func btnEditDesignTapped()
    {
        print("Edit")
        
        let customizeVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
        customizeVC.selectedController = .comeFromCart
        self.navigationController?.pushViewController(customizeVC, animated: true)
        
    }

    
    @objc func btnEditMeasurmentTapped()
    {
        print("Edit measurment")
        let measurmentVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MeasurmentVC") as! MeasurmentVC
        measurmentVC.selectedController = .comeFromCart
        self.navigationController?.pushViewController(measurmentVC, animated: true)
    }
    
}

//MARK: - TableView Delegate and DataSource

extension CartVC : UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblCart
        {
            if arrCartList.count == 0
            {
                let lbl = UILabel()
                lbl.text = getCommonString(key: "Cart_is_empty_key")
//                lbl.text = strMessage
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor.appThemeBlackColor
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                if !isEnglish {
//                    lbl.transform = CGAffineTransform(scaleX: -1, y: 1)
                   
                }
                self.vwBottom.isHidden = true
                /*
                if getUserDetail("is_login_guest") == "1" {
                    self.vwBottom.isHidden = true
                }
                */
                return 0
            }
            self.vwBottom.isHidden = false
            tableView.backgroundView = nil
            return self.arrCartList.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblCart
        {
            return self.arrCartList[section]["data"].count
        }
        if arrCartList[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Tailors_key") {
            return arrCartList[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["customize_design"].arrayValue.count
        } else if arrCartList[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Accessories_key") {
            return arrCartList[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["options"].arrayValue.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tblCart
        {
            if arrCartList[indexPath.section]["type"].stringValue == getCommonString(key: "Accessories_key") {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTblcell") as! ProductsTblcell
                let dict = arrCartList[indexPath.section]["data"][indexPath.row]
                let innerArray = self.arrCartList[indexPath.section]["data"].arrayValue
                cell.viewFabrics.isHidden = true
                
                if isEditMode{
                    cell.vwDelete.isHidden = false
                    cell.btnDeleteOutlet.tag = indexPath.row
                    cell.btnDeleteOutlet.accessibilityValue = "\(indexPath.section)"
                    cell.handlerDeleteValue = {[weak self] section,row in
                        print("section \(section),row \(row)")
                        let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                        print("dictInner \(dictInner)")
                        var dictCart = JSON()
                        dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                        dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                        dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                        dictCart["is_fabric_added"].stringValue = ""
                        dictCart["tbl_fabric_design_id"].stringValue = ""
                        dictCart["quantity"].intValue = 0
                        self?.setupRemoveAlertController(section: section, row: row, dict: dictCart)
                    }
                } else {
                    cell.vwDelete.isHidden = true
                }
                
                if innerArray.count <= 1 {
                    cell.vwCombineDelivery.isHidden = true
                }
                else {
                    cell.vwCombineDelivery.isHidden = false
                }
                if self.arrCartList.count > 1 {
                    cell.vwCombineDelivery.isHidden = false
                }
                
                if indexPath.row == innerArray.count - 1{
                    self.tblCart.separatorStyle = .none
                } else {
                    self.tblCart.separatorStyle = .singleLine
                }
                
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductName.text = dict["product_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblPrice.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                cell.imgProduct.sd_setImage(with: dict["product_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                
                /*cell.btnCombineDeliveryOutlet.isSelected = dict["is_combine_delivery"].stringValue == "1" ? true : false
                cell.btnCombineDeliveryOutlet.tag = indexPath.row
                cell.btnCombineDeliveryOutlet.accessibilityValue = "\(indexPath.section)"*/
                cell.switchCombineDeliveryOutlet.isOn = dict["is_combine_delivery"].stringValue == "1" ? true : false
                cell.switchCombineDeliveryOutlet.tag = indexPath.row
                cell.switchCombineDeliveryOutlet.accessibilityValue = "\(indexPath.section)"
                
                cell.handlerCombineDeliveryValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["is_combine_delivery"].stringValue = dictInner["is_combine_delivery"].stringValue == "0" ? "1" : "0"
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    self?.addRemoveCombineDelivery(section: section, row: row, dict: dictCart)
                }
                
                cell.btnPlus.tag = indexPath.row
                cell.btnPlus.accessibilityValue = "\(indexPath.section)"
                cell.handlerPlusValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    let qun = dictInner["quantity"].intValue + 1
                    if qun > dictInner["available_stock"].intValue {
                        return
                    }
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                    dictCart["is_fabric_added"].stringValue = ""
                    dictCart["tbl_fabric_design_id"].stringValue = ""
                    dictCart["quantity"].intValue = dictInner["quantity"].intValue + 1
                    self?.addRemoveItem(section: section, row: row, dict: dictCart)
                }
                
                cell.btnMinus.tag = indexPath.row
                cell.btnMinus.accessibilityValue = "\(indexPath.section)"
                cell.handlerMinusValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                    dictCart["is_fabric_added"].stringValue = ""
                    dictCart["tbl_fabric_design_id"].stringValue = ""
                    dictCart["quantity"].intValue = dictInner["quantity"].intValue - 1
                    if dictInner["quantity"].intValue == 1{
                        self?.setupRemoveAlertController(section: section, row: row, dict: dictCart)
                        return
                    }
                    self?.addRemoveItem(section: section, row: row, dict: dictCart)
                }
                
                if dict["available_stock"].intValue <= 0{
                    cell.lblOutOfStock.text =  getCommonString(key: "Out_of_stock_key")
                    [cell.btnMinus,cell.btnPlus].forEach { (btn) in
                        btn?.isHidden = true
                    }
                } else {
                    let qun = dict["quantity"].intValue
                    if qun > dict["available_stock"].intValue {
                        cell.lblOutOfStock.text =  "\(getCommonString(key: "Only_key")) \(dict["available_stock"].intValue) \(getCommonString(key: "available_in_stock_key"))"
                    } else {
                        cell.lblOutOfStock.text =  ""
                    }
                    [cell.btnMinus,cell.btnPlus].forEach { (btn) in
                        btn?.isHidden = false
                    }
                }
                
                cell.vwTblInnerDesign.isHidden = false
                cell.tblInnerDesign.isHidden = false
                cell.lblFabQuantity.isHidden = true
                
                cell.tblInnerDesign.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                return cell
                
            } else if arrCartList[indexPath.section]["type"].stringValue == getCommonString(key: "Fabrics_key") {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTblcell") as! ProductsTblcell
                let dict = arrCartList[indexPath.section]["data"][indexPath.row]
                let innerArray = self.arrCartList[indexPath.section]["data"].arrayValue
                
                if isEditMode{
                    cell.vwDelete.isHidden = false
                    cell.btnDeleteOutlet.tag = indexPath.row
                    cell.btnDeleteOutlet.accessibilityValue = "\(indexPath.section)"
                    cell.handlerDeleteValue = {[weak self] section,row in
                        print("section \(section),row \(row)")
                        let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                        print("dictInner \(dictInner)")
                        var dictCart = JSON()
                        dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                        dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                        dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                        dictCart["is_fabric_added"].stringValue = ""
                        dictCart["tbl_fabric_design_id"].stringValue = ""
                        dictCart["quantity"].intValue = 0
                        self?.setupRemoveAlertController(section: section, row: row, dict: dictCart)
                    }
                } else {
                    cell.vwDelete.isHidden = true
                }
                
                if innerArray.count <= 1 {
                    cell.vwCombineDelivery.isHidden = true
                }
                else {
                    cell.vwCombineDelivery.isHidden = true
                }
                if self.arrCartList.count > 1 {
                    cell.vwCombineDelivery.isHidden = false
                }
                
                if indexPath.row == innerArray.count - 1 {
                    self.tblCart.separatorStyle = .none
                } else {
                    self.tblCart.separatorStyle = .singleLine
                }
                cell.viewFabrics.isHidden = true
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductName.text = dict["fabricdesign_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblPrice.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                cell.imgProduct.sd_setImage(with: dict["fabric_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                
                /*cell.btnCombineDeliveryOutlet.isSelected = dict["is_combine_delivery"].stringValue == "1" ? true : false
                cell.btnCombineDeliveryOutlet.tag = indexPath.row
                cell.btnCombineDeliveryOutlet.accessibilityValue = "\(indexPath.section)"*/
                
                if dict["available_stock"].floatValue <= 0{
                    cell.lblOutOfStock.text =  getCommonString(key: "Out_of_stock_key")
                    [cell.btnMinus,cell.btnPlus].forEach { (btn) in
                        btn?.isHidden = true
                    }
                } else {
                    let qun = dict["quantity"].floatValue
                    if qun > dict["available_stock"].floatValue {
                        cell.lblOutOfStock.text =  "\(getCommonString(key: "Only_key")) \(dict["available_stock"].stringValue) \(getCommonString(key: "available_in_stock_key"))"
                    } else {
                        cell.lblOutOfStock.text =  ""
                    }
                    [cell.btnMinus,cell.btnPlus].forEach { (btn) in
                        btn?.isHidden = false
                    }
                }
                
                cell.switchCombineDeliveryOutlet.isOn = dict["is_combine_delivery"].stringValue == "1" ? true : false
                cell.switchCombineDeliveryOutlet.tag = indexPath.row
                cell.switchCombineDeliveryOutlet.accessibilityValue = "\(indexPath.section)"
                
                cell.handlerCombineDeliveryValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dict \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["is_combine_delivery"].stringValue = dictInner["is_combine_delivery"].stringValue == "0" ? "1" : "0"
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    self?.addRemoveCombineDelivery(section: section, row: row, dict: dictCart)
                }
                
                cell.btnPlus.tag = indexPath.row
                cell.btnPlus.accessibilityValue = "\(indexPath.section)"
                cell.handlerPlusValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dict \(dict)")
                    let qun = dictInner["quantity"].doubleValue + 0.5
                    if qun > dictInner["available_stock"].doubleValue {
                        return
                    }
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                    dictCart["is_fabric_added"].stringValue = ""
                    dictCart["tbl_fabric_design_id"].stringValue = ""
                    dictCart["quantity"].doubleValue = dictInner["quantity"].doubleValue + 0.5
                    self?.addRemoveItem(section: section, row: row, dict: dictCart)
                }
                
                cell.btnMinus.tag = indexPath.row
                cell.btnMinus.accessibilityValue = "\(indexPath.section)"
                cell.handlerMinusValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                    dictCart["is_fabric_added"].stringValue = ""
                    dictCart["tbl_fabric_design_id"].stringValue = ""
                    dictCart["quantity"].floatValue = dictInner["quantity"].floatValue - 0.5
                    if dictInner["quantity"].floatValue == 0.5{
                        self?.setupRemoveAlertController(section: section, row: row, dict: dictCart)
                        return
                    }
                    self?.addRemoveItem(section: section, row: row, dict: dictCart)
                }
                
                cell.vwTblInnerDesign.isHidden = true
                cell.tblInnerDesign.isHidden = true
                cell.constantTblInnerHeight.constant = 0                
                cell.lblFabQuantity.isHidden = false
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                
                cell.lblFabQuantity.textColor = UIColor.appThemeLightGrayColor
                cell.lblFabQuantity.text = "\(dict["quantity"].stringValue) \(getCommonString(key: "Meter_key"))"                
                return cell
            }
            else
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "CartTailorTblCell") as! CartTailorTblCell
                
                let dict = self.arrCartList[indexPath.section]["data"][indexPath.row]
                let innerArray = self.arrCartList[indexPath.section]["data"].arrayValue
                
                if isEditMode{
                    cell.vwDelete.isHidden = false
                    cell.btnDeleteOutlet.tag = indexPath.row
                    cell.btnDeleteOutlet.accessibilityValue = "\(indexPath.section)"
                    cell.handlerDeleteValue = {[weak self] section,row in
                        print("section \(section),row \(row)")
                        let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                        print("dictInner \(dictInner)")
                        var dictCart = JSON()
                        dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                        dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                        dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                        dictCart["is_fabric_added"].stringValue = dictInner["is_fabric_added"].stringValue
                        dictCart["tbl_fabric_design_id"].stringValue = ""
                        if dictInner["is_fabric_added"].stringValue == "1"{
                            let dictFabric = dictInner["fabrics"][0]
                            dictCart["tbl_fabric_design_id"].stringValue = dictFabric["tbl_fabric_design_id"].stringValue
                        }
                        dictCart["quantity"].intValue = 0
                        self?.setupRemoveAlertController(section: section, row: row, dict: dictCart)
                    }
                } else {
                    cell.vwDelete.isHidden = true
                }
                
                
                if indexPath.row == innerArray.count - 1{
                    self.tblCart.separatorStyle = .none
                } else {
                    self.tblCart.separatorStyle = .singleLine
                }

                if innerArray.count <= 1 {
                    cell.vwCombineDelivery.isHidden = true
                }
                else {
                    cell.vwCombineDelivery.isHidden = true
                }
                if self.arrCartList.count > 1 {
                    cell.vwCombineDelivery.isHidden = true
                }
                
                cell.btnEditDesign.tag = indexPath.row
                cell.btnEditDesign.accessibilityValue = "\(indexPath.section)"
                cell.handlerEditDesignValue =  {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    let customizeVC = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
                    customizeVC.selectedController = .comeFromCart
                    customizeVC.dictCustomize = dictInner
                    customizeVC.handlerUpdateCart = {[weak self] in
                        self?.getCartList()
                    }
                    self?.navigationController?.pushViewController(customizeVC, animated: true)
                }
                cell.btnEditMeasurment.tag = indexPath.row
                cell.btnEditMeasurment.accessibilityValue = "\(indexPath.section)"
                cell.handlerEditMeasurmentValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    let measurmentVC = GlobalVariables.measurmentStoryboard.instantiateViewController(withIdentifier: "MeasurmentVC") as! MeasurmentVC
                    measurmentVC.selectedController = .comeFromCart
                    measurmentVC.dictCartOrder = dictInner
                    objCartModel.dictProductJson = dictInner["tailor_json"]
                    measurmentVC.handlerUpdateCart = {[weak self] in
                        self?.getCartList()
//                        self?.navigationController?.popViewController(animated: false)
                    }
                    self?.navigationController?.pushViewController(measurmentVC, animated: true)
                }
                
                cell.img.sd_setImage(with: dict["design_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                cell.lblDesignName.text = dict["design_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblProductQty.isHidden = true
                cell.lblPriceValue.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                
                /*cell.btnCombineDeliveryoutlet.isSelected = dict["is_combine_delivery"].stringValue == "1" ? true : false
                cell.btnCombineDeliveryoutlet.tag = indexPath.row
                cell.btnCombineDeliveryoutlet.accessibilityValue = "\(indexPath.section)"
                
                cell.btnLaundryOutlet.isSelected = dict["is_laundry"].stringValue == "1" ? true : false
                cell.btnLaundryOutlet.tag = indexPath.row
                cell.btnLaundryOutlet.accessibilityValue = "\(indexPath.section)"*/
                
                cell.switchCombineDeliveryOutlet.isOn = dict["is_combine_delivery"].stringValue == "1" ? true : false
                cell.switchCombineDeliveryOutlet.tag = indexPath.row
                cell.switchCombineDeliveryOutlet.accessibilityValue = "\(indexPath.section)"
                
                cell.switchLaundryOutlet.isOn = dict["is_laundry"].stringValue == "1" ? true : false
                cell.switchLaundryOutlet.tag = indexPath.row
                cell.switchLaundryOutlet.accessibilityValue = "\(indexPath.section)"
                
                cell.handlerCombineDeliveryValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["is_combine_delivery"].stringValue = dictInner["is_combine_delivery"].stringValue == "0" ? "1" : "0"
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    self?.addRemoveCombineDelivery(section: section, row: row, dict: dictCart)
                }
                
                cell.handlerLaundryValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["is_laundry"].stringValue = dictInner["is_laundry"].stringValue == "0" ? "1" : "0"
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    self?.addRemoveLaudry(section: section, row: row, dict: dictCart)
                }
                
                cell.btnPlus.isHidden = true
                cell.btnPlus.tag = indexPath.row
                cell.btnPlus.accessibilityValue = "\(indexPath.section)"
                cell.handlerPlusValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                    dictCart["is_fabric_added"].stringValue = dictInner["is_fabric_added"].stringValue
                    dictCart["tbl_fabric_design_id"].stringValue = ""
                    if dictInner["is_fabric_added"].stringValue == "1"{
                        let dictFabric = dictInner["fabrics"][0]
                        dictCart["tbl_fabric_design_id"].stringValue = dictFabric["tbl_fabric_design_id"].stringValue
                        let qun = dictFabric["quantity"].doubleValue + 0.5
                        if qun > dictFabric["available_stock"].doubleValue {
                            return
                        }
                    }
                    dictCart["quantity"].floatValue = dictInner["quantity"].floatValue + 1
                    self?.addRemoveItem(section: section, row: row, dict: dictCart)
                }
                
                cell.btnMinus.isHidden = true
                cell.btnMinus.tag = indexPath.row
                cell.btnMinus.accessibilityValue = "\(indexPath.section)"
                cell.handlerMinusValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrCartList[section]["data"][row])!
                    print("dictInner \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    dictCart["product_id"].stringValue = dictInner["product_id"].stringValue
                    dictCart["is_fabric_added"].stringValue = dictInner["is_fabric_added"].stringValue
                    dictCart["tbl_fabric_design_id"].stringValue = ""
                    if dictInner["is_fabric_added"].stringValue == "1"{
                        let dictFabric = dictInner["fabrics"][0]
                        dictCart["tbl_fabric_design_id"].stringValue = dictFabric["tbl_fabric_design_id"].stringValue
                    }
                    dictCart["quantity"].intValue = dictInner["quantity"].intValue - 1
                    if dictInner["quantity"].intValue == 1{
                        self?.setupRemoveAlertController(section: section, row: row, dict: dictCart)
                        return
                    }
                    self?.addRemoveItem(section: section, row: row, dict: dictCart)
                }
                
                cell.tblInnerDesign.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblInnerDesign.isHidden = false
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                
                cell.vwLaundry.isHidden = false
                //For fabric hidden
                if dict["is_fabric_added"].stringValue == "1"{
                    cell.vwLaundry.isHidden = false
                    cell.vwFabric.isHidden = false
                    let dictFabric = dict["fabrics"][0]
                    if dictFabric["available_stock"].floatValue <= 0{
                        cell.lblOutOfStock.text =  getCommonString(key: "Out_of_stock_key")
                    } else {
                        let qun = dictFabric["quantity"].floatValue
                        if qun > dictFabric["available_stock"].floatValue {
                            cell.lblOutOfStock.text =  "\(getCommonString(key: "Only_key")) \(dictFabric["available_stock"].stringValue) \(getCommonString(key: "available_in_stock_key"))"
                        } else {
                            cell.lblOutOfStock.text =  ""
                        }
                    }
                    cell.lblFabricName.text = dictFabric["fabricdesign_name"].stringValue
                    cell.lblMeterValue.text = "\(dictFabric["quantity"].stringValue) \(getCommonString(key: "Meter_key"))"
                    cell.lblFabricPriceValue.text = dictFabric["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                    cell.imgFabric.sd_setImage(with: dictFabric["fabric_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                } else {
                    cell.vwFabric.isHidden = true
                }
                
                cell.layoutSubviews()
                return cell
            }
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InnerCustomDesignTblCell") as! InnerCustomDesignTblCell
            if arrCartList[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Accessories_key"){
                let dict = arrCartList[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["options"][indexPath.row]
                if isEnglish {
                    cell.lblCustomizeDesignTitle.text = "\(dict["option_name"].stringValue)"
                } else {
                    cell.lblCustomizeDesignTitle.text = " \(dict["option_name"].stringValue) : "
                }
                cell.lblCustomizeDesignValue.text = dict["option_value_name"].stringValue                
                return cell
            }
            if arrCartList[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Tailors_key"){
                let dict = arrCartList[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["customize_design"][indexPath.row]
                if isEnglish {
                    cell.lblCustomizeDesignTitle.text = "\(dict["style_name"].stringValue) : "
                } else {
                   cell.lblCustomizeDesignTitle.text = " \(dict["style_name"].stringValue) :"
                }
                
                cell.lblCustomizeDesignValue.text = dict["design_name"].stringValue
                return cell
            }
            return cell
        }
        
    }
    
    //set header for
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == self.tblCart {
            let vw = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)?[0] as? HeaderView
            if !isEnglish {
                vw?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            vw?.lblTitleName.text = self.arrCartList[section]["type"].stringValue
            return vw
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == self.tblCart
        {
            return 45
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if tableView == self.tblCart
        {
            let delete = UITableViewRowAction(style: .destructive, title: getCommonString(key: "Delete_key")) { (action, index) in
                let dict:JSON = (self.arrCartList[indexPath.section]["data"][indexPath.row])
                var dictCart = JSON()
                dictCart["cart_id"].stringValue = dict["cart_id"].stringValue
                dictCart["category_id"].stringValue = dict["category_id"].stringValue
                dictCart["product_id"].stringValue = dict["product_id"].stringValue
                dictCart["tbl_fabric_design_id"].stringValue = ""
                 
                if dict["category_id"].stringValue == enumCategoryId.accessories.rawValue {
                    dictCart["is_fabric_added"].stringValue = ""
                    dictCart["quantity"].intValue = 0
                }
                else if dict["category_id"].stringValue == enumCategoryId.tailor.rawValue{
                    dictCart["is_fabric_added"].stringValue = dict["is_fabric_added"].stringValue
                    if dict["is_fabric_added"].stringValue == "1"{
                        let dictFabric = dict["fabrics"][0]
                        dictCart["tbl_fabric_design_id"].stringValue = dictFabric["tbl_fabric_design_id"].stringValue
                    }
                    dictCart["quantity"].intValue = 0
                } else {
                    dictCart["is_fabric_added"].stringValue = ""
                    dictCart["quantity"].floatValue = 0.0
                }
                self.setupRemoveAlertController(section: indexPath.section, row: indexPath.row, dict: dictCart)                
            }
            delete.backgroundColor = .red
            return [delete]
        }
        return []
    }
}

extension CartVC: delegateGuestPopupRedirection
{
    func redirectToCreateAccount() {
        
        self.view.endEditing(true)
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        obj.hidesBottomBarWhenPushed = true
        obj.selectedController = .fromGuesdUserDetailPopup
        self.navigationController?.pushViewController(obj, animated: false)
        
    }
    
    func redirectToContinue() {
 
        //TODO: - When user continue as Guest
        if getUserDetail("is_login_guest") == "1" {
            let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "AddNewAddressVc") as! AddNewAddressVc
            obj.hidesBottomBarWhenPushed = true
            obj.selectedContollerForAddDetails = .addGuestUserDetails
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else {
            
            let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "AddressWithPaymentMethodVc") as! AddressWithPaymentMethodVc
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
        /*
        let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "AddressWithPaymentMethodVc") as! AddressWithPaymentMethodVc
        self.navigationController?.pushViewController(obj, animated: true)
        */
    }
    
}

//MARK:- API

extension CartVC {
    @objc func upperRefreshTable()
    {
        getCartList()
        self.upperReferesh.endRefreshing()
    }
    
    func getCartList()
    {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlCartList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "is_login_guest" : getUserDetail("is_login_guest") == "1" ? "1" : "0",
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString]
            
            
            print("param : \(param)")
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        if data.count == 0{
                            self.arrCartList = []
                            self.strMessage = json["msg"].stringValue
                            if isEnglish {
                                self.navigationItem.rightBarButtonItem = nil
                            } else {
                                self.navigationItem.leftBarButtonItem = nil
                            }
                        } else {
                            self.arrCartList = []
                            if data["products"].exists(){
                                if data["products"].arrayValue.count != 0{
                                    var dict = JSON()
                                    dict["type"].stringValue = getCommonString(key: "Accessories_key")
                                    dict["data"] = JSON(data["products"].arrayValue)
                                    self.arrCartList.append(dict)
                                }
                            }
                            if data["fabrics"].exists(){
                                if data["fabrics"].arrayValue.count != 0{
                                    var dict = JSON()
                                    dict["type"].stringValue = getCommonString(key: "Fabrics_key")
                                    dict["data"] = JSON(data["fabrics"].arrayValue)
                                    self.arrCartList.append(dict)
                                }
                            }
                            if data["tailors"].exists(){
                                if data["tailors"].arrayValue.count != 0{
                                    var dict = JSON()
                                    dict["type"].stringValue = getCommonString(key: "Tailors_key")
                                    dict["data"] = JSON(data["tailors"].arrayValue)
                                    self.arrCartList.append(dict)
                                }
                            }
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrCartList = []
                        self.strMessage = json["msg"].stringValue
                    }
                    print("cart \(self.arrCartList)")
                    self.tblCart.reloadData()
                    self.viewDidAppear(false)
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addRemoveItem(section:Int,row:Int,dict:JSON)
    {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlAddRemoveCart)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "cart_id" : dict["cart_id"].stringValue,
                          "category_id" : dict["category_id"].stringValue,
                          "product_id" : dict["product_id"].stringValue,
                          "is_fabric_added":dict["is_fabric_added"].stringValue,
                          "tbl_fabric_design_id":dict["tbl_fabric_design_id"].stringValue,
                          "quantity":dict["quantity"].stringValue
                          ]
            
            
            print("param : \(param)")
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        if json["data"].exists(){
                            let data = json["data"]
                            var dictCart:JSON = self.arrCartList[section]["data"][row]
                            dictCart["quantity"].stringValue = data["quantity"].stringValue
                            dictCart["price"].stringValue = data["price"].stringValue
                            if dict["is_fabric_added"].stringValue == "1" ||  dict["is_fabric_added"].stringValue == "0"{
                                var dictFabric = dictCart["fabrics"][0]
                                dictFabric["quantity"].stringValue = data["fabric_quantity"].stringValue
                                dictFabric["price"].stringValue = data["fabrics_price"].stringValue
                                dictCart["fabrics"][0] = dictFabric
                            }
                            self.arrCartList[section]["data"][row] = dictCart
                            UIView.performWithoutAnimation {
                                self.tblCart.reloadRows(at: [IndexPath(row: row, section: section)], with: .none)
                            }
                        } else {
                            var arrCartRemove = self.arrCartList[section]["data"].arrayValue
                            arrCartRemove.remove(at: row)
                            self.arrCartList[section]["data"] = JSON(arrCartRemove)
                            if self.arrCartList[section]["data"].arrayValue.count == 0{
                                self.arrCartList.remove(at: section)
                                UIView.performWithoutAnimation {
                                   self.tblCart.reloadData()
                                }
                            } else {
                                UIView.performWithoutAnimation {
                                    self.tblCart.reloadSections(IndexSet(arrayLiteral: section), with: .none)
                                }
                            }
                        }
                        if self.arrCartList.count == 0{
                            if isEnglish {
                                self.navigationItem.rightBarButtonItem = nil
                            } else {
                                self.navigationItem.leftBarButtonItem = nil
                            }
                        }
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addRemoveLaudry(section:Int,row:Int,dict:JSON)
    {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlAddRemoveLaundry)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "cart_id" : dict["cart_id"].stringValue,
                          "category_id" : dict["category_id"].stringValue,
                          "is_laundry" : dict["is_laundry"].stringValue
            ]
            
            
            print("param : \(param)")
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        var dictCart:JSON = self.arrCartList[section]["data"][row]
                        dictCart["is_laundry"].stringValue = data["is_laundry"].stringValue
                        self.arrCartList[section]["data"][row] = dictCart
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    UIView.performWithoutAnimation {
                        self.tblCart.reloadRows(at: [IndexPath(row: row, section: section)], with: .none)
                    } 
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addRemoveCombineDelivery(section:Int,row:Int,dict:JSON)
    {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlAddRemoveCombineDelivery)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "cart_id" : dict["cart_id"].stringValue,
                          "category_id" : dict["category_id"].stringValue,
                          "is_combine_delivery" : dict["is_combine_delivery"].stringValue
            ]
            
            
            print("param : \(param)")
            
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        var dictCart:JSON = self.arrCartList[section]["data"][row]
                        dictCart["is_combine_delivery"].stringValue = data["is_combine_delivery"].stringValue
                        self.arrCartList[section]["data"][row] = dictCart
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    UIView.performWithoutAnimation {
                        self.tblCart.reloadRows(at: [IndexPath(row: row, section: section)], with: .none)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            UIView.performWithoutAnimation {
                self.tblCart.reloadRows(at: [IndexPath(row: row, section: section)], with: .none)
            }
            makeToast(strMessage: networkMsg)
        }
    }
}
