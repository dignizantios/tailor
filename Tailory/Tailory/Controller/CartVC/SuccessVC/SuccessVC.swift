//
//  SuccessVC.swift
//  Tailory
//
//  Created by Jaydeep on 19/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit
import AlamofireSwiftyJSON
import Alamofire

class SuccessVC: UIViewController {
    
    //MARK:- Variable Declration
    
    var dictSuccess = JSON()
    var dictPaymentDetail = JSON()
    var selectedPaymentGateway = isCheckPaymentType.cash
    var dictSuccessPayment = JSON()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwTransferId: UIView!
    @IBOutlet weak var vwReferenceId: UIView!
    @IBOutlet weak var vwFailed: UIView!
    @IBOutlet weak var imgSuccess: UIImageView!
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblTotlaAmountTitle: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblPaymentMethodTitle: UILabel!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    @IBOutlet weak var lblReferenceTitle: UILabel!
    @IBOutlet weak var lblRefereceId: UILabel!
    @IBOutlet weak var lblPaymentIdTitle: UILabel!
    @IBOutlet weak var lblPaymentId: UILabel!
    @IBOutlet weak var lblTransferIdTitle: UILabel!
    @IBOutlet weak var lblTransferId: UILabel!
    @IBOutlet weak var lblResultTitle: UILabel!
    @IBOutlet weak var lblResultType: UILabel!
    @IBOutlet weak var btnContinueOutlet: UIButton!
    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var lblFailedStatus: UILabel!
    @IBOutlet weak var btnRetryPayment: CustomButton!
    @IBOutlet weak var btnCreateAccountOutlet: UIButton!
    
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.post(name: .didRefreshOrderList, object: nil, userInfo: nil)
        
        self.vwFailed.isHidden = true
        
        lblFailedStatus.text = getCommonString(key: "Failed_status_key")
        lblFailedStatus.font = themeFont(size: 15, fontname: .heavy)
        lblFailedStatus.textColor = .black
        
        btnRetryPayment.setTitle(getCommonString(key: "Retry_payment_key"), for: .normal)
        btnRetryPayment.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        
        btnCreateAccountOutlet.setTitle(getCommonString(key: "Create_account_key"), for: .normal)
        
        btnCreateAccountOutlet.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        
        if getUserDetail("is_login_guest") == "1" {
            btnCreateAccountOutlet.isHidden = false
        }
        
        
        if !isEnglish {
            setupArabicUI()
        }
        if selectedPaymentGateway == .fatoorah {
            webview.uiDelegate = self
            webview.navigationDelegate = self
            webview.load(URLRequest(url: dictPaymentDetail["RedirectUrl"].url!))
        } else {
            setupUI()
            [vwTransferId,vwReferenceId].forEach { (vw) in
                vw?.isHidden = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        if selectedPaymentGateway == .fatoorah {
            self.setupNavigationbarwithBackButton(titleText: "", barColor: UIColor(red: 47/255, green: 142/255, blue: 182/255, alpha: 1.0))
            let btnClose = CustomButton()
            btnClose.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            btnClose.setImage(UIImage(named: "ic_close_without_bg"), for: .normal)
            btnClose.addTarget(self, action: #selector(btnCloseAction), for: .touchUpInside)
            if isEnglish {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnClose)
            } else {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnClose)
            }
            
        }

        /*if navigationController?.view.semanticContentAttribute == .forceRightToLeft {
            appdelgate.tailerTabbarVC.selectedIndex = 4
        }
        else {
            appdelgate.tailerTabbarVC.selectedIndex = 0
        }*/
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    @IBAction func btnContinueTapped(_ sender: UIButton) {
        
        self.navigationController?.popToRootViewController(animated: false)

        if isEnglish
        {
            appdelgate.tailerTabbarVC.selectedIndex = 0
        }
        else
        {
            appdelgate.tailerTabbarVC.selectedIndex = 4
        }
        
    }
    
    @objc func btnCloseAction(){
        
        if self.vwFailed.isHidden == false{
            NotificationCenter.default.post(name: .didRefreshOrderList, object: nil, userInfo: nil)
            appdelgate.tailerTabbarVC.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: false)
            return
        }
        
        let alertController = UIAlertController(title: getCommonString(key: "Tailory_key"), message: getCommonString(key: "Payment_cancel_message_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.vwFailed.isHidden = false
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    //MARK:- Setup UI
    
    func setupUI() {
       
        self.vwFailed.isHidden = true
        self.webview.isHidden = true
        [lblTotlaAmountTitle,lblPaymentMethodTitle,lblReferenceTitle,lblPaymentIdTitle,lblTransferIdTitle,lblResultTitle,lblFailedStatus].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .heavy)
            lbl?.textColor = .black
        }
        [lblTotalAmount,lblPaymentMethod,lblRefereceId,lblPaymentId,lblTransferId,lblResultType].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .medium)
            lbl?.textColor = .black
        }
        
        btnContinueOutlet.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnContinueOutlet.setTitle(getCommonString(key: "Continue_key"), for: .normal)
        
        lblTotlaAmountTitle.text = getCommonString(key: "Total_amount_key")+":"
        lblPaymentMethodTitle.text = getCommonString(key: "Payment_method_key")+":"
        lblReferenceTitle.text = getCommonString(key: "Reference_id_key")+":"
        lblPaymentIdTitle.text = getCommonString(key: "Payment_id_key")+":"
        lblTransferIdTitle.text = getCommonString(key: "Transfer_id_key")+":"
        lblResultTitle.text = getCommonString(key: "Result_key")+":"
        
        lblResultType.textColor = UIColor.green
        lblTotalAmount.text = dictSuccess["total_amount"].stringValue + " \(getCommonString(key: "KD_key"))"
        if dictSuccess["payment_type"].stringValue == "1" {
            lblPaymentMethod.text = getCommonString(key: "Credit_card_key")
        } else if dictSuccess["payment_type"].stringValue == "2" {
            lblPaymentMethod.text = getCommonString(key: "Fatoorah_key")
        } else if dictSuccess["payment_type"].stringValue == "3"{
           lblPaymentMethod.text = getCommonString(key: "Cash_on_delivery_key")
        }
        
        lblRefereceId.text = dictSuccessPayment["ReferenceId"].stringValue
        lblPaymentId.text = dictSuccess["payment_id"].stringValue
        lblTransferId.text = dictSuccessPayment["TransactionId"].stringValue
        lblResultType.text = getCommonString(key: "Success_key")
        
        lblFailedStatus.text = getCommonString(key: "Failed_status_key")
        btnRetryPayment.setTitle(getCommonString(key: "Retry_payment_key"), for: .normal)
        btnRetryPayment.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        
        if !isEnglish {
            setupArabicUI()
        }
    
    }
    
    func setupArabicUI(){
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        self.imgSuccess.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        self.webview.transform = CGAffineTransform(scaleX: -1, y: 1)
        btnContinueOutlet.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
        btnRetryPayment.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
        [lblTotlaAmountTitle,lblPaymentMethodTitle,lblReferenceTitle,lblPaymentIdTitle,lblTransferIdTitle,lblResultTitle,lblFailedStatus].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblTotalAmount,lblPaymentMethod,lblRefereceId,lblPaymentId,lblTransferId,lblResultType].forEach { (lbl) in
            lbl?.textAlignment = .left
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        
        if isEnglish {
            edgePan.edges = .left
        }
        else {
            edgePan.edges = .right
        }
        
        view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        
        if recognizer.state == .recognized {
            appdelgate.tailerTabbarVC.selectedIndex = 4
            
            appdelgate.tailerTabbarVC = TailerTabBarVC()
            if isEnglish {
                appdelgate.tailerTabbarVC.selectedIndex = 0
            }
            else {
                appdelgate.tailerTabbarVC.selectedIndex = 4
            }
            appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC    
        }
    }
    
    @IBAction func btnRetryPaymentAction(_ sender:UIButton){
        self.webview.isHidden = false
        webview.load(URLRequest(url: dictPaymentDetail["RedirectUrl"].url!))
        self.vwFailed.isHidden = true
    }
    
    @IBAction func btnCreateAccountAction(_ sender: UIButton) {
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        let rearNavigation = UINavigationController(rootViewController: vc)
        appdelgate.window?.rootViewController = rearNavigation
    }
}

//MARK:- WkWEbview

extension SuccessVC : WKUIDelegate,WKNavigationDelegate {
   
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        print("navigation URL \(navigationAction.request.url!)")
        
        if let urlStr = navigationAction.request.url?.absoluteString {
            print("navigationAction URL \(urlStr)")
            if urlStr.contains("payment_transaction_status"){
                print("Got it")
                getJsonDecode(url: urlStr)
                decisionHandler(.cancel)
                return
            }
        }
        decisionHandler(.allow)
    }
}

//MARK:- API

extension SuccessVC {
    func getJsonDecode(url:String) {
        CommonService().ServiceGETMethod(url: url, param: [:]) { (respones) in
            if let json = respones.value
            {
                print("JSON : \(json)")
                if json["TransactionStatus"].stringValue == "2" {
                    self.dictSuccessPayment = json
                    self.manageTransaction()
                } else{
                    self.vwFailed.isHidden = false
                }
            }
            else
            {
                makeToast(strMessage:serverNotResponding)
            }
        }
    }
    
    func redirectToOrderDetailScreen(dict:JSON){
        appdelgate.tailerTabbarVC = TailerTabBarVC()
        appdelgate.tailerTabbarVC.selectedIndex = 1
        isComeFromPush = true
        var dictData = JSON()
        dictData["address_id"].stringValue = dict["address_id"].stringValue
        dictData["id"].stringValue = dict["id"].stringValue
        dictPushData = dictData
        appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
    }
    
    func manageTransaction() {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlOrder)\(urlManageTransaction)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "order_id":dictSuccess["order_id"].stringValue,
                          "reference_id":dictSuccessPayment["ReferenceId"].stringValue,
            "track_id":dictSuccessPayment["TrackId"].stringValue,
            "transaction_id":dictSuccessPayment["ReferenceId"].stringValue,
            "paymentid":dictSuccessPayment["PaymentId"].stringValue,
            "payment_id":dictSuccess["payment_id"].stringValue,
            "transaction_status":dictSuccessPayment["TransactionStatus"].stringValue,
            "payment_gateway":dictSuccessPayment["PaymentGateway"].stringValue]
            
            
            print("param : \(param)")
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                        if getUserDetail("is_login_guest") == "1" {
                            self.navigationController?.isNavigationBarHidden = true
                            self.navigationController?.navigationBar.barTintColor = .white
                            self.navigationController?.setNavigationBarHidden(true, animated: false)
                            self.setupUI()
                            makeToast(strMessage: json["msg"].stringValue)
                        } else {
                            self.redirectToOrderDetailScreen(dict: json["data"])
                        }                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    NotificationCenter.default.post(name: .didRefreshOrderList, object: nil, userInfo: nil)
                }
                else {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
}

