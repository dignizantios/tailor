//
//  PaymentMethodTblCell.swift
//  Tailory
//
//  Created by Haresh on 29/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class PaymentMethodTblCell: UITableViewCell {

    
    //MARK: - Outlet
    
    @IBOutlet weak var vwPayment: UIView!
    @IBOutlet weak var lblPaymentName: UILabel!
    @IBOutlet weak var imgCheckmark : UIImageView!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblPaymentName.textColor = UIColor.appThemeBlackColor
        lblPaymentName.font = themeFont(size: 14, fontname: .roman)
        
        if !isEnglish
        {
            vwPayment.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblPaymentName.textAlignment = .right
            lblPaymentName.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
