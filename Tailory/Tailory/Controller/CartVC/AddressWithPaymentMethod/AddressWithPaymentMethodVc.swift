//
//  AddressWithPaymentMethodVc.swift
//  Tailory
//
//  Created by Haresh on 29/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class AddressWithPaymentMethodVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblSelectDeliveryAddress: UILabel!
    @IBOutlet weak var lblMainAddress: UILabel!
    @IBOutlet weak var lblSubAddress: UILabel!
    @IBOutlet weak var lblDeliveryoption: UILabel!
    @IBOutlet weak var tblDeliveryOption: UITableView!
    @IBOutlet var vwBtnCountinue: UIView!
    @IBOutlet weak var btnContinueOutlet: CustomButton!
    
    //MARK: - Variable
    
    var arrayPaymentOption : [JSON] = []
    var dictAddress = JSON()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblDeliveryOption.register(UINib(nibName: "PaymentMethodTblCell", bundle: nil), forCellReuseIdentifier: "PaymentMethodTblCell")
        self.tblDeliveryOption.tableFooterView = UIView()
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Addresses_key"))
        
        let rightButton = UIBarButtonItem(image: UIImage(named: "ic_my_measurement_add_new_detail_plush"), style: .plain, target: self, action: #selector(addAddressTapped))
        rightButton.tintColor = UIColor.white
        
        if isEnglish
        {
            self.navigationItem.rightBarButtonItem = rightButton
        }
        else
        {
            self.navigationItem.leftBarButtonItem = rightButton
        }
    }
    
    //MARK: - Setup
    
    func setupUI()
    {
        
        [lblSelectDeliveryAddress,lblDeliveryoption].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        lblMainAddress.textColor = UIColor.appThemeBlackColor
        lblMainAddress.font = themeFont(size: 17, fontname: .roman)
        lblSubAddress.textColor = UIColor.appThemeLightGrayColor
        lblSubAddress.font = themeFont(size: 15, fontname: .roman)
        
        
        lblSelectDeliveryAddress.text = getCommonString(key: "Please_select_delivery_address_key")
        lblDeliveryoption.text = getCommonString(key: "Delivery_Option_key")
        
//        lblMainAddress.text = "Salwa"
        lblSubAddress.text = "Block 5 strret jaddhah 66"
        
        if !isEnglish
        {
            self.setupArabicUI()
        }
        
        btnContinueOutlet.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnContinueOutlet.setTitle(getCommonString(key: "Continue_key"), for: .normal)
        
        setArray()
        
        getDefaultAddress()
    }
    
    func setupArabicUI()
    {
  [lblSelectDeliveryAddress,lblMainAddress,lblSubAddress,lblDeliveryoption].forEach { (lbl) in
            lbl?.textAlignment = .right
        }
        self.vwBtnCountinue.transform = CGAffineTransform(scaleX: -1, y: 1)
        btnContinueOutlet.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    
    func setArray()
    {
        
        var dict = JSON()
        dict["payment_method"] = "Normal"
        dict["selected"] = "1"
        self.arrayPaymentOption.append(dict)
        
        dict = JSON()
        dict["payment_method"] = "Express"
        dict["selected"] = "0"
        self.arrayPaymentOption.append(dict)
        
        self.tblDeliveryOption.reloadData()
    }
    
}

//MARK: - IBAction

extension AddressWithPaymentMethodVc
{
    @IBAction func btnClickOnAddressTapped(_ sender: UIButton) {
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyAddressVc") as! MyAddressVc
        obj.selectedController = .fromCart
        obj.dictAddress = dictAddress
        obj.handlerUpdateAddress = {[weak self] dict in
            self?.setupAddress(dict: dict)
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnContinueAction(_ sender:UIButton){
        
        if dictAddress.count == 0 {
            makeToast(strMessage: getCommonString(key: "Please_select_address_key"))
        }
        else {
            let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "CheckoutVc") as! CheckoutVc
            obj.dictAddress = dictAddress
            self.navigationController?.pushViewController(obj, animated: true)
        }
        /*
        let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "CheckoutVc") as! CheckoutVc
        obj.dictAddress = dictAddress
        self.navigationController?.pushViewController(obj, animated: true)
         */
    }
    
}

//MARK: - Other

extension AddressWithPaymentMethodVc
{
    @objc func addAddressTapped()
    {
        let obj = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "SelectLocationVc") as! SelectLocationVc
        obj.handlerAddNewAddress = {[weak self] dict in
            self?.setupAddress(dict: dict)
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func setupAddress(dict:JSON) {
        dictAddress = dict
        self.lblMainAddress.text = dict["address_name"].stringValue
        var strSubAddress = "\n\(getCommonString(key: "Floor_key")):\(dict["floor"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Building_key")):\(dict["building"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Avenu_key")):\(dict["avenue"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Street_key")):\(dict["street"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Block_key")):\(dict["block"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Postal_code_key")):\(dict["postal_code"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "City_key")):\(dict["city"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Governorate_key")):\(dict["governorate"].stringValue) \n\n"
        strSubAddress += "\(getCommonString(key: "Country_key")):\(dict["country"].stringValue) "
        self.lblSubAddress.text = strSubAddress
    }
    
}




//MARK: - TableView method

extension AddressWithPaymentMethodVc : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrayPaymentOption.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentMethodTblCell") as! PaymentMethodTblCell
        
        var dict = self.arrayPaymentOption[indexPath.row]
        
        cell.lblPaymentName.text = dict["payment_method"].stringValue
        
        if dict["selected"].stringValue == "0"
        {
            cell.imgCheckmark.isHidden = true
        }
        else
        {
            cell.imgCheckmark.isHidden = false
        }
        
        return cell
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<self.arrayPaymentOption.count
        {
            self.arrayPaymentOption[i]["selected"] = "0"
        }
        
        self.arrayPaymentOption[indexPath.row]["selected"] = "1"
        
        self.tblDeliveryOption.reloadData()
        
    }
    
}

extension AddressWithPaymentMethodVc {
    
    func getDefaultAddress()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlMyDefaultAddress)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token")
                          ]
            
           
            
            print("param : \(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let dict = json["data"]
                        
                        if dict["address_name"].exists()
                        {
                            self.setupAddress(dict: dict)
                        }
                        else {
                            self.lblMainAddress.text = getCommonString(key: "Address_title_key")
                            self.lblSubAddress.text = getCommonString(key: "Please_select_address_key")//json["msg"].stringValue
                        }
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}

