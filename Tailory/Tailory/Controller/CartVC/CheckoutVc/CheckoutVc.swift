//
//  CheckoutVc.swift
//  Tailory
//
//  Created by Haresh on 27/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire

class CheckoutVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwLaundry: UIView!
    @IBOutlet weak var vwMain: UIView!
    
    @IBOutlet weak var lblProducts: UILabel!
    @IBOutlet weak var tblProductList: UITableView!
    @IBOutlet weak var constantHeightTblProductList: NSLayoutConstraint!
    
    @IBOutlet weak var lblDeliveryAddress: UILabel!
    
    @IBOutlet weak var lblMainAddress: UILabel!
    @IBOutlet weak var lblSubAddress: UILabel!
    
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var tblPaymentList: UITableView!
    @IBOutlet weak var constantHeightPaymentList: NSLayoutConstraint!
    
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var lblDeliveryValue: UILabel!
    
    @IBOutlet weak var lbllandary: UILabel!
    @IBOutlet weak var lbllandaryValue: UILabel!
    
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblSubtotalValue: UILabel!
    
    @IBOutlet weak var lblDeliveryCharge: UILabel!
    @IBOutlet weak var lblDeliveryChargeValue: UILabel!
    
    @IBOutlet weak var lblTotalPay: UILabel!
    @IBOutlet weak var lblTotalPayValue: UILabel!
    
    @IBOutlet weak var btnCheckOut: CustomButton!

    //MARK: - Variable
    
    var arrayProductList : [JSON] = []
    var strMessage = String()
    var dictAddress = JSON()
    var dictCheckout = JSON()
    var arrPayment:[JSON] = []
    var dictPayment = JSON()
    var dictAddressDetail = JSON()
    var dictSuceess = JSON()
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblProductList.register(UINib(nibName: "ProductsTblcell", bundle: nil), forCellReuseIdentifier: "ProductsTblcell")

        self.tblPaymentList.register(UINib(nibName: "PaymentTblCell", bundle: nil), forCellReuseIdentifier: "PaymentTblCell")
        
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Checkout_key"))
        
        tblProductList.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        tblPaymentList.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tblProductList.removeObserver(self, forKeyPath: "contentSize")
        
        tblPaymentList.removeObserver(self, forKeyPath: "contentSize")
        
    }
    
    //MARK: - SetupUI
    
    func setupUI()
    {
        
        [lblProducts,lblDeliveryAddress,lblPayment,lblDelivery].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .heavy)
        }
        
        lblMainAddress.textColor = UIColor.appThemeBlackColor
        lblMainAddress.font = themeFont(size: 17, fontname: .roman)
        lblSubAddress.textColor = UIColor.appThemeLightGrayColor
        lblSubAddress.font = themeFont(size: 15, fontname: .roman)
        
        [lblSubtotal,lblDeliveryCharge,lblTotalPay,lbllandary,lblDeliveryValue].forEach { (lbl) in
            
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 14, fontname: .light)
            lbl?.textAlignment = .left
            
        }
        
        [lblSubtotalValue,lblDeliveryChargeValue,lblTotalPayValue,lbllandaryValue].forEach { (lbl) in
            
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 14, fontname: .light)
            lbl?.textAlignment = .right
        }
        
        btnCheckOut.setupThemeButtonUI(backColor: UIColor.appThemeCyprusColor)
        btnCheckOut.setTitle(getCommonString(key: "Checkout_key"), for: .normal)
        
        lblProducts.text = getCommonString(key: "Products_key")
        lblDeliveryAddress.text = getCommonString(key: "Delivery_address_key")
        lblPayment.text = getCommonString(key: "Payment_key")
        lblDelivery.text = getCommonString(key: "Delivery_key")
        
        lblSubtotal.text = getCommonString(key: "Sub_total_key")
        lblDeliveryCharge.text = getCommonString(key: "Delivery_charge_key")
        lblTotalPay.text = getCommonString(key: "Total_to_pay_key")
        lbllandary.text = getCommonString(key: "Laundry_key")
        
        //Remove static data enter when build
        
        setArray()
        
        if !isEnglish
        {
            self.setUpArabicUI()
        }
        
        getCheckoutList()
        
    }
    
    func setUpArabicUI()
    {
        
        self.vwMain.transform = CGAffineTransform(scaleX: -1, y: 1)
        self.tblProductList.transform = CGAffineTransform(scaleX: -1, y: 1)
        [lblProducts,lblDeliveryAddress,lblMainAddress,lblSubAddress,lblSubtotal,lblDeliveryCharge,lblTotalPay,lblPayment,lblDeliveryValue,lblDelivery,lbllandary].forEach { (lbl) in
            lbl?.textAlignment = .right
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        [lblSubtotalValue,lblDeliveryChargeValue,lblTotalPayValue,lbllandaryValue].forEach { (lbl) in
            
            lbl?.textAlignment = .left
            lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
    }
    
    //MARK:- Observe Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblProductList.contentSize.height)")
            self.constantHeightTblProductList.constant = tblProductList.contentSize.height
            
            self.constantHeightPaymentList.constant = tblPaymentList.contentSize.height
        }
        
    }
    
    //MARK: - Array setup
    
    func setArray()
    {
        var dict = JSON()
        dict["payment_name"].stringValue = getCommonString(key: "Credit_card_key")
        dict["is_selected"].stringValue = "1"
        dict["payment_type"].stringValue = "2"
        arrPayment.append(dict)
        
        dictPayment = dict
        
        /*dict = JSON()
        dict["payment_name"].stringValue = getCommonString(key: "Fatoorah_key")
        dict["is_selected"].stringValue = "0"
        dict["payment_type"].stringValue = "2"
        arrPayment.append(dict)
        
        dict = JSON()
        dict["payment_name"].stringValue = getCommonString(key: "Cash_on_delivery_key")
        dict["is_selected"].stringValue = "0"
        dict["payment_type"].stringValue = "3"
        arrPayment.append(dict)*/
        
        self.tblPaymentList.reloadData()
    }
    
    func setupAddress(dict:JSON) {
        dictAddressDetail = dict
        if getUserDetail("is_login_guest") == "1" {
            self.lblMainAddress.text = self.dictAddress["address_name"].stringValue
            var strSubAddress = "\n\(getCommonString(key: "Floor_key")):\(self.dictAddress["floor"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Building_key")):\(self.dictAddress["building"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Avenu_key")):\(self.dictAddress["avenue"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Street_key")):\(self.dictAddress["street"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Block_key")):\(self.dictAddress["block"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Postal_code_key")):\(self.dictAddress["postal_code"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "City_key")):\(self.dictAddress["city"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Governorate_key")):\(self.dictAddress["governorate"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Country_key")):\(self.dictAddress["country"].stringValue) "
            self.lblSubAddress.text = strSubAddress
        }
        else {
            self.lblMainAddress.text = dict["address_name"].stringValue
            var strSubAddress = "\n\(getCommonString(key: "Floor_key")):\(dict["floor"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Building_key")):\(dict["building"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Avenu_key")):\(dict["avenue"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Street_key")):\(dict["street"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Block_key")):\(dict["block"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Postal_code_key")):\(dict["postal_code"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "City_key")):\(dict["city"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Governorate_key")):\(dict["governorate"].stringValue) \n\n"
            strSubAddress += "\(getCommonString(key: "Country_key")):\(dict["country"].stringValue) "
            self.lblSubAddress.text = strSubAddress
        }
        /*
        self.lblMainAddress.text = dict["address_name"].stringValue
        var strSubAddress = "\(getCommonString(key: "Floor_key")):\(dict["floor"].stringValue) "
        strSubAddress += "\(getCommonString(key: "Building_key")):\(dict["building"].stringValue) "
        strSubAddress += "\(getCommonString(key: "Avenu_key")):\(dict["avenue"].stringValue) "
        strSubAddress += "\(getCommonString(key: "Street_key")):\(dict["street"].stringValue) "
        strSubAddress += "\(getCommonString(key: "Block_key")):\(dict["block"].stringValue) "
        strSubAddress += "\(getCommonString(key: "Postal_code_key")):\(dict["postal_code"].stringValue) "
        strSubAddress += "\(getCommonString(key: "City_key")):\(dict["city"].stringValue) "
        strSubAddress += "\(getCommonString(key: "Governorate_key")):\(dict["governorate"].stringValue) "
        strSubAddress += "\(getCommonString(key: "Country_key")):\(dict["country"].stringValue) "
        self.lblSubAddress.text = strSubAddress
        */
    }
    
    func setupAmountData(dict:JSON){
        dictCheckout = dict
        self.lblSubtotalValue.text = dict["sub_total_amount"].stringValue + " \(getCommonString(key: "KD_key"))"
        lbllandaryValue.text = dict["laundry"].stringValue + " \(getCommonString(key: "KD_key"))"
        lblDeliveryChargeValue.text = dict["delivery_charge"].stringValue + " \(getCommonString(key: "KD_key"))"
        lblTotalPayValue.text = dict["total_amount"].stringValue + " \(getCommonString(key: "KD_key"))"
        lblDeliveryValue.text = getCommonString(key: "Normal_key")
        
        if dict["laundry"].stringValue == "0" || dict["laundry"].stringValue == "" || dict["laundry"].stringValue == "0.0"{
            self.vwLaundry.isHidden = true
        } else {
            self.vwLaundry.isHidden = false
        }
    }

}

//MARK: - IBAction

extension CheckoutVc
{
    @IBAction func btnCheckoutTapped(_ sender: UIButton) {
        if arrPayment.contains(where: { (json) -> Bool in
            if json["is_selected"].stringValue == "1" {
                return true
            }
            return false
        }){
            postAddOrder()
        } else {
            makeToast(strMessage: getCommonString(key: "Please_select_one_payment_gateway_key"))
        }
    }
    
    func redirectToOrderDetailScreen(dict:JSON){
        appdelgate.tailerTabbarVC = TailerTabBarVC()
        appdelgate.tailerTabbarVC.selectedIndex = 1
        isComeFromPush = true
        var dictData = JSON()
        dictData["address_id"].stringValue = dict["address_id"].stringValue
        dictData["id"].stringValue = dict["order_id"].stringValue
        dictPushData = dictData
        appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
    }
}


//MARK: - TableView delegate DataSource

extension CheckoutVc : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblProductList {
            return self.arrayProductList.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblProductList
        {
            return self.arrayProductList[section]["data"].count
        }
        else if tableView == tblPaymentList {
          return arrPayment.count
        }
        if arrayProductList[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Tailors_key") {
            return arrayProductList[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["customize_design"].arrayValue.count
        } else if arrayProductList[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Accessories_key") {
            return arrayProductList[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["options"].arrayValue.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblProductList
        {
            if arrayProductList[indexPath.section]["type"].stringValue == getCommonString(key: "Accessories_key") {
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTblcell") as! ProductsTblcell
                
                [cell.btnPlus,cell.btnMinus].forEach { (btn) in
                    btn?.isHidden = true
                }
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductQty.font = themeFont(size: 13, fontname: .medium)
                cell.viewFabrics.isHidden = true
                let dict = arrayProductList[indexPath.section]["data"][indexPath.row]
                let innerArray = self.arrayProductList[indexPath.section]["data"].arrayValue
                
                if indexPath.row == innerArray.count - 1{
                    self.tblProductList.separatorStyle = .none
                } else {
                    self.tblProductList.separatorStyle = .singleLine
                }
                
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductName.text = dict["product_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblPrice.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                cell.imgProduct.sd_setImage(with: dict["product_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)

                cell.vwCombineDelivery.isHidden = true
                cell.tblInnerDesign.isHidden = false
                cell.lblFabQuantity.isHidden = true
                
                cell.tblInnerDesign.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                return cell
            } else if arrayProductList[indexPath.section]["type"].stringValue == getCommonString(key: "Fabrics_key") {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTblcell") as! ProductsTblcell
                [cell.btnPlus,cell.btnMinus].forEach { (btn) in
                    btn?.isHidden = true
                }
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductQty.font = themeFont(size: 13, fontname: .medium)
                cell.viewFabrics.isHidden = true
                let dict = arrayProductList[indexPath.section]["data"][indexPath.row]
                let innerArray = self.arrayProductList[indexPath.section]["data"].arrayValue
                
                if indexPath.row == innerArray.count - 1 {
                    self.tblProductList.separatorStyle = .none
                } else {
                    self.tblProductList.separatorStyle = .singleLine
                }
                
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductName.text = dict["fabricdesign_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblPrice.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                cell.imgProduct.sd_setImage(with: dict["fabric_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
//                cell.imgProduct.sd_setImage(with: dict["fabricdesign_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                
                cell.vwCombineDelivery.isHidden = true
                
                cell.tblInnerDesign.isHidden = true
                cell.constantTblInnerHeight.constant = 0
                cell.lblFabQuantity.isHidden = false
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                
                cell.lblFabQuantity.textColor = UIColor.appThemeLightGrayColor
                cell.lblFabQuantity.text = "\(dict["quantity"].stringValue) \(getCommonString(key: "Meter_key"))"
                
                return cell
            } else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsTblcell") as! ProductsTblcell
                
                let dict = arrayProductList[indexPath.section]["data"][indexPath.row]
                let innerArray = arrayProductList[indexPath.section]["data"].arrayValue
                
                if indexPath.row == innerArray.count - 1{
                    self.tblProductList.separatorStyle = .none
                } else {
                    self.tblProductList.separatorStyle = .singleLine
                }
                
                cell.lblProductQty.textColor = UIColor.appThemeBlackColor
                cell.lblProductQty.font = themeFont(size: 13, fontname: .medium)
                
                // For Laundry
                cell.lblCombineDelivery.text = getCommonString(key: "Laundry_key")
                
                cell.switchCombineDeliveryOutlet.isOn = dict["is_laundry"].stringValue == "1" ? true : false
                cell.switchCombineDeliveryOutlet.tag = indexPath.row
                cell.switchCombineDeliveryOutlet.accessibilityValue = "\(indexPath.section)"
                
                cell.handlerCombineDeliveryValue = {[weak self] section,row in
                    print("section \(section),row \(row)")
                    let dictInner:JSON = (self?.arrayProductList[section]["data"][row])!
                    print("dict \(dictInner)")
                    var dictCart = JSON()
                    dictCart["cart_id"].stringValue = dictInner["cart_id"].stringValue
                    dictCart["is_laundry"].stringValue = dictInner["is_laundry"].stringValue == "0" ? "1" : "0"
                    dictCart["category_id"].stringValue = dictInner["category_id"].stringValue
                    self?.addRemoveLaudry(section: section, row: row, dict: dictCart)
                }
                
                //Setup Data
                
                cell.imgProduct.sd_setImage(with: dict["design_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                cell.lblProductName.text = dict["design_name"].stringValue
                cell.lblProductQty.text = dict["quantity"].stringValue
                cell.lblPrice.text = dict["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                [cell.btnPlus,cell.btnMinus].forEach { (btn) in
                    btn?.isHidden = true
                }
                cell.lblFabQuantity.isHidden = true
                cell.tblInnerDesign.register(UINib(nibName: "InnerCustomDesignTblCell", bundle: nil), forCellReuseIdentifier: "InnerCustomDesignTblCell")
                cell.tblInnerDesign.isHidden = false
                cell.tblInnerDesign.tag = indexPath.row
                cell.tblInnerDesign.accessibilityValue = "\(indexPath.section)"
                cell.tblInnerDesign.delegate = self
                cell.tblInnerDesign.dataSource = self
                cell.tblInnerDesign.reloadData()
                cell.tblInnerDesign.layoutIfNeeded()
                cell.tblInnerDesign.layoutSubviews()
                
                //For fabric hidden
                if dict["is_fabric_added"].stringValue == "1"{
                    cell.vwCombineDelivery.isHidden = false
                    cell.viewFabrics.isHidden = false
                    let dictFabric = dict["fabrics"][0]
                    cell.lblFabricName.text = dictFabric["fabricdesign_name"].stringValue
                    cell.lblMeterValue.text = "\(dictFabric["quantity"].stringValue) \(getCommonString(key: "Meter_key"))"
                    cell.lblFabricPriceValue.text = dictFabric["price"].stringValue + " \(getCommonString(key: "KD_key"))"
                    cell.imgFabric.sd_setImage(with: dictFabric["fabric_image"].url, placeholderImage: UIImage(named: "ic_taylor_details_more_info_splash_holder"), options: .lowPriority, completed: nil)
                } else {
                    cell.vwCombineDelivery.isHidden = true
                    cell.viewFabrics.isHidden = true
                }
                cell.contentView.layoutIfNeeded()
                cell.contentView.layoutSubviews()
                return cell
            }
        }
        else if tableView == tblPaymentList
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentTblCell") as! PaymentTblCell
            let dict = arrPayment[indexPath.row]
            cell.lblPaymentName.text = dict["payment_name"].stringValue
            if dict["is_selected"].stringValue == "1"{
                cell.btnSelectedPaymentOutlet.isHidden = false
            } else {
                cell.btnSelectedPaymentOutlet.isHidden = true
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "InnerCustomDesignTblCell") as! InnerCustomDesignTblCell
        if arrayProductList[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Accessories_key"){
            let dict = arrayProductList[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["options"][indexPath.row]
            if isEnglish {
                cell.lblCustomizeDesignTitle.text = "\(dict["option_name"].stringValue) : "
            } else {
               cell.lblCustomizeDesignTitle.text = " : \(dict["option_name"].stringValue)"
            }
            cell.lblCustomizeDesignValue.text = dict["option_value_name"].stringValue
            return cell
        }
        if arrayProductList[Int(tableView.accessibilityValue!)!]["type"].stringValue == getCommonString(key: "Tailors_key"){
            let dict = arrayProductList[Int(tableView.accessibilityValue!)!]["data"][tableView.tag]["customize_design"][indexPath.row]
//            if isEnglish {
                 cell.lblCustomizeDesignTitle.text = "\(dict["style_name"].stringValue) : "
            /*} else {
                 cell.lblCustomizeDesignTitle.text = " : \(dict["style_name"].stringValue)"
            }*/
            cell.lblCustomizeDesignValue.text = dict["design_name"].stringValue          
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblPaymentList{
            /*for i in 0..<arrPayment.count {
                var dict = arrPayment[i]
                dict["is_selected"].stringValue = "0"
                arrPayment[i]  = dict
            }
            var dict = arrPayment[indexPath.row]
            dict["is_selected"].stringValue = "1"
            arrPayment[indexPath.row]  = dict
            
            dictPayment = dict
            tableView.reloadData()*/
            dictPayment = arrPayment[indexPath.row]
            
        }
    }
    
    //set header for
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == self.tblProductList
        {
            let vw = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)?[0] as? HeaderView
            
            if !isEnglish {
                vw?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            vw?.lblTitleName.text = self.arrayProductList[section]["type"].stringValue
//            vw?.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255)
            return vw
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == self.tblProductList
        {
            return 45
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

extension CheckoutVc {
    func getCheckoutList()
    {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlCheckoutList)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
//                          "is_login_guest" : getUserDetail("is_login_guest") == "0" ? "0" : "1",
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString,
                          "address_id": getUserDetail("is_login_guest") == "1" ? "" : dictAddress["address_id"].stringValue]
            
//            "address_id": getUserDetail("is_login_guest") == "1" ? "0" : dictAddress["address_id"].stringValue
            print("param : \(param)")
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.arrayProductList = []
                        let data = json["data"]
                        if data["products"].exists(){
                            if data["products"].arrayValue.count != 0{
                                var dict = JSON()
                                dict["type"].stringValue = getCommonString(key: "Accessories_key")
                                dict["data"] = JSON(data["products"].arrayValue)
                                self.arrayProductList.append(dict)
                            }
                        }
                        if data["fabrics"].exists(){
                            if data["fabrics"].arrayValue.count != 0{
                                var dict = JSON()
                                dict["type"].stringValue = getCommonString(key: "Fabrics_key")
                                dict["data"] = JSON(data["fabrics"].arrayValue)
                                self.arrayProductList.append(dict)
                            }
                        }
                        if data["tailors"].exists(){
                            if data["tailors"].arrayValue.count != 0{
                                var dict = JSON()
                                dict["type"].stringValue = getCommonString(key: "Tailors_key")
                                dict["data"] = JSON(data["tailors"].arrayValue)
                                self.arrayProductList.append(dict)
                            }
                        }
                        self.setupAddress(dict: json["data"]["delivery_address"])
                        self.setupAmountData(dict: json["data"])
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayProductList = []
                        self.strMessage = json["msg"].stringValue
                    }
                    self.tblProductList.reloadData()
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func postAddOrder() {
        self.view.endEditing(true)
        if dictPayment["payment_type"].stringValue == "" {
            makeToast(strMessage: getCommonString(key: "Please_select_payment_method_key"))
        }
        else {
            if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
                let url = "\(urlOrder)\(urlAddOrder)"
                
                let param  = ["lang" : lang,
                              "user_id" : getUserDetail("user_id"),
                              "access_token" : getUserDetail("access_token"),
                              "address_id": getUserDetail("is_login_guest") == "0" ? dictAddress["address_id"].stringValue : "1",
                              "payment_type":dictPayment["payment_type"].stringValue,
                              "is_login_guest":getUserDetail("is_login_guest") == "0" ? "0" : "1",
                              "sub_total_amount":dictCheckout["sub_total_amount"].stringValue,
                              "laundry":dictCheckout["laundry"].stringValue,
                              "delivery_charge":dictCheckout["delivery_charge"].stringValue,
                              "total_amount":dictCheckout["total_amount"].stringValue]
                
                //"address_id":dictAddress["address_id"].stringValue
                print("param : \(param)")
                showLoader()
                
                CommonService().Service(url: url, param: param) { (respones) in
                    
                    self.hideLoader()
                    
                    if let json = respones.value {
                        print("JSON : \(json)")                        
                        if json["flag"].stringValue == GlobalVariables.strSuccessResponse {
                            let data = json["data"]
                            self.dictSuceess = json["data"]
                            if data["result"].stringValue == "1"{
                                if getUserDetail("is_login_guest") == "1" {
                                    let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
                                    obj.dictSuccess = self.dictSuceess
                                    self.navigationController?.pushViewController(obj, animated: true)
                                } else {
                                    self.redirectToOrderDetailScreen(dict: data)
                                }
                            }else {
                                self.getPaymentGateway()
                            }
                        }
                        else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                            self.logoutAPICalling()
                        }
                        else {
                            makeToast(strMessage: json["msg"].stringValue)
                        }
                        self.tblProductList.reloadData()
                    }
                    else {
                        makeToast(strMessage:serverNotResponding)
                    }
                }
            }
            else {
                makeToast(strMessage: networkMsg)
            }
        }
    }
    
    func getPaymentGateway() {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)! {
            let url = "\(urlPayment)\(urlFatoorahPayment)"
            
            let parameter  = ["InvoiceValue" : dictCheckout["total_amount"].stringValue,
                          "CustomerName" : getUserDetail("full_name"),
                          "CustomerBlock" :dictAddressDetail["block"].stringValue,
                          "CustomerStreet":dictAddressDetail["street"].stringValue,
                          "CustomerHouseBuildingNo":dictAddressDetail["building"].stringValue,
                          "CustomerAddress":"\(dictAddressDetail["floor"].stringValue) \(dictAddressDetail["avenue"].stringValue) \(dictAddressDetail["city"].stringValue) \(dictAddressDetail["governorate"].stringValue)",
                          "CountryCodeId":"1",
                          "CustomerMobile":getUserDetail("phone_no"),
                          "CustomerEmail":getUserDetail("email"),
                          "DisplayCurrencyId":"1",
                          "SendInvoiceOption":"2",
                          "CallBackUrl":"\(kCallBackUrl)\(kPaymentTrasactionStatus)",
                          "Language":isEnglish == true ? "2" : "1"]
            
            let dict = JSON(parameter)
            
            let param:[String:String] = ["invoiceCreate":dict.rawString() ?? ""]
            
            
            print("param : \(param)")
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse {                        
                        let obj = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
                        obj.dictPaymentDetail = json["data"]
                        obj.dictSuccess = self.dictSuceess
                        obj.selectedPaymentGateway = .fatoorah
                        self.navigationController?.pushViewController(obj, animated: true)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied {
                        self.logoutAPICalling()
                    }
                    else {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else {
            makeToast(strMessage: networkMsg)
        }
    }
    
    func addRemoveLaudry(section:Int,row:Int,dict:JSON) {
        self.view.endEditing(true)
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlAddRemoveLaundry)"
            
            let param  = ["lang" : lang,
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "cart_id" : dict["cart_id"].stringValue,
                          "category_id" : dict["category_id"].stringValue,
                          "is_laundry" : dict["is_laundry"].stringValue,
                          "is_cart":"1",
                          "is_login_guest":getUserDetail("is_login_guest") == "1" ? "1" : "0",
                          "address_id":getUserDetail("is_login_guest") == "1" ? "" : dictAddress["address_id"].stringValue]
            
            print("param : \(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        self.arrayProductList = []
                        let data = json["data"]
                        if data["products"].exists(){
                            if data["products"].arrayValue.count != 0{
                                var dict = JSON()
                                dict["type"].stringValue = getCommonString(key: "Accessories_key")
                                dict["data"] = JSON(data["products"].arrayValue)
                                self.arrayProductList.append(dict)
                            }
                        }
                        if data["fabrics"].exists(){
                            if data["fabrics"].arrayValue.count != 0{
                                var dict = JSON()
                                dict["type"].stringValue = getCommonString(key: "Fabrics_key")
                                dict["data"] = JSON(data["fabrics"].arrayValue)
                                self.arrayProductList.append(dict)
                            }
                        }
                        if data["tailors"].exists(){
                            if data["tailors"].arrayValue.count != 0{
                                var dict = JSON()
                                dict["type"].stringValue = getCommonString(key: "Tailors_key")
                                dict["data"] = JSON(data["tailors"].arrayValue)
                                self.arrayProductList.append(dict)
                            }
                        }
                        self.setupAddress(dict: json["data"]["delivery_address"])
                        self.setupAmountData(dict: json["data"])
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                        self.arrayProductList = []
                        self.strMessage = json["msg"].stringValue
                    }
                    UIView.performWithoutAnimation {
                        self.tblProductList.reloadRows(at: [IndexPath(row: row, section: section)], with: .none)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}
