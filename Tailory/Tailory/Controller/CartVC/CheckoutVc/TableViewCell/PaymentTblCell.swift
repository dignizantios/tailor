//
//  PaymentTblCell.swift
//  Tailory
//
//  Created by Haresh on 27/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class PaymentTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblPaymentName: UILabel!
    @IBOutlet weak var btnSelectedPaymentOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblPaymentName.textColor = UIColor.appThemeBlackColor
        lblPaymentName.font = themeFont(size: 14, fontname: .light)
        lblPaymentName.textAlignment = .left
        
        if !isEnglish
        {
            lblPaymentName.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblPaymentName.textAlignment = .right
            btnSelectedPaymentOutlet.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
