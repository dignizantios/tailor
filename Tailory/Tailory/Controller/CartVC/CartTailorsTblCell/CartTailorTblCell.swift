//
//  CartTailorTblCell.swift
//  Tailory
//
//  Created by Haresh on 27/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class CartTailorTblCell: UITableViewCell {
    
    //MARK:- Varible Declration
    
    var handlerCombineDeliveryValue:(Int,Int) -> Void = {_,_ in   }
    var handlerLaundryValue:(Int,Int) -> Void = {_,_ in   }
    var handlerPlusValue:(Int,Int) -> Void = {_,_ in   }
    var handlerMinusValue:(Int,Int) -> Void = {_,_ in   }
    var handlerEditDesignValue:(Int,Int) -> Void = {_,_ in   }
    var handlerEditMeasurmentValue:(Int,Int) -> Void = {_,_ in   }
    var handlerDeleteValue:(Int,Int) -> Void = {_,_ in   }

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblProductQty: UILabel!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var lblDesignName: UILabel!
    
    @IBOutlet weak var constantStckBottom: NSLayoutConstraint!
    @IBOutlet weak var vwLaundry: UIView!
    @IBOutlet var vwCombineDelivery: UIView!
    
//    @IBOutlet weak var lblCollarStyle: UILabel!
//    @IBOutlet weak var lblCuffs: UILabel!
//    @IBOutlet weak var lblSideLine: UILabel!
    
    @IBOutlet weak var tblInnerDesign: UITableView!
    @IBOutlet weak var constantTblInnerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lblPriceValue: UILabel!
    
    @IBOutlet var vwEdit: UIView!
    @IBOutlet var vwEditHeight: NSLayoutConstraint!
    @IBOutlet weak var btnEditMeasurment: CustomButton!
    @IBOutlet weak var btnEditDesign: CustomButton!

    @IBOutlet weak var lblCombineDelivery: UILabel!
    @IBOutlet weak var btnCombineDeliveryoutlet: UIButton!
    @IBOutlet weak var lblLaundry: UILabel!
    
    @IBOutlet weak var vwFabric: UIView!
    @IBOutlet weak var lblFabric: UILabel!
    
    @IBOutlet weak var imgFabric: UIImageView!
    @IBOutlet weak var lblFabricName: UILabel!
    @IBOutlet weak var lblMeterValue: UILabel!
    @IBOutlet weak var lblFabricPriceValue: UILabel!
    @IBOutlet weak var btnLaundryOutlet: UIButton!
    
    @IBOutlet var vwTrackOrder: UIView!
    @IBOutlet var btnTrackOrder: CustomButton!
    @IBOutlet var btnReorder: UIButton!
    @IBOutlet weak var switchCombineDeliveryOutlet: UISwitch!
    @IBOutlet weak var switchLaundryOutlet: UISwitch!
    
    @IBOutlet var lblOutOfStock: UILabel!
    @IBOutlet weak var vwDelete: UIView!
    @IBOutlet weak var btnDeleteOutlet: CustomButton!
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        img.layer.cornerRadius = 5.0
        img.layer.masksToBounds = true
        
        lblDesignName.font = themeFont(size: 16, fontname: .medium)
        lblDesignName.textColor = UIColor.appThemeBlackColor
        
        lblProductQty.textColor = UIColor.appThemeBlackColor
        lblProductQty.font = themeFont(size: 15, fontname: .light)
        
//        [lblCollarStyle,lblCuffs,lblSideLine].forEach { (lbl) in
//            lbl?.font = themeFont(size: 13, fontname: .light)
//        }
        
        lblCombineDelivery.textColor = UIColor.appThemeBlackColor
        lblCombineDelivery.font = themeFont(size: 15, fontname: .roman)
        
        lblLaundry.textColor = UIColor.appThemeBlackColor
        lblLaundry.font = themeFont(size: 15, fontname: .roman)

        [btnEditMeasurment,btnEditDesign].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .medium)
            btn?.backgroundColor = UIColor(red: 242/255, green: 237/255, blue: 226/255, alpha: 1.0)
            btn?.cornerRadius = 3
        }
        
        
        lblFabricPriceValue.font = themeFont(size: 14, fontname: .medium)
        lblFabricPriceValue.textColor = UIColor.appThemeSilverColor
        
        lblPriceValue.font = themeFont(size: 14, fontname: .medium)
        lblPriceValue.textColor = UIColor.appThemeSilverColor

        lblFabricName.font = themeFont(size: 16, fontname: .medium)
        lblFabricName.textColor = UIColor.appThemeBlackColor
        
        lblMeterValue.font = themeFont(size: 14, fontname: .medium)
        lblMeterValue.textColor = UIColor.appThemeLightGrayColor
        
        lblFabric.font = themeFont(size: 15, fontname: .medium)
        lblFabric.textColor = UIColor.appThemeBlackColor

        
        btnEditMeasurment.setTitle(getCommonString(key: "Edit_measurment_key"), for: .normal)
        btnEditDesign.setTitle(getCommonString(key: "Edit_design_key"), for: .normal)
        
        lblFabric.text = getCommonString(key: "Fabric_key")
        lblLaundry.text = getCommonString(key: "Laundry_key")
        lblCombineDelivery.text = getCommonString(key: "Combine_delivery_key")
        tblInnerDesign.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)

        [btnTrackOrder, btnReorder].forEach { (btn) in
            btn?.isHidden = true
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        }
        
        lblOutOfStock.font = themeFont(size: 12, fontname: .medium)
        lblOutOfStock.textColor = UIColor.red
        
        btnTrackOrder.setTitle(getCommonString(key: "Track_order_key"), for: .normal)
        btnReorder.setTitle(getCommonString(key: "Reorder_key"), for: .normal)
        
        btnDeleteOutlet.titleLabel?.font = UIFont(name: themeFonts.heavy.rawValue, size: 35)
        
        if !isEnglish
        {
            self.contentView.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.img.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            [btnEditMeasurment,btnEditDesign].forEach { (btn) in
                btn?.titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
            }
            
            [lblDesignName,lblPriceValue,lblCombineDelivery,lblFabric,lblFabricName,lblMeterValue,lblFabricPriceValue, lblLaundry,lblOutOfStock].forEach { (lbl) in
                lbl?.transform = CGAffineTransform(scaleX: -1, y: 1)
                lbl?.textAlignment = .right
            }
            
            lblProductQty.transform = CGAffineTransform(scaleX: -1, y: 1)
            btnTrackOrder.transform = CGAffineTransform(scaleX: -1, y: 1)            
            
        }

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblInnerDesign.contentSize.height)")
            self.contentView.layoutIfNeeded()
            self.contentView.layoutSubviews()
//            if tblInnerDesign.contentSize.height <= 68
//            {
//                self.constantStckBottom.constant = 0
//            }
//            else
//            {
//                self.constantStckBottom.constant = 3
            
                self.constantTblInnerHeight.constant = tblInnerDesign.contentSize.height
//            }
            
        }
    }
    
    //MARK:- Action
    
    @IBAction func btnCombineDelivryAction(_ sender: UIButton) {
        handlerCombineDeliveryValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnLaundryAction(_ sender: UIButton) {
        handlerLaundryValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnMinusAction(_ sender : UIButton){
        handlerMinusValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnPlusAction(_ sender : UIButton){
        handlerPlusValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnEditDesignAction(_ sender : UIButton){
        handlerEditDesignValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnEditMesurementAction(_ sender : UIButton){
        handlerEditMeasurmentValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func switchCombineDelivryAction(_ sender: UISwitch) {
           handlerCombineDeliveryValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
       
    @IBAction func switchLaundryAction(_ sender: UISwitch) {
           handlerLaundryValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        handlerDeleteValue(Int(sender.accessibilityValue!)!,sender.tag)
    }
}
