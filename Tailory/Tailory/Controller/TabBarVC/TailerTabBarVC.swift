//
//  CustomTabBarVC.swift
//  Tailory
//
//  Created by Khushbu on 15/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class TailerTabBarVC: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        let screenWidth = UIScreen.main.bounds.width
        let tabBarHeight = self.tabBar.frame.size.height
        self.tabBar.tintColor = UIColor.appThemeCyprusColor
        
        let home = GlobalVariables.homeStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let tabbarOneItem = UITabBarItem(title: getCommonString(key: "Home_key"), image: UIImage(named: "ic_home_home_button_select"), selectedImage: UIImage(named: "ic_home_home_button_unselect"))
        tabbarOneItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+24)
        home.tabBarItem = tabbarOneItem
        
//        let accessories = GlobalVariables.accessoriesStoryboard.instantiateViewController(withIdentifier: "AccessoriesVC") as! AccessoriesVC
//        let tabbarTwoItem = UITabBarItem(title: getCommonString(key: "Accessories_key"), image: UIImage(named: "ic_home_accessories_button_select"), selectedImage: UIImage(named: "ic_home_accessories_button_unselect"))
//        tabbarTwoItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+24)
//        accessories.tabBarItem = tabbarTwoItem

        let order = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "MyOrderVc") as! MyOrderVc
        let tabbarTwoItem = UITabBarItem(title: getCommonString(key: "Orders_key"), image: UIImage(named: "ic_home_cart_button_select"), selectedImage: UIImage(named: "ic_home_cart_button_unselect"))
        tabbarTwoItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+24)
        order.tabBarItem = tabbarTwoItem

        let favorites = GlobalVariables.FavouriteStoryboard.instantiateViewController(withIdentifier: "FavoritesVC") as! FavoritesVC
        let tabbarThreeItem = UITabBarItem(title: getCommonString(key: "Favorites_key"), image: UIImage(named: "ic_home_favorites_button_select"), selectedImage: UIImage(named: "ic_home_favorites_button_unselect"))
        tabbarThreeItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+24)
        favorites.tabBarItem = tabbarThreeItem
        
//        let cart = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
//        let tabBarFourItem = UITabBarItem(title: getCommonString(key: "Cart_key"), image: UIImage(named: "ic_home_cart_button_select"), selectedImage: UIImage(named: "ic_home_cart_button_unselect"))
//        tabBarFourItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+24)
//        cart.tabBarItem = tabBarFourItem
        
        let profile = GlobalVariables.profileStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let tabBarFiveItem = UITabBarItem(title: getCommonString(key: "Profile_key"), image: UIImage(named: "ic_home_profile_button_select"), selectedImage: UIImage(named: "ic_home_profile_button_select"))
        tabBarFiveItem.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -tabBarHeight/2+24)
        profile.tabBarItem = tabBarFiveItem
        
        
        let n1 = UINavigationController(rootViewController: home)
     //   let n2 = UINavigationController(rootViewController: accessories)
        let n2 = UINavigationController(rootViewController: order)
        let n3 = UINavigationController(rootViewController: favorites)
//        let n4 = UINavigationController(rootViewController: cart)
        let n5 = UINavigationController(rootViewController: profile)
        
        n1.isNavigationBarHidden = false
        n2.isNavigationBarHidden = false
        n3.isNavigationBarHidden = false
//        n4.isNavigationBarHidden = false
        n5.isNavigationBarHidden = false
        
        self.viewControllers = [n1,n2,n3,n5]
        
        if !isEnglish {
            self.viewControllers = [n5,n3,n2,n1]
        }
        
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor.lightGray,
            NSAttributedString.Key.font: themeFont(size: 13, fontname: .light),
            ] as [NSAttributedString.Key : Any]
        
        let attributesSelected = [
            NSAttributedString.Key.foregroundColor: UIColor.appThemeCyprusColor,
            NSAttributedString.Key.font: themeFont(size: 13, fontname: .light),
            ] as [NSAttributedString.Key : Any]
        
        UITabBarItem.appearance().setTitleTextAttributes(attributes, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(attributesSelected, for: .selected)
        
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 5
        tabBar.layer.shadowColor = UIColor.appThemeCyprusColor.cgColor
        tabBar.layer.shadowOpacity = 0.3
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        let navigationController = viewController as? UINavigationController
        navigationController?.popToRootViewController(animated: true)
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard
            let tab = tabBarController.viewControllers?.index(of: viewController), [2].contains(tab)
            else {
                if let vc = tabBarController.viewControllers?[tabBarController.selectedIndex] as? UINavigationController {
                    vc.dismiss(animated: true, completion: nil)
                    vc.popToRootViewController(animated: false)
                }
                return true
        }
        return true
    }
    
}
