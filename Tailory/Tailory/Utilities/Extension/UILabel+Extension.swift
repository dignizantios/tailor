//
//  UILabel+Extension.swift
//  Tailory
//
//  Created by YASH on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation

extension UILabel
{
    
    func addImageToLabel(imageName: String, strText : String, aboveString : String)
    {
        
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:imageName)
        //Set bound to reposition
        let imageOffsetY:CGFloat = -5.0;
        
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        
        let str = NSMutableAttributedString(string: aboveString)
        
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        
        str.append(attachmentString)
        
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(str)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: strText)
        completeText.append(textAfterIcon)
        
        self.attributedText = completeText
       
    }
    
    func addDifferenceColorinLabel(str1:String,color1:UIColor,str2:String,color2:UIColor)
    {
        
        let attrs1 = [NSAttributedString.Key.foregroundColor : color1]
        let attrs2 = [NSAttributedString.Key.foregroundColor : color2]
        
        let attributedString1 = NSMutableAttributedString(string:str1, attributes:attrs1)
        let attributedString2 = NSMutableAttributedString(string:str2, attributes:attrs2)
        
        attributedString1.append(attributedString2)
        
        self.attributedText = attributedString1
        
    }
    
}
