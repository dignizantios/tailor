//
//  UIViewController+Extension.swift
//  Tailory
//
//  Created by om on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import AVFoundation
import SwiftyJSON
import AlamofireSwiftyJSON
import Alamofire
import CoreLocation
import GoogleMaps
import DropDown

extension UIViewController
{
    @IBAction func btnDismissAction(_ sender:UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func configureReviewRatingView(ratingView : HCSStarRatingView)
    {
        ratingView.allowsHalfStars = false
        ratingView.maximumValue = 5
        ratingView.minimumValue = 0
        ratingView.emptyStarImage = #imageLiteral(resourceName: "ic_star_unselected_small")
        ratingView.filledStarImage = #imageLiteral(resourceName: "ic_star_selected_big")
    }
    func configureRatingView(ratingView : HCSStarRatingView)
    {
        ratingView.allowsHalfStars = false
        ratingView.maximumValue = 5
        ratingView.minimumValue = 0
        ratingView.emptyStarImage = #imageLiteral(resourceName: "ic_star_unselected_small")
        ratingView.filledStarImage = #imageLiteral(resourceName: "ic_star_selected_small")
    }
    
    //MARK: - Set TimeStamp
    
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) \(getCommonString(key: "Years_ago_key"))"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Years_ago_key"))"
            } else {
                return "\(getCommonString(key: "Last_year_key"))"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Month_ago_key"))"
            } else {
                return "\(getCommonString(key: "Last_month_key"))"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) \(getCommonString(key: "Weeks_ago_key"))"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Week _ago_key"))"
            } else {
                return "\(getCommonString(key: "Last_week_key"))"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) \(getCommonString(key: "Days_ago_key"))"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Day_ago_key"))"
            } else {
                return "\(getCommonString(key: "Yesterday_key"))"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Hour_ago_key"))"
            } else {
                return "\(getCommonString(key: "An_hour_ago_key"))"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 \(getCommonString(key: "Min_ago_key"))"
            } else {
                return "\(getCommonString(key: "A_minute_ago_key"))"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) \(getCommonString(key: "seconds_ago_key"))"
        } else {
            return "\(getCommonString(key: "Just_now_key"))"
        }
        
    }
    
    
    //MARK: - Images with String
    
    
    func getAttributedString(imgAttachment:UIImage,strPostText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(image1String)
        fullString.append(NSAttributedString(string: strPostText))
        return fullString
    }
    
    
    func getAttributedString(imgAttachment:UIImage,strPreText:String) -> NSAttributedString {
        let fullString = NSMutableAttributedString(string: "")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = imgAttachment
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(NSAttributedString(string: strPreText))
        fullString.append(image1String)
        return fullString
    }
    
    
    //MARK: - Call Method
    
    
    func callUser(strPhoneNumber:String)
    {
        
        let phone = strPhoneNumber
        
        var phoneStr: String = "telprompt://\(phone)"
        phoneStr = phoneStr.replacingOccurrences(of: "+", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "(", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: ")", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: "-", with: "")
        phoneStr = phoneStr.replacingOccurrences(of: " ", with: "")
        
        let urlPhone = URL(string: phoneStr)
        if UIApplication.shared.canOpenURL(urlPhone!)
        {
            //UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(urlPhone!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(urlPhone!)
            }
            
        }
        else
        {
            // KSToastView.ks_showToast("Call facility is not available!!!", duration: ToastDuration)
        }
        
    }
    
    //MARK:- key board hide
    func hidekeyBoardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyBoard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
    
    func reverseGeoCoding(latitude : Double,longitude : Double,completion:@escaping(GMSAddress)-> ())
    {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        var currentAddress = String()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                print("address - ",address)
                let lines = address.lines as? [String]
                currentAddress = (lines?.joined(separator: ",") ?? "")
                print("Address - ",currentAddress)
                print("country code \(address)")
                completion(address)
            }
        }
    }
    
    /*func getCountryCode(){
        CLGeocoder().reverseGeocodeLocation(location, completionHandler:
            {(placemarks, error) in
                if error {println("reverse geodcode fail: \(error.localizedDescription)")}
                let pm = placemarks as [CLPlacemark]
                if pm.count > 0 { self.showAddPinViewController(placemarks[0] as CLPlacemark) }
        })
    }*/
    
    //MARK: - Configure Dropdown
    func configureDropdown(dropdown : DropDown,sender:UIControl)
    {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .automatic
        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        //        dropdown.topOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = sender.frame.size.width
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
    }
}
