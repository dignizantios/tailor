//
//  NSNotificationName+Extension.swift
//  Tailory
//
//  Created by Jaydeep on 19/10/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    static let didRefreshOrderList = Notification.Name("refreshOrderList")
    static let didRefreshOrderDetailScreen = Notification.Name("refreshOrderDetail")
    static let manageAddCartButton = Notification.Name("manageAddToCartButton")
}
