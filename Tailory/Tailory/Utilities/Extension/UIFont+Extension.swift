//
//  UIFont+Extension.swift
//  Tailory
//
//  Created by YASH on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
    case book = "AvenirLTStd-Book"
    case black = "AvenirLTStd-Black"
    case heavy = "AvenirLTStd-Heavy"
    case medium = "AvenirLTStd-Medium"
    case roman = "AvenirLTStd-Roman"
    case light = "AvenirLTStd-Light"
    case arLight = "GESSTwoLight-Light"
}

extension UIFont
{

}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
        if isEnglish {
            return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
        } else {
            return UIFont(name: themeFonts.arLight.rawValue, size: CGFloat(size) - 2.0)!
        }
        
    }
    else
    {
        if isEnglish {
            return UIFont(name: fontname.rawValue, size: CGFloat(size))!
        } else {
           return UIFont(name: themeFonts.arLight.rawValue, size: CGFloat(size))!
        }
    }
    
}
