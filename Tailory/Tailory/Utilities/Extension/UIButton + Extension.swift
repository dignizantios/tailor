//
//  UIButton + Extension.swift
//  Tailory
//
//  Created by YASH on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit


extension UIButton
{
    
    func setupThemeButtonUI(backColor : UIColor)
    {
        self.setTitleColor(UIColor.white, for: .normal)
        self.backgroundColor = backColor
        self.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        
    }
    
}
