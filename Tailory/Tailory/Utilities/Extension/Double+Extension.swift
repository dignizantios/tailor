//
//  Double+Extension.swift
//  Tailory
//
//  Created by Jaydeep on 10/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func toString() -> String {
        return String(format: "%.1f",self)
    }
}

