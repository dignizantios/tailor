//
//  Enum+Extension.swift
//  Tailory
//
//  Created by Jaydeep on 28/08/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import Foundation
import UIKit

enum accessoriesDetailType:String {
    case select = "1"
    case radio = "2"
    case check = "3"
    case text = "4"
    case textarea = "5"
}

enum accessoriesValidParam:String{
    case min = "1"
    case max = "2"
    case numeric = "3"
    case alphabets = "4"
    case decimal = "5"
}

enum enumCategoryId:String{
    case tailor = "1"
    case fabrics = "2"
    case accessories = "3"
}

enum enumCustomDesign:String {
    case addMyDesign = "1"
    case addCustomDesign = "0"
}

enum enumExistMeasurement:String{
    case existMesurement = "1"
    case isNotExistMeasurement = "0"
}

enum enumExistAddress:String{
    case existAddress = "1"
    case isNotExistAddress = "0"
}

enum enumIsAddedFabric:String{
    case addedFabric = "1"
    case notAddedFabric = "0"
}

enum enumIsAddedLaudry:String{
    case addedLaudry = "1"
    case notAddedLaudry = "0"
}

enum isComeFromFav {
    case fromFav
    case notFromFav
}

enum isCheckPaymentType {
    case cash
    case fatoorah
}

enum isComeFromEditDesign {
    case edit
    case add
}
