//
//  UIColor+Extension.swift
//  Tailory
//
//  Created by YASH on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

extension UIColor {
//    Cyprus
    static var appThemeSilverColor: UIColor { return UIColor.init(displayP3Red: 167/255, green: 129/255, blue: 52/255, alpha: 1.0) }
    static var appThemeCyprusColor: UIColor { return UIColor.init(displayP3Red: 12/255, green: 32/255, blue: 67/255, alpha: 1.0)}
    static var appThemeNobelColor: UIColor { return UIColor.init(displayP3Red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)}
    static var appThemeBlackColor: UIColor { return UIColor.init(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)}
    static var appThemeLightSilverColor: UIColor { return UIColor.init(displayP3Red: 243/255, green: 237/255, blue: 226/255, alpha: 1.0)}
    static var appThemeLightGrayColor: UIColor { return UIColor.init(displayP3Red: 147/255, green: 147/255, blue: 151/255, alpha: 1.0)}
    static var appThemeDarkGrayColor: UIColor { return UIColor.init(displayP3Red: 63/255, green: 63/255, blue: 63/255, alpha: 1.0)}
    static var appThemelightBlueColor: UIColor { return UIColor.init(displayP3Red: 237/255, green: 242/255, blue: 245/255, alpha: 1.0)}
    
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
   

