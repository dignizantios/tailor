//
//  HeaderView.swift
//  Liber
//
//  Created by YASH on 28/09/18.
//  Copyright © 2018 YASH. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    
    //MARK:- Outlet Zone
    
    @IBOutlet var cwBG: UIView!
    @IBOutlet weak var lblTitleName: UILabel!
    @IBOutlet var btnLogin: UIButton!
    
    //MARK:- ViewLifeCycle
    
    override func awakeFromNib() {
       
        lblTitleName.textColor = UIColor.appThemeDarkGrayColor
        lblTitleName?.font = themeFont(size: 15, fontname: .medium)
        btnLogin.isHidden = true
        [btnLogin].forEach { (btn) in
            
            btn?.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
            btn?.backgroundColor = UIColor.clear
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .medium)
        }
        
        
        if !isEnglish {
            
//            cwBG.transform = CGAffineTransform(scaleX: -1, y: 1)
            lblTitleName.transform = CGAffineTransform(scaleX: -1, y: 1)
            btnLogin.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            lblTitleName.textAlignment = .right
        }
        
    }

}
