//
//  Basicstuff.swift
//  Tailory
//
//  Created by YASH on 13/07/19.
//  Copyright © 2018 YASH. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents
import NVActivityIndicatorView
import AlamofireSwiftyJSON
import SwiftyJSON
import Alamofire
import CoreLocation

struct GlobalVariables {
    
    //MARK: - Storyboard variable
    static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    static let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
    static let profileStoryboard = UIStoryboard(name: "Profile", bundle: nil)
    static let accessoriesStoryboard = UIStoryboard(name: "Accessories", bundle: nil)
    static let cartStoryboard = UIStoryboard(name: "Cart", bundle: nil)
    static let measurmentStoryboard = UIStoryboard(name: "Measurment", bundle: nil)
    static let fabricsStoryboard = UIStoryboard(name: "Fabrics", bundle: nil)
    static let FavouriteStoryboard = UIStoryboard(name: "Favourite", bundle: nil)
    
    //MARK:-  googleMapKey
    static let googleMapKey = "AIzaSyCFHbNX2X6EL7iJFnJWEVKw_X1NFE31W70"

    static let passwordLegth = 8
    static let mobileLegth = 15
    static let ageLegth = 3
    
    //MARK: - Static value of API
    
    static let strSuccessResponse = "1"
    static let strAccessDenied = "-1"
    static let strDeviceType = "1"
    static let strNotLogin = "2"
    
    static let strCurrency = getCommonString(key: "KD_key")
    
    static let googleZoomLevel:Float = 18
    
}

let appdelgate = UIApplication.shared.delegate as! AppDelegate
let Defaults = UserDefaults.standard
var navImage = UIImage()
var statusbarHeight = UIApplication.shared.statusBarFrame.size.height

var userCurrentLocation:CLLocation?
var dictTailorDetail = JSON()
var dictFabricDetail = JSON()
var isFavoriteReload = false
var guestDictAddress = JSON()


var isFromTailor : Bool = false
var isCartCount = Int()

//MARK: - Setup mapping

let mapping:StringMapping = StringMapping.shared()
let Arabic = "ar"
let English = "en"
var isEnglish = true
var lang = "en"

func setLangauge(language:String)
{
    
    if language == Arabic
    {
        isEnglish = false
        Defaults.set(Arabic, forKey: "language")
        Defaults.set(1, forKey: "lang")
        lang = "ar"
        Defaults.synchronize()
    }
    else
    {
        isEnglish = true
        Defaults.set(English, forKey: "language")
        Defaults.set(0, forKey: "lang")
        lang = "en"
        Defaults.synchronize()
    }
    print("Language - \(Defaults.value(forKey: "language") ?? "")")
    
    StringMapping.shared().setLanguage()
}

func getCommonString(key:String) -> String
{
    return mapping.string(forKey: key) ?? ""
}

func getCurrentTimeZone() -> String
{
    var localTimeZoneName: String
    {
        return TimeZone.current.identifier
    }
    return localTimeZoneName // "America/Sao_Paulo"
}

//MARK: - Storagae


func getUserDetail(_ forKey: String) -> String{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return "" }
    let data = JSON(userDetail)
    return data[forKey].stringValue
}

func setValueUserDetail(forKey: String ,forValue: String) {
    
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetails") as? Data else { return }
    var data = JSON(userDetail)
    data[forKey].stringValue = forValue
    
    guard let rowdata = try? data.rawData() else {return}
    Defaults.setValue(rowdata, forKey: "userDetails")
    Defaults.synchronize()
}


//MARK: - Set Toaster

func makeToast(strMessage : String){
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
    MDCSnackbarManager.setBottomOffset((UIScreen.main.bounds.size.height/2)-100)
    MDCSnackbarManager.messageFont = themeFont(size: 16, fontname: .medium)
}


extension UIViewController : NVActivityIndicatorViewable
{
    //MARKL - Fonts
    func printFonts()
    {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    
    
    //MARK: - Validation email
    
    func isValidEmail(emailAddressString:String) -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    public func isValidPassword(passwordString:String) -> Bool {
        let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: passwordString)
    }

    // MARK: -  For Loader NVActivityIndicatorView Process
    
    func showLoader()
    {
        let LoaderString:String = getCommonString(key: "Loading_key")
        let LoaderType:Int = 32
        let LoaderSize = CGSize(width: 30, height: 30)
        
        startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.ballSpinFadeLoader)
        
    }
    
    func containsSpecialCharacters(string: String) -> Bool {        
        do {
            let regex = try NSRegularExpression(pattern: "[^a-z0-9]", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) {
                return true
            } else {
                return false
            }
        } catch {
            debugPrint(error.localizedDescription)
            return true
        }
    }
    
    func doStringContainsNumber(string : String) -> Bool{
        let numberRegEx  = ".*[0-9]+.*"
        let testCase = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let containsNumber = testCase.evaluate(with: string)
        return containsNumber
    }
    
    func doStringContainsAlpha(string : String) -> Bool{
        let numberRegEx  = ".*[a-zA-Z]+.*"
        let testCase = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let containsNumber = testCase.evaluate(with: string)
        return containsNumber
    }
    
    
    func hideLoader()
    {
        stopAnimating()
    }
    
    //MARK: - Navigation Controller Setup
    
    func setupNavigationbarwithBackButton(titleText:String,barColor:UIColor)
    {
        
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationItem.hidesBackButton = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeCyprusColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        if !isEnglish {
            ///---- for arabic right-to-left swipe popController show
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
//            navigationController?.navigationBar.semanticContentAttribute = .forceRightToLeft
        }
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow") , style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .white
        self.navigationItem.leftBarButtonItem = leftButton
        
        let HeaderView = UILabel.init(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
        HeaderView.isUserInteractionEnabled = false
        HeaderView.text = titleText
        HeaderView.textColor = .white
        HeaderView.font = themeFont(size: 18, fontname: .book)
        
        self.navigationController?.navigationBar.barTintColor = barColor
        
        self.navigationItem.titleView = HeaderView
    }
    
    func setUpNavigationBarWhiteWithTitleAndBack(strTitle : String)
    {
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeCyprusColor
        self.navigationController?.navigationBar.isTranslucent = false
        /* self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.navigationBar.backgroundColor = UIColor.clear*/
        
        if !isEnglish {
            ///---- for arabic right-to-left swipe popController show
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }
        
        if isEnglish
        {
            let leftButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped))
            leftButton.tintColor = UIColor.white
            
            let homeButton = UIBarButtonItem(image: UIImage(named: "ic_my_home"), style: .plain, target: self, action: #selector(backToHomeAction))
            homeButton.tintColor = UIColor.white
            
            self.navigationItem.leftBarButtonItems = [leftButton,homeButton]
        }
        else
        {
            let rightButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonTapped))
            rightButton.tintColor = UIColor.white
            
            let homeButton = UIBarButtonItem(image: UIImage(named: "ic_my_home"), style: .plain, target: self, action: #selector(backToHomeAction))
            homeButton.tintColor = UIColor.white
            self.navigationItem.rightBarButtonItems = [rightButton,homeButton]
        }
        
        
                
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 17, fontname: .heavy)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    func setNavigationShadow()
    {
        
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
        self.navigationController?.navigationBar.layer.masksToBounds = false
    }

    func setUpNavigationBarWithTitleAndCart(strTitle : String)
    {

        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.appThemeCyprusColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        if !isEnglish {
            ///---- for arabic right-to-left swipe popController show
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }

        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 17, fontname: .medium)
        
        self.navigationItem.titleView = HeaderLabel
        
        if isEnglish
        {
            setBadgeCartButtonToNavigationBar()
        }
        else
        {
            setBadgeCartButtonToNavigationBarArabic()
        }
        
    }
    
    func setBadgeCartButtonToNavigationBar()
    {
        let containBtn = UIButton(frame: CGRect(x:0,y:0,width:40,height:50))
        
        let label = UILabel(frame: CGRect(x:30,y:8,width:20,height:20))
        label.text = isCartCount > 0 ? "\(isCartCount)" : ""
        if isCartCount > 99{
            label.text = "99+"
        }
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.backgroundColor = isCartCount > 0 ? UIColor.red : UIColor.clear
        label.font = themeFont(size: 11, fontname: .medium)
        label.layer.cornerRadius = (label.frame.width/2)
        label.layer.masksToBounds = true
        
        let imageview : UIImageView = UIImageView(frame: CGRect(x:(25-12.5),y:(25-12.5),width:25,height:25))
        imageview.image = UIImage(named: "ic_home_cart_button_unselect")
        
        containBtn.addSubview(imageview)
        containBtn.addSubview(label)
        
        containBtn.addTarget(self, action: #selector(cartTapped), for: .touchUpInside)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: containBtn)

    }
    
    func setBadgeCartButtonToNavigationBarArabic()
    {
        
        let containBtn = UIButton(frame: CGRect(x:0,y:0,width:40,height:50))
        
        let label = UILabel(frame: CGRect(x:0,y:8,width:20,height:20))
//        let label = UILabel(frame: CGRect(x:0,y:0,width:25,height:25))
        label.text = isCartCount > 0 ? "\(isCartCount)" : ""
        if isCartCount > 99{
            label.text = "99+"
        }
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.backgroundColor = isCartCount > 0 ? UIColor.red : UIColor.clear
        label.font = themeFont(size: 11, fontname: .medium)
        label.layer.cornerRadius = (label.frame.width/2)
        label.layer.masksToBounds = true
        
        let imageview : UIImageView = UIImageView(frame: CGRect(x:(25-12.5),y:(25-12.5),width:25,height:25))
        imageview.image = UIImage(named: "ic_home_cart_button_unselect")
        
        containBtn.addSubview(imageview)
        containBtn.addSubview(label)
        
        containBtn.addTarget(self, action: #selector(cartTapped), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: containBtn)
        
    }
    
    @objc func cartTapped()
    {
        let cart = GlobalVariables.cartStoryboard.instantiateViewController(withIdentifier: "CartVC") as! CartVC
        
        self.navigationController?.pushViewController(cart, animated: true)
        
    }
    
    func setUpNavigationBarWithBackHeaderTitleAndRightButtons(strTitle: String) {
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.appThemeCyprusColor
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        //setNavigationShadow()
        
        if !isEnglish {
            ///---- for arabic right-to-left swipe popController show
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }
        
        let headerLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        headerLabel.isUserInteractionEnabled = false
        headerLabel.text = strTitle
        headerLabel.textColor = UIColor.white
        headerLabel.numberOfLines = 2
        headerLabel.textAlignment = .center
        headerLabel.font = themeFont(size: 17, fontname: .medium)
        
        navigationItem.titleView = headerLabel
        
        navigationItem.setHidesBackButton(true, animated: true)
        
        
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped))
        backButton.tintColor = UIColor.white
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "ic_my_home"), style: .plain, target: self, action: #selector(backToHomeAction))
        homeButton.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItems = [backButton,homeButton]
        
        navigationItem.hidesBackButton = true
        /*
        let rightSortButton = UIBarButtonItem(image: UIImage(named: "ic_sort"), style: .plain, target: self, action: #selector(sortButtonTapped))
        rightSortButton.tintColor = .white
        
        let rightFilterButton = UIBarButtonItem(image: UIImage(named: "ic_filter"), style: .plain, target: self, action: #selector(filterButtonTapped))
        rightFilterButton.tintColor = .white
        
        let rightSearchButton = UIBarButtonItem(image: UIImage(named: "ic_search"), style: .plain, target: self, action: #selector(searchButtonTapped))
        rightSearchButton.tintColor = .white
        
        navigationItem.rightBarButtonItems = [rightSearchButton, rightFilterButton, rightSortButton]
        */
    }
    
    func setUpNavigationBarWithBackHeaderTitleAndRightButtonsArabic(strTitle: String) {
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.appThemeCyprusColor
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        //setNavigationShadow()
        
        if !isEnglish {
            ///---- for arabic right-to-left swipe popController show
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }
        
        let headerLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        headerLabel.isUserInteractionEnabled = false
        headerLabel.text = strTitle
        headerLabel.textColor = UIColor.white
        headerLabel.numberOfLines = 2
        headerLabel.textAlignment = .center
        headerLabel.font = themeFont(size: 17, fontname: .medium)
        
        navigationItem.titleView = headerLabel
        navigationItem.setHidesBackButton(true, animated: true)
        
        /*
        let SortButton = UIBarButtonItem(image: UIImage(named: "ic_sort"), style: .plain, target: self, action: #selector(sortButtonTapped))
        SortButton.tintColor = .white
        
        let FilterButton = UIBarButtonItem(image: UIImage(named: "ic_filter"), style: .plain, target: self, action: #selector(filterButtonTapped))
        FilterButton.tintColor = .white
        
        let SearchButton = UIBarButtonItem(image: UIImage(named: "ic_search"), style: .plain, target: self, action: #selector(searchButtonTapped))
        SearchButton.tintColor = .white

        navigationItem.leftBarButtonItems = [SortButton, FilterButton, SearchButton ]
        */
        
        let backButton = UIBarButtonItem(image: UIImage(named: "ic_tailors_white_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonTapped))
        backButton.tintColor = UIColor.white
        
        let homeButton = UIBarButtonItem(image: UIImage(named: "ic_my_home"), style: .plain, target: self, action: #selector(backToHomeAction))
        homeButton.tintColor = UIColor.white
               
        navigationItem.hidesBackButton = true
        
        navigationItem.rightBarButtonItems = [backButton,homeButton]
    }
    
    @objc func backToHomeAction(){
        appdelgate.tailerTabbarVC = TailerTabBarVC()
        if isEnglish  {
            appdelgate.tailerTabbarVC.selectedIndex = 0
        } else {
            appdelgate.tailerTabbarVC.selectedIndex = 4
        }
        
        appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
        
    }
    

    @objc func backButtonTapped()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func sortButtonTapped() {
        
    }
    
    @objc func filterButtonTapped() {
        
    }
    
    @objc func searchButtonTapped() {
        
    }
    
    
    //MARK: - Textfield Done button (NumberPad)
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.appThemeSilverColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction(textfield:)))
        done.tintColor = .white
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textfield:UITextField)
    {
        self.view.endEditing(true)
    }

    
    //MARK: - Date and Time Formatter
    
    func stringTodate(Formatter:String,strDate:String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate = dateFormatter.date(from: strDate)!
        return FinalDate
    }
    
    func DateToString(Formatter:String,date:Date) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Formatter
        //  dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale!
        let FinalDate:String = dateFormatter.string(from: date)
        return FinalDate
    }
    
    func stringTodate(OrignalFormatter : String,YouWantFormatter : String,strDate:String) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = OrignalFormatter
        guard let convertedDate = dateformatter.date(from: strDate) else {
            return ""
        }
        dateformatter.dateFormat = YouWantFormatter
        let convertedString = dateformatter.string(from: convertedDate)
        return convertedString
        
    }
    
    func strTodt(OrignalFormatter:String,YouWantFormatter:String,strDate:String) -> Date
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        return dateFormatterPrint.date(from: strDate)!
    }
    
    func dtTostr(OrignalFormatter:String,YouWantFormatter:String,Date:Date) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = OrignalFormatter
        let strdate: String = dateFormatterGet.string(from: Date)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = YouWantFormatter
        let date: Date = dateFormatterPrint.date(from: strdate)!
        return dateFormatterPrint.string(from: date)
    }
    
    // Redirect to controller    
    
    
    
    //MARK: - Logout API
    
    func logoutAPICalling()
    {
        self.view.endEditing(true)
       
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(urlUser)\(urlLogout)"
            
            print("URL: \(url)")
            
            let param = ["lang" : lang,
                         "user_id" : getUserDetail("user_id"),
                         "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString
                        ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        Defaults.removeObject(forKey: "userDetails")
                        Defaults.synchronize()
                        
                        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
                        let rearNavigation = UINavigationController(rootViewController: vc)
                        appdelgate.window?.rootViewController = rearNavigation
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
 
    }
    
}

