//
//  AppDelegate.swift
//  Tailory
//
//  Created by Haresh on 13/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import UserNotifications
import Firebase
import SwiftyJSON
import Crashlytics

protocol CurrentLocationDelegate {
    func didUpdateLocation(lat:Double!,lon:Double!)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var tailerTabbarVC = TailerTabBarVC()
    var delegate:CurrentLocationDelegate?
    var locationManager = CLLocationManager()
    var lattitude  = Double()
    var longitude = Double()
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        
//        Crashlytics.sharedInstance().crash()
        
        if Defaults.value(forKey: "language") as? String == Arabic {
            setLangauge(language: Arabic)
            isEnglish = false
        }
        else {
            setLangauge(language: English)
        }

        GMSServices.provideAPIKey(GlobalVariables.googleMapKey)
        setUpQuickLocationUpdate()
        
        Messaging.messaging().delegate = self
        
        registerForRemoteNotification()
        
        //-- splunkmint
        
//        Crashlytics.sharedInstance().crash()
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        Defaults.removeObject(forKey: "LaundryPrice")
        Defaults.synchronize()
    }
}

//MARK:- Location Delegate

extension AppDelegate:CLLocationManagerDelegate
{
    func setUpQuickLocationUpdate()
    {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
//        self.locationManager.requestAlwaysAuthorization()
//        self.locationManager.requestLocation()
//        self.locationManager.requestWhenInUseAuthorization()
//        self.locationManager.allowsBackgroundLocationUpdates = true
//        locationManager.startMonitoringSignificantLocationChanges()
//        locationManager.pausesLocationUpdatesAutomatically = false
//        locationManager.distanceFilter = 1
        self.locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        
        if let latestLocation = locations.first
        {
            
            print("latestLocation:=\(latestLocation.coordinate.latitude), *=\(latestLocation.coordinate.longitude)")
            
            userCurrentLocation = latestLocation
            
            //  KSToastView.ks_showToast("Location Update Successfully", duration: ToastDuration)
            if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude
            {
                userCurrentLocation = latestLocation
                lattitude = latestLocation.coordinate.latitude
                longitude = latestLocation.coordinate.longitude
                self.delegate?.didUpdateLocation(lat:latestLocation.coordinate.latitude,lon:latestLocation.coordinate.longitude)
                locationManager.stopUpdatingLocation()
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            //    manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            if #available(iOS 10.0, *) {
                openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            if #available(iOS 10.0, *) {
                openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
            //   default:
            // break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error :- \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        locationManager.stopUpdatingLocation()
    }
    
    
    
    //MARK:- open Setting
    
    func openSetting()
    {
        let alertController = UIAlertController (title: getCommonString(key: "Tailors_key"), message: getCommonString(key: "Go_to_Setting_key"), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: getCommonString(key: "Setting_key"), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        //     let cancelAction = UIAlertAction(title: mapping.string(forKey: "Cancel_key"), style: .default, handler: nil)
        //    alertController.addAction(cancelAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
}

//MARK: - Register FCM

extension AppDelegate: MessagingDelegate {
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UIApplication.shared.registerForRemoteNotifications()
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print("Device Token = ", token)
        
        Defaults.set(token, forKey: "device_token")
        Defaults.synchronize()
        
        Messaging.messaging().apnsToken = deviceToken
        
        print("FCM Token = \(Messaging.messaging().fcmToken ?? "nill")")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error = ",error.localizedDescription)
        
    }
    
    // For < 10 This Notification Method is called
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Push for ios < 10 version")
        print("User Info = ",userInfo)
        
        if application.applicationState == .active {
            
        }
        else if application.applicationState == .background {
           
        }
        
    }
    
    
    // MARK: UNUserNotificationCenter Delegate // >= iOS 10
    // While App on Foreground mode......
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    
        let userInfo = notification.request.content.userInfo
        print("info:=\(JSON(userInfo))")
        
        let dict = JSON(userInfo)
        if dict["notification_type"].stringValue == "1"{
            completionHandler([.alert, .badge, .sound])
        } else {
            if isOnOrderScreen {
                NotificationCenter.default.post(name: .didRefreshOrderList, object: nil, userInfo: nil)
            } else if isOnOrderDetailScreen {
                if strOrderId == dict["order_id"].stringValue{
                    var dictData = JSON()
                    dictData["address_id"].stringValue = dict["address_id"].stringValue
                    dictData["id"].stringValue = dict["order_id"].stringValue
                    NotificationCenter.default.post(name: .didRefreshOrderDetailScreen, object: dictData, userInfo: nil)
                     NotificationCenter.default.post(name: .didRefreshOrderList, object: nil, userInfo: nil)
                } else {
                    completionHandler([.alert, .badge, .sound])
                }
            }
            else {
                completionHandler([.alert, .badge, .sound])
            }
        }
        completionHandler([.alert, .badge, .sound])
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("info:=\(JSON(userInfo))")
        
        let dict = JSON(userInfo)
        if dict["notification_type"].stringValue == "2"{
            tailerTabbarVC = TailerTabBarVC()
            tailerTabbarVC.selectedIndex = 1
            isComeFromPush = true
            var dictData = JSON()
            dictData["address_id"].stringValue = dict["address_id"].stringValue
            dictData["id"].stringValue = dict["order_id"].stringValue
            dictPushData = dictData
            self.window?.rootViewController = tailerTabbarVC
        }
        completionHandler()
    }
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //  self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
}
