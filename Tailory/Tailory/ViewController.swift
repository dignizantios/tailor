//
//  ViewController.swift
//  Tailory
//
//  Created by Haresh on 13/07/19.
//  Copyright © 2019 YASH. All rights reserved.
//


enum isCheckParentControllerForLanguage
{
    case firstTimeLanuch
    case fromProfileSection
}

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class ViewController: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblSelectLanguage: UILabel!
    @IBOutlet weak var btnEnglish: CustomButton!
    @IBOutlet weak var btnArabic: CustomButton!
    
    //MARK: - Variable
    
    var selectedController = isCheckParentControllerForLanguage.firstTimeLanuch
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if Defaults.value(forKey: "language") as? String == Arabic {
            isEnglish = false
        }
        
        if selectedController == .firstTimeLanuch
        {
            self.navigationController?.navigationBar.isHidden = true
            if getUserDetail("user_id") != ""{
                appdelgate.tailerTabbarVC = TailerTabBarVC()
                appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
            }
        }
        else if selectedController == .fromProfileSection
        {
            self.navigationController?.navigationBar.isHidden = false
//            setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Change_language_key"))
            
            if isEnglish
            {
                let leftButton = UIBarButtonItem(image: UIImage(named: "ic_login_back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped))
                leftButton.tintColor = UIColor.white
                
                let homeButton = UIBarButtonItem(image: UIImage(named: "ic_my_home"), style: .plain, target: self, action: #selector(backToHomeAction))
                homeButton.tintColor = UIColor.white
                
                self.navigationItem.leftBarButtonItems = [leftButton,homeButton]
            }
            else
            {
                let rightButton = UIBarButtonItem(image: UIImage(named: "ic_login_back_arrow_arabic"), style: .plain, target: self, action: #selector(backButtonTapped))
                rightButton.tintColor = UIColor.white
                
                let homeButton = UIBarButtonItem(image: UIImage(named: "ic_my_home"), style: .plain, target: self, action: #selector(backToHomeAction))
                homeButton.tintColor = UIColor.white
                
                self.navigationItem.rightBarButtonItems = [rightButton,homeButton]
            }
            
            if #available(iOS 13.0, *) {
                let app = UIApplication.shared
                let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                
                let statusbarView = UIView()
                statusbarView.backgroundColor = UIColor.clear
                view.addSubview(statusbarView)
              
                statusbarView.translatesAutoresizingMaskIntoConstraints = false
                statusbarView.heightAnchor
                    .constraint(equalToConstant: statusBarHeight).isActive = true
                statusbarView.widthAnchor
                    .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                statusbarView.topAnchor
                    .constraint(equalTo: view.topAnchor).isActive = true
                statusbarView.centerXAnchor
                    .constraint(equalTo: view.centerXAnchor).isActive = true
              
            } else {
                let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                statusBar?.backgroundColor = UIColor.clear
            }
            self.navigationItem.hidesBackButton = true
            self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.navigationBar.backgroundColor = UIColor.clear
            
        }
        
    }
    
    //MARK: - Setup UI
    
    func setupUI()
    {
        
        lblSelectLanguage.textColor = UIColor.white
        lblSelectLanguage.font = themeFont(size: 24, fontname: .medium)
        
        let attributedString = NSMutableAttributedString(string: getCommonString(key: "Please_select_your_key"))
//            + "\n" + getCommonString(key: "language_key"))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment = .center
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        lblSelectLanguage.attributedText = attributedString
        
        btnEnglish.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        btnArabic.setupThemeButtonUI(backColor: UIColor.clear)
        btnArabic.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        btnArabic.borderColor = UIColor.appThemeSilverColor
        btnArabic.borderWidth = 1.0
        
        btnEnglish.setTitle(getCommonString(key: "English_key"), for: .normal)
//        btnArabic.setTitle(getCommonString(key: "Arabic_key"), for: .normal)
        btnArabic.setTitle("عربي", for: .normal)
        
        if selectedController == .firstTimeLanuch
        {
            
        }
        else if selectedController == .fromProfileSection
        {
            if isEnglish
            {
                self.selectedUnselectedBtn(selectedBtn: btnEnglish, unselectedBtn: btnArabic)
            }
            else
            {
                self.selectedUnselectedBtn(selectedBtn: btnArabic, unselectedBtn: btnEnglish)
            }
            
        }
        
    }
    
}

//MARK: - IBAction method

extension ViewController
{
    
    @IBAction func btnEnglishTapped(_ sender: UIButton) {
        
        if selectedController == .firstTimeLanuch
        {
            self.selectedUnselectedBtn(selectedBtn: btnEnglish, unselectedBtn: btnArabic)
            setLangauge(language: English)
            
            redirectToLogin()
        }
        else if selectedController == .fromProfileSection
        {
            if isEnglish {
                self.redirectToHomeAfterChangeLanguage()
                return
            }
            if getUserDetail("is_login_guest") == "1" {
                setLangauge(language: English)
                self.redirectToHomeAfterChangeLanguage()
            } else {
                changeLanguage()
            }
        }
    }
    
    @IBAction func btnArabicTapped(_ sender: UIButton) {
        
        if selectedController == .firstTimeLanuch
        {
            self.selectedUnselectedBtn(selectedBtn: btnArabic, unselectedBtn:btnEnglish)
            setLangauge(language: Arabic)
            redirectToLogin()
        }
        else if selectedController == .fromProfileSection
        {
            if !isEnglish {
                self.redirectToHomeAfterChangeLanguage()
                return
            }
            if getUserDetail("is_login_guest") == "1" {
                setLangauge(language: Arabic)
                self.redirectToHomeAfterChangeLanguage()
            } else {
                changeLanguage()
            }
        }
    }
}

//MARK: - other Action

extension ViewController
{
    
    func redirectToLogin()
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "LoginVc") as! LoginVc
        obj.selectedController = .firstTimeLanuch
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func redirectToHomeAfterChangeLanguage()
    {
        appdelgate.tailerTabbarVC = TailerTabBarVC()
        appdelgate.window?.rootViewController = appdelgate.tailerTabbarVC
    }
    
    func selectedUnselectedBtn(selectedBtn:CustomButton,unselectedBtn:CustomButton)
    {
        selectedBtn.setupThemeButtonUI(backColor: UIColor.appThemeSilverColor)
        unselectedBtn.setupThemeButtonUI(backColor: UIColor.clear)
        unselectedBtn.setTitleColor(UIColor.appThemeSilverColor, for: .normal)
        unselectedBtn.borderColor = UIColor.appThemeSilverColor
        unselectedBtn.borderWidth = 1.0
    }
    
}

extension ViewController{
    func changeLanguage()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(urlUser)\(urlChangeLanguage)"
            
            let param  = ["lang" : isEnglish ? "ar" : "en",
                          "user_id" : getUserDetail("user_id"),
                          "access_token" : getUserDetail("access_token"),
                          "device_token" : Defaults.value(forKey: "device_token") as? String ?? UIDevice.current.identifierForVendor!.uuidString]
            
            
            print("param : \(param)")
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
//                        makeToast(strMessage: json["msg"].stringValue)
                        if isEnglish {
                            print("Set Arabic")
                            setLangauge(language: Arabic)
                        } else {
                            setLangauge(language: English)
                            print("Dont do anything")
                        }
                        self.redirectToHomeAfterChangeLanguage()
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(strMessage:serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
    }
}
